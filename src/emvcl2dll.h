//-----------------------------------------------------------------------------
//  File          : emvcl2dll.h
//  Module        : 
//  Description   : header for emv contactless dll
//  Author        : Pody
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g                                         |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |            Char size : Leading c                                         |
// |             Int size : Leading i                                         |
// |            Byte size : Leading b                                         |
// |            Word size : Leading w                                         |
// |           Dword size : Leading d                                         |
// |          DDword size : Leading dd                                        |
// |                Array : Leading a, (ab = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  2008 Apr      Pody        Initial Version.
//-----------------------------------------------------------------------------


#ifndef _EMVCL2DLL_H_
#define _EMVCL2DLL_H_

#include "system.h"

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
//HW type - specified in makefile! 
#ifdef CREON
  #define EMVCLDLL_ID                     (BYTE)19
  #define emvcldll_call                   app19_call
#endif
#ifdef SR300
  #define EMVCLDLL_ID                     (BYTE)17
  #define emvcldll_call(x1,x2,x3,x4)      lib_app_call(EMVCLDLL_ID, x1, x2, x3, x4)
#endif
#ifndef EMVCLDLL_ID //not specified
  #define EMVCLDLL_ID                     (BYTE)100
  #define emvcldll_call(x1,x2,x3,x4)      lib_app_call(EMVCLDLL_ID, x1, x2, x3, x4)
#endif

// define
#define MAX_CL_CFG                    (BYTE)16  // max emvcl config PC1201 8->16
#define MAX_CL_KEY                    (BYTE)32  // max emvcl keyset
#define CL_IFDSN_LEN                  (BYTE)8   // emvcl IFDSN length
#define MAX_CL_REVRID                 (BYTE)5   // max revocate rid 
#define MAX_CL_REVKEY                 (BYTE)30  // max revocate key/RID


// define kernel legacy type that defined by emv
// - first byte in CLT_KERNEL_INFO
#define KTYPE_ANY             (BYTE)0xFF
#define KTYPE_EMVCL           (BYTE)0x00
#define KTYPE_JWAVE           (BYTE)0x01
#define KTYPE_PPASS           (BYTE)0x02
#define KTYPE_PWAVE           (BYTE)0x03
#define KTYPE_AEPAY           (BYTE)0x04

// define CLT, contactless user tags
#define CLT_START              (WORD)0xDFA0    // N2C pls ensure this value can be used! user tag start number
#define DBT_START              (WORD)0xDFF0    // N2C pls ensure this value can be used! user tag start number
enum {
//DFA0
  CLT_GFLAG = CLT_START,           // 0-INTERNAL: appl global flag
  CLT_RLOC,                        // 1-INTERNAL: return error location
  CLT_RACT,                        // 2-INTERNAL: return action code
  CLT_SETUP_TBUF,                  // 3-INTERNAL: temp buffer used in emvclSetup
  CLT_KERNEL_INFO,                 // 4-APP_CFG: kernel info = [EmvKernelType(1)|KernelDllId(1)] 
  CLT_PP_IN_FLAG,                  // 5-APP_CFG: pre-processing input flag
  CLT_CL_FLOOR_LIMIT,              // 6-APP_CFG: pre-processing terminal contactless floor limit
  CLT_CL_TRAN_LIMIT,               // 7-APP_CFG: pre-processing terminal contactless trans limit
  CLT_CL_CVM_LIMIT,                // 8-APP_CFG: pre-processing terminal cvm required limit
  CLT_PPR,                         // 9-INTERNAL: pre-processing result struct PP_RESULT
  CLT_PPR_ITEMS,                   // A-INTERNAL: pre-processing result items: 1st + 2nd +...
  CLT_T61_ITEMS,                   // B-INTERNAL: matched appl Tag 61 itmes: 1st + 2nd + ..
  CLT_MATCH_ITEMS,                 // C-INTERNAL: sorted match items, MATCH_ITEM [PPR_ID(1)|T61_ID(1)|PRIO(1)]: 1st + 2nd + ..
  CLT_TRAN_TYPE,                   // D-INPUT: trans type: void, refund, adjust, service sale, cash withdraw, goods sales with cashback, service sale with cashback
  CLT_PAY_SCHEME,                  // E-OUTPUT: payment scheme
  CLT_ODA_DATA,                    // F-INTERNAL: data for read records for ODA usage
//DFB0
  CLT_MAGS_IDR,                    // 0-APP_CFG: paypass mag-stripe indicator (support or not: 0x01/0x00)
  CLT_T1DD,                        // 1-OUTPUT: paypass magstripe track 1 discretionary data from card
  CLT_T2DD,                        // 2-OUTPUT: paypass magstripe track 2 discretionary data from card in ascii format
  CLT_DEF_UDOL,                    // 3-APP_CFG: paypass default terminal UDOL
  CLT_ODA_METHOD,                  // 4-INTERNAL: ODA method should be used: 0(no need), 1(CDA), 2(DDA), 3(SDA) 
  CLT_TRAN_CVM,                    // 5-OUTPUT: transaction CVM, indicate the outcome CVM selection
  CLT_CFG_HDR,                     // 6-INTERNAL: cl config header, same as CL_CFGP struct, but without abTLV
  CLT_GENAC_RCP,                   // 7-INTERNAL: gen AC ref control param, b7-8:00(AAC), 40(TC), 80(ARQC), C0(AAR)
  CLT_GENAC_RESP,                  // 8-INTERNAL: gen AC resp excluding SDAD (tag 9F4B) for CDA use
  CLT_RET_WTAGS,                   // 9-INTERNAL: return wtags list based on payment type 
  CLT_RET_TAGS,                    // A-APP_CFG: return tags list based on customer
  CLT_GPO_PDOL,                    // B-INTERNAL: pdol data when GPO, need for verify CDA
  CLT_GAC1_CDOL,                   // C-INTERNAL: cdol data when GAC1, need for verify CDA
  CLT_REMOVAL_SIGNAL,              // D-APP_CFG: customize the removal signal
  CLT_VFLAG,                       // E-APP_CFG: visa config flag
  CLT_TERM_ENTRY_CAP,              // F-APP_CFG: pwave Terminal Entry Capability: SupVSDC[05], NotVSDC[08]
//DFC0
  CLT_T2_PAN,                      // 0-OUTPUT: track2 PAN
  CLT_T2_EXPDATE,                  // 1-OUTPUT: track2 exp date 
  CLT_PPSE_FCI,                    // 2-OUTPUT: PC1106 provide extra info when PPSE sel appl fail
  CLT_VT95,                        // 3-OUTPUT: PC1106 Visa T95(TVR), provide TVR for Pwave Kernel, support DDA failed only. 
  CLT_PP_TCAP_BLCVM,               // 4-APP_CFG: PC1109 paypass Term Capa(9F33) will be used if below CVM Lmt (optional) 
  CLT_APDU2APP,                    // 5-APP_CFG: PC1201 If exist, call back appl when apdu cmd. 
                                   //   Fmt:[ApId&Num(2)|Class&Ins(2)|Class&Ins(2)...] If Class&Ins=FFFF, return to appl for all ApduCmd. 
  CLT_XRATE_TBL,                   // 6-APP_CFG: PC1201 ExhangeRateTable, Fmt:[CurrCode(2)+CurrCode2(2)+BcdRate(4)|...]
                                   //   BcdRate = DecimalPlace(1digit)+Rate(7digits), eg "\x71\x21\x23\x45"=0.1212345
  CLT_END,
//***** Debug Tags *******
//DFF0
  DBT_RESULT = DBT_START,          // 0-DebugModeOnly: debug output the result information
  DBT_CARDTIME,                    // 1-DebugModeOnly: debug output the card required time
  DBT_APDU_DBCOM,                  // 2-debug apdu command out to port, eg 0x01 or 0x02 for COM1 or COM2
  DBT_LOG_DBCOM,                   // 3-debug log to port, if 0xff, then save log for CLCMD_GET_TAGS(DBT_LOG) 
  DBT_LOG,                         // 4-debug log tag, can use CLCMD_GET_TAGS to get debug log info
  DBT_END,
};

// define preprocess bitwide input flag in tag CLT_PP_IN_FLAG
enum {
  PPF_STATUS_CHECK_SUP = 1,       // 80 status check support flag
  PPF_ZERO_AMT_CHECK_SUP,         // 40 zero amount check support flag
  PPF_CL_TRAN_LIMIT_NA,           // 20 tran limit disable
  PPF_CL_CVM_LIMIT_NA,            // 10 cvm limit disable
  PPF_CL_FLOOR_LIMIT_NA,          // 08 floor limit disable
  PPF_ZERO_AMT_OPTION2,           // 04 use zero amount option2 (terminated)
  PPF_END
};

//CLT_PAY_SCHEME values - assume mask 0xF0 to different payment associations 
#define V_MSI                   (BYTE)0x10    // visa MSI  - not supported by kernel
#define V_DDA                   (BYTE)0x11    // visa DDA+MSI or Fast DDA - not supported by kernel
#define V_DCVV                  (BYTE)0x13    // visa dCVV - not supported by kernel
#define V_FVSDC                 (BYTE)0x14    // visa full VSDC - not supported by kernel
#define V_VLP                   (BYTE)0x15    // visa fast DDA + VLP  - not supported by kernel
#define V_WAVE2                 (BYTE)0x16    // visa wave 2
#define V_WAVE3                 (BYTE)0x17    // visa wave 3 (qVSDC)
#define V_MSD                   (BYTE)0x18    // visa MSD
#define M_MAGS                  (BYTE)0x20    // mastercard magstripe
#define M_MCHIP                 (BYTE)0x21    // mastercard mchip
#define M_MXI                   (BYTE)0x22    // mastercard MXI
#define J_WAVE1                 (BYTE)0x60    // jcb wave 1 - appl need to map from VSDC
#define J_WAVE2                 (BYTE)0x61    // jcb wave 2 - appl need to map from VSDC
#define J_WAVE3                 (BYTE)0x62    // jcb wave 3 - appl need to map from VSDC
#define P_PBOC                  (BTYE)0x90    // pboc - PC1106 not supported by kernel
#define P_QPBOC                 (BYTE)0x91    // qPBOC - PC1106 appl need to map from VSDC
#define P_MSD                   (BYTE)0x92    // MSD - PC1106 appl need to map from VSDC
#define P_UPCARD                (BYTE)0x93    // upcard - PC1106 not supported by kernel

//CLT_ODA_METHOD
#define ODA_NONE      (BYTE)0x00
#define ODA_CDA       (BYTE)0x01
#define ODA_DDA       (BYTE)0x02
#define ODA_SDA       (BYTE)0x03

//CLT_TRAN_CVM (bitwise)
enum {
  TRAN_CVM_SIGN = 1, 
  TRAN_CVM_ONL_PIN,
//  TRAN_NO_CVM,
  TRAN_CVM_END
};

//CLT_TRAN_TYPE
#define TRAN_DEFAULT              (BYTE)0x00
#define TRAN_VOID                 (BYTE)0x01
#define TRAN_REFUND               (BYTE)0x02
#define TRAN_ADJUST               (BYTE)0x03
#define TRAN_SALE                 (BYTE)0x04
#define TRAN_CASH                 (BYTE)0x05
#define TRAN_GOODS_CASH           (BYTE)0x06
#define TRAN_SERVICE_CASH         (BYTE)0x07
#define TRAN_AUTH                 (BYTE)0x08

// CLT_VFLAG (bitwise)
enum {
  VFLAG_WAVE2_SUP = 1,  //80 support wave2
  VFLAG_CVN17_NA,       //40 CVN17 not allow
  VFLAG_DUPLICATE_ENB,  //20 !qPBOC Allow Duplicate Tag data from card
  VFLAG_END,
};

// EMV Process Error Loc
#define CLTRC_APPFIND             (BYTE)0x01    // application search 
#define CLTRC_APPSEL              (BYTE)0x02    // application select 
#define CLTRC_CONFIG              (BYTE)0x03    // configuration 
#define CLTRC_IAP                 (BYTE)0x04    // initiate appl processing
#define CLTRC_RDAPPL              (BYTE)0x05    // read appl data 
#define CLTRC_ODA                 (BYTE)0x06    // offline data authentication 
#define CLTRC_SDA                 (BYTE)0x07    // static data authentication 
#define CLTRC_DDA                 (BYTE)0x08    // dynamic data authentication 
#define CLTRC_RESTRIC             (BYTE)0x09    // processing restrictions 
#define CLTRC_CVM                 (BYTE)0x0A    // cardholder verification 
#define CLTRC_RISK                (BYTE)0x0B    // terminal risk management
#define CLTRC_ACTION              (BYTE)0x0C    // action analysis 
#define CLTRC_ONLINE              (BYTE)0x0D    // online processing 
#define CLTRC_COMPLETE            (BYTE)0x0E    // completion processing 

// EMV Process Error Code
#define CLERR_CONFIG              (BYTE)0x00
#define CLERR_MEMORY              (BYTE)0x01
#define CLERR_SMCIO               (BYTE)0x02
#define CLERR_DATA                (BYTE)0x03
#define CLERR_CANCEL              (BYTE)0x04
#define CLERR_NOAPPL              (BYTE)0x05    // no matching application
#define CLERR_NOMORE              (BYTE)0x06    // no more application
#define CLERR_BLOCKED             (BYTE)0x07    // card blocked
#define CLERR_CARDL1              (BYTE)0x08    // level 1 card 
#define CLERR_RESTART             (BYTE)0x09    // card restart failure
#define CLERR_SEQ                 (BYTE)0x10    // command sequence
#define CLERR_SETUP               (BYTE)0x11    // parameter not loaded
#define CLERR_KEY                 (BYTE)0x12    // key error
#define CLERR_NOLOG               (BYTE)0x13    // no transaction log on card
#define CLERR_NOTMATCH            (BYTE)0x14    
//contactless error
#define CLERR_TOI                 (BYTE)0x40    // try other interface
#define CLERR_DDA                 (BYTE)0x41    // DDA Auth Failure
#define CLERR_PDCL                (BYTE)0x42    // Power Down CL
#define CLERR_OCARD               (BYTE)0x43    // overses card
#define CLERR_WTXTO               (BYTE)0x44    // PC1008 Wtx Timeout
#define CLERR_CARDEXP             (BYTE)0x45    // !qPBOC Card Expired
#define CLERR_PP_OVER_LMT         (BYTE)0x46    // PC1106 preprocess all over limit
#define CLERR_SCH_NOTMATCH        (BYTE)0x47    // !clPBOC
#define CLERR_AP_CALL             (BYTE)0x48    // PC1201 appl call return error

// Action Code
#define CLACT_SIGNATURE           (BYTE)0x01    // signature
#define CLACT_DDAAC               (BYTE)0x02    // Combine DDA/AC
#define CLACT_2AC                 (BYTE)0x04    // Second AC
#define CLACT_DEFCVM              (BYTE)0x08    // Default CVM 
#define CLACT_ONLINE              (BYTE)0x10    // Online processing
#define CLACT_REFERRAL            (BYTE)0x20    // Referral
#define CLACT_HOSTAPP             (BYTE)0x40    // Host Approved
#define CLACT_APPROVED            (BYTE)0x80    // Trans Approved 

// struct define
typedef struct {
  WORD  wLen;
  BYTE  *pbMsg;
} CL_IO; //same as sIO_t in emvdll

// define CL_CFGP.bEType
#define ETYPE_EMV             (BYTE)0x01
#define ETYPE_VSDC            (BYTE)0x02
#define ETYPE_MCHIP           (BYTE)0x03
#define ETYPE_JSMART          (BYTE)0x04
#define ETYPE_PBOC            (BYTE)0x05
#define ETYPE_AMEX            (BYTE)0x06 
#define ETYPE_DZIP            (BYTE)0x07 
#define ETYPE_CL_BIT          (BYTE)0x80
#define ETYPE_EMVCL           (BYTE)(ETYPE_CL_BIT|ETYPE_EMV)
#define ETYPE_PWAVE           (BYTE)(ETYPE_CL_BIT|ETYPE_VSDC)
#define ETYPE_PPASS           (BYTE)(ETYPE_CL_BIT|ETYPE_MCHIP)
#define ETYPE_JWAVE           (BYTE)(ETYPE_CL_BIT|ETYPE_JSMART)
#define ETYPE_QPBOC           (BYTE)(ETYPE_CL_BIT|ETYPE_PBOC)
#define ETYPE_AEPAY           (BYTE)(ETYPE_CL_BIT|ETYPE_AMEX)
#define ETYPE_DZPAY           (BYTE)(ETYPE_CL_BIT|ETYPE_DZIP)

// define CL_CFGP.bBitField
#define BIT_PARTIAL_NA            (BYTE)0x01  //partial not allow
#define BIT_REFERRAL_NA           (BYTE)0x02  //referral not allow

#define CLCFG_HDR_SIZE            (WORD)23    //PC1109 size from bEtype to abTACDefault
#define CLCFG_TLV_LEN             (WORD)200
#define CLCFGP_TLV_LEN            (WORD)512
#pragma pack(1)
typedef struct {
  BYTE  bEType;
  BYTE  bBitField;
  DWORD dRSBThresh;       // Threshold for Biased Random Sel
  BYTE  bRSTarget;        // Target for Random Sel
  BYTE  bRSBMax;          // Max target for Biased Random Sel
  BYTE  abTACDenial[5];
  BYTE  abTACOnline[5];
  BYTE  abTACDefault[5];
  BYTE  abTLV[CLCFG_TLV_LEN];
} CL_CFG; //same as APP_CFG in emvdll
typedef struct {
  BYTE  bEType;
  BYTE  bBitField;
  DWORD dRSBThresh;       // Threshold for Biased Random Sel
  BYTE  bRSTarget;        // Target for Random Sel
  BYTE  bRSBMax;          // Max target for Biased Random Sel
  BYTE  abTACDenial[5];
  BYTE  abTACOnline[5];
  BYTE  abTACDefault[5];
  BYTE  abTLV[CLCFGP_TLV_LEN];
} CL_CFGP; //PC1109 increase TLV data buffer size
#pragma pack()

typedef struct {
  DWORD dExpo;
  DWORD dKeySize;
  BYTE  abKey[256];
} CL_RSA_KEY; //same as RSA_KEY in emvdll

typedef struct {
  BYTE  bKeyIdx;
  BYTE  abRID[5];
  CL_RSA_KEY sCAKey;
  WORD  wExpiryMMYY;
  WORD  wEffectMMYY;
  WORD  wChksum;
} CL_KEY_ROOM;

typedef struct {
  BYTE  bKeyIdx;
  BYTE  abRID[5];
  WORD  wExpiryMMYY;
  WORD  wEffectMMYY;
} CL_KEY_INFO;

// !qPBOC++
// Key Revocate Return Code
#define CL_CRL_SETUP_OK         0
#define CL_CRL_LENGTH		        1
#define CL_CRL_RIDFULL          2
#define CL_CRL_CRLFULL          3
#define CL_CRL_NOTFOUND         4
#define CL_CRL_EXIST            5

typedef struct {
  BYTE abRID[5];
  BYTE bCertIdx;
  BYTE abCertSN[3];
} CL_REVO_INFO;

typedef struct {
  BYTE bUsed;
  BYTE bCertIdx;
  BYTE abCertSN[3];
} CL_REVO_LIST;

typedef struct {
  BYTE abRID[5];
  CL_REVO_LIST sCrl[MAX_CL_REVKEY];
} CL_REVO_TBL;

// !qPBOC--


//----------------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Globals
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Generic API
//----------------------------------------------------------------------------
enum {
  CLXF_SETUP = 0,
  CLXF_MSG,
  CLXF_OSF_OWNER_EXPORT,  //PC1202 for internal use only
  CLXF_CL_WAITCARD,       //PC1203 for appl to wait cl card
  CLXF_CL_CLOSE,          //PC1203 for appl to close cl
  CLXF_CL_APDUCMD,        //PC1203 for appl to send cl cmd
  CLXF_NUM,
};

//extern BOOLEAN emvclSetup(DWORD bEntry, sIO_t* sio_in, sIO_t* sio_out);
// bEntry define
#define CLCMDX_INT_CALL             (DWORD)0x80000000    //internal setup call
#define CLCMDX_CFG_PLUS             (DWORD)0x40000000    //PC1109 use CL_CFGP instead of CL_CFG for AID read func
#define CLCMD_IFDSN_RW              (BYTE)0x01
#define CLCMD_KEY_LOADING           (BYTE)0x02
#define CLCMD_CLR_AID               (BYTE)0x03
#define CLCMD_LOAD_AID              (BYTE)0x04
#define CLCMD_CLOSE_LOAD            (BYTE)0x05
#define CLCMD_ADD_AID               (BYTE)0x06
#define CLCMD_DEL_AID               (BYTE)0x07
#define CLCMD_READ_AID              (BYTE)0x08
#define CLCMD_READ_AIDIDX           (BYTE)0x09
#define CLCMD_READ_KEY              (BYTE)0x0A
#define CLCMD_CONFIG_STATUS         (BYTE)0x0B
#define CLCMD_DNLOAD_PARAM          (BYTE)0x0C
#define CLCMD_SET_KEY_VERSION       (BYTE)0x0D
#define CLCMD_DEL_MULTI_ACQ         (BYTE)0x0E
#define CLCMD_READ_MULTI_ACQ        (BYTE)0x0F
#define CLCMD_READ_CHKSUM           (BYTE)0x10
#define CLCMD_CLR_CRL               (BYTE)0x11
#define CLCMD_ADD_CRL               (BYTE)0x12
#define CLCMD_DEL_CRL               (BYTE)0x13
#define CLCMD_SET_PINPAD            (BYTE)0x14
#define CLCMD_EXIST_CRL             (BYTE)0x17
//!DUPLICATE WITH emvdll 
//#define IOCMD_SET_INTERFACE         23
//#define IOCMD_PUT_PROPRIETARY_TAGS  24  //Reserved
//#define IOCMD_SET_PINBYPASS         25  // !2011-11-14 ECR11-0069 support Subsequent PIN Bypass
#define CLCMD_SYNC_STATE            (BYTE)0x1A  // !clPBOC
//define for EMVCL ONLY
#define CLCMD_GET_TAGS              (BYTE)0x80
#define CLCMD_EPTYPE_RW             (BYTE)0x81  //PC1109 set or get entrypoint type, use kernel legacy type KTYPE_xx
#define CLCMD_PUT_TAGS              (BYTE)0x82  //PC1201 put tlv parameters
#define emvclSetup(x,y,z)           (BOOLEAN) emvcldll_call(CLXF_SETUP,(DWORD)x,(DWORD)y,(DWORD)z)

//extern BOOLEAN emvclMsg(DWORD bEntry, sIO_t * pin, sIO_t * pout);
// bEntry define
#define CLMSGX_AUTO_TO_FINISH       (DWORD)0x80000000    //auto until finish
#define CLMSG_PROC_INIT             (BYTE)0x00
#define CLMSG_PROC_START            (BYTE)0x01
#define CLMSG_ENTER_SEL             (BYTE)0x02
#define CLMSG_ONL_PIN               (BYTE)0x03
#define CLMSG_ONL_RESULT            (BYTE)0x04
#define CLMSG_VOICE_REFER           (BYTE)0x05
#define CLMSG_AMT_IAP               (BYTE)0x06
#define CLMSG_VALID_CARD            (BYTE)0x07
#define CLMSG_ENTER_AMT             (BYTE)0x08
#define CLMSG_RESTART               (BYTE)0x09
#define emvclMsg(x,y,z)             (BOOLEAN) emvcldll_call(CLXF_MSG,(DWORD)x,(DWORD)y,(DWORD)z)

// contactless function return
#define EMVCL_CL_OK             (int)0
#define EMVCL_CL_ERR            (int)-1
#define EMVCL_CL_TIMEOUT        (int)-2
#define EMVCL_CL_MORE_CARDS     (int)-3
#define EMVCL_CL_WTX_TIMEOUT    (int)-4
//extern int emvclWaitCard(DWORD aTout10ms, BYTE *aLvParam);
#define emvclWaitCard(x,y)          (int) emvcldll_call(CLXF_CL_WAITCARD,(DWORD)x,(DWORD)y,(DWORD)0)
//extern int emvclClose(DWORD aWaitRemove10ms);
#define emvclClose(x)               (int) emvcldll_call(CLXF_CL_CLOSE,(DWORD)x,(DWORD)0,(DWORD)0)
//extern int emvclApduCmd(BYTE *aInOut, WORD aLen, WORD *aSw12);
#define emvclApduCmd(x,y,z)         (int) emvcldll_call(CLXF_CL_APDUCMD,(DWORD)x,(DWORD)y,(DWORD)z)


//-----------------------------------------------------------------------------
#endif //_EMVCL2DLL_H_



