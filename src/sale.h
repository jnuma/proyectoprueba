//-----------------------------------------------------------------------------
//  File          : sale.h
//  Module        :
//  Description   : Declrartion & Defination for sale.c
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _SALE_H_
#define _SALE_H_
#include "common.h"


extern DDWORD gValorTotalCaja;

extern void ReferralProcess(void);
#if(NO_USED)
extern DWORD SaleTrans(DWORD aInputMode);
#endif
extern DWORD CompraTrans(DWORD aInputMode, BYTE aTrans);	//kt-291012
extern BOOLEAN SelCuenta( void );
extern DWORD AvanceTrans(DWORD aInputMode, BYTE aTrans);  //kt-160413
extern BOOLEAN procesoCompraCaja1(BYTE *aPanAscii);
extern BOOLEAN procesoCompraCaja2(BYTE *aPanAscii);
extern BOOLEAN PagoDivididoTrans(void);
extern BOOLEAN VirtualMerchantVisa(void); //kt-231112
extern BOOLEAN ConsultaCosto (void); //MFBC/03/12/12
extern BOOLEAN ConsultaCostoTrans (void); //MFBC/03/12/12
extern DWORD AvanceFallbackTrans(DWORD aInputMode);
extern BOOLEAN monedero(BYTE *aOTP, DWORD aLenOTP);
extern DWORD pagoDivididoTarjeta (void);
extern DWORD superCupoFalabella (void);
extern DWORD avanceSuperCupo (void);


#endif // _SALE_H_

