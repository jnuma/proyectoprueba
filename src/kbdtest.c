//-----------------------------------------------------------------------------
//  File          : kbdtest.c
//  Module        :
//  Description   : Include Keyboard test routines.
//  Author        : Lewis
//  Notes         :
//
//  Naming conventions
//  ~~~~~~~~~~~~~~~~~~
//             Constant : Leading K
//      Global Variable : Leading g
//    Function argument : Leading a
//       Local Variable : All lower case
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  04 Jun  2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include "coremain.h"
#include "midware.h"
#include "hwdef.h"
#include "sysutil.h"
#include "kbdtest.h"

//-----------------------------------------------------------------------------
//      Prototypes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      Defines
//-----------------------------------------------------------------------------
struct KEY_INFO {
  BYTE code;
  DWORD line;
  DWORD pos;
  const char *disp_str;
};

//-----------------------------------------------------------------------------
//      Globals
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      Constant
//-----------------------------------------------------------------------------
static struct KEY_INFO KKeyInfo[] = {
  //Line: 2,4,6,8,A,C,E
  //Loc : 0123456789ABCD
  //linea4 MWKEY_1
  {MWKEY_LEFT           , MW_LINE4,  3, "<"},
  {MWKEY_RIGHT        , MW_LINE4,  8, ">"},
  {MWKEY_POWER     , MW_LINE4,  12, "ON"},
  {MWKEY_DN             , MW_LINE4,  15, "v"},
  {MWKEY_UP             , MW_LINE4,  19, "^"},
  //linea6
  {MWKEY_1               , MW_LINE6,  5, "1"},
  {MWKEY_2               , MW_LINE6,  9, "2"},
  {MWKEY_3               , MW_LINE6,  14, "3"},
  {MWKEY_CANCL      , MW_LINE6,  19, "X"}, //Rojo 9
  //linea7
  {MWKEY_4               , MW_LINE7,  5, "4"},
  {MWKEY_5               , MW_LINE7,  9, "5"},
  {MWKEY_6               , MW_LINE7,  14, "6"},
  {MWKEY_CLR           , MW_LINE7,  19, "<-"},  //Amarilla 13
  //linea8
  {MWKEY_7               , MW_LINE8,  5, "7"},
  {MWKEY_8               , MW_LINE8,  9, "8"},
  {MWKEY_9               , MW_LINE8,  14, "9"},
  //linea9
  {MWKEY_ASTERISK , MW_LINE9, 5, "*"},
  {MWKEY_0               , MW_LINE9, 9, "0"},
  {MWKEY_SHARP      , MW_LINE9, 14, "#"},
  {MWKEY_ENTER      , MW_LINE9,  19, "ENTER"}, //Verde 20

 // {MWKEY_DOT      , MW_LINE2, 10, "."},

 // {MWKEY_SHARP    , MW_LINE2, 12, "#"},
  //line3
 // {MWKEY_FUNC     , MW_LINE3,  0, "F"},
//  {MWKEY_FUNC1    , MW_LINE3,  1, "1"},
//  {MWKEY_FUNC2    , MW_LINE3,  2, "2"},
//  {MWKEY_FUNC3    , MW_LINE3,  3, "3"},
//  {MWKEY_FUNC4    , MW_LINE3,  4, "4"},
//  {MWKEY_FUNC5    , MW_LINE3,  5, "5"},
//  {MWKEY_FUNC6    , MW_LINE3,  6, "6"},
//  {MWKEY_FUNC7    , MW_LINE3,  7, "7"},
//  {MWKEY_FUNC8    , MW_LINE3,  8, "8"},
//  {MWKEY_UP       , MW_LINE3,  9, "Up"},
//  {MWKEY_DN       , MW_LINE3, 11, "Dn"},
//  //line4
//  {MWKEY_POWER    , MW_LINE4,  0, "P "},
//  {MWKEY_ENTER    , MW_LINE4,  2, "E "},
//  {MWKEY_CLR      , MW_LINE4,  4, "C "},
//  {MWKEY_CANCL    , MW_LINE4,  6, "X "},
//  {MWKEY_00       , MW_LINE4,  8, "00"},
  //line5
//  {MWKEY_LEFT     , MW_LINE5,  0, "L"},
//  {MWKEY_LEFT1    , MW_LINE5,  1, "1"},
//  {MWKEY_LEFT2    , MW_LINE5,  2, "2"},
//  {MWKEY_LEFT3    , MW_LINE5,  3, "3"},
//  {MWKEY_LEFT4    , MW_LINE5,  4, "4"},
//  {MWKEY_LEFT5    , MW_LINE5,  5, "5"},
  //{MWKEY_RIGHT    , MW_LINE5,  6, "R"},
//  {MWKEY_RIGHT1   , MW_LINE5,  7, "1"},
//  {MWKEY_RIGHT2   , MW_LINE5,  8, "2"},
//  {MWKEY_RIGHT3   , MW_LINE5,  9, "3"},
//  {MWKEY_RIGHT4   , MW_LINE5, 10, "4"},
//  {MWKEY_RIGHT5   , MW_LINE5, 11, "5"},
  //line6&7&8                  
//  {MWKEY_TRANS    , MW_LINE6,  0, "Tx"},
//  {MWKEY_SETTLE   , MW_LINE6,  3, "St"},
//  {MWKEY_FEED     , MW_LINE6,  6, "Fd"},
//  {MWKEY_OTHERS   , MW_LINE6,  9, "Ot"},
//  {MWKEY_OCARD    , MW_LINE6, 12, "Oc"},
//  {MWKEY_BKSP     , MW_LINE7,  0, "Bs"},
//  {MWKEY_EPS      , MW_LINE7,  3, "Ep"},
//  {MWKEY_ALPHA    , MW_LINE7,  6, "Ah"},
//  {MWKEY_MENU     , MW_LINE7,  9, "Mu"},
//  {MWKEY_REPRN    , MW_LINE7, 12, "Rp"},
//  {MWKEY_SELECT   , MW_LINE8,  0, "Se"},
//  {MWKEY_FBACK    , MW_LINE8,  3, "Fb"},
};

//*****************************************************************************
//  Function        : GetKeyIndex
//  Description     : Get Key Table Index.
//  Input           : aKeyCode;         // Key Code
//  Return          : Index;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static int GetKeyIndex(DWORD aKeyCode)
{
  DWORD i;

  // Show Default Keyboard Layout
  for (i = 0; i < sizeof(KKeyInfo)/sizeof(struct KEY_INFO); i++) {
    if (KKeyInfo[i].code == aKeyCode) {
      return i;
    }
  }
  return -1;
}
//*****************************************************************************
//  Function        : KeyPressTest
//  Description     : Test Key Press & Key Status.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void KeyPressTest(void)
{
  DWORD i;
  DWORD ch;
  BOOLEAN cancel_pressed;
  //BYTE tmp[MW_MAX_LINESIZE+1];
  int  key_index;

  //DispClrBelowMW(MW_LINE2);
	DispLineMW("TECLADO", MW_LINE1, MW_REVERSE |MW_CLRDISP | MW_CENTER | MW_BIGFONT); //MFBC/20/02/13

  // Show Default Keyboard Layout
	for (i = 0; i < sizeof(KKeyInfo)/sizeof(struct KEY_INFO); i++)
	{

		if (KKeyInfo[i].code == MWKEY_CANCL)
		{
			DispPutCMW(K_PushCursor);
			os_disp_textc(COLOR_BLACK);
			os_disp_backc(COLOR_RED);
			DispLineMW(KKeyInfo[i].disp_str, KKeyInfo[i].line|KKeyInfo[i].pos, MW_SPFONT);
			DispPutCMW(K_PopCursor);
		}
		else if (KKeyInfo[i].code == MWKEY_CLR)
		{
			DispPutCMW(K_PushCursor);
			os_disp_textc(COLOR_BLACK);
			os_disp_backc(COLOR_VISAYELLOW);
			DispLineMW(KKeyInfo[i].disp_str, KKeyInfo[i].line|KKeyInfo[i].pos, MW_SPFONT);
			DispPutCMW(K_PopCursor);
		}

		else if (KKeyInfo[i].code == MWKEY_ENTER)
		{
			DispPutCMW(K_PushCursor);
			os_disp_textc(COLOR_BLACK);
			os_disp_backc(COLOR_GREEN);
			DispLineMW(KKeyInfo[i].disp_str, KKeyInfo[i].line|KKeyInfo[i].pos, MW_SPFONT);
			DispPutCMW(K_PopCursor);
		}

		else
			DispLineMW(KKeyInfo[i].disp_str, KKeyInfo[i].line|KKeyInfo[i].pos, MW_SPFONT);
	}

  // Print Seperator
//  for (i = MW_LINE2; i <= MW_LINE8; i+=(MW_LINE3-MW_LINE2)) {
//    DispLineMW("|", i+14, MW_SPFONT);
//  }

  cancel_pressed = FALSE;
//  DispLineMW("SCode:", MW_LINE3+15, MW_SPFONT);
//  DispLineMW("KCode:", MW_LINE6+15, MW_SPFONT);
  while (1) {
   // Show Keyboard Action
    if ((ch = KbdScodeMW())!=0) {
//      sprintf(tmp, "  %04X", ch);
//      DispLineMW(tmp, MW_LINE4+15, MW_SPFONT);
      key_index = GetKeyIndex(ch & 0xFF);
      if (key_index != -1) {
    	  if ((ch & 0x8000) == 0x8000) { // Key Released

    		  if(KKeyInfo[key_index].line == MW_LINE6 && KKeyInfo[key_index].pos == 19 )
    		  {
    			  DispPutCMW(K_PushCursor);
    			  os_disp_textc(COLOR_BLACK);
    			  os_disp_backc(COLOR_RED);
    			  DispLineMW(KKeyInfo[key_index].disp_str, KKeyInfo[key_index].line | KKeyInfo[key_index].pos|KKeyInfo[key_index].pos, MW_SPFONT);
    			  DispPutCMW(K_PopCursor);
    		  }

    		  else if(KKeyInfo[key_index].line == MW_LINE7 && KKeyInfo[key_index].pos == 19 )
    		  {
    			  DispPutCMW(K_PushCursor);
    			  os_disp_textc(COLOR_BLACK);
    			  os_disp_backc(COLOR_VISAYELLOW);
    			  DispLineMW(KKeyInfo[key_index].disp_str, KKeyInfo[key_index].line | KKeyInfo[key_index].pos|KKeyInfo[key_index].pos, MW_SPFONT);
    			  DispPutCMW(K_PopCursor);
    		  }

    		  else if(KKeyInfo[key_index].line == MW_LINE9 && KKeyInfo[key_index].pos == 19 )
    		  {
    			  DispPutCMW(K_PushCursor);
    			  os_disp_textc(COLOR_BLACK);
    			  os_disp_backc(COLOR_GREEN);
    			  DispLineMW(KKeyInfo[key_index].disp_str, KKeyInfo[key_index].line | KKeyInfo[key_index].pos|KKeyInfo[key_index].pos, MW_SPFONT);
    			  DispPutCMW(K_PopCursor);
    		  }

    		  else if(key_index != MWKEY_CANCL || key_index != MWKEY_CLR || key_index != MWKEY_ENTER )
    			  DispLineMW(KKeyInfo[key_index].disp_str, KKeyInfo[key_index].line|KKeyInfo[key_index].pos, MW_SPFONT);


    	  }

    	  else
    	  {  // Key Pressed
    		  DispLineMW(KKeyInfo[key_index].disp_str, KKeyInfo[key_index].line|KKeyInfo[key_index].pos, MW_REVERSE|MW_SPFONT);

    	  }
      }
    }

    // Check for Key Pressed
    if ((ch=GetCharMW())!= 0) {
      if (ch == MWKEY_CANCL) {
        if (cancel_pressed == TRUE)
          break;
        cancel_pressed = TRUE;
      }
//      if (ch != 0x00) {
//        sprintf(tmp, "  %04X", ch);
//        DispLineMW(tmp, MW_LINE7+15, MW_SPFONT);
//      }
    }
    SleepMW();
  }
}

