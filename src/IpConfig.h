/*
 * IpConfig.h
 *
 *  Created on: 2/08/2012
 *      Author: MB
 */

#ifndef IPCONFIG_H_
#define IPCONFIG_H_

//-----------------------------------------------------------------------------
//   IP Related
//-----------------------------------------------------------------------------
// IP related
#define EDITIP_CURSOR_MAX     3
#define EDITIP_COL            0
#define EDITPORT_CURSOR_MAX   3
#define EDITPORT_COL          0
#define IP_LEN                4

extern void TermIPFunc(void);
extern void DispTermIP(BYTE *aIP);
extern BOOLEAN ConfigTermIP(BYTE *aIP);
extern void ShowIp(unsigned char *aIp, int aPos, int aReverse);
extern BOOLEAN ToggleDHCP(BYTE *aDHCP, BYTE *aIP);
extern BOOLEAN EditIp(BYTE *aIp, DWORD aPos);
extern BOOLEAN UpdateTermID(void);
extern BOOLEAN UpdTermData(struct TERM_DATA *aDat);
extern BOOLEAN configDNS (BYTE *DNS);
extern void DispTermDNS(BYTE *dns);
extern void CleanTermianlIP (void);		// **SR**			01/10/13


#endif /* IPCONFIG_H_ */
