//-----------------------------------------------------------------------------
//  File          : Void.c
//  Module        :
//  Description   : Include routines for Void transaction.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include "util.h"
#include "sysutil.h"
//#include "key2dll.h"
#include "message.h"
#include "constant.h"
#include "corevar.h"
#include "chkoptn.h"
#include "input.h"
#include "print.h"
#include "tranutil.h"
#include "hostmsg.h"
#include "reversal.h"
#include "offline.h"
#include "emvtrans.h"
#include "record.h"
#include "input.h"
#include "files.h"
#include "sale.h" //MFBC/24/04/13
#include "keytrans.h"
#include "key2dll.h"
#include "cajas.h"
#include "ctltrans.h"

//*****************************************************************************
//  Function        : VoidTrans
//  Description     : Do a Void transaction.
//  Input           : aRecIdx;          // record index
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void VoidTrans(DWORD aRecIdx)
{
	struct TXN_RECORD org_rec;

	INPUT.b_trans = VOID;

	if (STIS_TERM_DATA.b_stis_mode < TRANS_MODE)
	{
		DispErrorMsg(GetConstMsg(EDC_VD_NOT_ALLOW));
		return;
	}

	if (aRecIdx == -1)
	{
		DispHeader(NULL);
		aRecIdx = SearchRecord(FALSE);
		if (aRecIdx == -1)
			return;
	}

	DispHeader(NULL);
	APM_GetBatchRec(aRecIdx, (BYTE * ) &org_rec, sizeof(org_rec));
	if (org_rec.b_trans_status & VOIDED)
	{
		DispErrorMsg(GetConstMsg(EDC_AJ_TXN_VOIDED));
		return;
	}

	APM_GetTermCfg(&STIS_TERM_CFG);
	APM_GetAcqTbl(org_rec.w_host_idx, &STIS_ACQ_TBL(0));
	APM_GetIssuer(org_rec.w_issuer_idx, &STIS_ISS_TBL(0));
	INPUT.w_host_idx = org_rec.w_host_idx;
	INPUT.w_issuer_idx = org_rec.w_issuer_idx;

	if (VoidBlocked() || (org_rec.b_trans < SALE_SWIPE))
	{
		DispErrorMsg(GetConstMsg(EDC_VD_NOT_ALLOW));
		return;
	}

	if (org_rec.b_trans_status & VOIDED)
	{
		DispErrorMsg(GetConstMsg(EDC_VD_VOIDED));
		return;
	}

	if (HostSettlePending())
	{
		DispErrorMsg(GetConstMsg(EDC_AJ_CLOSE_BATCH));
		return;
	}

	DispLineMW(GetConstMsg(EDC_AJ_TOTAL_PROMPT), MW_LINE1,
			MW_CLRDISP | MW_REVERSE | MW_CENTER | MW_BIGFONT);
	DispAmount(org_rec.dd_amount,
			((SaleType(org_rec.b_trans)) ? MW_LINE3 : MW_LINE3 + 0x01),
			MW_BIGFONT);
	if (PromptYesNo() != 2)
		return;

	DispHeader(STIS_ISS_TBL(0).sb_card_name);
	P_DATA.b_type = CUS_SLIP;
	if (((org_rec.b_trans_status & OFFLINE) != 0) || VoidOffline())
	{
		org_rec.b_trans_status |= (VOIDED | ADJUSTED_OFF);
		RSP_DATA.b_response = TRANS_ACP;
		RSP_DATA.w_rspcode = '0' * 256 + '0';
		ReadRTC(&org_rec.s_dtg);
		IncTraceNo();
		memcpy(&RECORD_BUF, &org_rec, sizeof(struct TXN_RECORD));
		UpdateRecord(aRecIdx);
		PackRecordP(FALSE, FALSE);
		TransEnd(TRUE);
		return;
	}
	ClearResponse();
	PackComm(INPUT.w_host_idx, FALSE);
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
		if (ReversalOK())
		{
			OfflineSent(0);
			//APM_GetBatchRec(aRecIdx, (BYTE *)&RECORD_BUF);
			ByteCopy((BYTE *) &TX_DATA, (BYTE *) &org_rec,
					TX_DATA.sb_pin - (BYTE *) &TX_DATA);
			TX_DATA.dd_amount = TX_DATA.dd_org_amount;
			TX_DATA.b_trans = VOID;
			PackProcCode(org_rec.b_trans, TX_DATA.b_acc_ind);
			TX_DATA.sb_proc_code[0] = TX_DATA.sb_proc_code[0] | 0x02;
			IncTraceNo();
			PackHostMsg();
			UpdateHostStatus(REV_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();
				if (RSP_DATA.b_response <= TRANS_REJ)
					UpdateHostStatus(NO_PENDING);
			}
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				org_rec.b_trans_status |= VOIDED;
				memcpy(org_rec.sb_trace_no, TX_DATA.sb_trace_no,
						sizeof(org_rec.sb_trace_no));
				memcpy(org_rec.sb_auth_code, RSP_DATA.sb_auth_code,
						sizeof(org_rec.sb_auth_code));
				ReadRTC(&org_rec.s_dtg);
				memcpy(&RECORD_BUF, &org_rec, sizeof(struct TXN_RECORD));
				UpdateRecord(aRecIdx);
				PackRecordP(FALSE, FALSE);
			}
		}
	TransEnd(TRUE);
	Delay10ms(50);
}
#endif
//*****************************************************************************
//  Function        : VoidTransVisa
//  Description     : Do a Void transaction.
//  Input           : aRecIdx;          // record index
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int VoidTransVisa(DWORD aRecIdx, BYTE aTrans)
{
	struct TXN_RECORD org_rec;
	struct TABLA_CERO tabla_cero;
	WORD rec_cnt;
	INPUT.b_transAcep = FALSE; //MFBC/25/04/13
	// struct TABLA_CUATRO tabla_cuatro; //kt-140912
	// struct TABLA_TRES tabla_tres;
	//BYTE PassAdmin[4];
	BYTE dispPuntos[20];
	BYTE panBCD[10];
	WORD entry = 0;
	DWORD keyin = 0;
	// struct DATETIME dtg_Void;
	memset(dispPuntos, 0x00, sizeof(dispPuntos));
	ClearResponse();
	PackCommTest(INPUT.w_host_idx, FALSE); //MFBC/05/03/13


	INPUT.b_trans = aTrans;

	//DispHeader(NULL);
	DispHeaderTrans();
	//DispClrBelowMW(MW_LINE3);

	if(!Cajas()){				// Cuando esta integrado a Cajas No pide el password al princio de la transaccion  // **SR** 09/10/13
		if(TransPassAdmin(MW_LINE2) == FALSE)
		{
			APM_ResetComm();
			return -1;
		}
	}

	APM_PreConnect();
	GetTablaCero(&tabla_cero);

	rec_cnt = APM_GetRecCount(); //kt-220413
	if (rec_cnt == 0)
	{
		LongBeep();
		TextColor("NO EXISTEN", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP
				| MW_SMFONT, 0);
		TextColor("TRANSACCIONES", MW_LINE6, COLOR_RED, MW_CENTER | MW_SMFONT,
				3);
		APM_ResetComm(); //MFBC/05/03/13
		return -1;
	}

	if (Cajas()) //kt-091012 cajas para anulacion
	{
		tipoTransCaja = ANULACION;
		if (!transaccionCaja(tipoTransCaja , TRUE, FALSE)){
			APM_ResetComm(); //MFBC/05/03/13
			return -1;
		}
		memcpy(INPUT.sb_trace_caja, gCajas.numeroRecibo_43, 6);
	}
	NextProcCajas();

	if (aRecIdx == -1)
	{
		DispHeaderTrans();
		//DispHeader(NULL);
		aRecIdx = SearchRecord(FALSE);

		if (aRecIdx == -1 || aRecIdx == -2 )
		{
			if(aRecIdx == -1)
				DispErrorMsg(GetConstMsg(EDC_AJ_TXN_VOIDED));
			APM_ResetComm(); //MFBC/05/03/13
			ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
			ResetTerm();
			return -1;
		}
	}
	DispHeaderTrans();
	APM_GetBatchRec(aRecIdx, (BYTE *) &org_rec, sizeof(org_rec));
	gRecIdx = aRecIdx;

	CompletDataVoidCaja();		// **SR** 23/08/13

	if (org_rec.b_trans_status & VOIDED)
	{
		DispErrorMsg(GetConstMsg(EDC_AJ_TXN_VOIDED));
		APM_ResetComm(); //MFBC/05/03/13
		ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
		ResetTerm();
		return -1;
	}
	// GetTablaCuatro(org_rec.w_host_idx, &tabla_cuatro); //kt-140912
	// GetTablaTres(org_rec.w_issuer_idx, &tabla_tres);
	INPUT.w_host_idx = org_rec.w_host_idx;
	INPUT.w_issuer_idx = org_rec.w_issuer_idx;

	//	if (org_rec.b_trans == RETIRO_MULTIBOL) {  //MFBC/24/02/13
	//		LongBeep();
	//		TextColor("No Permitido", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER
	//				| MW_CLRDISP, 3);
	//		ResetTerm();
	//		return;
	//	}

	if (org_rec.b_trans_status & VOIDED)
	{
		DispErrorMsg(GetConstMsg(EDC_VD_VOIDED));
		ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
		ResetTerm();
		return -1;
	}

	if(Cajas()){				// Cuando esta integrado a Cajas No pide el password al princio de la transaccion  // **SR** 09/10/13
		if(TransPassAdmin(MW_LINE2) == FALSE)
		{
			APM_ResetComm();
			ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
			ResetTerm();
			return -1;
		}
	}

	while (TRUE)
	{

		DispClrBelowMW(MW_LINE2);
		TextColor("CONFIRMAR MONTO:", MW_LINE3, COLOR_VISABLUE, MW_LEFT
				| MW_BIGFONT, 0); //MFBC/26/02/13
		DispAmnt(org_rec.dd_amount  , MW_LINE5, MW_BIGFONT); //kt-081012
		displaySI_NO_2();

		switch(APM_YesNo())
		{

		case -1:
			APM_ResetComm(); //MFBC/05/03/13
			ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
			ResetTerm();
			return -1;

		case 0:
			APM_ResetComm(); //MFBC/05/03/13
			ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
			ResetTerm();
			return -1;

		case 2:
			break;

		default:
			continue;
		}
		break;
	}

	memcpy(&gOrg_rec, &org_rec, sizeof(org_rec));
	//	if (!GetCard(0, false, VOID))

	if (org_rec.b_entry_mode == FALLBACK || org_rec.b_entry_mode == SWIPE)
	{

		entry = 3;
	}
	else if (org_rec.b_entry_mode == ICC)
	{
		entry = 2;
	}
	else if (org_rec.b_entry_mode == CONTACTLESS)
	{
		INPUT.dd_amount = org_rec.dd_amount;
		if (CTLPreProcess() == FALSE)
		{
			//DispErrorMsg(GetConstMsg(EDC_CTL_PREPROCESS_ERR));
			DispLineMW("ERROR PRE-PROCESS", MW_LINE4, MW_CLRDISP
					| MW_CENTER | MW_SMFONT);
			ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
			ResetTerm();
			return -1;
		}

		// Set timer
		TimerSetMW(gTimerHdl[TIMER_DISP], 1500);
		TextColor("ACERQUE LA TARJETA", MW_LINE1, COLOR_VISABLUE, MW_CENTER
				| MW_CLRDISP | MW_SMFONT, 0);
		TextColor("MONTO:", MW_LINE8, COLOR_VISABLUE, MW_SMFONT | MW_LEFT,
				0);
		DispAmnt(org_rec.dd_amount, MW_LINE8, MW_SMFONT);
		do
		{
			SleepMW();
			if (keyin == MWKEY_CANCL) {
				return FALSE;
			}
			DispDefinedLogo(DP_LOGO_LANDINGZONE, FALSE);
			if (INPUT.dd_amount != 0 && CtlUnderTransLimit((DWORD)(
					INPUT.dd_amount)))
			{

				if (CTLWaitCard())
				{
					CTLTrans(aTrans);
					ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
					ResetTerm();
					CL_Close(0);
					return -1;
				}
			}

			keyin = APM_GetKeyin();

		} while (TimerGetMW(gTimerHdl[TIMER_DISP]) != 0);
		CL_Close(0);
		ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
		APM_ResetComm(); //MFBC/05/03/13
		ResetTerm();
		return -1;
	}

	if (!GetCard(0, FALSE, INPUT.b_trans, entry)) //kt-111012
	{
		APM_ResetComm(); //MFBC/05/03/13
		ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
		ResetTerm();
		return -1;
	}

	if (memcmp(INPUT.sb_pan, org_rec.sb_pan, sizeof(org_rec.sb_pan) ) != 0)
	{
		LongBeep();
		TextColor("TARJETA INVALIDA", MW_LINE5, COLOR_RED, MW_SMFONT
				| MW_CENTER | MW_CLRDISP, 3);
		APM_ResetComm(); //MFBC/05/03/13
		ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
		ResetTerm();
		return -1;
	}

	//tener en cuanta para pedir PIN en caso de que sea debito
	memset(INPUT.sb_pin, 0x00, sizeof(INPUT.sb_pin));

	if (org_rec.b_tipo_cuenta == 0x10 || org_rec.b_tipo_cuenta == 0x20)
	{
		if (PinDebito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				APM_ResetComm(); //MFBC/05/03/13
				ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
				ResetTerm();
				return -1;
			}
		}
	}
	else
	{
		if (PinCredito())
		{
			if (!getPinBlock(panBCD, TRUE)) //kt-060912
			{
				APM_ResetComm(); //MFBC/05/03/13
				ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
				ResetTerm();
				return -1;
			}
		} //fin-kt
	}

	memcpy(org_rec.sb_pin, INPUT.sb_pin, 8);

	//	ClearResponse();
	//	//PackComm(INPUT.w_host_idx, FALSE);
	//	PackCommTest(INPUT.w_host_idx, FALSE);
	if (!ConsultaCosto()) //MFBC/24/04/13
	{
		APM_ResetComm();
		ErrorTransCajas(CA_CANCEL);	// **SR** 23/08/13
		return -1;
	}


	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			//OfflineSent(0);

			org_rec.b_trans_void = org_rec.b_trans;


			//	memcpy(&RECORD_BUF, &org_rec, sizeof(struct TXN_RECORD));
			memcpy(&INPUT, &org_rec, sizeof(struct INPUT_DATA));

			INPUT.b_trans = aTrans;
			//kt-291112
			MoveInput2Tx();

			IncTraceNo();
			memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3);

			PackHostMsg(); //Aqui ya empaqueta para enviar manu.
			UpdateHostStatus(REV_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{

				RSP_DATA.b_response = CheckHostRsp();

				if (RSP_DATA.w_rspcode == '5' * 256 + '5')  //MFBC/10/12/13
				{
					RSP_DATA.b_response = NuevoPIN();
				}

				if (RSP_DATA.b_response == TRANS_REJ)
					UpdateHostStatus(NO_PENDING);
			}
			else
				ErrorTransCajas(CA_COMM);	// **SR** 23/08/13

			if (RSP_DATA.b_response == TRANS_ACP)
			{

				//ReadRTC(&TX_DATA.s_dtg); // MFBC /24/10/13  esto se hacia para que la impresion del voucher de anulacion mostrara la hora de la misma
				// pero se estaba guardando

				TX_DATA.b_transAcep = TRUE; //MFBC/25/04/13
				//org_rec.b_trans_status |= VOIDED; //MFBC/15/05/13
				TX_DATA.b_trans_status |= VOIDED;  //MFBC/15/05/13

				memcpy(TX_DATA.sb_auth_code, org_rec.sb_auth_code,  //MFBC/08/06/13
						sizeof(org_rec.sb_auth_code));

				memcpy(RSP_DATA.sb_auth_code, org_rec.sb_auth_code,  //MFBC/08/06/13
						sizeof(org_rec.sb_auth_code));

				memcpy(TX_DATA.sb_rrn, RSP_DATA.sb_rrn, sizeof(TX_DATA.sb_rrn));

				memcpy(&RECORD_BUF, &TX_DATA, sizeof(struct TXN_RECORD));
				//	RECORD_BUF.b_trans = TX_DATA.b_trans;


				UpdateRecord(aRecIdx);
				ReadRTC(&RECORD_BUF.s_dtg);
				SendCajas(1);  // **SR** 23/08/13
				PackRecordP(FALSE, FALSE);
			}

			if (RSP_DATA.b_response == TRANS_REJ)
			{
				SendCajas(1);  // **SR** 23/08/13
				if (P_DATA.b_flag_Consulta_Costo) //MFBC/24/04/13
				{
					PackInputP();
					TransEnd(TRUE);
					return FALSE;
				}
				TransEnd(FALSE);
				return FALSE;
			}
		}
		else
			ErrorTransCajas(CA_COMM);			// **SR** 09/09/13
	}
	else
		ErrorTransCajas(CA_COMM);	// Si no conecta tiene que enviar XX a la Caja **SR** 23/08/13
	TransEnd((BOOLEAN)(RSP_DATA.b_response == TRANS_ACP));
	Delay10ms(50);

	ResetTerm();
	return TRUE;
}

void VoidFallbackTrans(BYTE  aTrans )
{
	BYTE panBCD[10];
	INPUT.b_transAcep = FALSE; //MFBC/25/04/13
	ClearResponse();
	PackCommTest(INPUT.w_host_idx, FALSE); //MFBC/05/03/13
	APM_PreConnect();

	if (memcmp(INPUT.sb_pan, gOrg_rec.sb_pan, sizeof(gOrg_rec.sb_pan) != 0))
	{
		LongBeep();
		TextColor("TARJETA INVALIDA", MW_LINE5, COLOR_RED, MW_SMFONT
				| MW_CENTER | MW_CLRDISP, 3);
		APM_ResetComm(); //MFBC/05/03/13
		ResetTerm();
		return;
	}

	if (TX_DATA.b_tipo_cuenta == 0x10 || TX_DATA.b_tipo_cuenta == 0x20)
	{
		if (PinDebito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				APM_ResetComm(); //MFBC/05/03/13
				ResetTerm();
				return;
			}

		}
	}
	else
	{
		if (PinCredito())
		{
			if (!getPinBlock(panBCD, TRUE)) //kt-060912
			{
				APM_ResetComm(); //MFBC/05/03/13
				ResetTerm();
				return;
			}
		} //fin-kt
	}

	//	ClearResponse();
	//	//PackComm(INPUT.w_host_idx, FALSE);
	//	PackCommTest(INPUT.w_host_idx, FALSE);
	if (!ConsultaCosto()) //MFBC/24/04/13
	{
		APM_ResetComm();
		return;
	}


	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
		if (ReversalOK())
		{
			//OfflineSent(0);

			//APM_GetBatchRec(aRecIdx, (BYTE *)&RECORD_BUF);
			gOrg_rec.b_trans_void = gOrg_rec.b_trans;

			/*			ByteCopy((BYTE *) &TX_DATA, (BYTE *) &gOrg_rec, TX_DATA.sb_pin
					- (BYTE *) &TX_DATA);
			ByteCopy((BYTE *) &TX_DATA.s_trk1buf, (BYTE *) &INPUT.s_trk1buf,
					sizeof(struct MW_TRK1BUF)); //kt-121012
			ByteCopy((BYTE *) &TX_DATA.s_trk2buf, (BYTE *) &INPUT.s_trk2buf,
					sizeof(struct MW_TRK2BUF));
			 */ //MFBC/28/05/13
			//	ReadRTC(&TX_DATA.s_dtg);
			memcpy(&INPUT, &gOrg_rec, sizeof(struct INPUT_DATA));
			INPUT.b_trans = aTrans;
			//kt-291112
			/*
			INPUT.dd_amount = TX_DATA.dd_org_amount;
			INPUT.dd_iva = TX_DATA.dd_iva;
			INPUT.dd_base = TX_DATA.dd_base;
			 */

			/*memcpy(INPUT.sb_comercio_v, TX_DATA.sb_comercio_v,
					sizeof(TX_DATA.sb_comercio_v));
			memcpy(INPUT.sb_cuota, TX_DATA.sb_cuota, sizeof(TX_DATA.sb_cuota));
			 */   //MFBC/28/05/13
			MoveInput2Tx();

			memcpy(RSP_DATA.sb_rrn, &TX_DATA.sb_rrn, sizeof(RSP_DATA.sb_rrn));
			memcpy(RSP_DATA.sb_auth_code, &TX_DATA.sb_auth_code,
					sizeof(RSP_DATA.sb_auth_code));
			//fin-kt

			/*
			TX_DATA.dd_amount = TX_DATA.dd_org_amount;
			TX_DATA.b_trans = INPUT.b_trans;
			TX_DATA.sb_proc_code[0] = TX_DATA.sb_proc_code[0] | 0x02;
			 */  //MFBC/28/05/13

			//memcpy( TX_DATA.sb_trace_no  );

			IncTraceNo();
			memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3);
			PackHostMsg();
			//ReadRTC(&INPUT.s_dtg);
			//memcpy(&TX_DATA.s_dtg, &INPUT.s_dtg, sizeof(struct DATETIME));
			UpdateHostStatus(REV_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();

				if (RSP_DATA.w_rspcode == '5' * 256 + '5')  //MFBC/10/12/13
				{
					RSP_DATA.b_response = NuevoPIN();
				}

				if (RSP_DATA.b_response == TRANS_REJ)
					UpdateHostStatus(NO_PENDING);
			}
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				//gOrg_rec.b_trans = TX_DATA.b_trans;
				TX_DATA.b_transAcep = TRUE;
				TX_DATA.b_trans_status |= VOIDED;

				/*memcpy(gOrg_rec.sb_trace_no, TX_DATA.sb_trace_no,
						sizeof(gOrg_rec.sb_trace_no));
				 */
				memcpy(gOrg_rec.sb_auth_code, RSP_DATA.sb_auth_code,
						sizeof(gOrg_rec.sb_auth_code));
				//ReadRTC(&gOrg_rec.s_dtg);

				memcpy(&RECORD_BUF, &TX_DATA, sizeof(struct TXN_RECORD));

				UpdateRecord(gRecIdx);
				PackRecordP(FALSE, FALSE);
			}
			if (Cajas()) //kt-091012
			{
				tipoTransCaja = ANULACION_2;
				if (!transaccionCaja(tipoTransCaja, FALSE, TRUE))
				{
					APM_ResetComm(); //MFBC/05/03/13
					ResetTerm();
					return;
				}
			}
		}
	TransEnd(TRUE);
	Delay10ms(50);

	ResetTerm();
	return;

}

