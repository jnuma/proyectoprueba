/*
 * VisaMenus.c
 *
 *  Created on: 1/08/2012
 *      Author: Manuel Barbaran
 */
#include <stdlib.h>
#include "sysutil.h"
#include "menu.h"
#include "VisaMenus.h"
#include "hardware.h"
#include "message.h"
#include "SPGLib.h"
#include "init.h"
#include "void.h"
#include "sale.h"
#include "keytrans.h"
#include "func.h"
#include "input.h"
#include "coremain.h"
#include "files.h"
#include "cajas.h"
#include "Corevar.h"
#include "print.h"
#include "testrans.h"
#include "util.h"
#include "tranutil.h"
#include "settle.h"
#include "Tecnico.h"
#include "Chkoptn.h"
#include "Constant.h"
#include "kbdtest.h"
#include "TMLPTTESt.h"

static const struct MENU_ITEM KMenuVisa[] =
{
		{ 1, "Operativo" },
		{ 2, "Administrativo" },
		{ 3, "Tecnico" },
		{ -1, NULL }, };

static const struct MENU_DAT KVisa =
{ "MENU", KMenuVisa, };

static const struct MENU_ITEM KReportesItems[] =
{
		{ 1, "Detalle Impresora" },
		{ 2, "Totales Impresora" },
		{ 3, "Detalle pantalla" },
		{ 4, "Totales pantalla" }, //kt-261112
		{ 5, "Otras tarjetas" },
		{ -1, NULL }, };

static const struct MENU_DAT KReportesMenu =
{ "REPORTES", KReportesItems, };


static const struct MENU_ITEM KReportesItems2[] =
{
		{ 1, "Detalle Impresora" },
		{ 2, "Totales Impresora" },
		{ 3, "Detalle pantalla" },
		{ 4, "Totales pantalla" }, //kt-261112
		{ 5, "Otras tarjetas" },
		{ 6, "Multimerchant" },
		{ -1, NULL }, };

static const struct MENU_DAT KReportesMenu2 =
{ "REPORTES", KReportesItems2, };

static const struct MENU_ITEM KRepOtrasItems[] = //LFGD 04082013
{
		{ 1, "CMR" },
		{ 2, "LA POLAR " },
		{ -1, NULL }, };

static const struct MENU_DAT KRepOtrasMenu =
{ "OTRAS TARJETAS", KRepOtrasItems, };

static const struct MENU_ITEM KRepItems[] = //LFGD 04082013
{
		{ 1, "Detalle Impresora" },
		{ 2, "Totales Impresora" },
		{ 3, "Detalle pantalla" },
		{ 4, "Totales pantalla" },
		{ -1, NULL }, };

static const struct MENU_DAT KRepLaPMenu =
{ "REPORTE LA POLAR", KRepItems, };

static const struct MENU_DAT KRepCMRsMenu =
{ "REPORTE CMR", KRepItems, };

static const struct MENU_ITEM KImprRef[] =
{
		{ 1, "Ultima transaccion" },
		{ 2, "Numero de Recibo" },
		{ -1, NULL }, };

static const struct MENU_DAT KImprRefMenu =
{ "DUPLICADO", KImprRef, };


//Daniel 08/10/12
static const struct MENU_ITEM KBancosItems[] =
{
		{ 1, "Avance" },
		{ 2, "Impuesto" },
		{ -1, NULL }, };

static const struct MENU_DAT KBancosMenu =
{ "BANCOS", KBancosItems, };

static const struct MENU_ITEM KPruebasItems[] =
{
		{ 1, "Impresora" },
		{ 2, "Teclado" },
		{ 3, "Version" },
		{ -1, NULL }, };

static const struct MENU_DAT KPruebasMenu =
{ "PRUEBAS", KPruebasItems, };

static const struct MENU_ITEM KtablaIvasItems[] = //kt-311012
{
		{ 1, "Crear" },
		{ 2, "Eliminar" },
		{ 3, "Imprimir reporte" }, //kt-211112
		{ -1, NULL }, };

static const struct MENU_DAT KTablaIvasMenu = //kt-311012
{ "CONFIGURAR IVAS", KtablaIvasItems, };

static const struct MENU_ITEM KTrEspecialesItems[] =
{
		{ 1, "AVANCE" },
		{ 2, "CMR Extendido" },
		{ -1, NULL }, };

static const struct MENU_DAT KTrEspecialesMenu =
{ "Tr.Especiales", KTrEspecialesItems, };


//*****************************************************************************
//  Function        : InitMenuVisa
//  Description     : Despliega Menu Inicial.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void InitMenuVisa(void)
{
	while (1)
	{
		int select = 0;

		select = MenuSelectVisa(&KVisa, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			MenuComercio();
			break;

		case 2:
			MenuAdministrativo();
			break;

		case 3:
			MenuTecnico();
			break;

		default:
			break;

		}
	}
}
#endif
//*****************************************************************************
//  Function        : MenuComercio
//  Description     : Despliega Menu Comercio.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void MenuComercio(void)
{
	struct TABLA_CERO tabla_cero;

	int select = 0;
	GetTablaCero(&tabla_cero);

	while (1)
	{
		select = 0;

		if ((memcmp(&gTablaCero.b_dir_ciudad[39], "\x38", 1) == 0 || memcmp(
				&gTablaCero.b_dir_ciudad[39], "\x37", 1) == 0)
				&& gConfigComercio.enableCardExito)
		{

			select = MenuSelectVisa(&KComercioMenu3, select);
		}
		else if (memcmp(&gTablaCero.b_dir_ciudad[39], "\x38", 1) == 0
				|| memcmp(&gTablaCero.b_dir_ciudad[39], "\x37", 1) == 0)
		{

			select = MenuSelectVisa(&KComercioMenu2, select);
		}
		else if (gConfigComercio.enableCardExito)
			select = MenuSelectVisa(&KComercioMenuExito, select);

		else
			select = MenuSelectVisa(&KComercioMenu, select);

		if (select == -1)
			return;

		printf("sel:<%d>", select);
		APM_WaitKey(9000, 0);

		switch (select)
		{
		case 1:
			flagAnul = TRUE; //LFGD 03/13/2013
			VoidTransVisa(-1, VOID);
			flagAnul = FALSE;
			break;

		case 2:
			SettleTransVisa();
			break;

		case 3:
			ConsultaSaldo(SALDO, FALSE);
			break;

		case 4:
			ConsultaFactura(FALSE);
			break;

		case 5:
			//AvanceTrans(0);
			MenuBancos();

			break;

		case 6:
			tipoChequeMenu();
			break;

		case 7:
			FidelidadMenu();//kt-081012
			break;

		case 8:
			PagoDivididoTrans();
			break;

		case 9:
			bonoObsequioTrans();
			break;

		case 10:
			mnuPagosMoviles();//MFBC/01/04/13
			break;

		case 11:
			recaudoExitoTrans();
			break;

		case 12:
			recaudoExitoTrans();

			//leerBINES(1);
			break;

		default:
			break;

		}
		RefreshDispAfter(0);
		ResetTerm();
		return;
	}

}
#endif
//void repBono(void)//kt-220413
//{
//
//	while (1)
//	{
//		int select = 0;
//
//		select = MenuSelectVisa(&KRepBonoMenu, select);
//
//		if (select == -1)
//			return;
//
//		switch (select)
//		{
//		case 1:
//			PackReportOtherCards(TRUE, 1);
//			break;
//
//		case 2:
//			PackReportOtherCards(FALSE, 1);
//			break;
//
//		case 3:
//
//			break;
//
//		case 4:
//
//			break;
//
//		default:
//			break;
//		}
//	}
//
//}
//
//void repLapolar(void)
//{
//
//	while (1)
//	{
//		int select = 0;
//
//		select = MenuSelectVisa(&KRepLaPMenu, select);
//
//		if (select == -1)
//			return;
//
//		switch (select)
//		{
//		case 1:
//			PackReportOtherCards(TRUE, 2);
//			break;
//
//		case 2:
//			PackReportOtherCards(FALSE, 2);
//			break;
//
//		case 3:
//
//			break;
//
//		case 4:
//
//			break;
//
//		default:
//			break;
//		}
//	}
//
//}
//
//void repCMR(void)
//{
//
//	while (1)
//	{
//		int select = 0;
//
//		select = MenuSelectVisa(&KRepCMRsMenu, select);
//
//		if (select == -1)
//			return;
//
//		switch (select)
//		{
//		case 1:
//			PackReportOtherCards(TRUE, 3);
//			break;
//
//		case 2:
//			PackReportOtherCards(FALSE, 3);
//			break;
//
//		case 3:
//
//			break;
//
//		case 4:
//
//			break;
//
//		default:
//			break;
//		}
//	}
//
//}


void mnuOtras(void) //LFGD 04082013  //kt-220413 modifique casi todo
{

	while (1)
	{
		int select = 0;

		select = MenuSelectVisa(&KRepOtrasMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			PackReportOtherCards(TRUE, "CMR", 3);
			break;

		case 2:
			PackReportOtherCards(TRUE, "LA POLAR", 8);
			break;

		default:
			break;
		}
	}
}

//*****************************************************************************
//  Function        : MenuReportes
//  Description     : Despliega Menu para reportes.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void MenuReportes(void) //kt-140912  //kt-290413
{
	BYTE *p_tabla12;
	int idComerciotmp = -1;

	/*Esto es para saber si existe multicomercio*/
	/**************************************************************/
	p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));
	gIdTabla12 = OpenFile(KTabla12File);

	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);

	idComerciotmp = ((struct TABLA_12*) p_tabla12)->b_id;

	CloseFile(gIdTabla12);
	FreeMW(p_tabla12);
	/**************************************************************/

	while (1)
	{
		int select = 0;

		if(idComerciotmp <= 0)
			select = MenuSelectVisa(&KReportesMenu, select);
		else
			select = MenuSelectVisa(&KReportesMenu2, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			//PackVoucherSettle();
			PackReportComercio(TRUE, FALSE); //kt-110413
			MsgBufSetup();
			PackMsgBufLen();
			fCommitAllMW();
			RefreshDispAfter(0);
			break;

		case 2:
			//PackVoucherSettleTotal(FALSE);
			PackReportComercio(FALSE, FALSE); //kt-110413
			MsgBufSetup();
			PackMsgBufLen();
			fCommitAllMW();
			RefreshDispAfter(0);
			break;

		case 3:
			//DisplayBatch(0);
			PackReportComercio(TRUE, TRUE); //kt-110413
			gf_MultiComer = FALSE;
			g_TypeCount = 0xFF;
			MsgBufSetup();
			PackMsgBufLen();
			fCommitAllMW();
			RefreshDispAfter(0);
			break;

		case 4:
			//DispTransTotal();
			PackReportComercio(FALSE, TRUE); //kt-110413
			gf_MultiComer = FALSE;
			g_TypeCount = 0xFF;
			MsgBufSetup();
			PackMsgBufLen();
			fCommitAllMW();
			RefreshDispAfter(0);
			break;

		case 5:

			mnuOtras(); //LFGD 04082013
			MsgBufSetup();
			PackMsgBufLen();
			fCommitAllMW();
			RefreshDispAfter(0);
			break;

		case 6:
			ReportComerciosVirtuales(); //multimerchant
			RefreshDispAfter(0);
			break;

		default:
			break;
		}
	}
}

//*****************************************************************************
//  Function        : MenuImpreReferencia
//  Description     : Despliega Menu para imprimir reportes .
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

void MenuImpreReferencia(void) //kt-140912
{

	while (1)
	{
		int select = 0;

		select = MenuSelectVisa(&KImprRefMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			//ReprintLast();
			SearchLastTrans(TRUE);
			break;

		case 2:
			//ReprintTxn(FALSE, FALSE); //MFBC/21/02/
			SearchLastTrans(FALSE);
			break;

		default:
			break;
		}
	}
}

static const struct MENU_ITEM KAdminItems[] =
{
		{ 1, "Duplicado" },
		{ 2, "Ult Trx Offline" },
		{ 3, "Reportes" },
		{ 4, "Inicializacion" },
		{ 5, "Logon" },
		{ 6, "Echo Test" },
		{ 7, "POS ECR" },
		{ 8, "Pruebas" },
		{ 9, "Referencia" },
		{ 10, "Password" },
		{ -1, NULL }, };

//*****************************************************************************
//  Function        : MenuAministrativo
//  Description     : Despliega Menu Administrativo.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

void MenuAdministrativo(void)
{

	int idxPrint = 0;
	int idxMaster = 0;
	static struct MENU_ITEM_DOS KAppsItems[20];
	static struct MENU_DAT_DOS KAppsMenu[1];
	struct TABLA_CERO tabla_Cero; //MFBC/02/05/13
	GetTablaCero(&tabla_Cero);

	memset(&KAppsItems, 0x00, 20);

	//printf("\f confi <%c>", tabla_Cero.b_dir_ciudad[43] ); WaitKey(3000, 0);
	while (TRUE)
	{
		if (KAdminItems[idxMaster].iID == 7 &&  tabla_Cero.b_dir_ciudad[43] != '1' &&  tabla_Cero.b_dir_ciudad[43] != '3' )
			idxMaster++;


		if (KAdminItems[idxMaster].pcDesc == NULL)
			break;
		KAppsItems[idxPrint].iID = idxPrint + 1;
		KAppsItems[idxPrint].pcDesc = NULL;
		KAppsItems[idxPrint].iDX = KAdminItems[idxMaster].iID;
		memset(KAppsItems[idxPrint].scDescDat, 0x20, sizeof(KAppsItems[idxPrint].scDescDat));
		memcpy(KAppsItems[idxPrint].scDescDat, KAdminItems[idxMaster].pcDesc,	strlen(KAdminItems[idxMaster].pcDesc));
		idxPrint++;
		idxMaster++;
	}

	KAppsItems[idxPrint].iID = -1;
	KAppsItems[idxPrint].pcDesc = NULL;
	memcpy(KAppsMenu[0].scHeader, "ADMINISTRATIVO", 14);
	KAppsMenu[0].psMenuItem = KAppsItems;




	while (1)
	{
		os_beep_close();
		int select = 0;

		select = MenuDinamico(&KAppsMenu[0], select);

		if (select == -1)
		{
			IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
			return;
		}


		switch (select)
		{
		case 1:
			MenuImpreReferencia(); //kt-140912 //Duplicado
			break;

		case 2:
			if (!PackReportEMV()) //kt-240413 //Ult Trx Offline
			{
				LongBeep();
				TextColor("No hay Declinada", MW_LINE5, COLOR_RED,MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
				TextColor("Offline", MW_LINE6, COLOR_RED, MW_SMFONT | MW_CENTER,
						3);
				os_beep_close();
			}
			break;

		case 3:
			MenuReportes(); //kt-140912 //Reportes
			break;

		case 4:

			if(TransPassAdmin(MW_LINE2) == FALSE)
				return;

			InitTrans(FALSE);  //Inicializacion
			break;

		case 5:
			LogonTrans(FALSE); //kt-301012 //Logon
			break;

		case 6:
			TestTrans(); //kt-140912 //Echo Test
			break;

		case 7:
			if (tabla_Cero.b_dir_ciudad[43] == '1'  || tabla_Cero.b_dir_ciudad[43] == '3'){ // Si modo caja esta habilitado por inicializacion
				TextColor("TECNICO", MW_LINE1, COLOR_BLACK, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);		// **SR** 07/09/13
				if(passAdminComercio(MW_LINE2) != TRUE)			// Require contraseņa para poder ingresar **SR**/20/08/13
					return;
				configurarEcrPos();
			}
			break;

		case 8:
			Pruebas(); //Pruebas
			break;

		case 9:
			configReferencia();//Referencia
			break;

		case 10:
			cambioSuperPass(); //password
			break;

		}
	}

	RefreshDispAfter(0);
	ResetTerm();
	return;
}

void  habilitarModoCaja(void)  // Se modifica completamente la funcion **SR** 10/10/2013
{
	BYTE b_Option = 0x00;
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	int i = 0;
	WORD NumItem =  4;		// Cantidad de Item que se encuentran dentro el menu		**SR** 15/10/13
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	TextColor("      POS  ECR      ",MW_LINE1, COLOR_BLACK, MW_CLRDISP | MW_REVERSE | MW_LEFT | MW_SMFONT, 0);

	while(!((b_Option == 0x01 || b_Option == 0x02) && i > NumItem - 1)){

		DispClrBelowMW(MW_LINE2);
		DispSelecPage(i + 1, NumItem + 1, MW_LINE9);

		if(i == 0){
			TextColor("Habilitar Caja ?",MW_LINE3, COLOR_VISABLUE,  MW_LEFT | MW_SMFONT, 0);
			TextColor("ACTUAL: ", MW_LINE5, COLOR_VISABLUE, MW_LEFT | MW_SMFONT, 0);
			if (gConfigComercio.flagCaja == TRUE)
				TextColor("SI", MW_LINE5 + 8, COLOR_GREEN,  MW_LEFT | MW_SMFONT, 0);
			else
				TextColor("NO", MW_LINE5 + 8, COLOR_GREEN, MW_LEFT | MW_SMFONT, 0);
			displaySI_NOLine(MW_LINE8);
		}
		else if(i == 1){
			TextColor("COM POS ECR",MW_LINE3, COLOR_VISABLUE,  MW_LEFT | MW_SMFONT, 0);
			TextColor("ACTUAL: ", MW_LINE5, COLOR_VISABLUE, MW_LEFT | MW_SMFONT, 0);
			if (gConfigComercio.modoCajaTCP == TRUE)
				TextColor("LAN", MW_LINE5 + 8, COLOR_GREEN, MW_LEFT | MW_SMFONT, 0);
			else
				TextColor("COM1", MW_LINE5 + 8, COLOR_GREEN, MW_LEFT | MW_SMFONT, 0);
			displayTCP_RS232();
		}
		else if(i == 2){
			TextColor("Enviar Hora Caja:",MW_LINE3, COLOR_VISABLUE,  MW_LEFT | MW_SMFONT, 0);
			TextColor("ACTUAL: ", MW_LINE5, COLOR_VISABLUE, MW_LEFT | MW_SMFONT, 0);
			if (gConfigComercio.EnviarHoraCaja == TRUE)
				TextColor("SI", MW_LINE5 + 8, COLOR_GREEN,  MW_LEFT | MW_SMFONT, 0);
			else
				TextColor("NO", MW_LINE5 + 8, COLOR_GREEN, MW_LEFT | MW_SMFONT, 0);
			displaySI_NOLine(MW_LINE8);
		}
		b_Option = 0x00;

		while(TRUE){
			switch (WaitKey(2000,0)) {
			case MWKEY_ENTER:
				b_Option = 0x01;
				i ++;
				break;
			case MWKEY_CLR:
				b_Option = 0x02;
				i ++;
				break;
			case MWKEY_LEFT:
				i -- ;
				break;
			case MWKEY_RIGHT:
				i ++;
				if(i >= NumItem)
					i = NumItem - 1;
				break;
			case MWKEY_CANCL:
				FreeMW(conf_comer);
				return;
				break;
			default:
				continue;
				break;
			}

			if(i<0 ){
				i = 0;
				continue;
			}
			else if(i>NumItem)
				continue;

			break;
		}

		if(b_Option == 0x00)
			continue;

		DispClrBelowMW(MW_LINE2);

		if(i == 1){
			TextColor("Modo Caja", MW_LINE3,COLOR_VISABLUE,  MW_SMFONT | MW_CENTER, 0);

			if(b_Option == 0x01)
				((struct CONFIG_COMERCIO*) conf_comer)->flagCaja = TRUE;
			if(b_Option == 0x02)
				((struct CONFIG_COMERCIO*) conf_comer)->flagCaja = FALSE;


		}
		else if(i == 3){
			TextColor("Enviar Hora Caja", MW_LINE3, COLOR_VISABLUE,  MW_SMFONT | MW_CENTER, 0);
			if(b_Option == 0x01)
				((struct CONFIG_COMERCIO*) conf_comer)->EnviarHoraCaja = TRUE;
			if(b_Option == 0x02)
				((struct CONFIG_COMERCIO*) conf_comer)->EnviarHoraCaja = FALSE;

		}
		if(i == 2){

			if(b_Option == 0x01){
				((struct CONFIG_COMERCIO*) conf_comer)->modoCajaTCP = TRUE;
				TextColor("LAN", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
			}
			else if (b_Option == 0x02){
				((struct CONFIG_COMERCIO*) conf_comer)->modoCajaTCP = FALSE;
				TextColor("COM1", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
			}

		}

		if(b_Option == 0x01 || i == 2)
			TextColor("Habilitado!!", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
		else if (b_Option == 0x02)
			TextColor("Deshabilitado!!", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);


		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);

	}

	FreeMW(conf_comer);
	return;
}

void configurarEcrPos(void)		// Se cambia totalmente la funcion  **SR** 17/10/13
{
	habilitarModoCaja();
	return;
}


void Pruebas(void)
{
	int select = 0;
	while (1)
	{
		select = 0;
		select = MenuSelectVisa(&KPruebasMenu, select);

		if (select == -1)
			return;

		switch (select)
		{

		case 1:
			TMLptTest();
			break;

		case 2:
			KeyPressTest();
			break;

		case 3:
			version();
			break;

		default:
			break;
		}
		ResetTerm();
	}

}

void version(void)
{
	struct MW_APPL_INFO applicationInfo;
	BYTE V_EVM[2 + 1];
	BYTE Terminal[8 + 1];
	DWORD key = 0;
	struct TABLA_CUATRO tabla_cuatro;
	gIdTabla4 = OpenFile(KTabla4File);

	memset(V_EVM, 0x00, sizeof(V_EVM));
	memset(Terminal, 0x00, sizeof(Terminal));
	AppInfoGetMW(MY_EMV_ID, &applicationInfo);
	sprintf(V_EVM, "%02x", applicationInfo.b_version);
	GetTablaCuatro(0, &tabla_cuatro);
	memcpy(Terminal, gTablaCuatro.b_cod_terminal, 8);
	DispLineMW("VERSION", MW_LINE1,
			MW_REVERSE | MW_CLRDISP | MW_CENTER | MW_BIGFONT); //MFBC/28/02/13
	TextColor("APP: ", MW_LINE3, COLOR_VISABLUE, MW_LEFT | MW_BIGFONT, 0);
	TextColor(VERCB, MW_LINE3 + 6, COLOR_VISABLUE, MW_LEFT | MW_BIGFONT, 0);
	TextColor("TER: ", MW_LINE5, COLOR_VISABLUE, MW_LEFT | MW_BIGFONT, 0);
	TextColor(Terminal, MW_LINE5 + 6, COLOR_VISABLUE, MW_LEFT | MW_BIGFONT, 0);
	TextColor("EMV: ", MW_LINE7, COLOR_VISABLUE, MW_LEFT | MW_BIGFONT, 0);
	TextColor(V_EVM, MW_LINE7 + 6, COLOR_VISABLUE, MW_LEFT | MW_BIGFONT, 0);

	while (1)
	{
		SleepMW();
		key = APM_WaitKey(3000, 0);
		if (key != MWKEY_CANCL && key != -1)
			continue;
		else
			break;
	}

	CloseFile(gIdTabla4);
}

static const struct MENU_ITEM KOperativoMaster[] =
{
		{ 1, "ANULACION" },
		{ 2, "CIERRE" },
		{ -1, NULL }, };

int menuOperativoMaster(void)
{
	//	int maxItems = 0;
	int idxPrint = 0;
	int idxMaster = 0;
	int select = 0;
	//maxItems =	sizeof(KOperativoMaster);
	static struct MENU_ITEM_DOS KAppsItems[MAX_MENU_ITEMS];
	static struct MENU_DAT_DOS KAppsMenu[1];
	struct TABLA_CERO tabla_Cero; //MFBC/02/05/13
	GetTablaCero(&tabla_Cero);

	memset(&KAppsItems, 0x00, sizeof(struct MENU_ITEM_DOS) * MAX_MENU_ITEMS);

	while (TRUE)    
	{
		//		if (KOperativoMaster[idxMaster].iID == 1 )
		//			idxMaster++;
		//
		//		if (KOperativoMaster[idxMaster].iID == 2 )
		//			idxMaster++;

		if (KOperativoMaster[idxMaster].pcDesc == NULL)
			break;
		KAppsItems[idxPrint].iID = idxPrint + 1;
		KAppsItems[idxPrint].pcDesc = NULL;
		KAppsItems[idxPrint].iDX = KOperativoMaster[idxMaster].iID;
		memset(KAppsItems[idxPrint].scDescDat, 0x20, sizeof(KAppsItems[idxPrint].scDescDat));
		memcpy(KAppsItems[idxPrint].scDescDat, KOperativoMaster[idxMaster].pcDesc,	strlen(KOperativoMaster[idxMaster].pcDesc));
		idxPrint++;
		idxMaster++;
	}

	KAppsItems[idxPrint].iID = -1;
	KAppsItems[idxPrint].pcDesc = NULL;
	memcpy(KAppsMenu[0].scHeader, "OPERATIVO", 9);
	KAppsMenu[0].psMenuItem = KAppsItems;

	BYTE direccion[39 + 1];
	BYTE nombre[23 + 1];
	memset (nombre, 0x00, sizeof(nombre));
	memset (direccion, 0x00, sizeof(direccion));
	memcpy(direccion, gTablaCero.b_dir_ciudad, 37);
	memcpy(nombre, gTablaCero.b_nom_establ, 23);

	while (TRUE)
	{
		os_beep_close();
		select = 0;
		select = MenuDinamico(&KAppsMenu[0], select);
		if (select == -1)
		{
			IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
			return -1;
		}

		switch (select)
		{
//		case 1:
//			if(!Cajas())
//			{			// Si se encuentra integrado a cajas el proceso de seleccion de comercion lo realiza mas adelante **SR** 18/12/13
//				if (!VirtualMerchantVisa()){ //kt-180413
//					ResetTerm();
//					return 0;
//				}
//			}
//			//			if(ValidCard())
//			INPUT.b_trans = SALE_ICC;
//			if(!GetCard(0, FALSE, INPUT.b_trans, 1))
//				break;
//			CompraTrans(INPUT.b_entry_mode, SALE_SWIPE);
//			break;

		case 1:
			flagAnul = TRUE; //LFGD 03/13/2013
			if( VoidTransVisa(-1, VOID) == 1) //Anulacion
			{
				flagAnul = FALSE;
				RefreshDispAfter(0);
				ResetTerm();
				return 1;
			}
			flagAnul = FALSE;
			break;
		case 2:
			SettleTransVisa(FALSE);
			RefreshDispAfter(0);
			ResetTerm();
			break;

		default:
			break;

		}
		RefreshDispAfter(0);
		ResetTerm();
	}

	RefreshDispAfter(0);
	ResetTerm();
	return 0;
}

//NM-29/04/13 Agregue esta funcion
DWORD menuOperRecarga(void)
{

	WORD max_reg = 0;
	WORD contador = 0;
	int select = 0, i;
	//BYTE idOper[2 + 1];

	static struct MENU_ITEM_DOS KAppsItems[MAX_MENU_ITEMS];
	static struct MENU_DAT_DOS KAppsMenu[1];
	struct TABLA_SEIS p_tabla6;

	max_reg = GetTabla6Count();
	//printf("\freg: |%d|", max_reg);APM_WaitKey(3000,0);

	gIdTabla6 = OpenFile(KTabla6File);

	for (i = 0; i < max_reg; i++)
	{
		memset(&p_tabla6, 0x00, sizeof(struct TABLA_SEIS));
		GetTablaSeis(i, &p_tabla6);
		if (p_tabla6.b_tel[0] == 'T')
			continue;

		KAppsItems[contador].iID = contador + 1;
		memset(KAppsItems[contador].scDescDat, 0x20, sizeof(KAppsItems[contador].scDescDat));
		memcpy(KAppsItems[contador].scDescDat, &p_tabla6.b_descripcion[0], 20);
		//		memset(idOper, 0x00, sizeof(idOper));
		//		memcpy(idOper, &p_tabla6.b_cod_prepago[2], 2);
		KAppsItems[contador].iDX = i;
		KAppsItems[contador].pcDesc = NULL;
		contador++;
	}

	KAppsItems[contador].iID = -1;
	KAppsItems[contador].pcDesc = NULL;
	memcpy(KAppsMenu[0].scHeader, "OPERADOR CELULAR", 16);
	KAppsMenu[0].psMenuItem = KAppsItems;

	select = MenuDinamico(&KAppsMenu[0], select);

	if (select == -1)
	{
		CloseFile(gIdTabla6);
		return -1;
	}
	else
	{
		GetTablaSeis((select), &p_tabla6);
		CloseFile(gIdTabla6);
		return select;
	}

}

void menuTransEspeciales (void)
{
	int select;

	while (TRUE)
	{
		select = 0;
		select = MenuSelectVisa(&KTrEspecialesMenu, select);
		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			//			avanceSuperCupo();
			break;

		case 2:
			superCupoFalabella(); //MFBC/02/09/13 supercupo falabella
			break;
		}


	}

}
