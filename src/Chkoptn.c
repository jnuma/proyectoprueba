//-----------------------------------------------------------------------------
//  File          : chkoptn.c
//  Module        :
//  Description   : Include routines to check Applicaiton Config OPTION.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "util.h"
#include "corevar.h"
#include "chkoptn.h"
#include "files.h"
#include "tranutil.h"
#include "key2dll.h"
#include "Constant.h"
#include "sale.h"
#include "menu.h"

//*****************************************************************************
//  Function        : AdjustPSWReqd
//  Description     : Check whether password requiered for adjust transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)

BOOLEAN AdjustPSWReqd(void)
{
	return (STIS_TERM_CFG.b_local_option & 0x08);
}
//*****************************************************************************
//  Function        : SettlePSWReqd
//  Description     : Check whether password requiered for settlement.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN SettlePSWReqd(void)
{
	return (STIS_TERM_CFG.b_local_option & 0x10);
}
//*****************************************************************************
//  Function        : ManualPSWReqd
//  Description     : Check whether password requiered for manual entry.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ManualPSWReqd(void)
{
	return (STIS_TERM_CFG.b_local_option & 0x20);
}
//*****************************************************************************
//  Function        : DispMSRReqd
//  Description     : Check whether config show MSR data.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
BOOLEAN DispMSRReqd(void)
{
	return (STIS_TERM_CFG.sb_options[0] & 0x02);
}
//*****************************************************************************
//  Function        : TIPsReqd
//  Description     : Check whether config for TIPS processing.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
BOOLEAN TIPsReqd(void)
{
	return (STIS_TERM_CFG.sb_options[0] & 0x04);
}
//*****************************************************************************
//  Function        : PrintTimeReqd
//  Description     : Check whether config for print date/time on receipt.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN PrintTimeReqd(void)
{
	return (STIS_TERM_CFG.sb_options[0] & 0x20);
}
//*****************************************************************************
//  Function        : DDMMBusinessDate
//  Description     : Check whether config to use DDMM business date format.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
BOOLEAN DDMMBusinessDate(void)
{
	return (STIS_TERM_CFG.sb_options[0] & 0x40);
}
//*****************************************************************************
//  Function        : ComfirmTotal
//  Description     : Check whether config require user confirm totals.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
BOOLEAN ComfirmTotal(void)
{
	return (STIS_TERM_CFG.sb_options[0] & 0x80);
}

//*****************************************************************************
//  Function        : PromptTips
//  Description     : Check whether config for Prompt tips entry.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
BOOLEAN PromptTips(void)
{
	return (STIS_TERM_CFG.sb_reserved[1] & 0x02);
}
//*****************************************************************************
//  Function        : BlockLocalTrans
//  Description     : Check for disable local initialize transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : Not allow message is display when TRUE.
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN BlockLocalTrans(void)
{
	return (STIS_TERM_CFG.sb_reserved[1] & 0x04);
}
//*****************************************************************************
//  Function        : NumOfRcptCopy
//  Description     : Retrieve the number of receipt copy config.
//  Input           : N/A
//  Return          : num of copy.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BYTE NumOfRcptCopy(void)
{
	return (STIS_TERM_CFG.sb_reserved[4] >> 4);
}
//*****************************************************************************
//  Function        : ReferralDial
//  Description     : Check if auto dial for referral.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ReferralDial(void)
{
	return (STIS_TERM_CFG.b_dial_option & 0x10);
}

//*****************************************************************************
//  Function        : RefundOffline
//  Description     : Check whether config REFUND process offline.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN RefundOffline(void)
{
	return (STIS_ACQ_TBL(0).sb_options[0] & 0x10);
}
//*****************************************************************************
//  Function        : VoidOffline
//  Description     : Check whether config VOID process offline.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN VoidOffline(void)
{
	return (STIS_ACQ_TBL(0).sb_options[0] & 0x20);
}
//*****************************************************************************
//  Function        : PiggybackOffline
//  Description     : Check whether config for piggyback offline transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN PiggybackOffline(void)
{
	return (STIS_ACQ_TBL(0).sb_options[0] & 0x80);
}
//*****************************************************************************
//  Function        : MultiAdjustAllowed
//  Description     : Check Multiple adjustment allowed.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN MultiAdjustAllowed(void)
{
	return (STIS_ACQ_TBL(0).sb_reserved[3] & 0x80);
}
//*****************************************************************************
//  Function        : ExtraMsgLen
//  Description     : Check config for extra message len byte in host msg.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ExtraMsgLen(void)
{
	return (STIS_ACQ_TBL(0).b_reserved1 ? TRUE : FALSE);
}
//*****************************************************************************
//  Function        : CheckDuplicate
//  Description     : Check Duplicate transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN CheckDuplicate(void)
{
	return (STIS_ACQ_TBL(0).sb_reserved[11] & 0x20);
}
//*****************************************************************************
//  Function        : FallbackDialup
//  Description     : Config. for TCP connection fallback dialup.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN FallbackDialup(void)
{
	return (STIS_ACQ_TBL(0).sb_reserved[11] & 0x40);
}
//*****************************************************************************
//  Function        : GetHostType
//  Description     : Return Current Host Type.
//  Input           : aIdx;     // Acquirer Idx
//  Return          : host type.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BYTE GetHostType(BYTE aIdx)
{
	return (STIS_ACQ_TBL(aIdx).b_host_type);
}
#endif

//*****************************************************************************
//  Function        : PromptTips
//  Description     : Check whether config for Prompt tips entry.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
BOOLEAN EcrRefReqd(void)
{
	return (STIS_TERM_CFG.sb_options[1] & 0x04);
}

//*****************************************************************************
//  Function        : Prompt4DBC
//  Description     : Check whether config for 4DBC entry.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN Prompt4DBC(void)
{
	return (STIS_ACQ_TBL(0).sb_reserved[0] & 0x10);
}

//*****************************************************************************
//  Function        : HostSettlePending
//  Description     : Check whether settle settlement is require for
//                    current acquirer;
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : Error message is display when TRUE.
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN HostSettlePending(void)
{
	return (STIS_ACQ_TBL(0).b_pending & SETTLE_PENDING);
}
//*****************************************************************************
//  Function        : UnderFloorLimit
//  Description     : Check input aAmount is under issuer's floor limit.
//  Input           : 64 bit amount.
//  Return          : TRUE/FALSE;
//                    FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UnderFloorLimit(DDWORD aAmount)
{
	DDWORD limit;

	limit = (bcd2val(STIS_ISS_TBL(0).sb_floor_limit[0]) * 100 + bcd2val(STIS_ISS_TBL(0).sb_floor_limit[1]));
	limit *= 100;
	return (aAmount < limit);
}
//*****************************************************************************
//  Function        : ManualEntryAllow
//  Description     : Check whether config for manual entry.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ManualEntryAllow(void)
{
	return (STIS_ISS_TBL(0).sb_options[0] & 0x04);
}
//*****************************************************************************
//  Function        : OfflineAllowed
//  Description     : Check whether config for OFFLINE entry.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN OfflineAllowed(void)
{
	return (STIS_ISS_TBL(0).sb_options[0] & 0x10);
}
//*****************************************************************************
//  Function        : ExpDateReqd
//  Description     : Check whether config require expiry date.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ExpDateReqd(void)
{
	return (STIS_ISS_TBL(0).sb_options[0] & 0x08);
}
//*****************************************************************************
//  Function        : VoiceReferral
//  Description     : Check whether Voice referral required.
//  Input           : host response code
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN VoiceReferral(WORD aRspCode)
{
	return ((aRspCode <= '0' * 256 + '2') && (STIS_ISS_TBL(0).sb_options[0] & 0x20));
}
//*****************************************************************************
//  Function        : DescriptorReqd
//  Description     : Check whether config require product code.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN DescriptorReqd(void)
{
	return (STIS_ISS_TBL(0).sb_options[0] & 0x40);
}
//*****************************************************************************
//  Function        : AdjustAllowed
//  Description     : Check whether config allow adjustment.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN AdjustAllowed(void)
{
	return (STIS_ISS_TBL(0).sb_options[0] & 0x80);
}
//*****************************************************************************
//  Function        : CheckPAN
//  Description     : Check whether config require check PAN.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN CheckPAN(void)
{
	return (STIS_ISS_TBL(0).sb_options[1] & 0x01);
}
//*****************************************************************************
//  Function        : PrintReqd
//  Description     : Check whether config require print receipt.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN PrintReqd(void)
{
	return (STIS_ISS_TBL(0).sb_options[1] & 0x04);
}
//*****************************************************************************
//  Function        : DefaultCapture
//  Description     : Check whether config default as capture transaction(sale)
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN DefaultCapture(void)
{
	return (STIS_ISS_TBL(0).sb_options[1] & 0x08);
}
//*****************************************************************************
//  Function        : CheckExpDate
//  Description     : Check whether config require check expiry date.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN CheckExpDate(void)
{
	return (STIS_ISS_TBL(0).sb_options[1] & 0x10);
}
//*****************************************************************************
//  Function        : RefundBlocked
//  Description     : Check whether config BLOCK refund transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN RefundBlocked(void)
{
	return (STIS_ISS_TBL(0).sb_options[1] & 0x40);
}
//*****************************************************************************
//  Function        : AuthBlocked
//  Description     : Check whether config BLOCK non default AUTH transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN AuthBlocked(void)
{
	return (STIS_ISS_TBL(0).sb_options[1] & 0x80);
}
//*****************************************************************************
//  Function        : VoidBlocked
//  Description     : Check whether config BLOCK VOID transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN VoidBlocked(void)
{
	return (STIS_ISS_TBL(0).sb_options[2] & 0x01);
}
//*****************************************************************************
//  Function        : BlockAuthCode
//  Description     : Check whether config BLOCK authorization code.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN BlockAuthCode(void)
{
	return (STIS_ISS_TBL(0).sb_options[2] & 0x08);
}
//*****************************************************************************
//  Function        : MaskCardNo
//  Description     : Check whether config for mask PAN.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN MaskCardNo(void)
{
	return (STIS_ISS_TBL(0).sb_reserved[0] & 0x01);
}
//*****************************************************************************
//  Function        : ExpiryReject
//  Description     : Check for reject trans for expired card.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
BOOLEAN ExpiryReject(void)
{
	return (STIS_ISS_TBL(0).sb_reserved[1] & 0x10);
}
//*****************************************************************************
//  Function        : BlockLocalManual
//  Description     : Block Local Manual Keyin transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN BlockLocalManual(void)
{
	return (STIS_ISS_TBL(0).sb_reserved[0] & 0x40);
}
//*****************************************************************************
//  Function        : BlockLocalSwipe
//  Description     : Block Local SWIPE transaction.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN BlockLocalSwipe(void)
{
	return (STIS_ISS_TBL(0).sb_reserved[0] & 0x20);
}
//*****************************************************************************
//  Function        : Track1Enable
//  Description     : Check for Track 1 enable parameter.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN Track1Enable(void)
{
	return (STIS_ISS_TBL(0).sb_reserved[1] & 0x20);
}
//*****************************************************************************
//  Function        : EMVProcDisable
//  Description     : Check whether EMV process is disable.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN EMVProcDisable(void)
{
	return (STIS_ISS_TBL(0).sb_reserved[3] & 0x02);
}



//====================== OPCIONES INICIALIZACION VISA=====================
// OPT 1

BOOLEAN MontoDoble(void)
{
	return (gTablaCero.b_opt1 & 0x01);
}

BOOLEAN MostrarTrk2(void)
{
	return (gTablaCero.b_opt1 & 0x02);
}

BOOLEAN Propina(void)
{
	return (gTablaCero.b_opt1 & 0x04);
}

BOOLEAN MetodoReconc(void)
{
	return (gTablaCero.b_opt1 & 0x80);
}

// OPT 2
BOOLEAN UsoImpresora(void)
{
	return (gTablaCero.b_opt2 & 0x02);
}

BOOLEAN CedCajero(void)
{
	return (gTablaCero.b_opt2 & 0x02);
}

BOOLEAN DevOffline(void)
{
	return (gTablaCero.b_opt2 & 0x10);
}

BOOLEAN CancelOffline(void)
{
	return (gTablaCero.b_opt2 & 0x20);
}

BOOLEAN PreImpresion(void)
{
	return (gTablaCero.b_opt2 & 0x40);
}

// OPT 3

// OPT 4
BOOLEAN BaseIva(void) // Base Iva No mayor a la compra
{
	return (gTablaCero.b_opt4 & 0x80);
}

// OPCIONES LOCALES
BOOLEAN BloqueoTeclado(void)
{
	return (gTablaCero.b_opt_local & 0x01);
}

BOOLEAN ClaveDev(void)
{
	return (gTablaCero.b_opt_local & 0x04);
}

BOOLEAN ClaveAjus(void)
{
	return (gTablaCero.b_opt_local & 0x08);
}

BOOLEAN ClaveRep(void)
{
	return (gTablaCero.b_opt_local & 0x10);
}

BOOLEAN IvaAuto(void)
{
	return (gTablaCero.b_opt_local & 0x40);
}

// TRANSACCION POR DEFECTO
BYTE TransDefault(void)
{
	return (gTablaCero.b_trans_default);
}

BOOLEAN ClaveAnulacion(void) //kt
{
	return (gTablaCero.b_opt_local & 0x02);
}

// ----------------------------------------

BOOLEAN PinCredito(void)
{
	return (gTablaTres.b_opt1 & 0x02);
}

BOOLEAN PinDebito(void)
{
	return (gTablaTres.b_opt3 & 0x04);
}

BOOLEAN GetUlt4Digitos(void)
{
	return (gTablaTres.b_opt4 & 0x02);
}

BOOLEAN BaseIvaAutomatico(void)
{
	return (gTablaCero.b_opt_tel & 0x04);
}

BOOLEAN GetBaseAmount(void)
{
	return (gTablaCero.b_opt_tel & 0x01);
}

BOOLEAN IvaAutomatico(void)
{
	return (gTablaCero.b_opt_local & 0x40);
}

BOOLEAN CheckDigit(void)
{
	return (gTablaTres.b_opt2 & 0x01);
}

//kt-060912

/*** OPT TABLA CERO  ***/

BOOLEAN MostrarPan(void)
{
	return (gTablaCero.b_opt1 & 0x02);
}

BOOLEAN CedulaCajero(void)
{
	return (gTablaCero.b_opt2 & 0x04); //Se cambio a 04 por orden de credibanco antes 02
}

BOOLEAN superCupo (void)
{
	if ( (memcmp(&gTablaCero.b_dir_ciudad[39], "\x37", 1) == 0) || (memcmp(&gTablaCero.b_dir_ciudad[39], "\x38", 1) == 0)
			|| (memcmp(&gTablaCero.b_dir_ciudad[39], "\x34", 1) == 0) )
		return TRUE;
	else
		return FALSE;
}

BOOLEAN referencia (void)
{
	if ( (memcmp(&gTablaCero.b_dir_ciudad[39], "\x37", 1) == 0) || (memcmp(&gTablaCero.b_dir_ciudad[39], "\x39", 1) == 0)
			|| (memcmp(&gTablaCero.b_dir_ciudad[39], "\x36", 1) == 0) )
		return TRUE;
	else
		return FALSE;
}


BOOLEAN moduloDCC (void)
{
	if ( (memcmp(&gTablaCero.b_dir_ciudad[39], "\x34", 1) == 0) || (memcmp(&gTablaCero.b_dir_ciudad[39], "\x35", 1) == 0)
			|| (memcmp(&gTablaCero.b_dir_ciudad[39], "\x36", 1) == 0) )
		return TRUE;
	else
		return FALSE;
}

/*** OPT TABLA DOS  ***/

BOOLEAN NoServiceCode(void)
{
	BYTE *p_tabla2 = (void *) MallocMW(sizeof(struct TABLA_DOS));
	gIdTabla2 = -1;
	Delay10ms(5);
	gIdTabla2 = OpenFile(KTabla2File);

	GetTablaDos(INPUT.w_card_idx, (struct TABLA_DOS*) p_tabla2);
	CloseFile(gIdTabla2);
	FreeMW(p_tabla2);

	return (gTablaDos.b_opt1 & 0x04);
}


BOOLEAN IngresarCedula(void)
{
	BYTE *p_tabla2 = (void *) MallocMW(sizeof(struct TABLA_DOS));
	gIdTabla2 = OpenFile(KTabla2File);

	GetTablaDos(INPUT.w_card_idx, (struct TABLA_DOS*) p_tabla2);
	CloseFile(gIdTabla2);
	FreeMW(p_tabla2);

	return (gTablaDos.b_opt1 & 0x01);
}

BOOLEAN MostrarCreRotativo(void)
{
	BYTE *p_tabla2 = (void *) MallocMW(sizeof(struct TABLA_DOS));

	gIdTabla2 = OpenFile(KTabla2File);

	GetTablaDos(INPUT.w_card_idx, (struct TABLA_DOS*) p_tabla2);
	CloseFile(gIdTabla2);
	FreeMW(p_tabla2);

	return (gTablaDos.b_opt1 & 0x40);
}

/*** OPT TABLA TRES  ***/

BOOLEAN SeleccionCuenta(void)
{
	return (gTablaTres.b_opt1 & 0x01);
}

BOOLEAN FechaExpira(void)
{
	return (gTablaTres.b_opt1 & 0x08);
}

BOOLEAN VerificaFechaExpira(void)
{
	return (gTablaTres.b_opt2 & 0x10);
}

BOOLEAN ImprimaRecibo(void)
{
	return (gTablaTres.b_opt2 & 0x04);
}

BOOLEAN RequiereCuotas(void)
{
	return (gTablaTres.b_opt3 & 0x08);
}

BOOLEAN ImprimirEl(void)
{
	return (gTablaTres.b_opt3 & 0x40);
}

BOOLEAN CashBack(void)
{
	return (gTablaTres.b_opt3 & 0x02);
}

BOOLEAN NumFactura(void)				//kt-081012
{
	return (gTablaCero.b_opt1 & 0x20);
}



//fin-kt

DWORD LeerBinTipo (void) //MFBC/02/12/12
{
	BYTE fileName[10];
	BYTE *fdata = NULL;
	BYTE *ptr1;
	BYTE *ptr2;
	BYTE token[2];
	BYTE token2[2];
	BYTE binTemp[8+1];
	BYTE proCode[6+1];
	BYTE proCodeTemp[3+1];
	struct BIN_TIPO *ptrTipoBin = NULL;
	int fileSize = 0;
	int flen = 0;
	int i = 0;
	int indice = 0;
	int indice2 = 0;
	int indice3 = 0;
	int contBines = 0;
	int idFile = -1;

	memset(fileName, 0x00, sizeof(fileName));
	memset(token, 0x00, sizeof(token));
	memset(token2, 0x00, sizeof(token2));
	memset(binTemp, 0x00, sizeof(binTemp));
	memset(proCode, 0x00, sizeof(proCode));
	memset(proCodeTemp, 0x00, sizeof(proCodeTemp));

	if (INPUT.b_trans == VOID)
	{
		proCodeTemp[0] = 0x02;
		memcpy(	&proCodeTemp[1], &gOrg_rec.b_tipo_cuenta, 1);
		split(proCode, proCodeTemp, 3);
		memcpy(&INPUT.s_trk2buf, &gOrg_rec.s_trk2buf, sizeof(struct MW_TRK2BUF));
	}
	else
	{
		memcpy(&proCodeTemp[0], &KTransBitmap[INPUT.b_trans ].b_proc_code, 1);
		memcpy(&proCodeTemp[1], &INPUT.b_tipo_cuenta, 1);
		split(proCode, proCodeTemp, 3);

	}

	memcpy(&TX_DATA.sb_proc_code[0], &proCodeTemp[0], 3);

	strcpy(fileName, "FILETIPO");

	DispClrBelowMW(MW_LINE1);
	//fileSize = KDLL_KeyFileLen(fileName);
	idFile = fOpenMW(fileName);

	if (idFile < 0){
		fCloseMW(idFile);
		return BIN_NO_FILE;
	}

	fileSize = fLengthMW(idFile);

	if (fileSize <= 0)
	{
		fCloseMW(idFile);
		return BIN_NO_FILE;
	}

	fdata = (BYTE *) MallocMW(fileSize);

	if(fdata!= NULL)
	{
		fSeekMW(idFile, 0);
		flen = fReadMW(idFile, fdata, fileSize); // Jorge Numa

		if (flen)
		{
			for (i = 0; i <= flen; i++)
			{
				if (fdata[i] == 0x0A)
					contBines++;
			}
			//printf("\f Bines %d",contBines); WaitKey(3000, 0);
			ptrTipoBin= (struct BIN_TIPO *) MallocMW(sizeof(struct BIN_TIPO) * contBines);

			memset(ptrTipoBin, 0x00, (sizeof(struct BIN_TIPO) * contBines));
			token[0] = 0x0A;
			token2[0] = 0x3B;
			i = 0;
			while(indice < flen)
			{
				ptr1 = strtok(fdata + indice, token);
				indice += strlen(ptr1) + 1;
				indice3 = strlen(ptr1) + 1;
				indice2=0;
				ptr2 = strtok(ptr1 + indice2, token2);
				indice2+=strlen(ptr2) + 1;
				memcpy(ptrTipoBin[i].bines, ptr2, 8);
				ptr2 = strtok(ptr1 + indice2, token2);
				indice2+=strlen(ptr2) + 1;
				memcpy(ptrTipoBin[i].codTrans, ptr2, 6 );
				ptr2 = strtok(ptr1 + indice2, token2);
				indice2+=strlen(ptr2) + 1;
				memcpy(ptrTipoBin[i].costoAprobado, ptr2 , 6 );
				ptr2 = strtok(ptr1 + indice2, token2);
				indice2+=strlen(ptr2) + 1;
				memcpy(ptrTipoBin[i].costoNegado, ptr2 , 6 );
				i++;
			}
			i=0;
			//Comparo bines
			while(i<contBines)
			{
				memcpy(binTemp, ptrTipoBin[i].bines, 8);
				RTrim(binTemp, 0x20);

				if(memcmp(binTemp, &INPUT.s_trk2buf.sb_content[0], strlen(binTemp)) == 0)
				{
					//					printf("\f codeT %s \n", proCode); WaitKey(3000, 0);
					//					printf("codeAr  %d \n", ((struct APPDATA*) conf_AppData)->PortHost); WaitKey(3000, 0);

					if (memcmp(proCode, ptrTipoBin[i].codTrans, 4) == 0)
					{
						memcpy( &gBin_Tipo, &ptrTipoBin[i], sizeof(struct BIN_TIPO) );
						if( (memcmp(gBin_Tipo.costoAprobado, "NA", 2) == 0 ) || (memcmp(gBin_Tipo.costoNegado, "NA", 2) == 0 ) )
						{
							if(ConsultaCostoTrans() ){
								FreeMW(fdata);
								FreeMW(ptrTipoBin);
								fCloseMW(idFile);
								return BIN_ACP;
							}
							else{
								FreeMW(fdata);
								FreeMW(ptrTipoBin);
								fCloseMW(idFile);
								return BIN_FAIL;
							}
						}
						FreeMW(fdata);
						FreeMW(ptrTipoBin);
						fCloseMW(idFile);
						return BIN_ACP;
					}
				}
				i++;
			}

		}
		//		else
		//		{
		//			LongBeep();
		//			TextColor("No se ha cargado", MW_LINE4, COLOR_VISABLUE, MW_CENTER |MW_CLRDISP|MW_SMFONT , 0);
		//			TextColor("Archivo TIPO BIN", MW_LINE5, COLOR_VISABLUE, MW_CENTER |MW_SMFONT , 3);
		//		}

	}
	FreeMW(fdata);

	if (ptrTipoBin != NULL)
	{
		FreeMW(ptrTipoBin);
	}
	fCloseMW(idFile);
	return BIN_NO_FILE;
}


struct MENU_ITEM BancosItem[99];

struct MENU_DAT BancosMenu  = {
		"LISTA DE BANCOS",
		BancosItem,
};

struct MENU_ITEM Leyenda2Item[99];

struct MENU_DAT Leyenda2Menu  = {
		"LEYENDA 2",
		Leyenda2Item,
};

BOOLEAN fidelizacion_Internacional (void)
{
	BYTE fileName[22];
	BYTE PanAscii[23];
	int fileSize = 0;
	struct BINES_BONO *ptrBines = NULL;
	BYTE *fdata = NULL;
	int flen;
	int contBines = 0;
	int i;
	BYTE token[2];
	int indice = 0;
	BYTE *ptr1 = NULL;
	BYTE *ptr2 = NULL;
	int indice2;
	BYTE pipe;
	int lenRegistro;
	int idFile = -1;
	BOOLEAN find = FALSE;

	BYTE pan[3];


	memset(token, 0x00, sizeof(token));
	memset(fileName, 0x00, sizeof(fileName));
	memset( PanAscii, 0x00, sizeof(PanAscii) );


	memcpy(fileName, "FIDELINT", 8);


	idFile = fOpenMW(fileName);


	if (idFile < 0)
	{
		return TRUE;
	}


	DispClrBelowMW(MW_LINE1);
	fileSize = fLengthMW(idFile);


	if (fileSize <= 0) //MFBC/23/11/12
	{
		fCloseMW(idFile);
		return TRUE;
	}


	fdata = (BYTE *) MallocMW(fileSize);

	if (fdata != NULL)
	{
		fSeekMW(idFile, 0);
		flen = fReadMW(idFile, fdata, fileSize);

		if (flen > 0)
		{
			for (i = 0; i <= flen; i++)
			{
				if (fdata[i] == 0x0A)
					contBines++;
			}
			ptrBines = (struct BINES_BONO *) MallocMW(sizeof(struct BINES_BONO) * contBines );

			memset(ptrBines, 0x00, (sizeof(struct BINES_BONO) * contBines));
			token[0] = 0x0A;
			i = 0;
			while (indice < flen)
			{
				ptr1 = strtok(fdata + indice, token);
				indice += strlen(ptr1) + 1;
				lenRegistro = strlen(ptr1);
				indice2 = 0;
				pipe = 0;
				while (indice2 < lenRegistro)
				{
					ptr2 = strtok(ptr1 + indice2, "|");
					indice2 += strlen(ptr2) + 1;
					switch (pipe)
					{
					case 0:
						strcpy(ptrBines[i].binMenor, ptr2);
						break;
					case 1:
						strcpy(ptrBines[i].binMayor, ptr2);
						break;
					}
					pipe++;
				}
				i++;
			}
			memset(pan,0x00,sizeof(pan));
			split(pan,&INPUT.sb_pan[0],1);


			split(PanAscii, INPUT.sb_pan, 10);

			for (i = 0; i < contBines; i++)
			{
				if ((memcmp(PanAscii, ptrBines[i].binMenor, 10) >= 0)
						&& (memcmp(PanAscii, ptrBines[i].binMayor, 10) <= 0))
				{
					find = TRUE; // si encuentra los bines en el archivo retorna TRUE y no debe permitir continuar
					break;
				}
			}
		}
	}

	FreeMW(fdata);
	if (ptrBines != NULL)
	{
		FreeMW(ptrBines);
	}
	fCloseMW(idFile);
	return find;

}


BOOLEAN IsMulticomercio(void)
{
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));
	gIdTabla12 = -1;
	gIdTabla12 = OpenFile(KTabla12File);
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);

	if (((struct TABLA_12*) p_tabla12)->b_id <= 0)
	{
		CloseFile(gIdTabla12);
		FreeMW(p_tabla12);
		return FALSE;
	}

	CloseFile(gIdTabla12);
	FreeMW(p_tabla12);
	return TRUE;
}





