//-----------------------------------------------------------------------------
//  File          : reversal.c
//  Module        :
//  Description   : Include routines send reversal transaction to host.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include "util.h"
#include "sysutil.h"
#include "constant.h"
#include "corevar.h"
#include "chkoptn.h"
#include "tranutil.h"
#include "hostmsg.h"
#include "emvtrans.h"
#include "record.h"
#include "reversal.h"
#include "files.h"

//*****************************************************************************
//  Function        : ReversalOK
//  Description     : Send reversal transaction to host if available.
//  Input           : N/A
//  Return          : TRUE;     // sended;
//                    FALSE;    // send error.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ReversalOK(void)
{
	DWORD host_idx;

	host_idx = INPUT.w_host_idx;
	//  if (APM_GetPending(host_idx) != REV_PENDING)
	if (GetPendingVISA(host_idx) != REV_PENDING) //kt-130912
		return TRUE;

	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*)conf_AppData);
	APM_GetRevRec(host_idx, &RECORD_BUF, sizeof(RECORD_BUF));

	ByteCopy((BYTE *) &TX_DATA, (BYTE *) &RECORD_BUF, TX_DATA.sb_pin
			- &TX_DATA.b_trans); //kt-291112
	ByteCopy((BYTE *) &TX_DATA.s_icc_data, (BYTE *) &RECORD_BUF.s_icc_data,
			sizeof(struct ICC_DATA));
	memcpy(TX_DATA.sb_pin, RECORD_BUF.sb_pin, sizeof(RECORD_BUF.sb_pin));

	TX_DATA.b_org_trans = TX_DATA.b_trans; //MFBC/27/08/13  cambie la asinacion estaba despues del if

	if (TX_DATA.b_trans == VOID)
		TX_DATA.b_trans = EDC_REV_VOID; //kt-291112
	else
		TX_DATA.b_trans = EDC_REV; //kt-291112


	//IncTraceNo();
	// memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3);
	// memcpy(TX_DATA.sb_roc_no, INPUT.sb_roc_no, 3);

	PackHostMsg();

	ClearResponse();
	if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false)) == COMM_OK)
	{
		RSP_DATA.b_response = CheckHostRsp();

		if (RSP_DATA.b_response <= TRANS_REJ)
		{
			UpdateHostStatus(NO_PENDING);
			if (TX_DATA.b_trans != EDC_REV_VOID )
			{
				IncRocNo();
				memcpy(((struct APPDATA*) conf_AppData)->NumRoc,gAppDat.NumRoc,3);
			}
			//IncRocNo();
			((struct APPDATA*) conf_AppData)->reversal_count++;
			SaveDataFile((struct APPDATA*) conf_AppData);
			FreeMW(conf_AppData);
			return TRUE;

		}
		else{
			FreeMW(conf_AppData);
		}
	}

	return FALSE;
}

