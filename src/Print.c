//-----------------------------------------------------------------------------
//  File          : print.c
//  Module        :
//  Description   : Include print utility functions.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include "util.h"
#include "hardware.h"
#include "chkoptn.h"
#include "constant.h"
#include "logo.h"
#include "message.h"
#include "lptutil.h"
#include "print.h"
#include "files.h"
#include "tranutil.h"
#include "corevar.h"
#include "cajas.h"
#include "stdlib.h"
#include "func.h"
#include "coremain.h"   ////LFGD 02/14/2013
#include "ctltrans.h"
#include "sysutil.h"		//kt-180413
#include "sale.h"


BYTE g_TypeCount;			// variable exclusiva para los reportes de pantalla **SR**

//-----------------------------------------------------------------------------
// Print related
//-----------------------------------------------------------------------------
static char KLastRcptName[] =
{ "EDCReprn" };

//*****************************************************************************
//  Function        : PackLogo
//  Description     : Pack receipt logo.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackLogo(void)
{
	pack_nbyte(' ', ((MAX_CHAR_LINE_NORMAL - 23) / 2));
	pack_mem(STIS_TERM_CFG.sb_dflt_name, 23);
	pack_lf();
	pack_nbyte(' ', ((MAX_CHAR_LINE_NORMAL - 23) / 2));
	pack_mem(STIS_TERM_CFG.sb_name_loc, 23); /* line 2 */
	pack_lf();
	pack_nbyte(' ', ((MAX_CHAR_LINE_NORMAL - 23) / 2));
	pack_mem(&STIS_TERM_CFG.sb_name_loc[23], 23); /* line 3 */
	pack_lf();
}
//*****************************************************************************
//  Function        : PackDtg
//  Description     : Pack DATE/TIME on receipt.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void PackDtg(struct DATETIME *aDtg)
{
	BYTE tmp[18];

	ConvDateTime(tmp, aDtg, 1);
	pack_mem(tmp, 12);
	if (PrintTimeReqd())
		pack_mem(&tmp[12], 6);
	else
		pack_nbyte(' ', 6);
}
#endif
//*****************************************************************************
//  Function        : PackCurrentDate
//  Description     : Pack current date/time on receipt.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackCurrentDate(void)
{
	BYTE tmp[18];
	struct DATETIME dtg;

	pack_str("Date/Time:");
	pack_space(MAX_CHAR_LINE_NORMAL - 28);
	ReadRTC(&dtg);
	ConvDateTime(tmp, &dtg, 1);
	pack_mem(tmp, 18);
	pack_lf();
}
//*****************************************************************************
//  Function        : PackSeperator
//  Description     : Pack receipt seperator line.
//  Input           : aChar;        // character to be print.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackSeperator(BYTE aChar)
{
	pack_nbyte(aChar, MAX_CHAR_LINE_NORMAL);
	pack_lf();
}
//*****************************************************************************
//  Function        : PackRcptHeader
//  Description     : Pack EDC receipt header.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackRcptHeader(void)
{
	pack_mem("TERM ID :", 9);
	pack_mem(STIS_ACQ_TBL(0).sb_term_id, 8);
	pack_lf();
	pack_mem("MERCH ID:", 9);
	pack_mem(STIS_ACQ_TBL(0).sb_acceptor_id, 15);
	pack_lf();
}
//*****************************************************************************
//  Function        : PackAmt
//  Description     : Pack amount to predefine buffer.
//  Input           : aAmount;
//                    aOffset;          // start offset
//  Return          : len of amount string;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BYTE PackAmt(DDWORD aAmount, BYTE aOffset) //kt-150413
{
	struct DISP_AMT disp_amt;

	//ConvAmount(aAmount, &disp_amt, STIS_TERM_CFG.b_decimal_pos, STIS_TERM_CFG.b_currency);

	//ConvAmount(aAmount, &disp_amt, bcd2bin(gTablaCero.b_num_decimales), gTablaCero.b_simb_moneda); //kt-110912
	FormatAmount(aAmount, &disp_amt, bcd2bin(gTablaCero.b_num_decimales),
			gTablaCero.b_simb_moneda); // Jorge Numa 310113

	if (flagAnul == TRUE )
	{
		pack_space((BYTE)(aOffset - disp_amt.len) - 1);
		pack_byte('-');
	}
	else
	{
		pack_space((BYTE)(aOffset - disp_amt.len) );
	}

	pack_mem(disp_amt.content, disp_amt.len);
	return (disp_amt.len);
}

//*****************************************************************************
//  Function        : PackFF
//  Description     : Pack Form Seperator line.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackFF(void)
{
#if (LPT_SUPPORT)
	pack_byte(0x0C);
#else
	pack_lfs(2);
	pack_nbyte('_', MAX_CHAR_LINE_NORMAL);
	pack_lfs(4);
#endif
}
//*****************************************************************************
//  Function        : PackCountAmt
//  Description     : Pack count & amount;
//  Input           : count;
//                    amount;
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackCountAmt(WORD aCount, DDWORD aAmount, BOOLEAN tipIva) //LFGD 02/27/2013
{
	pack_space(6);
	if (tipIva == TRUE)
		bindec_data(aCount, 3);
	else
		pack_space(3);
	pack_space(6);
	PackAmt(aAmount, 17);
	pack_lf();
}

//*****************************************************************************
//  Function        : PackTotals
//  Description     : Pack total data.
//  Input           : aTot;    // pointer to data struct.
//  Return          : amount str's len;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void PackTotals(struct TOTAL_STRUCT *aTot) //LFGD 02/27/2013

{
	static const BYTE KBase[] =
	{	"BASE  "};
	static const BYTE KTips[] =
	{	"TIPS  "};
	static const BYTE KSales[] =
	{	"SALES "};
	static const BYTE KRefund[] =
	{	"REFND "};
	DDWORD amount;

	if (TIPsReqd())
	{
		pack_mem((BYTE *) KBase, 6);
		amount = aTot->s_sale_tot.dd_amount;
		amount -= aTot->s_tips_tot.dd_amount;
		PackCountAmt(aTot->s_sale_tot.w_count, amount, TRUE);
		pack_mem((BYTE *) KTips, 6);
		PackCountAmt(aTot->s_tips_tot.w_count, aTot->s_tips_tot.dd_amount,
				FALSE);
	}

	pack_mem((BYTE *) KSales, 6);
	PackCountAmt(aTot->s_sale_tot.w_count, aTot->s_sale_tot.dd_amount, TRUE);
	pack_mem((BYTE *) (KRefund), 6);
	PackCountAmt(aTot->s_refund_tot.w_count, aTot->s_refund_tot.dd_amount, TRUE);
}
#endif

//*****************************************************************************
//  Function        : PackTotalsVISA
//  Description     : Pack total data.
//  Input           : aTot;    // pointer to data struct.
//  Return          : amount str's len;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackTotalsVISA(struct TOTAL_STRUCT *aTot, BYTE tipoCuenta) //LFGD 03/15/2013	//kt-220413
{
	static const BYTE KBase[] =
	{ "COMPRA     :" };
	static const BYTE KIva[] =
	{ "IVA        :" };
	static const BYTE KIac[] =
	{ "IAC        :" };
	//	static const BYTE KTips[] =
	//	{ "PROPINA    :" };
	static const BYTE KSubTotal[] =
	{ "SUBTOTAL   :" };
	static const BYTE KTotal[] =
	{ "TOTAL      :" };
	static const BYTE KCancel[] =
	{ "CANCELADOS :" };
	/// LFGD 02/27/2013
	static const BYTE KBdevIVA[] =
	{ "B.DEVOL.IVA:" };
	static const BYTE KCrotat[] =
	{ "C.ROTATIVO :" };
	//	static const BYTE KPfact[] =
	//	{ "P.FACTURAS :" };
	//	static const BYTE KCancela[] =
	//	{ "CANCELADAS :" };
	static const BYTE KDonar[] =
	{ "DONACIONES :" };

	//	static const BYTE KTasaAer[] =
	//	{ "TASA AER.  :" };
	//
	//	static const BYTE KCashBack[] =  //MFBC/09/09/13
	//	{ "CASHBACK   :" };


	DDWORD amount, count;

	//flagAnul = TRUE; //kt-domingo
	pack_mem((BYTE *) KBase, 12);
	//amount = aTot->s_sale_tot.dd_amount;
	//amount -= aTot->s_tips_tot.dd_amount;
	PackCountAmt(aTot->s_sale_tot.w_count, aTot->s_sale_tot.dd_amount, TRUE);



	//	if (aTot->s_iva_tot.w_count > 0) //kt-190413
	//	{
	pack_mem((BYTE *) KIva, 12);
	PackCountAmt(aTot->s_iva_tot.w_count, aTot->s_iva_tot.dd_amount, FALSE);
	//	}

	pack_mem((BYTE *) KIac, 12);
	PackCountAmt(aTot->s_iac_tot.w_count, aTot->s_iac_tot.dd_amount, FALSE);


	//if (aTot->s_donacion_tot.w_count > 0)
	if (memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) != 0 &&
			( memcmp(&gTablaCero.b_dir_ciudad[39], "\x37", 1) != 0 || memcmp(	&gTablaCero.b_dir_ciudad[39], "\x39", 1) != 0 )) //MFBC/24/04/13
	{
		pack_mem((BYTE *) KDonar, 12);
		PackCountAmt(aTot->s_donacion_tot.w_count,
				aTot->s_donacion_tot.dd_amount, FALSE);
	}

	//if (aTot->s_tAero_tot.w_count > 0)
	pack_nbyte('-', 48);
	if (tipoCuenta != 0x50 && tipoCuenta != 0x60) //kt-290413
		pack_mem((BYTE *) KSubTotal, 12);
	else
		pack_mem((BYTE *) KTotal, 12);

	amount = aTot->s_sale_tot.dd_amount ;
	amount += aTot->s_tips_tot.dd_amount;
	amount += aTot->s_cash_back.dd_amount;
	amount += aTot->s_iva_tot.dd_amount;
	amount += aTot->s_iac_tot.dd_amount;
	//amount += aTot->s_donacion_tot.dd_amount;

	count = aTot->s_sale_tot.w_count;

	PackCountAmt(count, amount, TRUE);
	pack_mem((BYTE *) KCancel, 12);
	flagAnul = TRUE; //kt-domingo
	PackCountAmt(aTot->s_void_sale.w_count, aTot->s_void_sale.dd_amount, TRUE);
	pack_nbyte('=', 48);
	flagAnul = FALSE;

	if (tipoCuenta == 0x50)
	{

		//flagAnul = TRUE; //kt-domingo
		pack_mem((BYTE *) KBdevIVA, 12);
		PackCountAmt(aTot->s_base_tot.w_count, aTot->s_base_tot.dd_amount,
				FALSE);

		pack_mem((BYTE *) KCrotat, 12);
		PackCountAmt(aTot->s_cr_tot.w_count, aTot->s_cr_tot.dd_amount, TRUE);

		pack_nbyte('=', 48);
	}

}
//*****************************************************************************
//  Function        : PackEndOfRep
//  Description     : Pack END OF REPORT line.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackEndOfRep(void)
{
	char KEOR[] =
	{ "\x1BW1END OF REPORT\x1BW0" };
	pack_lf();
	pack_space(2);
	pack_mem(KEOR, strlen(KEOR));
	pack_lf();
}
//*****************************************************************************
//  Function        : SaveLastRcpt
//  Description     : Save Last Rcpt Copy.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SaveLastRcpt(BYTE *aDat, WORD aLen)
{
	int fd;
	char filename[32];

	if (aLen == 0)
		return;

	strcpy(filename, KLastRcptName);
	fd = fDeleteMW(filename);
	fd = fCreateMW(filename, FALSE);
	if (fd < 0)
	{
		DispLineMW("Reprint File ERR!", MW_MAX_LINE, MW_REVERSE | MW_CLREOL
				| MW_SPFONT);
		LongBeep();
		APM_WaitKey(9000, 0);
		return;
	}
	fWriteMW(fd, aDat, aLen);
	fCloseMW(fd);
	fCommitAllMW();
}
//*****************************************************************************
//  Function        : GetLastRcpt
//  Description     : Get Last Rcpt Copy.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetLastRcpt(BYTE *aDat, WORD aLen)
{
	int fd, len;
	char filename[32];

	strcpy(filename, KLastRcptName);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		return -1;
	}
	len = fReadMW(fd, aDat, aLen);
	fCloseMW(fd);
	return len;
}
//*****************************************************************************
//  Function        : ReprintLast
//  Description     : Reprint Last Saved Receipt.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ReprintLast(void)
{
	int rec_cnt = 0;
	int len;

	rec_cnt = APM_GetRecCount();
	if (rec_cnt == 0)
	{
		LongBeep();
		TextColor("NO EXISTEN TRANS", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER
				| MW_CLRDISP, 3);
		return;
	}

	if (rec_cnt == 1)
	{
		LongBeep();
		TextColor("No Permitido", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER
				| MW_CLRDISP, 3);
		return;
	}

	len = GetLastRcpt(MSG_BUF.sb_content, sizeof(MSG_BUF.sb_content));
	if (len == -1)
		return;
	DispLineMW(GetConstMsg(EDC_FN_REPRINT_BATCH), MW_LINE1, MW_CLRDISP
			| MW_REVERSE | MW_CENTER | MW_BIGFONT);
	Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
	PrintBuf(MSG_BUF.sb_content, len, FALSE);

}
//*****************************************************************************
//  Function        : ManualFeed
//  Description     : Feed Paper.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ManualFeed(void)
{
	BYTE tmp[] =
	{ "\x1BJ\x20" };

	IOCtlMW(gLptHandle, MW_TMLPT_RESET, NULL);
	LptPutN(tmp, 3);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////MB
void PrintDate(void)
{
	BYTE DateTime[14];
	BYTE date[11];
	BYTE Time[9];

	memset(DateTime, 0x00, sizeof(DateTime));
	memset(date, 0x00, sizeof(date));
	memset(Time, 0x00, sizeof(Time));
	//RtcGetMW(DateTime);
	sprintf(date + 0, "%c%c/%c%c/%c%c%c%c", DateTime[6], DateTime[7],
			DateTime[4], DateTime[5], DateTime[0], DateTime[1], DateTime[2],
			DateTime[3]);
	sprintf(Time + 0, "%c%c:%c%c:%c%c", DateTime[8], DateTime[9], DateTime[10],
			DateTime[11], DateTime[12], DateTime[13]);

	pack_space(14);
	pack_mem(date, 10);
	pack_space(4);
	pack_mem(Time, 8);
}

//*****************************************************************************
//  Function        : CodEstablecimiento
//  Description     : Empaqueta  el codigo de establecimiento sacado de la tabla 4
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void CodEstablecimiento(void)
{
	//	struct TABLA_CUATRO tabla_cuatro;
	//GetTablaCuatro(INPUT.w_host_idx, &tabla_cuatro);

	BYTE CodEstablecimiento[15];
	memset(CodEstablecimiento, 0x00, sizeof(CodEstablecimiento));

	memcpy(CodEstablecimiento, &gTablaCuatro.b_cod_estable[4], 11); //kt-110912
	LTrim(CodEstablecimiento, 0x20);
	pack_mem(CodEstablecimiento, strlen(CodEstablecimiento));
}

//*****************************************************************************
//  Function        : TipoCuenta
//  Description     : empaqueta las siglas del tipo de cuenta de la trans.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void TipoCuenta(void) //kt-101012
{
	BYTE TipCuenta[6];

	memset(TipCuenta, 0x00, sizeof(TipCuenta));

	//kt-220413
	switch (P_DATA.b_tipo_cuenta)
	{
	case 0x00:
		memcpy(TipCuenta, "DB   ", 5);
		break;
	case 0x10:
		memcpy(TipCuenta, "AH EL", 5);
		break;
	case 0x20:
		memcpy(TipCuenta, "CT EL", 5);
		break;
	case 0x30:
		memcpy(TipCuenta, "CR   ", 5);
		break;
	case 0x40:
		memcpy(TipCuenta, "RO   ", 5);
		break;
	case 0x50:
		memcpy(TipCuenta, "EF   ", 5);
		break;
	case 0x60:
		memcpy(TipCuenta, "EL   ", 5);
		break;
	case 0x70:
		memcpy(TipCuenta, "CR   ", 5);
		break;
	case 0x80:
		memcpy(TipCuenta, "CH   ", 5);
		break;

	}

	pack_mem(TipCuenta, 5);
}


//*****************************************************************************
//  Function        : PackPrintIss
//  Description     : empaqueta el nombre del emisor
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackPrintIss(void)
{
	BYTE nomIss[10];
	struct TABLA_TRES tabla_tres;
	GetTablaTres(INPUT.w_issuer_idx, &tabla_tres);
	memcpy(nomIss, gTablaTres.b_nom_emisor, 10);
	pack_mem(nomIss, 10);
}

void PackPanPrint(void)
{
	BYTE panTmp[21];
	BYTE panCopy[8];

	memset(panTmp, 0x00, sizeof(panTmp));
	memset(panCopy, 0x00, sizeof(panCopy));

	split(panTmp, P_DATA.sb_pan, 10);
	RTrim(panTmp, 'F');
	memcpy(&panCopy, "**", 2);
	strcat(&panCopy[2], &panTmp[strlen(panTmp) - 4]);
	pack_mem(panCopy, 6);
}

void NumReciboPack(void)
{
	BYTE numeroRecibo[14];
	memset(numeroRecibo, 0x00, sizeof(numeroRecibo));
	split(numeroRecibo, P_DATA.sb_roc_no, 3); // OJO imprimir la variable creada por nosotros para consecutivo (stan no sirve).		//kt-140912 P_DATA
	//	sprintf(numeroRecibo, "RECIBO: %02x%02x%02x%02x%02x%02x", TX_DATA.sb_trace_no[0], TX_DATA.sb_trace_no[1],TX_DATA.sb_trace_no[2]
	//,TX_DATA.sb_trace_no[3], TX_DATA.sb_trace_no[4], TX_DATA.sb_trace_no[5]);
	pack_str("RECIBO: ");
	pack_mem(numeroRecibo, strlen(numeroRecibo));
}

void TipoTrans(void)
{
	switch (P_DATA.b_trans)
	//kt-140912
	{
	case SALE_SWIPE:
		pack_str("COMPRA");
		break;

	case VOID: //kt-110912
		pack_str("ANULACION");
		break;

	default:
		break;

	}
}

void packlegal(void)
{
	pack_space(3);
	pack_str("PAGARE INCONDICIONALMENTE Y  A LA ORDEN DEL");
	pack_lf();
	pack_space(3);
	pack_str("ACREEDOR,  EL VALOR  TOTAL  DE  ESTE PAGARE");
	pack_lf();
	pack_space(3);
	pack_str("JUNTO CON LOS INTERESES A LAS TASAS MAXIMAS");
	pack_lf();
	pack_space(3);
	pack_str("           PERMITIDAS POR LEY.             ");
	pack_lfs(2);
}

void ExpDate(void)
{
	BYTE Exp[4];
	memset(Exp, 0x00, sizeof(Exp));

	split(Exp, P_DATA.sb_exp_date, 2); //kt-140912

	pack_byte(Exp[2]);
	pack_byte(Exp[3]);
	pack_str("/");
	pack_byte(Exp[0]);
	pack_byte(Exp[1]);
}

void PackHeader(BOOLEAN aReprint) //MFBC/31/01/13
{
	struct TABLA_CUATRO tabla_cuatro;
	// struct TABLA_TRES tabla_tres;
	//int gIdTabla3;
	//int gIdTabla4;
	BYTE nomEstablecimiento[23 + 1];
	//	BYTE Nom_estable[15+1];

	BYTE CodAgregado[15];
	BYTE NombreAgregado[16];
	BYTE TermAgrega[9];
	memset(CodAgregado, 0x00, sizeof(CodAgregado));
	memset(NombreAgregado, 0x00, sizeof(NombreAgregado));
	memset(TermAgrega, 0x00, sizeof(TermAgrega));
	BYTE BuffAux[10 + 1];
	gIdTabla3 = OpenFile(KTabla3File);
	gIdTabla4 = OpenFile(KTabla4File);

	//kt-101212
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));
	gIdTabla12 = OpenFile(KTabla12File);

	//	printf("\fentro");
	//	APM_WaitKey(9000, 0);
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	GetTablaCuatro(INPUT.w_host_idx, &tabla_cuatro);
	//	GetTablaTres(INPUT.w_issuer_idx, &tabla_tres);
	memset(nomEstablecimiento, 0x00, sizeof(nomEstablecimiento));
	memcpy(nomEstablecimiento, gTablaCero.b_nom_establ, 22);

	pack_lf();
	//pack_space(14); //NORA
	PackDateTimeVisa(&P_DATA.s_dtg);

	packVersion(TRUE); //LFGD 02/14/2013

	if ((gTablaCero.b_dir_ciudad[43] == '1' && gConfigComercio.flagCaja
			== FALSE) || Cajas() ) //kt-080413
	{
		pack_lfs(2);
		pack_space(11);
		pack_str("TRANSACCION FUERA DE CAJA");
	}

	pack_lfs(2);

	//	if (((struct TABLA_12*) p_tabla12)->b_id > 0
	//			&& (P_DATA.b_trans	== SALE_SWIPE || P_DATA.b_trans == SALE_ICC ||P_DATA.b_trans_void == SALE_SWIPE || P_DATA.b_trans_void == SALE_ICC)
	//			&& memcmp(P_DATA.sb_comercio_v,	&gTablaCuatro.b_cod_estable[4], 11) != 0) //kt-220413
	//	{
	//		pack_mem(P_DATA.sb_comercio_v, 9);
	//		pack_space(24);
	//		pack_mem(P_DATA.sb_nom_comer_v, 15);
	//		pack_space(34);
	//		pack_str("TERV: ");
	//		pack_mem(P_DATA.sb_terminal_id, 8);
	//		pack_lf();
	//	}
	//	memset(Nom_estable,0x00,sizeof(Nom_estable));
	//	memcpy(Nom_estable,((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
	//	DebugPoint(Nom_estable);
	//fin-kt
	//CodTerm();

	memcpy(CodAgregado, ((struct TABLA_12*) p_tabla12)->b_establ_virtual, sizeof(((struct TABLA_12*) p_tabla12)->b_establ_virtual)); //kt-110912
	LTrim(CodAgregado, 0x20);
	pack_mem(CodAgregado, strlen(CodAgregado));

	memcpy(NombreAgregado, ((struct TABLA_12*) p_tabla12)->b_nom_establ, sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ)); //kt-110912
	LTrim(NombreAgregado, ' ');
	RTrim(NombreAgregado, ' ');
	pack_space(MAX_CHAR_LINE_SMALL - 9 - strlen(NombreAgregado)); //MFBC/07/05/13
	pack_mem(NombreAgregado, strlen(NombreAgregado)); //MFBC/07/05/13

	pack_lf();

	pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes

	memcpy(TermAgrega, ((struct TABLA_12*) p_tabla12)->b_term_virtual, sizeof(((struct TABLA_12*) p_tabla12)->b_term_virtual)); //kt-110912
	pack_space(19);
	pack_str("T.AGR: ");
	pack_mem(TermAgrega, 8);
	pack_lf();

	CodEstablecimiento();

	LTrim(nomEstablecimiento, ' ');
	RTrim(nomEstablecimiento, ' ');
	pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomEstablecimiento)); //MFBC/07/05/13
	pack_mem(nomEstablecimiento, strlen(nomEstablecimiento)); //MFBC/07/05/13

	pack_lf();
	pack_space(35);
	pack_str("TER: ");
	pack_mem(gTablaCuatro.b_cod_terminal, 8);
	pack_lf();

	PackPrintIss();
	pack_space(12);
	TipoCuenta();

	if (RequiereCuotas()
			&& P_DATA.b_tipo_cuenta != 0x20 && P_DATA.b_tipo_cuenta != 0x10
			&& P_DATA.b_tipo_cuenta != 0x00 && P_DATA.b_tipo_cuenta != 0x90) //MFBC/27/08/13 agregue el tipod e transaccion de pago sin tarjeta
	{
		if(memcmp(P_DATA.sb_cuota, "00", 2) != 0)		// cuando es recuado Exito no imprime las cuotas **SR** 19/09/13
		{
			//printf("\f debug %02x%02x",P_DATA.sb_cuota[0], P_DATA.sb_cuota[1] ); WaitKey(3000, 0);
			pack_space(11);
			pack_str("CUOTAS: ");
			pack_mem(P_DATA.sb_cuota, sizeof(P_DATA.sb_cuota));
		}
	}
	pack_lf();
	PackPanPrint();
	//pack_lf();
	pack_space(16);
	//	ExpDate();
	pack_space(15);
	//pack_space(21);//NORA

	if (P_DATA.b_trans == SALE_ICC || P_DATA.b_trans == SALE_SWIPE || P_DATA.b_trans == VOID)
	{
		if (P_DATA.b_transAcep == TRUE) //MFBC/24/04/13
		{
			pack_str("RRN: ");
			pack_mem(&P_DATA.sb_rrn[6], 6); //Jorge Numa
		}
	}
	else
	{
		pack_str("RRN: ");
		pack_mem(&P_DATA.sb_rrn[6], 6); //Jorg                                                                                                        e Numa
	}
	pack_lf();
	NumReciboPack();
	pack_space(23);

	if (P_DATA.b_trans == SALE_ICC || P_DATA.b_trans == SALE_SWIPE || P_DATA.b_trans == VOID)
	{
		if (P_DATA.b_transAcep == TRUE && (memcmp(P_DATA.sb_auth_code, "      ", 6) != 0 )) //MFBC/24/04/13
		{
			pack_str("AUT: ");
			pack_mem(P_DATA.sb_auth_code, 6); //kt-140912
		}
	}
	else
	{
		if(memcmp(P_DATA.sb_auth_code, "      ", 6) != 0 )
		{
			pack_str("AUT: ");
			pack_mem(P_DATA.sb_auth_code, 6); //kt-140912
		}

	}

	//	if (NumFactura())
	//	{
	//		pack_lf();
	//		pack_space((MAX_CHAR_LINE_SMALL - 15));
	//		pack_str("FACT:");
	//		pack_mem(P_DATA.sb_num_ref, 10);
	//
	//	}

	//gConfigComercio.habBonoObsq = TRUE;
	if (Cajas()) //kt-300413
	{
		P_DATA.b_TransForCaja = TRUE;			// indica que la transaccion fue realizada por Cajas  **SR** 16/10/13


		memset(BuffAux, 0x30, 10);
		BuffAux[10] = 0x00;
		pack_lf();
		pack_str("OPE: ");
		pack_mem(gCajas.idCajero_83, 12);
		memcpy(P_DATA.sb_idCajero_83, gCajas.idCajero_83, sizeof(gCajas.idCajero_83));		// Se trasnfieren los datos al P_DATA para guardarlos en memoria  **SR** 16/10/13
		pack_space(16);
		pack_str("TRX: ");
		memcpy(&BuffAux[10 - strcspn(gCajas.numTrans_53, " ")], gCajas.numTrans_53, strcspn(gCajas.numTrans_53, " "));
		pack_mem(BuffAux, 10);
		memcpy(P_DATA.sb_numTrans_53, gCajas.numTrans_53, sizeof(gCajas.numTrans_53));
		pack_lf();
		pack_str("NUM CAJA: ");
		pack_mem(gCajas.numeroCaja_42, 10);
		memcpy(P_DATA.sb_numeroCaja_42, gCajas.numeroCaja_42, sizeof(gCajas.numeroCaja_42));
	}

	pack_lfs(2);

	CloseFile(gIdTabla12);
	CloseFile(gIdTabla3);
	CloseFile(gIdTabla4);
	FreeMW(p_tabla12);

}


//*****************************************************************************
//  Function        : Packpuntos
//  Description     : Pack Points to predefine buffer.
//  Input           : aPoints;
//                    aOffset;          // start offset
//  Return          : len of amount string;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

BYTE Packpuntos(DDWORD aPoints, BYTE aOffset, BOOLEAN aVoid, BOOLEAN aSinDecimal)
{
	struct DISP_AMT disp_amt;
	//
	//	if (P_DATA.b_trans == REDENCION_PUNTOS || P_DATA.b_trans_void == REDENCION_PUNTOS
	//			|| memcmp(gTablaTres.b_nom_emisor, "CMR", 3) == 0 )
	if (aSinDecimal == TRUE)
		FormatAmount(aPoints, &disp_amt, 0 , 0); // quito los decimales
	else
		FormatAmount(aPoints, &disp_amt, -1 , 0);

	//pack_space((BYTE)(aOffset - disp_amt.len));

	if (aVoid == TRUE )
	{
		pack_space((BYTE)(aOffset - disp_amt.len) - 1);
		pack_byte('-');
	}
	else
	{
		pack_space((BYTE)(aOffset - disp_amt.len) );
	}

	pack_mem(disp_amt.content, disp_amt.len);
	return (disp_amt.len);
}

void PackVoucher(BOOLEAN aReprint, BOOLEAN Duplicado)
{
	BYTE costo_trans[6 + 1]; //MFBC/23/04/13
	DDWORD CVM=0;
	struct TABLA_CERO tabla_cero;
	struct TABLA_TRES tabla_tres;
	GetTablaCero(&tabla_cero);
	memset(costo_trans, 0x00, sizeof(costo_trans));

	gIdTabla3 = OpenFile(KTabla3File);

	GetTablaTres(P_DATA.w_issuer_idx, &tabla_tres);

	gIdTabla3 = CloseFile(gIdTabla3);

	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();
	pack_space(19);
	pack_str("CREDIBANCO");

	PackHeader(aReprint); //MFBC/31/01/13

	if (P_DATA.b_flag_Consulta_Costo ) //MFBC/23/04/13
	{
		pack_space((MAX_CHAR_LINE_SMALL - 28) / 2); //MFBC/24/04/13
		pack_str("VALOR TRANSACCION:  ");

		if (!P_DATA.b_transAcep && P_DATA.w_rspcode != 'A' * 256 + 'P'
				&& P_DATA.w_rspcode != 'T' * 256 + 'A') //MFBC/26/04/13
		{
			LTrim(gBin_Tipo.costoNegado, '0');
			pack_mem(&gTablaCero.b_simb_moneda, 1);
			pack_mem(gBin_Tipo.costoNegado, strlen(gBin_Tipo.costoNegado));
			pack_lf();
			pack_space((MAX_CHAR_LINE_SMALL - 22) / 2); //MFBC/24/04/13
			pack_str("TRANSACCION RECHAZADA ");
			pack_lf();
		}
		else
		{
			P_DATA.b_costoTrans[sizeof(P_DATA.b_costoTrans) - 1] = 0x00;		// Ultima posicion en NULL para que pueda realizar correctamente el Rtrim //**SR** 01/10/13
			LTrim(P_DATA.b_costoTrans, '0');
			LTrim(P_DATA.b_costoTrans, ' ');		// Se eliminan los espacios		//**SR** 01/10/13
			RTrim(P_DATA.b_costoTrans, ' ');		// Se eliminan los espacios     //**SR** 01/10/13
			pack_mem(&gTablaCero.b_simb_moneda, 1);
			pack_mem(P_DATA.b_costoTrans, strlen(P_DATA.b_costoTrans)); //MFBC/25/04/13
			pack_lf();
		}
	}



	if ((P_DATA.b_flag_Consulta_Costo && P_DATA.b_transAcep)
			|| !P_DATA.b_flag_Consulta_Costo || P_DATA.w_rspcode == 'A' * 256
			+ 'P' || P_DATA.w_rspcode == 'T' * 256 + 'A') //MFBC/26/04/13)
	{


		pack_str("COMPRA NETA:                 ");
		PackAmt(P_DATA.dd_net_mount, 17);
		pack_lf();

		if (P_DATA.dd_iva != 0)
		{
			pack_str("IVA        :                 ");
			PackAmt(P_DATA.dd_iva, 17);
			pack_lf();
		}

		if (P_DATA.dd_IAC != 0)
		{
			pack_str("IAC        :                 ");
			PackAmt(P_DATA.dd_IAC, 17);
			pack_lf();
		}

		if (P_DATA.dd_donacion != 0)
		{
			pack_str("DONACION   :                 ");
			PackAmt(P_DATA.dd_donacion, 17);
			pack_lf();

		}

		if(P_DATA.dd_tip != 0)
		{
			pack_str("PROPINA    :                 ");
			PackAmt(P_DATA.dd_tip, 17);
			pack_lf();
		}

		if (P_DATA.dd_valor_efectivo != 0)		// **SR** 19/07/13
		{
			pack_str("EFECTIVO :                   ");
			PackAmt(P_DATA.dd_valor_efectivo, 17);
			pack_lf();

			pack_mem(MW_LPT_FONT_ENLARGED, 3);
			pack_str("TOTAL  :     ");
			//PackAmt(P_DATA.dd_amount + P_DATA.dd_valor_efectivo, 17);
			PackAmt(P_DATA.dd_amount , 17);
			pack_lfs(2);

		}
		else
		{
			pack_mem(MW_LPT_FONT_ENLARGED, 3);
			pack_str("TOTAL   :     ");
			//PackAmt(P_DATA.dd_amount + P_DATA.dd_valor_efectivo , 17);
			PackAmt(P_DATA.dd_amount  , 17);
			pack_lfs(2);
		}
		pack_mem(MW_LPT_FONT_SMALL, 3);

		if (P_DATA.b_trans == VOID ) //kt-160413
		{
			if (Duplicado)
			{
				pack_lf();
				pack_space((MAX_CHAR_LINE_SMALL - 17) / 2);
				pack_str("    ANULACION    "); //kt-140912
				pack_lf();
				pack_space((MAX_CHAR_LINE_SMALL - 17) / 2);
				pack_str("    DUPLICADO    "); //kt-140912
				pack_lfs(2);
			}
			else
			{
				pack_lfs(2);
				pack_space((MAX_CHAR_LINE_SMALL - 20) / 2);
				pack_str("     ANULACION     ");
				pack_lf();
				pack_str("       *SUJETO A VERIFICACION DE LA DIAN     ");
				pack_lfs(2);
			}
		}
		else
		{
			if (P_DATA.b_print_baseDev)
			{
				if (P_DATA.dd_base != 0)
				{
					pack_str("*BASE DEV IVA:               ");//29 espacios
					PackAmt(P_DATA.dd_base, 17);
					pack_lf(); //MFBC/11/06/13
				}
			}

			if ( memcmp(gTablaTres.b_nom_emisor, "CMR", 3) == 0 )
			{
				if(P_DATA.dd_Punts_Acum_sale != 0)
				{
					pack_lf();
					pack_str("PUNTOS ACUMULADOS EN COMPRA :");//29
					Packpuntos(P_DATA.dd_Punts_Acum_sale, 17, FALSE, TRUE);
				}

				if(P_DATA.dd_Total_puntos != 0)
				{
					pack_lf();
					pack_str("TOTAL PUNTOS ACUMULADOS :");//25
					Packpuntos(P_DATA.dd_Total_puntos, 21, FALSE, TRUE);
					pack_lf();
				}

			}


			if (Duplicado)
			{
				pack_lf();
				pack_space(15);
				pack_mem("    DUPLICADO    ", 17); //kt-140912
				pack_lf(); //MFBC/11/06/13
			}

			if (P_DATA.b_trans != SALE_CTL) //MFBC/08/06/13  subi la impresion del numero de referencia
			{
				if (referencia() == TRUE && P_DATA.b_terminal != 2) //kt-180413
				{
					if ( (atoi(P_DATA.sb_num_ref) !=0 ))
					{
						pack_lf();
						pack_space(15);
						pack_str("NUM. REFERENCIA:  ");//18
						pack_mem(P_DATA.sb_num_ref, 15);
						pack_lf();
					}
				}
			}

			if (P_DATA.b_print_baseDev)
			{
				pack_lf();
				pack_str("       *SUJETO A VERIFICACION DE LA DIAN     ");
				pack_lf();
			}

			if (P_DATA.dd_donacion != 0)
			{
				pack_lf();
				pack_str("          Gracias por su donacion a:         ");
				pack_lf();
				pack_space((MAX_CHAR_LINE_SMALL - strlen(P_DATA.b_EmpDona)) / 2);
				//pack_mem(&gTablaCinco.b_cod_ean[0], 15);
				pack_mem(P_DATA.b_EmpDona, strlen(P_DATA.b_EmpDona)); // MFBC/12/06/13
				pack_lfs(2);
			}

		}

		if (!aReprint)		// no requiere Fima para Recaudo EXITO **SR**  19/09/13
		{

			if (P_DATA.b_tipo_cuenta == 0x00 || P_DATA.b_tipo_cuenta
					== 0x30 || P_DATA.b_tipo_cuenta == 0x40)
			{
				if (P_DATA.b_trans != VOID ) //kt-220413
					packlegal();
			}

			if (TX_DATA.b_trans != SALE_CTL && P_DATA.b_trans != VOID)
			{
				pack_firma();
			}
			else
			{
				if (CTL_SIGN)
				{
					pack_firma();
				}
			}

			//	if (P_DATA.s_icc_data.b_tag_9f26_len != 0x00) //kt-180313
			if (P_DATA.b_entry_mode == ICC || P_DATA.b_entry_mode == CONTACTLESS ) //kt-180313
			{

				if (P_DATA.b_trans == SALE_CTL || gOrg_rec.b_trans == SALE_CTL)
				{
					pack_lf();
					pack_mem("Criptograma: ", 13);
					split_data(P_DATA.s_icc_data.sb_tag_9f26,
							P_DATA.s_icc_data.b_tag_9f26_len);
					pack_lf();
					pack_mem("AID    : ", 9);
					split_data(P_DATA.s_icc_data.sb_aid,
							P_DATA.s_icc_data.b_aid_len);
					pack_space(2);
					pack_mem(P_DATA.s_icc_data.sb_label,
							P_DATA.s_icc_data.b_tag_50_len);
					pack_lf();

					CVM = decbin8b(gCTLGEN.cvm_limit,12);
					if(CVM > P_DATA.dd_amount )
					{
						pack_space(16);
						pack_str("VALIDO SIN FIRMA");
						pack_lf();
						pack_mem(P_DATA.sb_holder_name, 26);

					}
				}
				else
				{
					packCriptograma(); //MFBC/14/08/13
				}
			}

		}
		else
		{
			if (P_DATA.b_tipo_cuenta == 0x00 || P_DATA.b_tipo_cuenta
					== 0x30 || P_DATA.b_tipo_cuenta == 0x40)
			{
				if (P_DATA.b_trans != VOID ) //kt-220413
					packlegal();
			}
			//	if (P_DATA.s_icc_data.b_tag_9f26_len != 0x00) //kt-domingo
			if (P_DATA.b_entry_mode == ICC || P_DATA.b_entry_mode == CONTACTLESS) //kt-180313
			{

				if (P_DATA.b_trans == SALE_CTL || gOrg_rec.b_trans == SALE_CTL)
				{
					pack_lf();
					pack_mem("Criptograma: ", 13);
					split_data(P_DATA.s_icc_data.sb_tag_9f26,
							P_DATA.s_icc_data.b_tag_9f26_len);
					pack_lf();
					pack_mem("AID    : ", 9);
					split_data(P_DATA.s_icc_data.sb_aid,
							P_DATA.s_icc_data.b_aid_len);
					pack_space(2);
					pack_mem(P_DATA.s_icc_data.sb_label,
							P_DATA.s_icc_data.b_tag_50_len);
					pack_lf();

					CVM = decbin8b(gCTLGEN.cvm_limit,12);
					if(CVM > P_DATA.dd_amount )
					{
						pack_space(16);
						pack_str("VALIDO SIN FIRMA");
						pack_lf();
						pack_mem(P_DATA.sb_holder_name, 26);

					}
				}
				else
				{
					packCriptograma();

				}

			}
		}


		if (P_DATA.s_icc_data.b_tag_9f26_len == 0x00 && P_DATA.b_entry_mode
				!= SIN_TARJETA) //MFBC/28/02/13
			pack_mem(P_DATA.sb_holder_name, 26);

		if (P_DATA.b_trans == SALE_CTL || gOrg_rec.b_trans == SALE_CTL)
		{
			if (P_DATA.s_icc_data.b_tag_9f12_len != 0)
			{
				pack_mem("AP.Name: ", 9);
				pack_mem(P_DATA.s_icc_data.sb_tag_9f12,
						P_DATA.s_icc_data.b_tag_9f12_len);
				//                pack_lf();

			}
		}

	}
	pack_lfs(6);
}


//*****************************************************************************
//  Function        : PackDetalles
//  Input           : tipoCuenta,  NumTrans,  IdEmisor
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//***************************************************************************
static void PackDetalles(BYTE tipoCuenta, WORD NumTrans, BYTE IdEmisor) //kt-220413
{
	BYTE buffer[21 + 1];
	BYTE panTmp[21];
	BYTE panCopy[10];
	//DWORD var_j;
	int rec_cnt2;

	for (rec_cnt2 = 0; rec_cnt2 < NumTrans; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

		//		if (IdEmisor != RECORD_BUF.w_issuer_idx)
		//			continue;

		if ((IdEmisor == RECORD_BUF.w_issuer_idx) && (memcmp(
				INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)
						== 0) && RECORD_BUF.b_tipo_cuenta == tipoCuenta)
		{
			//TARJETA
			memset(panTmp, 0x00, sizeof(panTmp));
			memset(panCopy, 0x00, sizeof(panCopy));

			split(panTmp, RECORD_BUF.sb_pan, 10);
			RTrim(panTmp, 'F');
			memcpy(&panCopy, "***", 3);
			strcat(&panCopy[3], &panTmp[strlen(panTmp) - 4]);
			pack_mem(panCopy, 7);

			//RECIBO/FACTURA
			pack_space(6);
			memset(buffer, 0x00, sizeof(buffer));
			split(buffer, RECORD_BUF.sb_roc_no, 3);
			buffer[6] = 0;
			pack_mem(buffer, strlen(buffer));
			pack_space(7);

			// TIPO DE CUENTA
			switch (RECORD_BUF.b_tipo_cuenta)
			{
			case 0x00:
				pack_mem("DB", 2);
				break;
			case 0x10:
				pack_mem("AH", 2);
				break;
			case 0x20:
				pack_mem("CT", 2);
				break;
			case 0x30:
				pack_mem("CR", 2);
				break;
			case 0x40:
				pack_mem("RO", 2);
				break;
			case 0x50:
				pack_mem("EF", 2);
				break;
			case 0x60:
				pack_mem("EL", 2);
				break;
			case 0x70:
				pack_mem("CR", 2);
				break;
			case 0x80:
				pack_mem("CH", 2);
				break;
			case 0x90:
				pack_mem("  ", 2);
				break;

			}
			//MONTO
			if (RECORD_BUF.b_trans_status & VOIDED) //kt-230413
				flagAnul = TRUE;
			else
				flagAnul = FALSE;

			//	PackAmt(RECORD_BUF.dd_amount + RECORD_BUF.dd_valor_efectivo, 17);
			PackAmt(RECORD_BUF.dd_amount , 17);
			flagAnul = FALSE;

			pack_lf();
		}

	}

}

//*****************************************************************************
//  Function        : PackDetalles
//  Input           : tipoCuenta,  NumTrans,  IdEmisor
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//***************************************************************************
static BOOLEAN PackDetallesPantalla(BYTE tipoCuenta, WORD NumTrans,
		BYTE IdEmisor) //kt-230413
{
	BYTE buffer[21 + 1];
	BYTE bufferDisplay[27];
	BYTE panTmp[21];
	BYTE panCopy[10];
	DWORD var_i = 0;
	DWORD Line = 0, keyin;
	BOOLEAN transaccion = FALSE;
	int rec_cnt2;
	BOOLEAN f_MultiComer = FALSE;

	Line = MW_LINE4;

	if(g_TypeCount == tipoCuenta && gf_MultiComer == TRUE)
		f_MultiComer = TRUE;
	for (rec_cnt2 = 0; rec_cnt2 < NumTrans; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));
		if ((IdEmisor == RECORD_BUF.w_issuer_idx) && (memcmp(
				INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)
						== 0))
		{

			if (RECORD_BUF.b_tipo_cuenta == tipoCuenta)
			{
				transaccion = TRUE;

				memset(buffer, 0x00, sizeof(buffer));
				memset(bufferDisplay, 0x20, sizeof(bufferDisplay));

				//TARJETA
				memset(panTmp, 0x00, sizeof(panTmp));
				memset(panCopy, 0x00, sizeof(panCopy));

				split(panTmp, RECORD_BUF.sb_pan, 10);
				RTrim(panTmp, 'F');
				memcpy(&panCopy, &panTmp[strlen(panTmp) - 4], 4);
				memcpy(&bufferDisplay[var_i], panCopy, 4);
				var_i += 5;

				//RECIBO/FACTURA
				memset(buffer, 0x00, sizeof(buffer));
				split(buffer, RECORD_BUF.sb_roc_no, 3);
				buffer[6] = 0;

				memcpy(&bufferDisplay[var_i], buffer, 6);
				var_i += 7;

				// TIPO DE CUENTA
				switch (RECORD_BUF.b_tipo_cuenta)
				{
				case 0x00:
					strcpy(&bufferDisplay[var_i], "DB");
					break;
				case 0x10:
					strcpy(&bufferDisplay[var_i], "AH");
					break;
				case 0x20:
					strcpy(&bufferDisplay[var_i], "CT");
					break;
				case 0x30:
					strcpy(&bufferDisplay[var_i], "CR");
					break;
				case 0x40:
					strcpy(&bufferDisplay[var_i], "RO");
					break;
				}
				var_i = 0;

				if (Line == MW_LINE4)
				{
					DispClrLineMW(MW_LINE4);
					DispClrLineMW(MW_LINE5);
					DispClrLineMW(MW_LINE6);
					DispClrLineMW(MW_LINE7);
					DispClrLineMW(MW_LINE8);
				}

				TextColor(bufferDisplay, Line, COLOR_BLACK,
						MW_SPFONT | MW_LEFT, 0);

				//MONTO
				if (RECORD_BUF.b_trans_status & VOIDED) //kt-230413
					flagAnul = TRUE;
				else
					flagAnul = FALSE;

				DispAmnt(RECORD_BUF.dd_amount, Line, MW_SPFONT);
				flagAnul = FALSE;

				Line += 0x0100;
				if (Line > MW_LINE8)
				{
					Line = MW_LINE4;

					while (true)
					{
						//keyin = APM_WaitKey(KBD_TIMEOUT, 0);
						keyin = APM_WaitKey(3000, 0);

						if (keyin == MWKEY_CANCL || keyin == -1)
							return TRUE;
						else if (keyin == MWKEY_ENTER){
							break;
							//							if(gf_MultiComer == TRUE)
							//								return FALSE;
							//							else
							//								return FALSE;
						}
					}
				}

			}
		}
	}

	if (transaccion)
	{
		while (true)
		{
			//keyin = APM_WaitKey(KBD_TIMEOUT, 0);
			keyin = APM_WaitKey(3000, 0);
			if (keyin == MWKEY_CANCL || keyin == -1)
				return TRUE;
			else if (keyin == MWKEY_ENTER){
				if(f_MultiComer == TRUE)
					return FALSE;
				else
					return 2;
			}
		}
	}

	return 2;
}
//*****************************************************************************
//  Function        : PackDetalles
//  Input           : tipoCuenta,  NumTrans,  IdEmisor
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//***************************************************************************
#if 0
static BOOLEAN PackCargaBonoPantalla(BYTE tipoCuenta, WORD NumTrans,
		BYTE IdEmisor) //kt-220413
{
	BYTE buffer[21 + 1];
	BYTE bufferDisplay[27];
	BYTE panTmp[21];
	BYTE panCopy[10];
	DWORD var_i = 0;
	DWORD Line = 0, keyin;
	BOOLEAN transaccion = FALSE;
	int rec_cnt2;

	Line = MW_LINE4;

	for (rec_cnt2 = 0; rec_cnt2 < NumTrans; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

		if (RECORD_BUF.b_trans == BONO_OBSEQUIO && RECORD_BUF.b_tipo_cuenta
				== tipoCuenta && (IdEmisor == RECORD_BUF.w_issuer_idx)
				&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
						== 0) && (memcmp(INPUT.sb_terminal_id,
								RECORD_BUF.sb_terminal_id, 8) == 0))
		{
			transaccion = TRUE;

			memset(buffer, 0x00, sizeof(buffer));
			memset(bufferDisplay, 0x20, sizeof(bufferDisplay));

			//TARJETA
			memset(panTmp, 0x00, sizeof(panTmp));
			memset(panCopy, 0x00, sizeof(panCopy));

			split(panTmp, RECORD_BUF.sb_pan, 10);
			RTrim(panTmp, 'F');
			memcpy(&panCopy, &panTmp[strlen(panTmp) - 4], 4);
			memcpy(&bufferDisplay[var_i], panCopy, 4);
			var_i += 5;

			//RECIBO/FACTURA
			memset(buffer, 0x00, sizeof(buffer));
			split(buffer, RECORD_BUF.sb_roc_no, 3);
			buffer[6] = 0;

			memcpy(&bufferDisplay[var_i], buffer, 6);
			var_i += 7;

			// TIPO DE CUENTA

			strcpy(&bufferDisplay[var_i], " CG");

			var_i = 0;

			if (Line == MW_LINE4)
			{
				DispClrLineMW(MW_LINE4);
				DispClrLineMW(MW_LINE5);
				DispClrLineMW(MW_LINE6);
				DispClrLineMW(MW_LINE7);
				DispClrLineMW(MW_LINE8);
			}

			TextColor(bufferDisplay, Line, COLOR_BLACK, MW_SPFONT | MW_LEFT, 0);

			//MONTO
			DispAmnt(RECORD_BUF.dd_amount, Line, MW_SPFONT);

			Line += 0x0100;
			if (Line > MW_LINE8)
			{
				Line = MW_LINE4;

				while (true)
				{
					keyin = APM_WaitKey(KBD_TIMEOUT, 0);

					if (keyin == MWKEY_CANCL)
						return FALSE;
					else if (keyin == MWKEY_ENTER)
						break;
				}
			}

		}
	}

	if (transaccion)
	{
		while (true)
		{
			keyin = APM_WaitKey(KBD_TIMEOUT, 0);

			if (keyin == MWKEY_CANCL)
				return FALSE;
			else if (keyin == MWKEY_ENTER)
				return TRUE;
		}
	}

	return TRUE;
}
#endif

///*****************************************************************************
//  Function        : DispTransTotal
//  Description     : Display transaction totals.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN DispTransTotal(struct TOTAL_STRUCT *aTot, BYTE tipoCuenta) //kt-220413
{
	//multibol
	static const BYTE KBase[] =
	{ "COMPRA :" };
	static const BYTE KIva[] =
	{ "IVA    :" };
	static const BYTE KIac[] =
	{ "IAC    :" };
	//	static const BYTE KTips[] =
	//	{ "PROPINA:" };
	static const BYTE KSubTotal[] =
	{ "SUBTOTAL  :" };
	static const BYTE KTotal[] =
	{ "TOTAL     :" };
	static const BYTE KCancel[] =
	{ "CANCELADOS:" };
	/// LFGD 02/27/2013
	static const BYTE KBdevIVA[] =
	{ "B.DEVOL.IVA:" };
	static const BYTE KCrotat[] =
	{ "C.ROTATIVO :" };
	//	static const BYTE KPfact[] =
	//	{ "P.FACTURAS :" };
	//	static const BYTE KCancela[] =
	//	{ "CANCELADAS :" };
	static const BYTE KDonar[] =
	{ "DONACIONES :" };
	//	static const BYTE KTasaAer[] =
	//	{ "TASA AER.:" };
	//	static const BYTE KCashBack[] =  //MFBC/09/09/13
	//	{ "CASHBACK:" };



	DDWORD amount, count;
	DWORD keyin;
	BYTE buffer[17];

	if (tipoCuenta == 0x50)
	{
		TextColor("      GRAN TOTAL     ", MW_LINE2, COLOR_VISABLUE, MW_SMFONT
				| MW_CENTER | MW_REVERSE, 0);
	}

	memset(buffer, 0x20, sizeof(buffer));

	bin2dec(aTot->s_sale_tot.w_count, &buffer[11], 3);
	buffer[14] = 0;

	TextColor(buffer, MW_LINE3, COLOR_BLACK, MW_LEFT | MW_SPFONT | MW_CLREOL, 0);
	TextColor((BYTE *) KBase, MW_LINE3, COLOR_BLACK, MW_LEFT | MW_SMFONT, 0);
	DispAmnt(aTot->s_sale_tot.dd_amount, MW_LINE3, MW_SPFONT);

	memset(buffer, 0x20, sizeof(buffer));

	//	if(aTot->s_iva_tot.dd_amount > 0)
	//	{
	TextColor((BYTE *) KIva, MW_LINE7, COLOR_BLACK, MW_LEFT | MW_SMFONT | MW_CLREOL, 0);
	DispAmnt(aTot->s_iva_tot.dd_amount, MW_LINE7, MW_SPFONT);
	//	}

	//	if(aTot->s_iac_tot.dd_amount > 0)
	//	{
	TextColor((BYTE *) KIac, MW_LINE8, COLOR_BLACK, MW_LEFT | MW_SMFONT | MW_CLREOL, 0);
	DispAmnt(aTot->s_iac_tot.dd_amount, MW_LINE8, MW_SPFONT);
	//}



	while (true)
	{
		//		keyin = APM_WaitKey(KBD_TIMEOUT, 0);
		keyin = APM_WaitKey(3000, 0);
		if (keyin == MWKEY_CANCL || keyin == -1)
			return 2;
		else if (keyin == MWKEY_ENTER)
			break;

	}

	DispClrLineMW(MW_LINE3);
	DispClrLineMW(MW_LINE4);
	DispClrLineMW(MW_LINE5);
	DispClrLineMW(MW_LINE6);
	DispClrLineMW(MW_LINE7);
	DispClrLineMW(MW_LINE8);


	if (memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) != 0 &&
			( memcmp(&gTablaCero.b_dir_ciudad[39], "\x37", 1) != 0 || memcmp(	&gTablaCero.b_dir_ciudad[39], "\x39", 1) != 0 )) //MFBC/24/04/13
	{
		TextColor((BYTE *) KDonar, MW_LINE4, COLOR_BLACK, MW_LEFT | MW_SMFONT,
				0);
		DispAmnt(aTot->s_donacion_tot.dd_amount, MW_LINE4, MW_SPFONT);
	}



	//if (aTot->s_tAero_tot.w_count > 0)

	amount = aTot->s_sale_tot.dd_amount;
	amount+= aTot->s_iva_tot.dd_amount;
	amount += aTot->s_iac_tot.dd_amount;
	amount+= aTot->s_cash_back.dd_amount;
	amount+= aTot->s_tips_tot.dd_amount;
	//amount+= aTot->s_donacion_tot.dd_amount;

	count = aTot->s_sale_tot.w_count;

	//PackCountAmt(count, amount, TRUE);

	memset(buffer, 0x20, sizeof(buffer));

	bin2dec(count, &buffer[11], 3);
	buffer[14] = 0;
	TextColor(buffer, MW_LINE7, COLOR_BLACK, MW_LEFT | MW_SPFONT, 0);
	DispAmnt(amount, MW_LINE7, MW_SPFONT);

	if (tipoCuenta != 0x50 && tipoCuenta != 0x60) //kt-290413
		TextColor((BYTE *) KSubTotal, MW_LINE7, COLOR_BLACK, MW_LEFT
				| MW_SPFONT, 0);
	else
		TextColor((BYTE *) KTotal, MW_LINE7, COLOR_BLACK, MW_LEFT | MW_SPFONT,
				0);

	memset(buffer, 0x20, sizeof(buffer));

	bin2dec(aTot->s_void_sale.w_count, &buffer[11], 3); //kt-220413
	buffer[14] = 0; //kt-220413
	TextColor(buffer, MW_LINE8, COLOR_BLACK, MW_LEFT | MW_SPFONT, 0);
	TextColor((BYTE *) KCancel, MW_LINE8, COLOR_BLACK, MW_LEFT | MW_SPFONT, 0);
	flagAnul = TRUE;
	DispAmnt(aTot->s_void_sale.dd_amount, MW_LINE8, MW_SPFONT);
	flagAnul = FALSE;

	while (true)
	{
		//keyin = APM_WaitKey(KBD_TIMEOUT, 0);
		keyin = APM_WaitKey(3000, 0);

		if (keyin == MWKEY_CANCL || keyin == -1)
			return 2;
		else if (keyin == MWKEY_ENTER)
			break;
	}

	DispClrLineMW(MW_LINE3);
	DispClrLineMW(MW_LINE4);
	DispClrLineMW(MW_LINE5);
	DispClrLineMW(MW_LINE6);
	DispClrLineMW(MW_LINE7);
	DispClrLineMW(MW_LINE8);

	if (tipoCuenta == 0x50)
	{

		//		pack_mem((BYTE *) KBdevIVA, 12);
		//		PackCountAmt(aTot->s_base_tot.w_count, aTot->s_base_tot.dd_amount,FALSE);

		TextColor((BYTE *) KBdevIVA, MW_LINE3, COLOR_BLACK,
				MW_LEFT | MW_SPFONT, 0);
		DispAmnt(aTot->s_base_tot.dd_amount, MW_LINE3, MW_SPFONT);

		//		pack_mem((BYTE *) KCrotat, 12);
		//		PackCountAmt(aTot->s_cr_tot.w_count, aTot->s_cr_tot.dd_amount, TRUE);

		memset(buffer, 0x20, sizeof(buffer));

		bin2dec(aTot->s_cr_tot.w_count, &buffer[13], 3);
		buffer[16] = 0;

		TextColor(buffer, MW_LINE4, COLOR_BLACK, MW_LEFT | MW_SPFONT, 0);
		TextColor((BYTE *) KCrotat, MW_LINE4, COLOR_BLACK, MW_LEFT | MW_SPFONT,
				0);
		DispAmnt(aTot->s_cr_tot.dd_amount, MW_LINE4, MW_SPFONT);

		//		pack_mem((BYTE *) KPfact, 12);
		//		PackCountAmt(aTot->s_bill_tot.w_count, aTot->s_bill_tot.dd_amount,TRUE);

		while (TRUE)
		{
			//keyin = APM_WaitKey(KBD_TIMEOUT, 0);
			keyin = APM_WaitKey(3000, 0);

			if (keyin == MWKEY_CANCL || keyin == -1)
				return 2;
			else if (keyin == MWKEY_ENTER){
				if(gf_MultiComer == TRUE)
					return FALSE;
				else
					return TRUE;

			}
		}
	}
	return TRUE;

}


static BOOLEAN PackTotales(BYTE tipoCuenta, WORD NumTrans, BYTE IdEmisor,
		BOOLEAN display) //kt-220413
{
	BOOLEAN flagElectron = 0;
	int rec_cnt2;
	int Aux =  0;

	memset(&TERM_TOT, 0, sizeof(struct TOTAL_STRUCT));

	for (rec_cnt2 = 0; rec_cnt2 < NumTrans; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

		if ((IdEmisor == RECORD_BUF.w_issuer_idx) && (memcmp(
				INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
				&& memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)== 0) //kt-271112
		{
			if (RECORD_BUF.b_tipo_cuenta == tipoCuenta)
			{
				flagElectron = 1;
				AddToTotals(TRUE);
			}

			if (tipoCuenta == 0x11) //kt-090413
			{
				if (RECORD_BUF.b_tipo_cuenta == 0x10
						|| RECORD_BUF.b_tipo_cuenta == 0x20 || RECORD_BUF.b_tipo_cuenta == 0x90)
				{
					flagElectron = 1;
					AddToTotals(TRUE);
				}

			}
		}
	}

	if (display)
	{
		if (flagElectron){
			Aux = DispTransTotal(&TERM_TOT, tipoCuenta);
			if (Aux == FALSE)
				return FALSE;
			else if(Aux == 2)
				return 2;
		}
	}
	else
	{
		if (flagElectron)
			PackTotalsVISA(&TERM_TOT, tipoCuenta);
	}
	return TRUE;
}

static BOOLEAN PackTotalCarga(BYTE tipoCuenta, WORD NumTrans, BOOLEAN display) //kt-220413
{
	BOOLEAN flagElectron = 0;
	int rec_cnt2;

	memset(&TERM_TOT, 0, sizeof(struct TOTAL_STRUCT));

	for (rec_cnt2 = 0; rec_cnt2 < NumTrans; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

		if ((memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
				&& memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)== 0) //kt-271112
		{
			flagElectron = 1;
			AddToTotals(TRUE);
		}
	}

	if (display)
	{
		if (flagElectron)
			if (!DispTransTotal(&TERM_TOT, tipoCuenta))
				return FALSE;
	}
	else
	{
		if (flagElectron)
			PackTotalsVISA(&TERM_TOT, tipoCuenta);
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : PackVoucherSettle
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//***************************************************************************
BOOLEAN PackVoucherSettle(WORD rec_cnt, WORD max_issuer, WORD virtual, BOOLEAN aRePrint) //kt-110413
{
	struct TABLA_CERO tabla_cero;
	struct TABLA_CUATRO tabla_cuatro;
	BYTE nomEstablecimiento[32 + 1];
	BYTE bufferaux[16+1];
	BYTE *nomAux = MallocMW(32);
	WORD rec_cnt2, rec_idx, issuer_idx, contTrans = 0;
	BYTE tmpCuenta = 0;
	BOOLEAN flagDebito = 0;
	BOOLEAN flagCredito = 0;
	BOOLEAN flagElectron = 0;
	BOOLEAN flagBono = 0;
	BYTE *p_tabla3;
	// BOOLEAN flagOtherTrans = FALSE;
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	gIdTabla4 = OpenFile(KTabla4File);
	GetTablaCuatro(INPUT.w_host_idx, &tabla_cuatro);
	memset(nomEstablecimiento, 0x00, sizeof(nomEstablecimiento));
	memset(bufferaux, 0x00, sizeof(bufferaux));

	for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));
		if (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0
				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)
						== 0))
			break;
	}

	if (rec_cnt2 == rec_cnt)
	{
		FreeMW(nomAux);
		CloseFile(gIdTabla4);
		return FALSE;
	}

	GetTablaCero(&tabla_cero);

	memcpy(bufferaux,((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
	memcpy(nomEstablecimiento, gTablaCero.b_nom_establ, 22);
	RTrim(nomEstablecimiento, ' ');
	LTrim(nomEstablecimiento, ' ');
	strcat(nomEstablecimiento," * ");
	strcat(nomEstablecimiento,bufferaux);
	memcpy(nomAux, nomEstablecimiento, strlen(nomEstablecimiento));		//**SR** 03/10/13

	p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));

	MsgBufSetup();

	//virtual = 0;
	if (virtual == 0)
	{
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_space(19);
		pack_str("CREDIBANCO");
		pack_lf();
		//pack_space(14);
		ReadRTC(&P_DATA.s_dtg); //LFGD
		PackDateTimeVisa(&P_DATA.s_dtg);
		pack_lf();
		packVersion(FALSE); //MFBC/28/02/13
		pack_lf();
		CodEstablecimiento();
		pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomAux)); //MFBC/07/05/13
		pack_mem(nomAux, strlen(nomAux)); //MFBC/07/05/13
		//		pack_space(14);
		//		pack_mem(gTablaCero.b_nom_establ, 23);
		pack_lf();
		pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
		pack_space(21);
		pack_str("TER: ");
		pack_mem(gTablaCuatro.b_cod_terminal, 8);
		if(aRePrint != TRUE && aRePrint != 0x03){		// si es re impresion para el segundo comercio no imprime el titulo  	// **SR** 03/10/13
			pack_lf();
			pack_mem(MW_LPT_FONT_ENLARGED, 3);
			pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
			pack_str(" INFORME DETALLADO");
			pack_mem(MW_LPT_FONT_SMALL, 3);
			pack_lf();
			pack_mem(((struct TABLA_12*) p_tabla12)->b_establ_virtual,9);
			pack_space(MAX_CHAR_LINE_SMALL - 9 - sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ)); //MFBC/07/05/13
			pack_mem(((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
			pack_lf();
		}
		pack_mem(MW_LPT_FONT_SMALL, 3);
		//		pack_byte(0x1B);
		//		pack_byte(0x46);
		//		pack_byte(0x31);

	}
	else if (virtual == 1)
	{
		if(aRePrint != TRUE && aRePrint != 0x03){		// si es re impresion para el segundo comercio no imprime el titulo  	// **SR** 03/10/13
			pack_mem(MW_LPT_FONT_ENLARGED, 3);
			pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
			pack_str(" INFORME DETALLADO");
			pack_mem(MW_LPT_FONT_SMALL, 3);
			pack_lf();
			pack_mem(((struct TABLA_12*) p_tabla12)->b_establ_virtual,9);
			pack_space(MAX_CHAR_LINE_SMALL - 9 - sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ)); //MFBC/07/05/13
			pack_mem(((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
			pack_lf();
		}
		pack_mem(MW_LPT_FONT_SMALL, 3);
		//		pack_byte(0x1B);
		//		pack_byte(0x46);
		//		pack_byte(0x31);

		pack_mem(INPUT.sb_comercio_v, 9);
		pack_space(14);
		pack_mem(INPUT.sb_nom_comer_v, 21);
		pack_lf();
	}
	else if (virtual == 2)
	{
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_space(19);
		pack_str("CREDIBANCO");
		pack_lf();
		//	pack_space(14);
		ReadRTC(&P_DATA.s_dtg); //LFGD
		PackDateTimeVisa(&P_DATA.s_dtg);
		pack_lf();
		packVersion(FALSE); //MFBC/28/02/13
		//PrintDate();
		pack_lf();

		CodEstablecimiento();
		pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomAux)); //MFBC/07/05/13
		pack_mem(nomAux, strlen(nomAux)); //MFBC/07/05/13
		pack_lf();
		pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
		pack_space(21);
		pack_str("TER: ");
		pack_mem(gTablaCuatro.b_cod_terminal, 8);
		if(aRePrint != TRUE && aRePrint != 0x03){		// si es re impresion para el segundo comercio no imprime el titulo  	// **SR** 03/10/13
			pack_lf();
			pack_mem(MW_LPT_FONT_ENLARGED, 3);
			pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
			pack_str(" INFORME DETALLADO");
			pack_mem(MW_LPT_FONT_SMALL, 3);
			pack_lf();
			pack_mem(((struct TABLA_12*) p_tabla12)->b_establ_virtual,9);
			pack_space(MAX_CHAR_LINE_SMALL - 9 - sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ)); //MFBC/07/05/13
			pack_mem(((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
			pack_lf();
		}
		pack_mem(MW_LPT_FONT_SMALL, 3);
		//		pack_byte(0x1B);
		//		pack_byte(0x46);
		//		pack_byte(0x31);
		if(gTablaDoce.b_id > 0){				//  Si es multicomercion Imprime el comercio abajo del titulo		// **SR** 25/09/13
			pack_mem(INPUT.sb_comercio_v, 9);
			pack_space(14);
			pack_mem(INPUT.sb_nom_comer_v, 21);
			pack_lf();
		}
	}

	FreeMW(nomAux);

	for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
	{
		flagDebito = 0;
		flagCredito = 0;
		flagElectron = 0;
		flagBono = 0;
		//		flagPuntos = 0;
		//issuer_idx2 = 0;

		if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
			continue;

		if(memcmp(gTablaTres.b_nom_emisor, "CMR", 3) == 0)   // agregue esta linea por que CMR no debe tener detallado(solo por otras trajetas)
			continue;

		contTrans = 0;
		for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
		{
			APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

			contTrans++;

			if ((gTablaTres.b_emisor_id - 1 == RECORD_BUF.w_issuer_idx)
					&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
							== 0) && (memcmp(INPUT.sb_terminal_id,
									RECORD_BUF.sb_terminal_id, 8) == 0))
			{
				//issuer_idx2 = issuer_idx + 1;
				//                if (RECORD_BUF.b_trans != BONO_OBSEQUIO && memcmp(gTablaTres.b_nom_emisor, "BONOREGALO", 10) != 0)
				//                {
				//                    flagOtherTrans = TRUE;
				//                }
				//
				switch (RECORD_BUF.b_tipo_cuenta)
				{
				case 0x00:
					flagDebito = 1;
					break;
				case 0x30:
					flagCredito = 1;
					break;
				case 0x10:
				case 0x20:
				case 0x40:
					flagElectron = 1;
					break;
				}

			}
		}

		for (rec_idx = 0; rec_idx < 4; rec_idx++)
		{
			switch (rec_idx)
			{
			case 0:
				if (flagDebito == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);  LFGD//02/20/2013
					pack_mem("  DEBITO ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);

					pack_lf();
					pack_str(
							"TARJETA      RECIBO      TIPO           COMPRA ");
					pack_lf();
					tmpCuenta = 0x00;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
				}
				break;
			case 1:

				if (flagElectron == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);    LFGD//02/20/2013
					pack_mem("ELECTRON ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);

					pack_lf();
					pack_str("TARJETA      RECIBO      TIPO           COMPRA ");
					pack_lf();
					//	PackDetalles(RECORD_BUF.b_tipo_cuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
					tmpCuenta = 0x10;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
					tmpCuenta = 0x20;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
					tmpCuenta = 0x40;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
				}

				break;
			case 2:
				if (flagCredito == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);    LFGD//02/20/2013
					pack_mem(" CREDITO ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);

					pack_lf();
					pack_str("TARJETA      RECIBO      TIPO           COMPRA ");
					pack_lf();
					tmpCuenta = 0x30;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
				}
				break;

			case 3:

				break;
			}
		}
	}

	if (contTrans == 0)
	{
		LongBeep();
		TextColor("NO EXISTEN TRANS", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT
				| MW_CLRDISP, 3);
		FreeMW(p_tabla3);
		CloseFile(gIdTabla4);
		return FALSE;
	}

	pack_nbyte('=', 48);

	//	if (!virtual)
	//		pack_lfs(5); //kt-280213
	//	else
	//		pack_lfs(4); //kt-280213
	if(aRePrint != 0x02 && aRePrint != TRUE)			// para cuando sea reporte detallado de todos lo comercion no imprime espacios
		pack_lfs(5);
	pack_lfs(5);

	FreeMW(p_tabla3);
	CloseFile(gIdTabla4);

	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	return TRUE;
}

//*****************************************************************************
//  Function        : DisplayBatch
//  Description     : Display batch record.
//  Input           : record index;     // display with specify record index.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int DisplayBatch(WORD rec_cnt, WORD max_issuer, WORD virtual) //kt-150413
{
	while(TRUE)
	{
		struct TABLA_CERO tabla_cero;
		struct TABLA_CUATRO tabla_cuatro;

		WORD rec_cnt2, rec_idx, issuer_idx, contTrans = 0;
		BYTE tmpbuf[MW_MAX_LINESIZE + 1];
		BYTE tmpCuenta = 0;
		BOOLEAN flagDebito = 0;
		BOOLEAN flagCredito = 0;
		BOOLEAN flagElectron = 0;
		BOOLEAN flagBono = 0;
		BYTE CodEstablecimiento[36];
		BYTE *p_tabla3;
		int Aux = 0;

		memset(tmpbuf, 0x00, sizeof(tmpbuf));

		gIdTabla4 = OpenFile(KTabla4File);
		GetTablaCuatro(INPUT.w_host_idx, &tabla_cuatro);
		for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
		{
			APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));
			if (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0
					&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)
							== 0))
				break;
		}

		if (rec_cnt2 == rec_cnt)
		{
			CloseFile(gIdTabla4);
			//            printf("\fDEBUG A");
			//            APM_WaitKey(9000,0);
			return -1;
		}

		GetTablaCero(&tabla_cero);

		p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));

		memset(CodEstablecimiento, 0x20, sizeof(CodEstablecimiento));

		if (virtual == 0)
		{
			memcpy(CodEstablecimiento,  gTablaDoce.b_establ_virtual, 9); //kt-110912
			LTrim(CodEstablecimiento, 0x20);
			sprintf(&CodEstablecimiento[11], "%s", gTablaDoce.b_nom_establ);

			//TextColor("INFORME DETALLADO", MW_LINE1, COLOR_GREEN, MW_SPFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor(CodEstablecimiento, MW_LINE1, COLOR_GREEN, MW_SPFONT
					| MW_RIGHT | MW_CLRDISP, 0);
			TextColor("ENTER<SIG PAG>  CANCEL<FIN>", MW_LINE9, COLOR_BLACK,
					MW_SPFONT | MW_CENTER | MW_REVERSE, 0);
		}
		else if (virtual == 1)
		{
			memset(CodEstablecimiento, 0x20, sizeof(CodEstablecimiento));
			memcpy(CodEstablecimiento, INPUT.sb_comercio_v, 11);
			sprintf(&CodEstablecimiento[12], "%s", INPUT.sb_nom_comer_v);

			//TextColor("INFORME DETALLADO", MW_LINE1, COLOR_GREEN, MW_SPFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor(CodEstablecimiento, MW_LINE1, COLOR_GREEN, MW_SPFONT
					| MW_RIGHT | MW_CLRDISP, 0);
			TextColor("ENTER<SIG PAG>  CANCEL<FIN>", MW_LINE9, COLOR_BLACK,
					MW_SPFONT | MW_CENTER | MW_REVERSE, 0);
		}

		for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
		{
			flagDebito = 0;
			flagCredito = 0;
			flagElectron = 0;
			flagBono = 0;

			//memset(bufferDisplay, 0x00, sizeof(bufferDisplay));

			if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
				continue;

			contTrans = 0;
			for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
			{
				APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

				contTrans++;

				if ((gTablaTres.b_emisor_id - 1 == RECORD_BUF.w_issuer_idx)
						&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
								== 0) && (memcmp(INPUT.sb_terminal_id,
										RECORD_BUF.sb_terminal_id, 8) == 0))
				{

					switch (RECORD_BUF.b_tipo_cuenta)
					{
					case 0x00:
						flagDebito = 1;
						break;
					case 0x30:
						flagCredito = 1;
						break;
					case 0x10:
					case 0x20:
					case 0x40:
						flagElectron = 1;
						break;
					}
				}
			}

			if(flagDebito == 1)
				g_TypeCount = 0x00;
			if(flagElectron == 1)
				g_TypeCount = 0x40;
			if(flagCredito == 1)
				g_TypeCount = 0x30;


			for (rec_idx = 0; rec_idx < 4; rec_idx++)
			{
				switch (rec_idx)
				{
				case 0:
					if (flagDebito == 1)
					{
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
						sprintf(&tmpbuf[10], "  DEBITO ");

						TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
								| MW_CENTER, 0);

						TextColor("TARJ RECIBO TIPO    COMPRA", MW_LINE3,
								COLOR_BLACK,
								MW_SPFONT | MW_CENTER | MW_REVERSE, 0);

						tmpCuenta = 0x00;
						Aux = PackDetallesPantalla(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
						if (Aux == FALSE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return FALSE;
						}
						else if (Aux == TRUE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return TRUE;
						}

					}
					break;
				case 1:
					if (flagElectron == 1)
					{
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
						sprintf(&tmpbuf[10], " ELECTRON ");

						TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
								| MW_CENTER, 0);
						TextColor("TARJ RECIBO TIPO    COMPRA", MW_LINE3,
								COLOR_BLACK, MW_SPFONT | MW_CENTER | MW_REVERSE, 0);

						tmpCuenta = 0x10;
						Aux = PackDetallesPantalla(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
						if (Aux == FALSE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return FALSE;
						}
						else if (Aux == TRUE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return TRUE;
						}
						tmpCuenta = 0x20;

						Aux = PackDetallesPantalla(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
						if (Aux == FALSE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return FALSE;
						}
						else if(Aux == TRUE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return TRUE;
						}

						tmpCuenta = 0x40;

						Aux = PackDetallesPantalla(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
						if (Aux == FALSE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return FALSE;
						}
						else if (Aux == TRUE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return TRUE;
						}
					}
					break;
				case 2:

					if (flagCredito == 1)
					{
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
						sprintf(&tmpbuf[10], " CREDITO ");

						TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
								| MW_CENTER, 0);
						TextColor("TARJ RECIBO TIPO    COMPRA", MW_LINE3,
								COLOR_BLACK, MW_SPFONT | MW_CENTER | MW_REVERSE, 0);

						tmpCuenta = 0x30;
						Aux = PackDetallesPantalla(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
						if (Aux == FALSE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							//                            printf("\fDEBUG C");
							//                            APM_WaitKey(9000,0);
							return FALSE;
						}
						else if(Aux == TRUE){
							FreeMW(p_tabla3);
							CloseFile(gIdTabla4);
							return TRUE;
						}

					}

					break;
					//				case 3:
					//					if (flagBono == 1)
					//					{
					//						memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
					//						sprintf(&tmpbuf[10], "  CARGA ");
					//
					//						TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
					//								| MW_CENTER, 0);
					//						TextColor("TARJ RECIBO TIPO          ", MW_LINE3,
					//								COLOR_BLACK, MW_SPFONT | MW_CENTER | MW_REVERSE, 0);
					//						tmpCuenta = 0x00;
					//						if (!PackCargaBonoPantalla(tmpCuenta, rec_cnt,
					//								gTablaTres.b_emisor_id - 1))
					//						{
					//						FreeMW(p_tabla3);
					//						CloseFile(gIdTabla4);
					//							return FALSE;
					//						}
					//					}
					//					break;
				}
			}
		}

		if (contTrans == 0)
		{
			LongBeep();
			TextColor("NO EXISTEN TRANS", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT
					| MW_CLRDISP, 3);
			FreeMW(p_tabla3);
			CloseFile(gIdTabla4);
			return FALSE;
		}

		FreeMW(p_tabla3);
		CloseFile(gIdTabla4);
	}
	//return TRUE;
	//    printf("\fDEBUG B");
	//    APM_WaitKey(9000,0);
}

//*****************************************************************************
//  Function        : DisplayTotales
//  Description     : Display batch record.
//  Input           : record index;     // display with specify record index.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int DisplayTotales(WORD rec_cnt, WORD max_issuer, WORD virtual) //kt-220413
{
	while (TRUE)
	{
		struct TABLA_CERO tabla_cero;
		WORD rec_cnt2, rec_idx, issuer_idx, contTrans = 0; //ktkt
		BYTE tmpCuenta = 0;
		BYTE tmpbuf[MW_MAX_LINESIZE + 1];
		BYTE CodEstablecimiento[36];
		BOOLEAN flagDebito = 0;
		BOOLEAN flagCredito = 0;
		BOOLEAN flagElectron = 0;
		BOOLEAN flagBono = 0;
		BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
		BYTE *p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));
		int Aux = 0;

		//PackReportPuntos(rec_cnt, max_issuer);

		for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
		{
			APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

			if (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0
					&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)== 0)) //kt-190413
				break;
		}

		if (rec_cnt2 == rec_cnt) //kt-190413
		{
			//			LongBeep();
			//			TextColor("NO EXISTEN TRANS 1", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT
			//					| MW_CLRDISP, 3);
			FreeMW(p_tabla3);
			FreeMW(p_tabla4);
			return -1;
		}

		gIdTabla4 = OpenFile(KTabla4File);


		GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);

		GetTablaCero(&tabla_cero);

		//MsgBufSetup();

		memset(CodEstablecimiento, 0x20, sizeof(CodEstablecimiento));

		if (virtual == 0)
		{
			memcpy(CodEstablecimiento, gTablaDoce.b_establ_virtual, 9); //kt-110912
			//			memcpy(CodEstablecimiento, &gTablaCuatro.b_cod_estable[4], 11); //kt-110912
			LTrim(CodEstablecimiento, 0x20);
			sprintf(&CodEstablecimiento[11], "%s", gTablaDoce.b_nom_establ);

			//TextColor("INFORME DETALLADO", MW_LINE1, COLOR_GREEN, MW_SPFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor(CodEstablecimiento, MW_LINE1, COLOR_GREEN, MW_SPFONT
					| MW_RIGHT | MW_CLRDISP, 0);
			TextColor("ENTER<SIG PAG>  CANCEL<FIN>", MW_LINE9, COLOR_BLACK,
					MW_SPFONT | MW_CENTER | MW_REVERSE, 0);
		}
		else if (virtual == 1)
		{
			memset(CodEstablecimiento, 0x20, sizeof(CodEstablecimiento));
			memcpy(CodEstablecimiento, INPUT.sb_comercio_v, 11);
			sprintf(&CodEstablecimiento[12], "%s", INPUT.sb_nom_comer_v);

			//TextColor("INFORME DETALLADO", MW_LINE1, COLOR_GREEN, MW_SPFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor(CodEstablecimiento, MW_LINE1, COLOR_GREEN, MW_SPFONT
					| MW_RIGHT | MW_CLRDISP, 0);
			TextColor("ENTER<SIG PAG>  CANCEL<FIN>", MW_LINE9, COLOR_BLACK,
					MW_SPFONT | MW_CENTER | MW_REVERSE, 0);
		}

		for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
		{
			flagDebito = 0;
			flagCredito = 0;
			flagElectron = 0;
			flagBono = 0;

			if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
				continue;

			contTrans = 0; //ktkt
			for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
			{
				APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

				contTrans++; //ktkt

				if ((gTablaTres.b_emisor_id - 1 == RECORD_BUF.w_issuer_idx)
						&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
								== 0) && (memcmp(INPUT.sb_terminal_id,
										RECORD_BUF.sb_terminal_id, 8) == 0))
				{

					switch (RECORD_BUF.b_tipo_cuenta)
					{
					case 0x00:
						flagDebito = 1;
						break;
					case 0x30:
						flagCredito = 1;
						break;
					default:
						flagElectron = 1;
						break;
					}
					//}
				}

			}

			for (rec_idx = 0; rec_idx < 4; rec_idx++)
			{
				switch (rec_idx)
				{
				case 0:
					if (flagDebito == 1)
					{
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
						sprintf(&tmpbuf[10], "  DEBITO ");

						TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
								| MW_CENTER | MW_REVERSE, 0);

						tmpCuenta = 0x00;

						Aux = PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1, TRUE);
						if (Aux == FALSE){
							CloseFile(gIdTabla4);
							FreeMW(p_tabla3);
							FreeMW(p_tabla4);
							return FALSE;
						}
						else if(Aux ==  2){
							CloseFile(gIdTabla4);
							FreeMW(p_tabla3);
							FreeMW(p_tabla4);
							return 2;
						}

					}
					break;
				case 1:
					if (flagElectron == 1) //kt-090413
					{
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
						sprintf(&tmpbuf[10], "ELECTRON ");

						TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
								| MW_CENTER | MW_REVERSE, 0);

						pack_lf();

						tmpCuenta = 0x11;
						Aux = PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1, TRUE);
						if (Aux == FALSE){
							CloseFile(gIdTabla4);
							FreeMW(p_tabla3);
							FreeMW(p_tabla4);
							return FALSE;
						}
						else if (Aux == 2){
							CloseFile(gIdTabla4);
							FreeMW(p_tabla3);
							FreeMW(p_tabla4);
							return 2;
						}

					}
					break;
				case 2:

					if (flagCredito == 1)
					{
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
						sprintf(&tmpbuf[10], "CREDITO ");

						TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
								| MW_CENTER | MW_REVERSE, 0);

						tmpCuenta = 0x30;
						Aux = PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id- 1, TRUE);
						if (Aux == FALSE){
							CloseFile(gIdTabla4);
							FreeMW(p_tabla3);
							FreeMW(p_tabla4);
							return FALSE;
						}
						else if (Aux == 2){
							CloseFile(gIdTabla4);
							FreeMW(p_tabla3);
							FreeMW(p_tabla4);
							return 2;
						}
					}

					break;
					//				case 3:
					//					if (flagBono == 1)
					//					{
					//					memcpy(tmpbuf, gTablaTres.b_nom_emisor, 10);
					//					sprintf(&tmpbuf[10], "    CARGA");
					//
					//					TextColor(tmpbuf, MW_LINE2, COLOR_VISABLUE, MW_SMFONT
					//							| MW_CENTER | MW_REVERSE, 0);
					//
					//					pack_lf();
					//
					//					tmpCuenta = 0x12;
					//					if (!PackTotalCarga(tmpCuenta, rec_cnt, TRUE))
					//					{
					//						CloseFile(gIdTabla4);
					//						FreeMW(p_tabla3);
					//						FreeMW(p_tabla4);
					//
					//						return FALSE;
					//					}
					//					}
				}
			}
		}

		if (contTrans == 0) //ktkt
		{
			//			LongBeep();
			//			TextColor("NO EXISTEN TRANS 2", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT
			//					| MW_CLRDISP, 3);
			FreeMW(p_tabla3);
			FreeMW(p_tabla4);
			CloseFile(gIdTabla4);
			return -1;
		}

		Aux = PackIssuerTotalsVISA(RECORD_BUF.w_issuer_idx, 1, TRUE);


		if (Aux  == FALSE){
			FreeMW(p_tabla3);
			FreeMW(p_tabla4);
			CloseFile(gIdTabla4);
			return FALSE;
		}
		else if(Aux == 2){
			FreeMW(p_tabla3);
			FreeMW(p_tabla4);
			CloseFile(gIdTabla4);
			return 2;
		}

		//flagAnul = FALSE;

		CloseFile(gIdTabla4);
		FreeMW(p_tabla3);
		FreeMW(p_tabla4);
	}
	//return TRUE;

}

//*****************************************************************************
//  Function        : PackReportComercio
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//***************************************************************************
void PackReportComercio(BOOLEAN ReporteDetallado, BOOLEAN ReporteDisplay) //kt-110413
{
	int contador = 0;
	//	WORD rec_cnt, CntComercio, tipoComercio;
	WORD rec_cnt;
	WORD maxComVirtual = GetTabla12Count();
	WORD maxContIssuer = GetTabla3Count();
	BYTE *temp_tabla12;
	struct TABLA_TRES tabla_tres;
	//	BOOLEAN f_RePrintDet = FALSE;
	//	int ret;

	gf_MultiComer = FALSE;
	g_TypeCount = 0xFF; 	// se coloca el valor en FF para que no sea igual que el 0x00 De Debito

	rec_cnt = APM_GetRecCount();
	if (rec_cnt == 0)
	{
		RSP_DATA.w_rspcode = 'N' * 256 + 'T';
		DispCtrlMW(MW_CLR_DISP);
		DispRspText(FALSE);
		ErrorDelay();
		return;
	}

	gIdTabla3 = OpenFile(KTabla3File);

	for (contador = 0; contador < rec_cnt; contador++)
	{
		APM_GetBatchRec(contador, &RECORD_BUF, sizeof(RECORD_BUF));

		GetTablaTres(RECORD_BUF.w_issuer_idx, &tabla_tres);

		if (  (memcmp(gTablaTres.b_nom_emisor, "CMR", 3) == 0) && (ReporteDetallado == TRUE) )
			continue;

		break;

	}

	CloseFile(gIdTabla3);

	if (contador == rec_cnt)
	{
		RSP_DATA.w_rspcode = 'N' * 256 + 'T';
		DispCtrlMW(MW_CLR_DISP);
		DispRspText(FALSE);
		ErrorDelay();
		return;
	}

	gIdTabla3 = OpenFile(KTabla3File);
	gIdTabla12 = OpenFile(KTabla12File);

	//	tipoComercio = ChoiseMerchant(maxComVirtual);
	ChoiseMerchant(maxComVirtual);

	//	if (tipoComercio == 0)
	//	{
	//		CloseFile(gIdTabla3);
	//		CloseFile(gIdTabla12);
	//		return;
	//	}

	temp_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));

	//	if (tipoComercio == 2)
	//	{

	if (ReporteDisplay)
	{
		if (ReporteDetallado){
			if(DisplayBatch(rec_cnt, maxContIssuer, FALSE) == TRUE){
				FreeMW(temp_tabla12);
				CloseFile(gIdTabla3);
				CloseFile(gIdTabla12);
				return;
			}
		}
		else{
			if(DisplayTotales(rec_cnt, maxContIssuer, FALSE) == 2){
				FreeMW(temp_tabla12);
				CloseFile(gIdTabla3);
				CloseFile(gIdTabla12);
				return;
			}
		}
	}
	else
	{
		if (ReporteDetallado){
			PackVoucherSettle(rec_cnt, maxContIssuer, FALSE, 0x02);			// **SR** Con 0x02 imprime el cabecera "INFORME DETALLADO" pero no impreme los espacios	**SR** 04/10/13
		}
		else
			PackVoucherSettleTotal(rec_cnt, maxContIssuer, FALSE, FALSE);
	}
	//	for (CntComercio = 0; CntComercio < maxComVirtual; CntComercio++)
	//	{
	//		f_RePrintDet =  TRUE;		// Mientras esta en TRUE no me imprime cabecera "INFORME DETALLADO"   **SR** 04/10/13
	//		if(CntComercio == maxComVirtual - 1)
	//			f_RePrintDet = 0x03;		// Cuando es el ultmo comercion con este valor imprme los espacios fatantes en el Voucher ni la cabecera "INFORME DETALLADO" **SR** 04/10/13
	//
	//		if (!GetTablaDoce(CntComercio, (struct TABLA_12*) temp_tabla12))
	//			continue;
	//
	//		memcpy(INPUT.sb_comercio_v,	((struct TABLA_12*) temp_tabla12)->b_establ_virtual, 9);
	//		memcpy(INPUT.sb_terminal_id,((struct TABLA_12*) temp_tabla12)->b_term_virtual, 8);
	//		memcpy(INPUT.sb_nom_comer_v,((struct TABLA_12*) temp_tabla12)->b_nom_establ, 15);
	//
	//		if (ReporteDisplay)
	//		{
	//			if (ReporteDetallado)
	//			{
	//				if (!DisplayBatch(rec_cnt, maxContIssuer, TRUE))
	//					continue;
	//				else{
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//					return;
	//				}
	//			}
	//			else
	//			{
	//				if (DisplayTotales(rec_cnt, maxContIssuer, TRUE) == FALSE)
	//					continue;
	//				else{
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//					return;
	//				}
	//			}
	//		}
	//		else
	//		{
	//			if (ReporteDetallado)
	//			{
	//				if (!PackVoucherSettle(rec_cnt, maxContIssuer, TRUE, f_RePrintDet))
	//					continue;
	//			}
	//			else
	//			{
	//				if (!PackVoucherSettleTotal(rec_cnt, maxContIssuer, FALSE,
	//						TRUE))
	//					continue;
	//			}
	//		}
	//	}
	//	}
	//	else if (tipoComercio == 1)
	//	{
	//
	//		if (ReporteDisplay)
	//		{
	//			if (ReporteDetallado)
	//			{
	//				ret = DisplayBatch(rec_cnt, maxContIssuer, TRUE);
	//				if (ret == -1)
	//				{
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//
	//					TextColor("NO EXISTEN", MW_LINE3, COLOR_RED, MW_SMFONT|MW_CLRDISP|MW_CENTER, 0);
	//					TextColor("TRANSACCIONES", MW_LINE4, COLOR_RED, MW_SMFONT|MW_CENTER, 3);
	//
	//					return;
	//				}
	//				if (ret == FALSE || ret == TRUE)
	//				{
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//					return;
	//				}
	//			}
	//			else
	//			{
	//				ret = DisplayTotales(rec_cnt, maxContIssuer, TRUE);
	//				if ( ret == -1)
	//				{
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//
	//					TextColor("NO EXISTEN", MW_LINE3, COLOR_RED, MW_SMFONT|MW_CLRDISP|MW_CENTER, 0);
	//					TextColor("TRANSACCIONES", MW_LINE4, COLOR_RED, MW_SMFONT|MW_CENTER, 3);
	//					return;
	//				}
	//				if ( ret == FALSE || ret == 2)
	//				{
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//					return;
	//				}
	//			}
	//		}
	//		else
	//		{
	//			if (ReporteDetallado)
	//			{
	//				if (!PackVoucherSettle(rec_cnt, maxContIssuer, 2, FALSE)) //kt-110413
	//				{
	//					/*			RSP_DATA.w_rspcode = 'N' * 256 + 'T';
	//					 DispCtrlMW(MW_CLR_DISP);
	//					 DispRspText(FALSE);
	//					 ErrorDelay();*/
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//
	//					TextColor("NO EXISTEN", MW_LINE3, COLOR_RED, MW_SMFONT|MW_CLRDISP|MW_CENTER, 0);
	//					TextColor("TRANSACCIONES", MW_LINE4, COLOR_RED, MW_SMFONT|MW_CENTER, 3);
	//					return;
	//				}
	//			}
	//			else
	//			{
	//
	//				if (!PackVoucherSettleTotal(rec_cnt, maxContIssuer, FALSE, 2))
	//				{
	//					//				RSP_DATA.w_rspcode = 'N' * 256 + 'T';
	//					//				DispCtrlMW(MW_CLR_DISP);
	//					//				DispRspText(FALSE);
	//					//				ErrorDelay();
	//					FreeMW(temp_tabla12);
	//					CloseFile(gIdTabla3);
	//					CloseFile(gIdTabla12);
	//					return;
	//				}
	//			}
	//		}
	//	}


	FreeMW(temp_tabla12);
	CloseFile(gIdTabla3);
	CloseFile(gIdTabla12);
	return;
}

void PackCierreMulticomercio(void) //kt-180413
{
	struct TABLA_CUATRO tabla_cuatro;
	struct TABLA_CERO tabla_cero;
	BYTE nomEstablecimiento[32 + 1];
	BYTE bufferaux[16 + 1];
	BYTE CodAgregado[15 + 1];
	BYTE *nomAux = MallocMW(32);

	GetTablaCero(&tabla_cero);
	memset(nomEstablecimiento, 0x00, sizeof(nomEstablecimiento));
	memset(bufferaux, 0x00, sizeof(bufferaux));
	memset(CodAgregado, 0x00, sizeof(CodAgregado));
	memcpy(bufferaux,INPUT.sb_nom_comer_v,15);
	memcpy(nomEstablecimiento, gTablaCero.b_nom_establ, 22);
	RTrim(nomEstablecimiento, ' ');
	LTrim(nomEstablecimiento, ' ');
	strcat(nomEstablecimiento," * ");
	strcat(nomEstablecimiento,bufferaux);
	memcpy(nomAux, nomEstablecimiento, strlen (nomEstablecimiento) );
	gIdTabla4 = OpenFile(KTabla4File);

	//printf("DEBUG 1"); APM_WaitKey(9000,0);

	GetTablaCuatro(INPUT.w_host_idx, &tabla_cuatro);

	MsgBufSetup();
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();
	pack_space(19);
	pack_str("CREDIBANCO");
	pack_lf();
	//pack_space(14);

	//printf("DEBUG 2"); APM_WaitKey(9000,0);
	ReadRTC(&P_DATA.s_dtg);
	PackDateTimeVisa(&P_DATA.s_dtg);
	packVersion(FALSE);

	pack_lf();
	//	CodEstablecimiento();
	memcpy(CodAgregado, gTablaDoce.b_establ_virtual, sizeof(gTablaDoce.b_establ_virtual)); //kt-110912
	LTrim(CodAgregado, 0x20);
	pack_mem(CodAgregado, strlen(CodAgregado));
	pack_space(MAX_CHAR_LINE_SMALL - 9 - strlen(nomAux)); //MFBC/07/05/13
	pack_mem(nomAux, strlen(nomAux)); //MFBC/07/05/13
	pack_lf();
	pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
	pack_space(21);
	pack_str("TER: ");
	pack_mem(gTablaCuatro.b_cod_terminal, 8);
	pack_lf();

	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_space((MAX_CHAR_LINE_NORMAL - 17) / 2);
	pack_str("CIERRE COMPLETADO");
	//pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf(); //kt-190413
	pack_nbyte('=', 32);
	pack_lfs(6);
	FreeMW(nomAux);
	CloseFile(gIdTabla4);
	PackMsgBufLen();

	//printf("DEBUG 4"); APM_WaitKey(9000,0);

	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);

	//printf("DEBUG 5"); APM_WaitKey(9000,0);
	return;
}

BOOLEAN PackVoucherSettleTotal(WORD rec_cnt, WORD max_issuer, BOOLEAN cierre,
		WORD virtual) //kt-220413
{
	struct TABLA_CERO tabla_cero;
	struct TABLA_CUATRO p_tabla4;
	struct TABLA_TRES p_tabla3;
	WORD rec_cnt2, rec_idx, issuer_idx, contTrans = 0; //ktkt
	BYTE tmpCuenta = 0;
	BOOLEAN flagDebito = 0;
	BOOLEAN flagCredito = 0;
	BOOLEAN flagElectron = 0;
	BOOLEAN flagBono = 0;
	BOOLEAN flagFacturas = FALSE;
	//   BOOLEAN flagOtherTrans = FALSE;
	BYTE nomEstablecimiento[32 + 1];
	BYTE bufferaux[16];
	BYTE *nomAux = MallocMW(32);
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);

	memset(nomEstablecimiento, 0x00, sizeof(nomEstablecimiento));
	memset(bufferaux, 0x00, sizeof(bufferaux));


	for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));
		flagFacturas = TRUE;
	}
	for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

		if ((memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)== 0))
			break;

	}

	if (rec_cnt2 == rec_cnt)
	{
		FreeMW(nomAux);
		//		FreeMW(p_tabla3);
		//		FreeMW(p_tabla4);
		return FALSE;
	}

	gIdTabla4 = OpenFile(KTabla4File);

	//GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);
	GetTablaCuatro(INPUT.w_host_idx, &p_tabla4);

	GetTablaCero(&tabla_cero);

	memcpy(bufferaux,((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
	memcpy(nomEstablecimiento, gTablaCero.b_nom_establ, 22);
	RTrim(nomEstablecimiento, ' ');
	LTrim(nomEstablecimiento, ' ');
	strcat(nomEstablecimiento," * ");
	strcat(nomEstablecimiento,bufferaux);
	memcpy(nomAux, nomEstablecimiento, strlen(nomEstablecimiento));

	//printf("\f estab <%s>", nomEstablecimiento); WaitKey(3000, 0);
	flagAnul = FALSE; //kt-230213
	//printf("\f estabDos <%s>", nomEstablecimiento); WaitKey(3000, 0);
	MsgBufSetup();



	if (virtual == 0)
	{
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_space(19);
		pack_str("CREDIBANCO");
		pack_lf();
		//		pack_space(14);
		ReadRTC(&P_DATA.s_dtg);
		PackDateTimeVisa(&P_DATA.s_dtg);
		pack_lf();
		//		packVersion();			// En el reporte de totales No tinene que imprimir ni el hash ni la Version  **SR** 25/09/13
		packVersion(FALSE); //MFBC/09/10/13
		pack_lf();
		CodEstablecimiento();
		pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomAux)); //MFBC/07/05/13
		pack_mem(nomAux, strlen(nomAux)); //MFBC/07/05/13
		pack_lf();
		pack_mem(gTablaCero.b_dir_ciudad, 14);
		pack_space(21);
		pack_str("TER: ");
		pack_mem(gTablaCuatro.b_cod_terminal, 8);
		pack_lf();
		pack_mem(MW_LPT_FONT_ENLARGED, 3);
		pack_space((MAX_CHAR_LINE_NORMAL - 20) / 2);
		if (cierre == TRUE) //LFGD
			pack_str(" INFORME DE CIERRE ");
		else
			pack_str(" INFORME DE TOTALES ");
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_mem(((struct TABLA_12*) p_tabla12)->b_establ_virtual,9);
		pack_space(MAX_CHAR_LINE_SMALL - 9 - sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ)); //MFBC/07/05/13
		pack_mem(((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
		pack_lf();
	}
	else if (virtual == 1)
	{
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_space(19);
		pack_str("CREDIBANCO");
		pack_lf();
		//		pack_space(14);
		ReadRTC(&P_DATA.s_dtg); //LFGD
		PackDateTimeVisa(&P_DATA.s_dtg);
		pack_lf();
		//		packVersion(); //MFBC/28/02/13		// En el reporte de totales No tinene que imprimir ni el hash ni la Version  **SR** 25/09/13
		packVersion(FALSE); //MFBC/09/10/13
		pack_lf();
		CodEstablecimiento();
		pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomAux)); //MFBC/07/05/13
		pack_mem(nomAux, strlen(nomAux)); //MFBC/07/05/13
		pack_lf();
		pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
		pack_space(21);
		pack_str("TER: ");
		pack_mem(gTablaCuatro.b_cod_terminal, 8);
		pack_lf();
		pack_mem(MW_LPT_FONT_ENLARGED, 3);
		pack_space((MAX_CHAR_LINE_NORMAL - 20) / 2);
		if (cierre == TRUE) //LFGD
			pack_str(" INFORME DE CIERRE ");
		else
			pack_str(" INFORME DE TOTALES ");
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_mem(((struct TABLA_12*) p_tabla12)->b_establ_virtual,9);
		pack_space(MAX_CHAR_LINE_SMALL - 9 - sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ)); //MFBC/07/05/13
		pack_mem(((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
		pack_lf();
		if(gTablaDoce.b_id > 0 && gf_PrintTerMaster != TRUE){				//  Si es multicomercion Imprime el comercio abajo del titulo		// **SR** 25/09/13
			pack_mem(INPUT.sb_comercio_v, 9);
			pack_space(14);
			pack_mem(INPUT.sb_nom_comer_v, 21);
			pack_lf();
		}
	}
	else if (virtual == 2)
	{
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_space(19);
		pack_str("CREDIBANCO");
		pack_lf();
		//		pack_space(14);
		ReadRTC(&P_DATA.s_dtg); //LFGD
		PackDateTimeVisa(&P_DATA.s_dtg);
		pack_lf();
		//		packVersion(); //MFBC/28/02/13		// En el reporte de totales No tinene que imprimir ni el hash ni la Version  **SR** 25/09/13
		packVersion(FALSE); //MFBC/09/10/13
		pack_lf();
		CodEstablecimiento();
		pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomAux)); //MFBC/07/05/13
		pack_mem(nomAux, strlen(nomAux)); //MFBC/07/05/13
		pack_lf();
		pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
		pack_space(21);
		pack_str("TER: ");
		pack_mem(gTablaCuatro.b_cod_terminal, 8);
		pack_lf();
		pack_mem(MW_LPT_FONT_ENLARGED, 3);
		pack_space((MAX_CHAR_LINE_NORMAL - 20) / 2);
		if (cierre == TRUE) //LFGD
			pack_str(" INFORME DE CIERRE ");
		else
			pack_str(" INFORME DE TOTALES ");
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_mem(((struct TABLA_12*) p_tabla12)->b_establ_virtual,9);
		pack_space(MAX_CHAR_LINE_SMALL - 9 - sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ)); //MFBC/07/05/13
		pack_mem(((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
		pack_lf();
		if(gTablaDoce.b_id > 0 && gf_PrintTerMaster != TRUE){				//  Si es multicomercion Imprime el comercio abajo del titulo		// **SR** 25/09/13
			pack_mem(INPUT.sb_comercio_v, 9);
			pack_space(14);
			pack_mem(INPUT.sb_nom_comer_v, 21);
			pack_lf();
		}
	}

	FreeMW(nomAux);


	if(flagFacturas == TRUE)
	{

		for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
		{
			flagDebito = 0;
			flagCredito = 0;
			flagElectron = 0;
			flagBono = 0;

			//if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
			if (!GetTablaTres(issuer_idx, &p_tabla3))
				continue;

			contTrans = 0; //ktkt
			for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
			{
				APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

				contTrans++; //ktkt

				if ((gTablaTres.b_emisor_id - 1 == RECORD_BUF.w_issuer_idx)
						&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
								== 0) && (memcmp(INPUT.sb_terminal_id,
										RECORD_BUF.sb_terminal_id, 8) == 0))
				{
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);

					//                if (RECORD_BUF.b_trans != BONO_OBSEQUIO && memcmp(gTablaTres.b_nom_emisor, "BONOREGALO", 10) != 0)
					//                {
					//                    flagOtherTrans = TRUE;
					//                }

					switch (RECORD_BUF.b_tipo_cuenta)
					{
					case 0x00:
						flagDebito = 1;
						break;
					case 0x30:
						flagCredito = 1;
						break;
					default:
						flagElectron = 1;
						break;
					}
				}
			}

			for (rec_idx = 0; rec_idx < 4; rec_idx++)
			{
				switch (rec_idx)
				{
				case 0:
					if (flagDebito == 1)
					{
						pack_mem(MW_LPT_INVERTED_ON, 3);
						pack_mem(gTablaTres.b_nom_emisor, 10);
						pack_space(29);
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						pack_mem("DEBITO   ", 9);
						pack_mem(MW_LPT_INVERTED_OFF, 3);
						pack_lf();

						tmpCuenta = 0x00;
						PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1,
								FALSE);
					}
					break;
				case 1:
					if (flagElectron == 1) //kt-090413
					{
						pack_mem(MW_LPT_INVERTED_ON, 3);
						pack_mem(gTablaTres.b_nom_emisor, 10);
						pack_space(29);
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						pack_mem("ELECTRON ", 9);
						pack_mem(MW_LPT_INVERTED_OFF, 3);

						pack_lf();

						tmpCuenta = 0x11;
						PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1,
								FALSE);
					}
					break;
				case 2:

					if (flagCredito == 1)
					{
						pack_mem(MW_LPT_INVERTED_ON, 3);
						pack_mem(gTablaTres.b_nom_emisor, 10);
						pack_space(29);
						//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
						pack_mem("CREDITO  ", 9);
						pack_mem(MW_LPT_INVERTED_OFF, 3);
						pack_lf();

						tmpCuenta = 0x30;
						PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1,
								FALSE);
					}
					break;
				case 3:
					//                if (flagOtherTrans == TRUE)
					//                {
					if (flagBono == 1)
					{
						pack_mem(MW_LPT_INVERTED_ON, 3);
						pack_mem(gTablaTres.b_nom_emisor, 10);
						pack_space(29);
						pack_mem("CARGA    ", 9);
						pack_mem(MW_LPT_INVERTED_OFF, 3);
						pack_lf();

						tmpCuenta = 0x12;
						PackTotalCarga(tmpCuenta, rec_cnt, FALSE);
					}
					//  }

					break;
				}
			}

			/*	pack_mem(MW_LPT_INVERTED_ON, 3);
		 pack_mem("TOTAL ", 6);
		 pack_mem(gTablaTres.b_nom_emisor, 10);
		 pack_space(32);
		 pack_mem(MW_LPT_INVERTED_OFF, 3);
		 pack_lf();
		 PackIssuerTotalsVISA(RECORD_BUF.w_issuer_idx, 0, FALSE);
			 */
		}

		if (contTrans == 0) //ktkt
		{
			LongBeep();
			TextColor("NO EXISTEN TRANS", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT
					| MW_CLRDISP, 3);
			//		FreeMW(p_tabla3);
			//		FreeMW(p_tabla4);
			CloseFile(gIdTabla4);
			gf_PrintTerMaster  = FALSE;		// **SR**  04/10/13
			FreeMW(p_tabla12);
			return FALSE;
		}
	}

	pack_mem(MW_LPT_INVERTED_ON, 3);
	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_mem("          GRAN TOTAL          ", 30);
	pack_mem(MW_LPT_INVERTED_OFF, 3);
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();
	PackIssuerTotalsVISA(RECORD_BUF.w_issuer_idx, 1, FALSE);

	flagAnul = FALSE;

	pack_lfs(6);

	CloseFile(gIdTabla4);
	//	FreeMW(p_tabla3);
	//	FreeMW(p_tabla4);

	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	gf_PrintTerMaster  = FALSE;		// **SR**  04/10/13
	FreeMW(p_tabla12);
	return TRUE;
}

void NomEstablecimiento(void)
{
	BYTE Aux1[50];
	memset(Aux1, 0x00, sizeof(Aux1));

	struct TABLA_CERO tabla_cero;
	GetTablaCero(&tabla_cero);

	memcpy(Aux1, gTablaCero.b_nom_establ, 21);
	RTrim(Aux1, ' ');
	LTrim(Aux1, ' ');
	pack_mem(Aux1, strlen(Aux1));

}

//*****************************************************************************
//  Function        : ReportComerciosVirtuales
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//***************************************************************************
void ReportComerciosVirtuales(void) //kt-211112
{
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));
	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
	struct TABLA_CERO tabla_cero;
	// TABLA_CUATRO tabla_cuatro;
	DWORD i;
	BYTE buffer[21];
	//	BYTE bufferaux[16];
	BYTE CodAgregado[15];
	BYTE nomEstablecimiento[23 + 1];
	memset(nomEstablecimiento, 0x00, sizeof(nomEstablecimiento));
	memset(buffer, 0x00, sizeof(buffer));
	//	memset(bufferaux, 0x00, sizeof(bufferaux));
	memset(CodAgregado, 0x00, sizeof(CodAgregado));

	WORD MaxCount12 = GetTabla12Count();
	gIdTabla12 = OpenFile(KTabla12File);

	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	gIdTabla4 = OpenFile(KTabla4File);
	GetTablaCuatro(0, (struct TABLA_CUATRO*) p_tabla4);
	//printf("\fMaxCount12 |%d|", MaxCount12);
	//APM_WaitKey(9000,0);

	if (((struct TABLA_12*) p_tabla12)->b_id <= 0)
	{
		FreeMW(p_tabla12);
		FreeMW(p_tabla4);
		CloseFile(gIdTabla4);
		CloseFile(gIdTabla12);
		return;
	}
	//	memcpy(bufferaux,((struct TABLA_12*) p_tabla12)->b_nom_establ,15);
	GetTablaCero(&tabla_cero);
	memcpy(nomEstablecimiento, gTablaCero.b_nom_establ, 22);
	RTrim(nomEstablecimiento, ' ');
	LTrim(nomEstablecimiento, ' ');
	//	strcat(nomEstablecimiento," * ");
	//	strcat(nomEstablecimiento,bufferaux);
	//GetTablaCuatro(0, &tabla_cuatro);

	MsgBufSetup();
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();
	pack_space(19);
	pack_str("CREDIBANCO");
	pack_lf();
	//	pack_space(14);
	//PackDateTimeVisa(&P_DATA.s_dtg);
	//PrintDate();
	//kt-120313
	ReadRTC(&P_DATA.s_dtg);
	PackDateTimeVisa(&P_DATA.s_dtg);
	//fin-kt
	pack_lf();
	//	CodEstablecimiento();

	memcpy(CodAgregado, gTablaDoce.b_establ_virtual, sizeof(gTablaDoce.b_establ_virtual)); //kt-110912
	LTrim(CodAgregado, 0x20);
	pack_mem(CodAgregado, strlen(CodAgregado));

	pack_space(MAX_CHAR_LINE_SMALL - 9 - strlen(nomEstablecimiento)); //MFBC/07/05/13
	pack_mem(nomEstablecimiento, strlen(nomEstablecimiento)); //MFBC/07/05/13
	pack_lf();
	pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
	pack_space(21);
	pack_str("TER: ");
	pack_mem(gTablaCuatro.b_cod_terminal, 8);
	pack_lf();
	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_space((MAX_CHAR_LINE_NORMAL - 28) / 2);
	pack_str("TABLA MULTIAGREGADOS ASOCIADOS");
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();
	pack_mem(MW_LPT_INVERTED_ON, 3);
	pack_str("CONS. COD.TERM   COD.ESTAB.  ESTABLECIMIENTO    ");
	pack_mem(MW_LPT_INVERTED_OFF, 3);

	for (i = 0; i <= MaxCount12; i++)
	{
		if (!GetTablaDoce(i, (struct TABLA_12*) p_tabla12))
			continue;

		pack_space(1);
		sprintf(buffer, "%03d", ((struct TABLA_12*) p_tabla12)->b_id);
		pack_mem(buffer, 3);
		pack_space(2);
		pack_mem(((struct TABLA_12*) p_tabla12)->b_term_virtual,sizeof(((struct TABLA_12*) p_tabla12)->b_term_virtual));
		pack_space(3);
		pack_mem(((struct TABLA_12*) p_tabla12)->b_establ_virtual,sizeof(((struct TABLA_12*) p_tabla12)->b_establ_virtual));
		pack_space(3);
		pack_mem(((struct TABLA_12*) p_tabla12)->b_nom_establ,sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ));
		pack_lf();

	}

	pack_nbyte('-', 48);
	pack_lf();
	packVersion(FALSE); //MFBC/28/02/13
	pack_lfs(5);
	FreeMW(p_tabla12);
	FreeMW(p_tabla4);
	CloseFile(gIdTabla12);
	CloseFile(gIdTabla4);

	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	MsgBufSetup();
	PackMsgBufLen();
}

void packVersion(BOOLEAN cod_hash)
{
	BYTE tmp1[9];
	BYTE tmp2[9];
	BYTE tmpHash[9];
	BYTE hash[9];
	int i;

	memset( tmp1, 0x00, sizeof(tmp1) );
	memset( tmp2, 0x00, sizeof(tmp2) );
	memset( tmpHash, 0x00, sizeof(tmpHash) );
	memset( hash, 0x00, sizeof(hash) );

	compress(tmp1, FechaHoraRec, 8);
	compress(tmp2, "AA240584BB060890", 8);

	for (i = 0; i < 8; i++) {
		tmpHash[i] = tmp1[i] ^ tmp2[i];
	}

	//split(hash, tmpHash, 8);

	memcpy(tmp1, tmpHash, 4);
	memcpy(tmp2, &tmpHash[4], 4);

	memset(tmpHash, 0x00, sizeof(tmpHash));

	for (i = 0; i < 4; i++) {
		tmpHash[i] = tmp1[i] ^ tmp2[i];
	}

	memset( hash, 0x00, sizeof(hash) );

	split(hash, tmpHash, 4);

	if(cod_hash)
	{
		pack_mem(hash, 8);
		pack_space(30);
	}
	else
	{
		pack_space(38);
	}

	//pack_str("SPXV01_C02");
	pack_str(VERCB);
}

void PackReportOtherCards(BOOLEAN ReporteDetallado, BYTE *nomEmisor, DWORD lenEmisor) //kt-220413
{
	WORD rec_cnt = 0, CntComercio, flagTodos;
	WORD maxComVirtual = GetTabla12Count();
	WORD maxContIssuer = GetTabla3Count();
	BYTE *temp_tabla12;

	rec_cnt = APM_GetRecCount();

	if (rec_cnt == 0)
	{

		RSP_DATA.w_rspcode = 'N' * 256 + 'T';
		DispCtrlMW(MW_CLR_DISP);
		DispRspText(FALSE);
		ErrorDelay();
		return;
	}

	if (Cajas() == FALSE && (memcmp(nomEmisor, "LA POLAR", 8 ) == 0) )
	{
		RSP_DATA.w_rspcode = 'N' * 256 + 'T';
		DispCtrlMW(MW_CLR_DISP);
		DispRspText(FALSE);
		ErrorDelay();
		return;
	}


	gIdTabla3 = OpenFile(KTabla3File);
	gIdTabla12 = OpenFile(KTabla12File);

	flagTodos = ChoiseMerchant(maxComVirtual);

	if (flagTodos == 0)
	{
		CloseFile(gIdTabla3);
		CloseFile(gIdTabla12);
		return;
	}

	temp_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));

	if (flagTodos == 2)
	{
		if (ReporteDetallado)
			PackVoucherOtherDet(rec_cnt, maxContIssuer, FALSE, nomEmisor, lenEmisor);

		//		else
		//			PackVoucherOtherCards(rec_cnt, maxContIssuer, nomEmisor);

		for (CntComercio = 0; CntComercio <= maxComVirtual; CntComercio++)
		{
			if (!GetTablaDoce(CntComercio, (struct TABLA_12*) temp_tabla12))
				continue;

			memcpy(INPUT.sb_comercio_v,
					((struct TABLA_12*) temp_tabla12)->b_establ_virtual, 9);
			memcpy(INPUT.sb_terminal_id,
					((struct TABLA_12*) temp_tabla12)->b_term_virtual, 8);
			memcpy(INPUT.sb_nom_comer_v,
					((struct TABLA_12*) temp_tabla12)->b_nom_establ, 15);

			if (ReporteDetallado)
			{
				if (!PackVoucherOtherDet(rec_cnt, maxContIssuer, TRUE,nomEmisor, lenEmisor))
					continue;
			}
			//			else
			//			{
			//				if (!PackVoucherOtherCards(rec_cnt, maxContIssuer,
			//						ReporteDetallado))
			//					continue;

			//			}
		}
	}
	else if (flagTodos == 1)
	{
		if (ReporteDetallado)
		{
			if (!PackVoucherOtherDet(rec_cnt, maxContIssuer, FALSE, nomEmisor, lenEmisor))
			{
				RSP_DATA.w_rspcode = 'N' * 256 + 'T';
				FreeMW(temp_tabla12);
				CloseFile(gIdTabla3);
				CloseFile(gIdTabla12);
				return;
			}
		}
		//		else
		//		{
		//
		//			if (!PackVoucherOtherCards(rec_cnt, maxContIssuer, iss))
		//			{
		//				RSP_DATA.w_rspcode = 'N' * 256 + 'T';
		//				DispCtrlMW(MW_CLR_DISP);
		//				DispRspText(FALSE);
		//				ErrorDelay();
		//				FreeMW(temp_tabla12);
		//				CloseFile(gIdTabla3);
		//				CloseFile(gIdTabla12);
		//				return;
		//			}
		//		}
	}

	FreeMW(temp_tabla12);
	CloseFile(gIdTabla3);
	CloseFile(gIdTabla12);

}

/*******************************************

 Voucher de reporte para tarjetas privadas.

 BONO OBSEQUIO iss = 1
 LA POLAR	  iss = 2
 CMR           iss = 3

 ********************************************/

BOOLEAN PackVoucherOtherCards(WORD rec_cnt, WORD max_issuer, DWORD iss) //
{
	struct TABLA_CERO tabla_cero;
	WORD rec_cnt2, rec_idx, issuer_idx, cont = 0; // issuer_idx2; //LFGD 02/20/2013
	BYTE tmpCuenta = 0;
	BOOLEAN flagDebito = 0;
	BOOLEAN flagCredito = 0;
	BOOLEAN flagElectron = 0;
	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
	BYTE *p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));
	BYTE binAux[10];
	BYTE nomEstablecimiento[23 + 1];

	memset(nomEstablecimiento, 0x00, sizeof(nomEstablecimiento));
	for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

		if (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0
				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)
						== 0))
			break;
	}

	if (rec_cnt2 == rec_cnt)
	{
		FreeMW(p_tabla3);
		FreeMW(p_tabla4);
		return FALSE;
	}

	//gIdTabla3 = OpenFile(KTabla3File);
	gIdTabla4 = OpenFile(KTabla4File);

	GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);

	GetTablaCero(&tabla_cero);
	memcpy(nomEstablecimiento, gTablaCero.b_nom_establ, 22);
	RTrim(nomEstablecimiento, ' ');
	LTrim(nomEstablecimiento, ' ');

	MsgBufSetup();
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();
	pack_space(19);
	pack_str("CREDIBANCO");
	pack_lf();
	//	pack_space(14);
	ReadRTC(&P_DATA.s_dtg); //LFGD
	PackDateTimeVisa(&P_DATA.s_dtg);
	pack_lf();
	packVersion(FALSE); //MFBC/28/02/13
	pack_lf();
	CodEstablecimiento();
	pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomEstablecimiento)); //MFBC/07/05/13
	pack_mem(nomEstablecimiento, strlen(nomEstablecimiento)); //MFBC/07/05/13
	pack_lf();
	pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
	pack_space(21);
	pack_str("TER: ");
	pack_mem(gTablaCuatro.b_cod_terminal, 8);
	pack_lf();
	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_space((MAX_CHAR_LINE_NORMAL - 20) / 2);
	if (iss == 1) //LFGD
		pack_str(" INFORME BONO OBSEQUIO ");
	else if (iss == 2) //LFGD
		pack_str(" INFORME LA POLAR ");
	else
		pack_str(" INFORME CMR ");
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();

	for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
	{
		flagDebito = 0;
		flagCredito = 0;
		flagElectron = 0;

		if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
			continue;

		for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
		{
			APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

			if (iss == 1)
			{

				if (issuer_idx != RECORD_BUF.w_issuer_idx)
					continue;
			}

			if (iss == 2)
			{
				memset(binAux, 0x00, sizeof(binAux));
				split(binAux, RECORD_BUF.sb_pan, 3);

				if (buscarBines(2, binAux) == FALSE)
					continue;
			}

			if (iss == 3)
			{
				memset(binAux, 0x00, sizeof(binAux));
				split(binAux, RECORD_BUF.sb_pan, 3);

				if (memcmp(binAux, "600001", 7) != 0 && memcmp(binAux,
						"627180", 7) != 0)
					continue;
			}
			cont = cont + 1;

			if ((gTablaTres.b_emisor_id - 1 == RECORD_BUF.w_issuer_idx)
					&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
							== 0) && (memcmp(INPUT.sb_terminal_id,
									RECORD_BUF.sb_terminal_id, 8) == 0))
			{

				GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);

				switch (RECORD_BUF.b_tipo_cuenta)
				{
				case 0x00:
					flagDebito = 1;
					break;
				case 0x30:
					flagCredito = 1;
					break;
				default:
					flagElectron = 1;
					break;
				}
			}

		}

		if (cont == 0)
		{
			RSP_DATA.w_rspcode = 'N' * 256 + 'T';
			DispCtrlMW(MW_CLR_DISP);
			DispRspText(FALSE);
			ErrorDelay();
			CloseFile(gIdTabla4);
			FreeMW(p_tabla3);
			FreeMW(p_tabla4);
			return FALSE;
		}

		for (rec_idx = 0; rec_idx < 3; rec_idx++)
		{
			switch (rec_idx)
			{
			case 0:
				if (flagDebito == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem("DEBITO   ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);
					pack_lf();

					tmpCuenta = 0x00;
					PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1,
							FALSE);

				}
				break;
			case 1:
				if (flagCredito == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem("CREDITO  ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);
					pack_lf();

					tmpCuenta = 0x30;
					PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1,
							FALSE);
				}
				break;
			case 2:
				if (flagElectron == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem("ELECTRON ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);

					pack_lf();

					//					tmpCuenta = 0x10;
					//					PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1, FALSE);
					//					tmpCuenta = 0x20;
					//					PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1, FALSE);
					//					tmpCuenta = 0x40;
					tmpCuenta = 0x11; //kt-110413
					PackTotales(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1,
							FALSE);

				}
				break;
			}
		}

	}

	pack_mem(MW_LPT_INVERTED_ON, 3);
	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_mem("          GRAN TOTAL          ", 30);
	pack_mem(MW_LPT_INVERTED_OFF, 3);
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_lf();
	PackIssuerTotalsVISA(RECORD_BUF.w_issuer_idx, 1, FALSE);

	flagAnul = FALSE; //kt-domingo
	pack_lfs(5);

	CloseFile(gIdTabla4);
	FreeMW(p_tabla3);
	FreeMW(p_tabla4);

	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	return TRUE;
}

BOOLEAN PackVoucherOtherDet(WORD rec_cnt, WORD max_issuer, BOOLEAN virtual,
		BYTE *nomEmisor, DWORD lenEmisor) //kt-220413
{
	int cntTrans = 0;
	struct TABLA_CERO tabla_cero;
	struct TABLA_CUATRO tabla_cuatro;

	WORD rec_cnt2, rec_idx, issuer_idx, cont = 0; // issuer_idx2; //LFGD 02/20/2013
	BYTE tmpCuenta = 0;
	BOOLEAN flagDebito = 0;
	BOOLEAN flagCredito = 0;
	BOOLEAN flagElectron = 0;
	BOOLEAN flagBono = 0;
	BYTE *nomAux = MallocMW(24);
	BYTE *p_tabla3;
	BYTE nomEstablecimiento[23 + 1];

	memset(nomEstablecimiento, 0x00, sizeof(nomEstablecimiento));
	//	BYTE binAux[20];

	gIdTabla4 = OpenFile(KTabla4File);
	GetTablaCuatro(INPUT.w_host_idx, &tabla_cuatro);

	for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
	{
		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

		if (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0
				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)
						== 0))
			break;
	}

	if (rec_cnt2 == rec_cnt)
	{
		FreeMW(nomAux);
		CloseFile(gIdTabla4);
		return FALSE;
	}

	GetTablaCero(&tabla_cero);
	memcpy(nomEstablecimiento, gTablaCero.b_nom_establ, 22);
	RTrim(nomEstablecimiento, ' ');
	LTrim(nomEstablecimiento, ' ');
	memcpy(nomAux, nomEstablecimiento, strlen (nomEstablecimiento) );

	p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));

	MsgBufSetup();

	if (!virtual)
	{
		pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_lf();
		pack_space(19);
		pack_str("CREDIBANCO");
		pack_lf();
		//		pack_space(14);
		ReadRTC(&P_DATA.s_dtg); //LFGD
		PackDateTimeVisa(&P_DATA.s_dtg);
		pack_lf();
		packVersion(FALSE); //MFBC/28/02/13
		//PrintDate();
		pack_lf();

		CodEstablecimiento();
		pack_space(MAX_CHAR_LINE_SMALL - 11 - strlen(nomAux)); //MFBC/07/05/13
		pack_mem(nomAux, strlen(nomAux)); //MFBC/07/05/13
		pack_lf();
		pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
		pack_space(21);
		pack_str("TER: ");
		pack_mem(gTablaCuatro.b_cod_terminal, 8);
		pack_lf();
		pack_mem(MW_LPT_FONT_ENLARGED, 3);

		if (memcmp(nomEmisor, "CMR", lenEmisor) == 0)
		{
			pack_space((MAX_CHAR_LINE_NORMAL - 22) / 2);
			pack_str(" INFORME DETALLADO ");
			pack_mem(nomEmisor, 3);
		}
		else
		{
			pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
			pack_str(" INFORME DETALLADO");
		}
		//pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_byte(0x1B);
		pack_byte(0x46);
		pack_byte(0x31);
		pack_lf();
	}
	else
	{
		pack_mem(MW_LPT_FONT_ENLARGED, 3);


		pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
		pack_str(" INFORME DETALLADO");
		//pack_mem(MW_LPT_FONT_SMALL, 3);
		pack_byte(0x1B);
		pack_byte(0x46);
		pack_byte(0x31);
		pack_lf();
		pack_mem(INPUT.sb_comercio_v, 9);
		pack_space(14);
		pack_mem(INPUT.sb_nom_comer_v, 21);
		pack_lf();
	}

	for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
	{
		flagDebito = 0;
		flagCredito = 0;
		flagElectron = 0;
		flagBono = 0;
		//issuer_idx2 = 0;

		if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
			continue;


		if (memcmp(gTablaTres.b_nom_emisor, nomEmisor, lenEmisor) != 0)
			continue;

		cntTrans++;


		for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
		{
			APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));

			if (gTablaTres.b_emisor_id - 1 != RECORD_BUF.w_issuer_idx)
				continue;

			cont = cont + 1;

			if ((gTablaTres.b_emisor_id - 1 == RECORD_BUF.w_issuer_idx)
					&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
							== 0) && (memcmp(INPUT.sb_terminal_id,
									RECORD_BUF.sb_terminal_id, 8) == 0))
			{
				//issuer_idx2 = issuer_idx + 1;
				switch (RECORD_BUF.b_tipo_cuenta)
				{
				case 0x00:
					flagDebito = 1;
					break;
				case 0x30:
					flagCredito = 1;
					break;
				case 0x10:
				case 0x20:
				case 0x40:
					flagElectron = 1;
					break;
				}
			}
		}

		if (cont == 0)
		{
			RSP_DATA.w_rspcode = 'N' * 256 + 'T';
			DispCtrlMW(MW_CLR_DISP);
			DispRspText(FALSE);
			ErrorDelay();
			FreeMW(nomAux);
			CloseFile(gIdTabla4);
			FreeMW(p_tabla3);
			return FALSE;
		}

		FreeMW(nomAux);
		for (rec_idx = 0; rec_idx < 4; rec_idx++)
		{
			switch (rec_idx)
			{
			case 0:
				if (flagDebito == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					pack_mem("  DEBITO ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);

					pack_lf();
					pack_str(
							"TARJETA      RECIBO      TIPO           COMPRA ");
					pack_lf();
					tmpCuenta = 0x00;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
				}
				break;
			case 1:
				if (flagCredito == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);    LFGD//02/20/2013
					pack_mem(" CREDITO ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);

					pack_lf();
					pack_str("TARJETA      RECIBO      TIPO           COMPRA ");
					pack_lf();
					tmpCuenta = 0x30;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
				}
				break;
			case 2:
				if (flagElectron == 1)
				{
					pack_mem(MW_LPT_INVERTED_ON, 3);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
					pack_mem(gTablaTres.b_nom_emisor, 10);
					pack_space(29);
					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);    LFGD//02/20/2013
					pack_mem("ELECTRON ", 9);
					pack_mem(MW_LPT_INVERTED_OFF, 3);

					pack_lf();
					pack_str("TARJETA      RECIBO      TIPO           COMPRA ");
					pack_lf();
					tmpCuenta = 0x10;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
					tmpCuenta = 0x20;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
					tmpCuenta = 0x40;
					PackDetalles(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
				}
				break;
			case 3:
				break;
			}
		}
	}
	pack_nbyte('=', 48);
	if (!virtual)
		pack_lfs(5); //kt-280213
	else
		pack_lfs(3); //kt-280213

	FreeMW(p_tabla3);
	CloseFile(gIdTabla4);

	if (cntTrans == 0) {
		//		printf("\f debugtres"); WaitKey(3000, 0);
		MsgBufSetup();
		PackMsgBufLen();
		return FALSE;
	}

	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	return TRUE;
}

BOOLEAN PackReportEMV(void) //kt-240413
{
	WORD i;

	//	if (strlen(gConfigComercio.gTAG_Tx_declinada.TAG_9F26) <= 1)
	//		return FALSE;

	MsgBufSetup();
	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_str("Tx Declinada offline");
	pack_lfs(2);
	pack_mem(MW_LPT_FONT_SMALL, 3);

	pack_str("Application Cryptogram");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F26, 14);
	pack_lf();

	for (i = 0; i < 4; i++) {
		if(gConfigComercio.gTAG_Tx_declinada.TAG_9F26[i] < 32 || gConfigComercio.gTAG_Tx_declinada.TAG_9F26[i] > 126)
		{
			MsgBufSetup();
			PackMsgBufLen();
			return FALSE;
		}
	}

	pack_str("Cryptogram Information Data");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F27, 2);
	pack_lf();

	pack_str("Issuer Aplication Data");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F10, 16);
	pack_lf();

	pack_str("Unpredictable Number");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F37, 8);
	pack_lf();

	pack_str("Application transaction Counter");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F36, 4);
	pack_lf();

	pack_str("Terminal Verification Result");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_95, 10);
	pack_lf();

	pack_str("Transaction Date");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9A, 6);
	pack_lf();

	pack_str("Transaction Type");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9C, 2);
	pack_lf();

	pack_str("Amount Authorized");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F03, 12);
	pack_lf();

	pack_str("Transaction Currency Code");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_5F2A, 4);
	pack_lf();

	pack_str("Application Interchange Profile");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_82, 4);
	pack_lf();

	pack_str("PAN - truncated");
	pack_lf();
	pack_mem(&gConfigComercio.gTAG_Tx_declinada.TAG_5A[12], 4);
	pack_lf();

	pack_str("Terminal Country Code");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F1A, 4);
	pack_lf();

	pack_str("CVM Result");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_9F34, 6);
	pack_lf();

	pack_str("PAN Sequence Number");
	pack_lf();
	pack_mem(gConfigComercio.gTAG_Tx_declinada.TAG_5F34, 2);
	pack_lfs(6);

	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);

	MsgBufSetup();
	PackMsgBufLen();

	return TRUE;
}

//*****************************************************************************
//  Function        : packCriptograma
//  Description     : Empaqueta informacion del tag 9F26
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void packCriptograma (void)
{
	pack_lf();
	pack_mem(P_DATA.sb_holder_name, sizeof(P_DATA.sb_holder_name));  //MFBC/09/09/13 se cambio el strlen
	pack_lf();
	pack_mem("Criptograma: ", 13);
	split_data(P_DATA.s_icc_data.sb_tag_9f26,
			P_DATA.s_icc_data.b_tag_9f26_len); //kt-111012
	pack_lf();
	pack_mem("TVR    : ", 9);
	split_data(P_DATA.s_icc_data.sb_tag_95,
			P_DATA.s_icc_data.b_tag_95_len); //kt-111012
	pack_lf();
	pack_mem("TSI    : ", 9);
	split_data(P_DATA.s_icc_data.sb_tag_9b,
			P_DATA.s_icc_data.b_tag_9b_len); //kt-111012
	pack_lf();
	pack_mem("AID    : ", 9);
	split_data(P_DATA.s_icc_data.sb_aid,
			P_DATA.s_icc_data.b_aid_len); // !2009-01-15
	pack_space(2);
	pack_mem(P_DATA.s_icc_data.sb_label,
			P_DATA.s_icc_data.b_tag_50_len);
	pack_lf();

}


/**************************************************
 * Descipcion : Imprime el reporte Del Cierre del POS con la Caja
 * **SR** 02/09/13
 ***************************************************/
void GetInfoTrans (void){

	int CountTrans = 0, i=0;

	CountTrans = APM_GetRecCount();

	memset(&TERM_TOT, 0x00, sizeof(struct TOTAL_STRUCT));

	for(i = 0; i< CountTrans; i ++){

		APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF));
		AddToTotals(FALSE);
	}

	return;
}

/**************************************************
 * Descipcion : Imprime el reporte Del Cierre del POS con la Caja
 * **SR** 02/09/13
 ***************************************************/

void ReportSelttCajas (void){

	int i = 0;
	BYTE sb_TypeTrans  	[20];
	BYTE sb_MontoPrint 	[20];
	BYTE sb_NumTrans		[3 + 1];
	BYTE sb_IVAPrint		[20];
	BYTE sb_BasePrint		[20];
	BYTE sb_PropPrint		[20];
	BYTE sb_DonaPrint		[20];
	BYTE b_Simbolo = gTablaCero.b_simb_moneda;
	BOOLEAN f_Venta = FALSE;
	BOOLEAN f_Anulacion = FALSE;

	MsgBufSetup();

	pack_mem(MW_LPT_FONT_SMALL, 3);  // Todo el formato de latra es peque�a para el reporte **SR** 04/09/13
	pack_lf();
	pack_space(19);
	pack_str("CREDIBANCO");
	pack_lf();
	PackDateTimeVisa(&P_DATA.s_dtg);
	pack_lf();
	packVersion(FALSE);
	CodEstablecimiento();
	pack_space(16);
	NomEstablecimiento();
	pack_lf();
	pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
	pack_space(21);
	pack_str("TER: ");
	pack_mem(gTablaCuatro.b_cod_terminal, 8);
	pack_lf();
	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_space((MAX_CHAR_LINE_NORMAL - 20) / 2);
	pack_str(" INFORME DE CIERRE ");
	pack_lf();
	pack_byte(0x1B);		// formato de la letra peque�a **SR** 02/09/13
	pack_byte(0x46);		// formato de la letra peque�a **SR** 02/09/13
	pack_byte(0x31);		// formato de la letra peque�a **SR** 02/09/13
	pack_lf();
	pack_space((MAX_CHAR_LINE_SMALL  - 16) / 2);
	pack_str("CIERRE INTEGRADO");
	pack_lf();

	GetInfoTrans();

	memset(sb_TypeTrans, 0x00, sizeof(sb_TypeTrans));

	for(i = 0; i<6; i++){

		memset(sb_TypeTrans , 0x00, sizeof (sb_TypeTrans ));
		memset(sb_NumTrans  , 0x00, sizeof (sb_NumTrans  ));
		memset(sb_MontoPrint, 0x00, sizeof (sb_MontoPrint));
		memset(sb_IVAPrint  , 0x00, sizeof (sb_IVAPrint  ));
		memset(sb_BasePrint , 0x00, sizeof (sb_BasePrint ));
		memset(sb_PropPrint , 0x00, sizeof (sb_PropPrint ));
		memset(sb_DonaPrint , 0x00, sizeof (sb_DonaPrint ));

		if(i == 0){
			if(TERM_TOT.s_sale_tot.w_count != 0){
				f_Venta = TRUE;
				memcpy(sb_TypeTrans, "VENTA", 5);
				sprintf(sb_NumTrans, "%03d", TERM_TOT.s_sale_tot.w_count);
				sprintf(sb_MontoPrint, "%c %d",b_Simbolo, TERM_TOT.s_sale_tot.dd_amount / 100);
				sprintf(sb_IVAPrint  , "%c %d",b_Simbolo, TERM_TOT.s_iva_tot.dd_amount / 100);
				sprintf(sb_BasePrint , "%c %d",b_Simbolo, TERM_TOT.s_base_tot.dd_amount / 100);
				sprintf(sb_PropPrint , "%c %d",b_Simbolo, TERM_TOT.s_tips_tot.dd_amount / 100);
				sprintf(sb_DonaPrint , "%c %d",b_Simbolo, TERM_TOT.s_donacion_tot.dd_amount / 100);
			}
			else
				continue;
		}

		else if(i == 1){
			if(TERM_TOT.s_void_sale.w_count != 0){
				f_Anulacion =  TRUE;
				memcpy(sb_TypeTrans, "ANULACIONES", 11);
				sprintf(sb_NumTrans, "%03d", TERM_TOT.s_void_sale.w_count);
				sprintf(sb_MontoPrint, "%c %d",b_Simbolo, TERM_TOT.s_void_sale.dd_amount / 100);
				sprintf(sb_IVAPrint  , "%c %d",b_Simbolo, TERM_TOT.s_void_iva_tot.dd_amount / 100);
				sprintf(sb_BasePrint , "%c %d",b_Simbolo, TERM_TOT.s_void_base_tot.dd_amount / 100);
				sprintf(sb_PropPrint , "%c %d",b_Simbolo, TERM_TOT.s_void_tips_tot.dd_amount / 100);
				sprintf(sb_DonaPrint , "%c %d",b_Simbolo, TERM_TOT.s_void_donacion_tot.dd_amount / 100);

			}
			else
				continue;
		}

		else
			continue;

		/*Encabezado*/
		pack_mem(MW_LPT_INVERTED_ON, 3);
		pack_mem(sb_TypeTrans,strlen(sb_TypeTrans));
		pack_space(48 - strlen(sb_TypeTrans) - 9);
		pack_mem("Trans:",6);
		pack_mem(sb_NumTrans,3);
		pack_mem(MW_LPT_INVERTED_OFF, 3);
		/****************/
		/****Montos***/
		if(f_Venta == TRUE || f_Anulacion == TRUE){		// la impresion de monto y IVA es unicamente para la compra o anulacion **SR** 03/09/13
			pack_mem("MONTO      :",12);
			pack_space(48 - 12 - strlen(sb_MontoPrint));
			pack_mem(sb_MontoPrint,strlen(sb_MontoPrint));
			pack_mem("IVA        :",12);
			pack_space(48 - 12 - strlen(sb_IVAPrint));
			pack_mem(sb_IVAPrint,strlen(sb_IVAPrint));
			pack_mem("BASE DEV   :",12);
			pack_space(48 - 12 - strlen(sb_BasePrint));
			pack_mem(sb_BasePrint,strlen(sb_BasePrint));
			pack_mem("PROPINA    :",12);
			pack_space(48 - 12 - strlen(sb_PropPrint));
			pack_mem(sb_PropPrint,strlen(sb_PropPrint));
			pack_mem("DONACION   :",12);
			pack_space(48 - 12 - strlen(sb_DonaPrint));
			pack_mem(sb_DonaPrint,strlen(sb_DonaPrint));
			f_Venta 		= FALSE;
			f_Anulacion 	= FALSE;
		}
		/****************/
		/**SUBTOTAL**/
		pack_nbyte('-', 48);
		pack_mem("SUBTOTAL   :",12);
		pack_space(48 - 12 - strlen(sb_MontoPrint));
		pack_mem(sb_MontoPrint,strlen(sb_MontoPrint));
		pack_nbyte('=', 48);
		pack_lf();
		/****************/

	}

	pack_mem(MW_LPT_FONT_ENLARGED, 3);
	pack_space((MAX_CHAR_LINE_NORMAL - 10) / 2);
	pack_str("GRAN TOTAL");
	pack_lf();
	pack_byte(0x1B);		// formato de la letra peque�a **SR** 02/09/13
	pack_byte(0x46);		// formato de la letra peque�a **SR** 02/09/13
	pack_byte(0x31);		// formato de la letra peque�a **SR** 02/09/13
	pack_str("CANT. TXN   :");
	memset(sb_NumTrans, 0x00, sizeof(sb_NumTrans));

	sprintf(sb_NumTrans, "%03d",TERM_TOT.s_tasa_ad_tot.w_count+ TERM_TOT.s_sale_tot.w_count);  // suma los contadores para el "GRAN TOTAL"  **SR** 04/09/13

	pack_space(48 - 13 - strlen(sb_NumTrans));
	pack_mem(sb_NumTrans, strlen(sb_NumTrans));

	pack_str("TOT VENTAS  :");
	memset(sb_MontoPrint, 0x00, sizeof(sb_MontoPrint));

	sprintf(sb_MontoPrint, "%c %d",b_Simbolo,(TERM_TOT.s_tasa_ad_tot.dd_amount + TERM_TOT.s_sale_tot.dd_amount ) / 100);		// suma los montos para el "GRAN TOTAL"  **SR** 04/09/13

	pack_space(48 - 13 - strlen(sb_MontoPrint));
	pack_mem(sb_MontoPrint, strlen(sb_MontoPrint));
	pack_nbyte('-', 48);
	pack_nbyte('-', 48);
	pack_nbyte('-', 48);
	pack_lf();
	pack_lf();
	pack_lf();

	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, TRUE);

	return;
}

void pack_firma(void)
{
	pack_str("Firma: _________________________________________");
	pack_lf();
	pack_str("C.C  : _________________________________________");
	pack_lf();
	pack_str("Tel. : _________________________________________");
	pack_lf();
}

