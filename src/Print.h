//-----------------------------------------------------------------------------
//  File          : print.h
//  Module        :
//  Description   : Declrartion & Defination for PRINT.C
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _PRINT_H_
#define _PRINT_H_
#include "common.h"
#include "corevar.h"

//-----------------------------------------------------------------------------
// Receipt control CODE
// 1. KRcptMark must pack for transaction receipt.
// 2. Data between two skip mark will not print for customer copy
// 3. logo mark will replace with logo data download from stis
// 4. replace mark will replace by some constant message.
//-----------------------------------------------------------------------------
extern const BYTE KLogoMark[3];
extern const BYTE KConstLogoMark[3];
extern const BYTE KRcptMark[3];
extern const BYTE KSkipMark[3];
extern const BYTE KReplaceMark[3];
extern const BYTE KDuplicateMark[3];
extern const BYTE KSignatureMark[3];

extern const BYTE KHalfDotMode[3]; // ESC D 1
extern const BYTE KFullDotMode[3]; // ESC D 0

extern BYTE g_TypeCount;

#define MAX_CHAR_LINE_NORMAL    30
#define MAX_CHAR_LINE_SMALL     48

/****** slip type ******/
#define CUS_SLIP        0
#define REPRINT_SLIP    1

extern void PackLogo(void);
extern void PackCurrentDate(void);
extern void PackSeperator(BYTE aChar);
extern void PackRcptHeader();
extern BYTE PackAmt(DDWORD aAmount, BYTE aOffset);
extern BYTE Packpuntos(DDWORD aPoints, BYTE aOffset, BOOLEAN aVoid, BOOLEAN aSinDecimal);
extern void PackFF(void);

#if (NO_USED)
extern void PackTotals(struct TOTAL_STRUCT *aTot);
extern void PackDtg(struct DATETIME *aDtg);
#endif

extern void PackTotalsVISA(struct TOTAL_STRUCT *aTot, BYTE tipoCuenta);
extern BOOLEAN DispTransTotal(struct TOTAL_STRUCT *aTot, BYTE tipoCuenta); //kt-150413
extern void PackEndOfRep(void);
extern void SaveLastRcpt(BYTE *aDat, WORD aLen);
extern int GetLastRcpt(BYTE *aDat, WORD aLen);
extern void ReprintLast(void);
extern void ManualFeed(void);
extern void PackVoucher(BOOLEAN aReprint, BOOLEAN Duplicado);
extern void packReportPagoDivi(void);
extern void PackHeader(BOOLEAN aReprint);
extern void PackAvance(BOOLEAN aReprint, BOOLEAN Duplicado);
extern void CabeceraCheques(BOOLEAN FlagTipo, BOOLEAN aReprint); //NM-24/04/13 Agregue "aReprint" para validar si es COPIA CHEQUE y q no debe imprimir firma
extern void PackCuentaCheque(void);
extern void NomEstablecimiento(void);
extern void CodEstablecimiento(void);
extern void TipoCuenta(void);
extern void PackPrintIss(void);
extern void PackPanPrint(void);
extern void ExpDate(void);
extern void NumReciboPack(void);
extern void PrintCajasExito(void);
extern void PrintExitoRecaudo(void);
extern BOOLEAN PackVoucherSettle(WORD rec_cnt, WORD max_issuer, WORD virtual, BOOLEAN aRePrint);				//kt-110413
extern BOOLEAN PackVoucherSettleTotal(WORD rec_cnt, WORD max_issuer, BOOLEAN cierre, WORD virtual); //KT-110413
extern void PackVoucherPuntos(BOOLEAN aReprint, BOOLEAN Duplicado);
//extern void PackVoucherMultibol(BOOLEAN aReprint, BYTE aTrans,BOOLEAN Duplicado); //kt-160413
extern void ReportComerciosVirtuales(void); //kt-211112
extern void PackReportComercio(BOOLEAN ReporteDetallado, BOOLEAN ReporteDisplay); //kt-110413
extern void packReportMultiIva(void); //kt-071212
extern void packVersion(BOOLEAN cod_hash); //LFGD 02/13/2013
extern void PackVoucherSingle(BOOLEAN aReprint, BOOLEAN Duplicado);
extern void PackHeaderAerolinea(BOOLEAN aReprint, BOOLEAN tip_trans);
extern void PackCierreMulticomercio(void);		//kt-180413

extern BOOLEAN PackVoucherOtherCards(WORD rec_cnt, WORD max_issuer, DWORD iss);
extern void PackReportOtherCards(BOOLEAN ReporteDetallado, BYTE *nomEmisor, DWORD lenEmisor); //kt-220413
extern BOOLEAN PackVoucherOtherDet(WORD rec_cnt, WORD max_issuer, BOOLEAN virtual,
		BYTE *nomEmisor, DWORD lenEmisor);	//kt-220413
extern BOOLEAN PackReportEMV(void);		//kt-240413
extern void packVoucherRecargaCelular (BOOLEAN aReprint, BOOLEAN Duplicado);
extern void packVoidAerolineas(BOOLEAN aReprint, BOOLEAN Duplicado); //MFBC/14/08/13
extern void packVoidPagoFactura(BOOLEAN aReprint, BOOLEAN Duplicado); //MFBC/14/08/13
extern void packCriptograma (void); //MFBC/14/08/13
extern void pack_AeroUno(BOOLEAN aReprint, BOOLEAN Duplicado) ; //001
extern void pack_AeroSeis(BOOLEAN aReprint, BOOLEAN Duplicado) ; //110//100
extern void pack_AeroAll (BOOLEAN aReprint, BOOLEAN Duplicado);//111

extern void ReportSelttCajas (void);			// **SR** 08/09/13

extern void pack_firma(void);
extern void PackCountAmt(WORD aCount, DDWORD aAmount, BOOLEAN tipIva) ;
extern void packlegal(void);

#endif // _PRINT_H_
