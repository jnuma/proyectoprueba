//-----------------------------------------------------------------------------
//  File          : testrans.c
//  Module        :
//  Description   : Include Host Test Transactions
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include "corevar.h"
#include "constant.h"
#include "input.h"
#include "tranutil.h"
#include "record.h"
#include "hostmsg.h"
#include "testrans.h"
#include "Keytrans.h"  //MFBC/10/12/12
#include "rs232.h"
#include "files.h"
#include "key2dll.h"
#include "reversal.h"

//*****************************************************************************
//  Function        : TestTrans
//  Description     : Do a test transaction with host.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN TestTrans(void) //MFBC/15/04/13 cambie la funcion
{
	//int host_idx;
	INPUT.b_trans = TEST_MSG;

	//host_idx = APM_SelectAcquirer(FALSE);
	/*
	 if (host_idx == -1) {
	 LongBeep();
	 RefreshDispAfter(0);
	 return FALSE;
	 }
	 */

	//DispHeader(NULL);
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));

	gIdTabla12 = OpenFile(KTabla12File);
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	CloseFile(gIdTabla12);
	FreeMW(p_tabla12);

	DispHeaderTrans();

	//    memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-261112

	memset(INPUT.sb_comercio_v, 0x20, sizeof(INPUT.sb_comercio_v));
	memset(INPUT.sb_terminal_id, 0x20, sizeof(INPUT.sb_terminal_id));
	memset(INPUT.sb_term_comer_v, 0x20, sizeof(INPUT.sb_term_comer_v));

	memcpy(INPUT.sb_comercio_v, gTablaDoce.b_establ_virtual,9);
	memcpy(INPUT.sb_term_comer_v, gTablaDoce.b_term_virtual, 8);
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);

	//APM_GetAcqTbl(host_idx, &STIS_ACQ_TBL(0));
	//PackComm(host_idx, FALSE);
	PackCommTest(0, FALSE);
	IncTraceNo();
	ClearResponse();
	MoveInput2Tx();
	PackProcCode(INPUT.b_trans, 0x00);
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		PackHostMsg();
		if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false)) == COMM_OK)
		{
			RSP_DATA.b_response = CheckHostRsp();
			AcceptBeep();
			TextColor("Comunicacion exitosa", MW_LINE6, COLOR_GREEN, MW_BIGFONT | MW_CENTER | MW_CLRDISP, 3); //MFBC/26/02/13
		}
		else
		{
			LongBeep();
			TextColor("POR FAVOR", MW_LINE3, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor("REINTENTE 1009", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER , 2);
			TextColor("Error de", MW_LINE3, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0); //MFBC/13/03/13
			TextColor("Comunicacion", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER , 3); //MFBC/13/03/13
		}
		//SaveTraceNo();
	}
	else
	{
		LongBeep();
		TextColor("POR FAVOR", MW_LINE3, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("REINTENTE 1009", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER , 2);
		TextColor("Error de", MW_LINE3, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0); //MFBC/13/03/13
		TextColor("Comunicacion", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER , 3); //MFBC/13/03/13
	}

	APM_ResetComm();//MFBC/13/03/13
	//TransEnd(FALSE);
	return TRUE;
}

//*****************************************************************************
//  Function        : LogonTrans
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN LogonTrans(BOOLEAN automatico) //MFBC/15/04/13 cambie la funcion
{

	INPUT.b_trans = LOGON;
	DispHeaderTrans();
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));

	gIdTabla12 = OpenFile(KTabla12File);
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	CloseFile(gIdTabla12);
	FreeMW(p_tabla12);

	memset(gActualiza_Logon, 0x00, sizeof(gActualiza_Logon));
	memset(gEWKPIN, 0x00, sizeof(gEWKPIN));

	if (gAppDat.estadoInit == FALSE)
	{
		LongBeep();
		DispClrBelowMW(MW_LINE2);
		TextColor("Debe Inicializar", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER, 0);
		TextColor("la Terminal", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER, 3);
		return FALSE; //MFBC/16/10/13
	}

	APM_PreConnect(); //MFBC/02/05/13

	if (automatico == FALSE)
	{
		if( passAdminComercio(MW_LINE2) != TRUE)
		{
			APM_ResetComm();
			return FALSE;
		}
	}

	memset(INPUT.sb_comercio_v, 0x20, sizeof(INPUT.sb_comercio_v));
	memset(INPUT.sb_terminal_id, 0x20, sizeof(INPUT.sb_terminal_id));
	memset(INPUT.sb_term_comer_v, 0x20, sizeof(INPUT.sb_term_comer_v));

	memcpy(INPUT.sb_comercio_v, gTablaDoce.b_establ_virtual,9);
	memcpy(INPUT.sb_term_comer_v, gTablaDoce.b_term_virtual, 8);
//	memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-261112
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8); //kt-261112

	PackCommTest(0, FALSE);
	PackProcCode(INPUT.b_trans, 0x00);
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			ClearResponse();
			INPUT.b_trans = LOGON;
			PackProcCode(INPUT.b_trans, 0x00);
			IncTraceNo();
			MoveInput2Tx();
			PackHostMsg();

			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false)) == COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();
				if (RSP_DATA.b_response == TRANS_ACP)
				{
					if(actualizar_init() == -1) //MFBC/08/04/13
					{
						LongBeep();
						TextColor("Fallo Logon!", MW_LINE5, COLOR_RED, MW_CENTER | MW_SMFONT | MW_CLRDISP, 3);
					}
					else if (actualizar_init() == 1)
					{
						AcceptBeep();
						TextColor("Logon Exitoso!", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT | MW_CLRDISP, 3);
						if (automatico == TRUE)
							bajar_Logon();
					}

				}
			}
			else
			{
				LongBeep();
				TextColor("Fallo Comunicacion", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER | MW_CLRDISP, 2); //MFBC/13/03/13
				TextColor("POR FAVOR", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER | MW_CLRDISP, 0); //MFBC/13/03/13
				TextColor("REINTENTE", MW_LINE6, COLOR_RED, MW_BIGFONT | MW_CENTER , 3); //MFBC/13/03/13

			}
		}
	}
	else //MFBC/15/04/13
	{
		LongBeep();
		TextColor("POR FAVOR", MW_LINE3, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("REINTENTE 1009", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER , 2);
		TextColor("Error de", MW_LINE3, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("Comunicacion", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER , 3); //MFBC/13/03/13
	}

	logon_auto = FALSE;
	APM_ResetComm();
	//TransEnd(FALSE);
	return TRUE;
}



DWORD  actualizar_init (void)
{
	BYTE filename[32];
	memset(filename, 0x00, sizeof(filename));
	strcpy(filename, KTabla0File);
	gIdTabla0 = 0;

	struct TABLA_CERO tabla_cero;
	GetTablaCero(&tabla_cero);

	//	memcpy(&tabla_cero.b_dir_ciudad[0], &gActualiza_Logon[0], 69); //Actualiza desde el  "b_dir_ciudad"  hasta  "b_nom_establ"

	if( g_sync_logon & 0x04)
	{
		//printf("\f entro actualizar logon"); WaitKey(3000, 0);
		memcpy(&tabla_cero.b_dir_ciudad[39], &gActualiza_Logon[0], 2); //(BYTE 39 => 39, 36=#referencia - 38, 34=Supercupofalabella - 37=Ambas)
		// DCC 34, 35, 36 - CODIGO DE DONACION(BYTE 40 !=0 entonces hay donacion)
		memcpy(&tabla_cero.b_dir_ciudad[41], &gActualiza_Logon[2], 2); //PORCENTAJE CASH BACK
		memcpy(&tabla_cero.b_dir_ciudad[43], &gActualiza_Logon[4], 1); //FLAG CAJAS
		memcpy(&tabla_cero.b_dir_ciudad[44], &gActualiza_Logon[5], 2); //PORCENTAJE AEROLINEAS
		gIdTabla0 = fOpenMW(filename);
		UpdTablaCero(gIdTabla0,&tabla_cero);
		fCloseMW(gIdTabla0);
	}
	else
	{
		LongBeep();
		TextColor("FALLO ACTUALIZACION",  MW_LINE4, COLOR_RED,
				MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("DE DATOS",  MW_LINE5, COLOR_RED,
				MW_SMFONT | MW_CENTER , 2);
	}

	if (g_sync_logon & 0x02)
	{

		//printf("\f entro actualizar llaves"); WaitKey(3000, 0);
		KDLL_KeyReset(EKEY_IDX);
		SleepMW();
		LoadEWKPin(gEWKPIN);
	}
	else
	{
		LongBeep();
		TextColor("FALLO ACTUALIZACION",  MW_LINE4, COLOR_RED,
				MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("DE LLAVE 3DES",  MW_LINE5, COLOR_RED,
				MW_SMFONT | MW_CENTER, 2);
	}

	if(g_sync_logon == 0x00)
		return -1;
	else if(g_sync_logon != 0x06)
		return 0;

	return 1;
}


void bajar_Logon (void)
{

	TX_DATA.b_trans = OUT_LOGON;
	PackCommTest(0, FALSE);

	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		ClearResponse();
		PackHostMsg();

		if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, TRUE)) == COMM_OK)
			return;

	}

}




