//-----------------------------------------------------------------------------
//  File          : cakey.c
//  Module        :
//  Description   : Inject CA key routine.
//  Author        : Lewis
//  Notes         : N/A
//
//  Naming conventions
//  ~~~~~~~~~~~~~~~~~~
//             Constant : Leading K
//      Global Variable : Leading g
//    Function argument : Leading a
//       Local Variable : All lower case
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  15 May  2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include "basecall.h"
#include "midware.h"
#include "hwdef.h"
#include "corevar.h"
#include "cakey.h"
#include "Corevar.h"
#include "getcert.h"




//-----------------------------------------------------------------------------
// Constant
//-----------------------------------------------------------------------------
/*const struct MW_KEY cakeyVisa[] = {
  // 4 visa.cer //
  0x00010001, // Exponent   //
  128,        // Key Length //
  0xB6249D9F, 0x26143B9C, 0x02857048, 0xC0655824,
  0x23CBD042, 0x37E92E2E, 0x2106EE9B, 0xE0F4F11F,
  0x89C38574, 0x33148F6A, 0x6E1D02EB, 0xF4E9071B,
  0x66DD6463, 0x0CD5F286, 0x56BA5B9C, 0x4516E144,
  0xFF2405DB, 0xEA926F19, 0x0F893B1E, 0x6F7E2895,
  0x0C93408A, 0x249F4557, 0xA9BF1ACB, 0x31C35D62,
  0xDDA929E6, 0xEBEF2AF9, 0xC9587847, 0x5D052BDF,
  0xFA2A06C1, 0xDF9E63C5, 0x983D8434, 0xD574A4F4,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,

  0xFFFFFFFF,
};*/



/*const struct MW_KEY cakeyVisa[] = {
  // 4 VisaInternet.cer
  0x00010001, // Exponent
  128,        // Key Length
  0xF796E2CE, 0x84D99BD8, 0xAEF35344, 0x432E8768,
  0x87BD2707, 0xB86854EC, 0x7B1047DC, 0x4C9AB915,
  0x4A665DDA, 0xCDF2B949, 0x347191C0, 0xFF2E002D,
  0xBE7A002B, 0xCA10344B, 0xCAACD563, 0xBDC0D587,
  0x32CF5A51, 0x772792AD, 0xD9F642F1, 0xC53D4214,
  0x7BC40E1D, 0x5FAC5DBA, 0x93294F13, 0x6A22A216,
  0xB7907EFB, 0x09A0344B, 0x7A4841B8, 0x0A37860C,
  0x0E22D5A5, 0x7A5242E9, 0x17B3BBAA, 0x05FA7534,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000,

  0xFFFFFFFF,
};*/



//*****************************************************************************
//  Function        : InjectCAKey
//  Description     : Inject Spectra key to base.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
/*void InjectCAKey(void)
{
  struct MW_KEY cakey;
  //BYTE tmp[MW_MAX_LINESIZE+1];

  //DispLineMW("CA KEY INJECTION", MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);

  gSSLKeyIdx = 0xFF;
 // if (!GetSSLKeyIdx(MW_LINE4, 0, 0xFF))
 //   return;


  memcpy(&cakey,cakeyVisa, sizeof(cakey));
  cakey.d_key_idx = gSSLKeyIdx;

  if (InjectCAKeyMW(&cakey)) {
    //sprintf(tmp, "Key %X Injected!", cakey.d_key_idx);
    //DispLineMW(tmp, MW_LINE5, MW_SPFONT);
	gSSL_KEY = cakey.d_key_idx;
  }
  //else
   // DispLineMW("Key Injection Fail!", MW_LINE5, MW_SPFONT);

  while (GetCharMW()==0) SleepMW();
}*/



//*****************************************************************************
//  Function        : InjectCAKey
//  Description     : Inject Spectra key to base.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN InjectCAKey(BYTE *certSslName)
{
  struct MW_KEY cakey;
  //BYTE tmp[MW_MAX_LINESIZE+1];

  //DispLineMW("CA KEY INJECTION", MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);

  /*if (!GetSSLKeyIdx(MW_LINE4, 0, 0xFF))
    return;*/

  DispClrBelowMW(MW_LINE2);
  if (!GetCAKey(&cakey, certSslName)) {
    //DispLineMW("Get CA Key Fail!", MW_LINE4, MW_CENTER|MW_SMFONT|MW_CLRDISP);
    //APM_WaitKey(300, 0);
    return FALSE;
  }

  //cakey.d_key_idx = gSSLKeyIdx;
  gSSL_KEY = cakey.d_key_idx;
  if (InjectCAKeyMW(&cakey)) {
    //sprintf(tmp, "Key %X Injected!", cakey.d_key_idx);
    //DispLineMW(tmp, MW_LINE5, MW_SPFONT);
	  return TRUE;
  }
  else{
	  TextColor("SSL Injection Fail!", MW_LINE4, COLOR_RED, MW_SPFONT|MW_CLRDISP,3);
    /*DispLineMW("SSL Injection Fail!", MW_LINE4, MW_SPFONT|MW_CLRDISP);
    APM_WaitKey(300, 0);*/
  }
  return FALSE;
}

