//-----------------------------------------------------------------------------
//  File          : sysutil.c
//  Module        :
//  Description   : Include Enhance System utitilies function.
//  Author        : Lewis
//  Notes         : No UI is include in the routines & call SYSTEM or UTILIB
//                  Only.
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include<stdlib.h>
#include "midware.h"
#include "util.h"
#include "hardware.h"
#include "files.h"
#include "corevar.h"
#include "constant.h"
#include "chkoptn.h"
#include "cajas.h"
#include "tranutil.h"
#include "hostmsg.h"
#include "sysutil.h"
#include "IpConfig.h"
#include "lptutil.h"


//-----------------------------------------------------------------------------
//      Defines
//-----------------------------------------------------------------------------
#define DEBUG_POINT         1
#define RESP_TIMEOUT      200
//-----------------------------------------------------------------------------
//      Constants
//-----------------------------------------------------------------------------
#define FLASH_START_ADDRESS         0x00100000
#define SRAM_END_ADDRESS            0x002FFFFF

BYTE FechaHoraRec[17];

static const BYTE KMonthText[13][3] =
{
		{ ' ', ' ', ' ' },
		{ 'E', 'N', 'E' },
		{ 'F', 'E', 'B' },
		{ 'M', 'A', 'R' },
		{ 'A', 'B', 'R' },
		{ 'M', 'A', 'Y' },
		{ 'J', 'U', 'N' },
		{ 'J', 'U', 'L' },
		{ 'A', 'G', 'O' },
		{ 'S', 'E', 'P' },
		{ 'O', 'C', 'T' },
		{ 'N', 'O', 'V' },
		{ 'D', 'I', 'C' } };

//*****************************************************************************
//  Function        : LongBeep
//  Description     : Generate long beep sound.
//  Input           : N/A
//  Return          : N/A
//  Note            : 30ms;
//  Globals Changed : N/A
//*****************************************************************************
void LongBeep(void)
{

	os_beep_open();
	BeepMW(30, 9, 1);
}
//*****************************************************************************
//  Function        : AcceptBeep
//  Description     : Generate a beep sound for accept.
//  Input           : N/A
//  Return          : N/A
//  Note            : 3 X 100ms
//  Globals Changed : N/A
//*****************************************************************************
void AcceptBeep(void)
{
	os_beep_open();
	BeepMW(10, 10, 3);
}
//*****************************************************************************
//  Function        : Short2Beep
//  Description     : Generate two short beep sound.
//  Input           : N/A
//  Return          : N/A
//  Note            : 2 X 20ms
//  Globals Changed : N/A
//*****************************************************************************
void Short2Beep(void)
{
	os_beep_open();
	BeepMW(2, 2, 2);
}
//*****************************************************************************
//  Function        : Short1Beep
//  Description     : Generate one short beep sound.
//  Input           : N/A
//  Return          : N/A
//  Note            : 1 X 20ms
//  Globals Changed : N/A
//*****************************************************************************
void Short1Beep(void)
{
	os_beep_open();
	BeepMW(2, 2, 1);
}
//*****************************************************************************
//  Function        : Delay1Sec
//  Description     : Delay for certain second or key pressed.
//  Input           : time;         // seconds to Delay
//                    beep_reqd;    // beep required ?
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void Delay1Sec(DWORD aTime, BOOLEAN aBeepReqd)
{
	if (aBeepReqd)
		LongBeep();

	aTime *= 200;  // convert to 5ms
	while (aTime)
	{
		SleepMW();
		if (GetCharMW() == MWKEY_CANCL)
			break;
		aTime--;
	}
}
//*****************************************************************************
//  Function        : ErrorDelay
//  Description     : Special Delay routine for error.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ErrorDelay(void)
{
	Delay1Sec(10, 1);
}
//*****************************************************************************
//  Function        : Delay10ms
//  Description     : Delay a number of 10ms .
//  Input           : cnt; // count of 10ms to Delay
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void Delay10ms(DWORD aCnt)
{
	aCnt *= 2;
	while (aCnt--)
		SleepMW();
}
//*****************************************************************************
//  Function        : SetRTC
//  Description     : Set Real Time clock.
//  Input           : aTDtg;     // pointer to structure DATETIME.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SetRTC(struct DATETIME *aTDtg)
{
	BYTE buffer[14];

	aTDtg->b_century = (aTDtg->b_year > 0x50) ? 0x19 : 0x20;
	split(buffer, (BYTE*) aTDtg, 7);
	RtcSetMW(buffer);
}
//*****************************************************************************
//  Function        : ReadRTC
//  Description     : Get Real Time clock.
//  Input           : dtg;     // pointer to structure struct DATETIME buffer.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ReadRTC(struct DATETIME *aTDtg)
{
	BYTE buffer[14];

	RtcGetMW(buffer);
	compress((BYTE*) aTDtg, buffer, 7);
}
//*****************************************************************************
//  Function        : ConvDateTime
//  Description     : Convert input DATETIME to specify string.
//  Input           : aBuf;       // pointer to output buf
//                    aTDtg;       // pointer struct DATETIME value
//                    aLongYear;  // convert to LONG year fmt when TRUE.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ConvDateTime(BYTE *aBuf, struct DATETIME *aTDtg, BOOLEAN aLongYear)
{
	memset(aBuf, ' ', 18);
	memcpy(aBuf, KMonthText[(BYTE) bcd2bin(aTDtg->b_month)], 3); /* month */
	split(&aBuf[4], &aTDtg->b_day, 1); /* day */
	if (aBuf[4] == '0')
		aBuf[4] = ' ';
	aBuf[6] = ',';
	split(&aBuf[8], &aTDtg->b_century, 2);
	split(&aBuf[13], &aTDtg->b_hour, 1);
	aBuf[15] = ':';
	//Delay10ms(100);
	split(&aBuf[16], &aTDtg->b_minute, 1);
	if (!aLongYear)
		memmove(&aBuf[8], &aBuf[10], 8);
}

void ConvDateTimeVisa(BYTE *aBuf, struct DATETIME *aTDtg)
{
	BYTE idx = 0;
	BYTE rocTemp[3];
	memset(rocTemp, 0x00, sizeof(rocTemp));

	memset(aBuf, ' ', 48);	//NM-25/04/13 Estaba 22

	memcpy(rocTemp, P_DATA.sb_roc_no, 3);

	if (gConfigComercio.numCaja == 0)	//NM-25/04/13 Agregue esta IF y ELSE
	{
		memcpy(&aBuf[0], "             ", 13);
		split(&aBuf[14], &aTDtg->b_day, 1);		// dia
		memcpy(&aBuf[16], "/", 1);
		split(&aBuf[17], &aTDtg->b_month, 1);	// mes
		memcpy(&aBuf[19], "/", 1);
		split(&aBuf[20], &aTDtg->b_century, 2);	// año
		memcpy(&aBuf[24], "    ", 4);
		split(&aBuf[28], &aTDtg->b_hour, 1);	// hora
		memcpy(&aBuf[30], ":", 1);
		split(&aBuf[31], &aTDtg->b_minute, 1);
		memcpy(&aBuf[33], ":", 1);
		split(&aBuf[34], &aTDtg->b_second, 1);
		memcpy(&aBuf[36], "             ", 13);

		memcpy(&FechaHoraRec[idx], &aBuf[14], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[17], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[22], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[28], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[31], 2 );
		idx += 2;
		split(&FechaHoraRec[idx], rocTemp, 3);

	}
	else
	{
		memcpy(&aBuf[0], "       ", 7);
		split(&aBuf[8], &aTDtg->b_day, 1);		// dia
		memcpy(&aBuf[10], "/", 1);
		split(&aBuf[11], &aTDtg->b_month, 1);	// mes
		memcpy(&aBuf[13], "/", 1);
		split(&aBuf[14], &aTDtg->b_century, 2);	// año
		memcpy(&aBuf[18], "    ", 4);
		split(&aBuf[22], &aTDtg->b_hour, 1);	// hora
		memcpy(&aBuf[24], ":", 1);
		split(&aBuf[25], &aTDtg->b_minute, 1);
		memcpy(&aBuf[27], ":", 1);
		split(&aBuf[28], &aTDtg->b_second, 1);
		memcpy(&aBuf[30], "   CAJA: ", 9);
		sprintf(&aBuf[39], "%04d", gConfigComercio.numCaja);
		memcpy(&aBuf[43], "      ", 6);


		memcpy(&FechaHoraRec[idx], &aBuf[8], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[11], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[17], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[22], 2 );
		idx += 2;
		memcpy(&FechaHoraRec[idx], &aBuf[25], 2 );
		idx += 2;
		split(&FechaHoraRec[idx], rocTemp, 3);

	}

	//	FreeMW(conf_comer);	//NM-25/04/13 Agregue esta linea
}

//*****************************************************************************
//  Function        : SetDTG
//  Description     : Sync RTC with input date time.
//  Input           : aDataTime;   // pointer to 6 byte date_time
//                                 // in bcd YYMMDDHHMMSS
//  Return          : System Year BCD value;
//  Note            : Year is adjust when necessary.
//  Globals Changed : N/A
//*****************************************************************************
BYTE SetDTG(BYTE *aDateTime)
{
	BYTE tmp;
	struct DATETIME dtg;

	ReadRTC(&dtg);
	tmp = 1;
	if ((aDateTime[1] == 0x12) && (dtg.b_month == 0x01))
		bcdsub(aDateTime, &tmp, 1);
	else if ((aDateTime[1] == 0x01) && (dtg.b_month == 0x12))
		bcdadd(aDateTime, &tmp, 1);
	memcpy(&dtg.b_year, aDateTime, 6);
	SetRTC(&dtg);

	return dtg.b_year;
}
//*****************************************************************************
//  Function        : ByteCopy
//  Description     : Same as memcpy. Use to avoid compiler bug.
//  Input           : dest;     // pointer to destination buffer
//                    src;      // pointer to source buffer
//                    len;      // number of bytes to copy
//  Return          : N/A
//  Note            : typedef struct {
//                      BYTE abc;
//                      WORD C;
//                    } a; b;
//                    memcpy(&a, &b, sizeof(a));
//                    IAR fail to generate correct code for about memcpy statement.
//  Globals Changed : N/A
//*****************************************************************************
void ByteCopy(BYTE *aDest, BYTE *aSrc, DWORD aLen)
{
	memcpy(aDest, aSrc, aLen);
}
//*****************************************************************************
//  Function        : CompressInputData
//  Description     : Fill the data with '0' & compress it.
//  Input           : aDest;    // pointer to destinate buffer;
//                    aSrc;     // pointer to src buffer.
//                              // 1st byte is len of input
//                    aLen;     // number of byte to compress.
//  Return          : N/A
//  Note            : Max len for input is 12.
//  Globals Changed : N/A
//*****************************************************************************
void CompressInputData(void *aDest, BYTE *aSrc, BYTE aLen)
{
	BYTE buffer[12];

	memset(buffer, '0', 12); /* 12 is the max len */
	memcpy(&buffer[aLen * 2 - aSrc[0]], &aSrc[1], aSrc[0]);
	compress(aDest, buffer, aLen);
}
//*****************************************************************************
//  Function        : CompressInputFData
//  Description     : Fill the data with 'F' & compress it.
//  Input           : aDest;    // pointer to destinate buffer;
//                    aSrc;     // pointer to src buffer.
//                              // 1st byte is len of input
//                    aLen;     // number of byte to compress.
//  Return          : N/A
//  Note            : Max len for input is 24.
//  Globals Changed : N/A
//*****************************************************************************
void CompressInputFData(void *aDest, BYTE *aSrc, BYTE aLen)
{
	BYTE tmp[25];

	memcpy(tmp, aSrc, aSrc[0] + 1);
	memset(&tmp[tmp[0] + 1], 'F', 24 - tmp[0]);
	compress(aDest, &tmp[1], aLen);
}
//*****************************************************************************
//  Function        : GetMonthText
//  Description     : Return pointer to three byte month text.
//  Input           : mm;    // month
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BYTE *GetMonthText(BYTE aMM)
{
	return (BYTE *) KMonthText[aMM];
}
//*****************************************************************************
//  Function        : MemFatalErr
//  Description     : Check for valid memory allocated pointer.
//                    Err: Prompt Error Message & Reset.
//  Input           : aMemPtr;          // pointer to allocated memory
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void MemFatalErr(void *aMemPtr)
{
	if (aMemPtr == NULL)
	{
		DispLineMW("Memory Error!", MW_LINE3, MW_CLRDISP|MW_BIGFONT);
		LongBeep();
		while (GetCharMW() == 0)
			SleepMW();
		ResetMW();
	}
}
//*****************************************************************************
//  Function        : DebugPoint
//  Description     : Display Msg on line 9 and wait for key press.
//  Input           : aStr;         // pointer to message string.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DebugPoint(char *aStr)
{
#ifdef DEBUG_POINT
	DispLineMW(aStr, MW_MAX_LINE, MW_REVERSE|MW_CLREOL|MW_SPFONT);
	while (GetCharMW() == 0)
		SleepMW();
#endif // DEBUG_POINT
}
//*****************************************************************************
//  Function        : ConvAmount
//  Description     : Converts a formated amount to a format easy for display
//                    and printing.
//  Input           : aAmt;       // DDWORD amount
//                    aDispAmt;   // pointer to sturct DISP_AMT buffer.
//                    aDecPos;    // Decimal Position
//                    aCurrSym;   // Currency Symbol.
//  Return          : TURE  => convertion ok
//                    FALSE => conversion fail, return all '9'
//  Note            : result = > len byte + $NNNN.NN
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ConvAmount(DDWORD aAmt, struct DISP_AMT *aDispAmt, DWORD aDecPos,
		BYTE aCurrSym)
{
	BYTE buffer[21];
	BYTE lead0len;
	BOOLEAN overflow;

	overflow = FALSE;
	dbin2asc(buffer, aAmt);

	if (memcmp(buffer, "00000000", 8) != 0)
	{
		overflow = TRUE;
		memset(&buffer[8], '9', 12);
	}
	lead0len = (BYTE) skpb(buffer, '0', 17);      // leading zero count
	aDispAmt->content[0] = aCurrSym;
	aDispAmt->len = 19 - lead0len;          // excluding decimal digits
	memcpy(&aDispAmt->content[1], &buffer[lead0len], aDispAmt->len - 1);
	if (aDecPos != 0)
	{
		buffer[17] = '.';
		memcpy(&aDispAmt->content[aDispAmt->len], &buffer[17], 3);
		aDispAmt->len += 3;
	}
	return overflow;
}
//*****************************************************************************
//  Function        : Disp2x16Msg
//  Description     : Display 2x16 message on lcd using DispLine
//  Input           : aMsg;         // pointer to 2x16 message
//                    aOffset;      // DispLine's offset
//                    aCtrl;        // DispLine's control 
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void Disp2x16Msg(const BYTE *aMsg, DWORD aOffset, DWORD aCtrl)
{
	DWORD next_line;
	BYTE tmpbuf[17];

	memset(tmpbuf, 0, sizeof(tmpbuf));
	memcpy(tmpbuf, aMsg, 16);
	DispLineMW(tmpbuf, aOffset, aCtrl|MW_CENTER);
	next_line =
			(aCtrl & MW_BIGFONT) ? (MW_LINE3 - MW_LINE1) :
					(MW_LINE2 - MW_LINE1);
	aCtrl &= ~MW_CLRDISP;
	memcpy(tmpbuf, &aMsg[16], 16);
	DispLineMW(tmpbuf, aOffset+next_line, aCtrl|MW_CENTER);
}

//*****************************************************************************
//  Function        : CheckPointerAddr
//  Description     : Check whether pointer is syster pointer (stack or malloc)
//                    or not
//  Input           : aPtr
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void CheckPointerAddr(void *aPtr)
{
#ifndef WIN32
	DWORD address;

	address = (DWORD) aPtr;
	if ((address < FLASH_START_ADDRESS) || (address > SRAM_END_ADDRESS))
		return;

	DispLineMW("Not A Valid Pointer!!", MW_MAX_LINE,
			MW_REVERSE|MW_CENTER|MW_SPFONT);
	LongBeep();
	while (GetCharMW() == 0)
		SleepMW();
#endif
}

//*****************************************************************************
//  Function        : RTrim
//  Description     : Remove character 'junk' to the Right of the string
//  Input           : char* aStr1, char* aStr2
//  Return          : string without 'junk'
//                    1 = Different
//  Note            : Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
BYTE *RTrim(BYTE *string, BYTE junk)
{
	BYTE* original = string + strlen(string);
	while (*--original == junk)
		;
	*(original + 1) = '\0';
	return string;
}

//*****************************************************************************
//  Function        : LTrim
//  Description     : Remove character 'junk' to the left of the string
//  Input           : char* aStr1, char* aStr2
//  Return          : string without 'junk'
//  Note            : Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
BYTE *LTrim(BYTE *string, BYTE junk)
{
	BYTE* original = string;
	char *p = original;
	int trimmed = 0;
	do
	{
		if (*original != junk || trimmed)
		{
			trimmed = 1;
			*p++ = *original;
		}
	} while (*original++ != '\0');
	return string;
}

//*****************************************************************************
//  Function        : StrCmp
//  Description     : Compares the C string str1 to the C string str2.
//  Input           : char* aStr1, char* aStr2
//  Return          : 0 = indicates that both strings are equal
//                    1 = Different
//  Note            : Esta funcion fue implementada porque memcmp does'nt ...
//                    works with string char
//  Globals Changed : N/A by Jorge Numa
//*****************************************************************************
int StrCmp(char* aStr1, char* aStr2)
{
	while (*aStr1 != '\0' || *aStr2 != '\0')
	{
		if (*aStr1 != *aStr2)
			return 1;

		aStr1++;
		aStr2++;
	}
	return 0;
}

//*****************************************************************************
//  Function        : RSetStr
//  Description     : Set the caracter 'aCar' to the right of the string = aStr
//  Input           : char* aStr1, char* aStr2
//  Return          : Nothing
//  Note            : By Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
void RSetStr(BYTE *aStr, WORD aLen, BYTE aCar)
{
	WORD i;

	if (strlen(aStr) >= aLen)
	{
		return;
	}

	for (i = strlen(aStr); i < aLen; i++)
	{
		*(aStr + i) = aCar;
	}
}

//*****************************************************************************
//  Function        : LSetStr
//  Description     : Set the caracter 'aCar' to the left of the string = aStr
//  Input           : char* aStr1, char* aStr2
//  Return          : Nothing
//  Note            : By Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
void LSetStr(BYTE *aStr, WORD aLen, BYTE aCar)
{
	WORD i;
	WORD j;
	BYTE tmpStr[aLen + 1];
	BYTE tmp[aLen + 1];

	if (strlen(aStr) >= aLen)
		return;

	memset(tmpStr, 0, sizeof(tmpStr));
	memcpy(tmpStr, aStr, strlen(aStr));

	for (i = 0; i < aLen - strlen(aStr); i++)
	{
		*(tmp + i) = aCar;
	}

	for (i = aLen - strlen(aStr), j = 0; i < aLen; i++, j++)
	{
		*(tmp + i) = *(tmpStr + j);
	}

	memcpy(aStr, tmp, aLen);

}

void byteToBcd(BYTE valor, BYTE * buffer)
{

	buffer[0] = (valor / 10) << 4;
	buffer[0] |= valor % 10;
}

void worToBcd(int valor, BYTE * buffer)
{

	BYTE valor1;
	valor1 = valor / 100;
	byteToBcd(valor1, buffer);
	valor1 = valor % 100;
	byteToBcd(valor1, buffer + 1);
}

int bcd2ToInt(BYTE * buffer, BYTE len)
{          //CHEQUES_POSTFECHADOS

	BYTE i;
	BYTE buff[3];
	BYTE buff2[20];
	memset(buff, 0x00, 3);
	memset(buff2, 0x00, sizeof(buff2));
	for (i = 0; i < len; i++)
	{
		sprintf(buff, "%02x", buffer[i]);
		strcat(buff2, buff);
	}
	return (atoi(buff2));
}

//*****************************************************************************
//  Function        : displaySI_NO
//  Description     : mustra en pantalla las opciones si y no con el color de cada tecla ENTER / CLEAR
//  Input           : N/A
//  Return          : Nothing
//  Note            : By Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
void displaySI_NO(void)
{
	TextColor("SI", MW_LINE9, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE9 + 2, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("ENTER", MW_LINE9 + 3, COLOR_GREEN, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", MW_LINE9 + 8, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("NO", MW_LINE9 + 10, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE9 + 12, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("CLEAR", MW_LINE9 + 13, COLOR_YELLOW, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", MW_LINE9 + 18, COLOR_VISABLUE, MW_SMFONT, 0);
	return;
}

void displaySI_NOLine(WORD aLine)			// **SR** 10/10/13
{
	TextColor("SI", aLine, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", aLine + 2, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("ENTER", aLine + 3, COLOR_GREEN, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", aLine + 8, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("NO", aLine + 10, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", aLine + 12, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("CLEAR", aLine + 13, COLOR_YELLOW, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", aLine + 18, COLOR_VISABLUE, MW_SMFONT, 0);
	return;
}


//*****************************************************************************
//  Function        : displaySI_NO_2
//  Description     : mustra en pantalla las opciones si y no con el color de cada tecla ENTER / CANCEL
//  Input           : N/A
//  Return          : Nothing
//  Note            : By Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
void displaySI_NO_2(void)
{
	TextColor("SI", MW_LINE9, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE9 + 2, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("ENTER", MW_LINE9 + 3, COLOR_GREEN, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", MW_LINE9 + 8, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("NO", MW_LINE9 + 10, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE9 + 12, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("CANCEL", MW_LINE9 + 13, COLOR_RED, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", MW_LINE9 + 19, COLOR_VISABLUE, MW_SMFONT, 0);
	return;
}

void DisplayHabPAN(void)
{
	TextColor("NO", MW_LINE8, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE8 + 2, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("0", MW_LINE8 + 3, COLOR_RED, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", MW_LINE8 + 4, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("SI", MW_LINE7, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE7 + 2, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("1", MW_LINE7 + 3, COLOR_YELLOW, MW_SMFONT | MW_REVERSE, 0);
	TextColor(">", MW_LINE7 + 4, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("SI", MW_LINE6, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE6 + 2, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("2", MW_LINE6 + 3, COLOR_GREEN, MW_SMFONT| MW_REVERSE, 0);
	TextColor(">", MW_LINE6 + 4, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("(*)", MW_LINE6 + 5, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("SI", MW_LINE5, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("<", MW_LINE5 + 2, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("3", MW_LINE5 + 3, COLOR_BLACK, MW_SMFONT| MW_REVERSE, 0);
	TextColor(">", MW_LINE5 + 4, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("(*)", MW_LINE5 + 5, COLOR_VISABLUE, MW_SMFONT, 0);

}

void displayTCP_RS232(void)          //Daniel Cajas TCP
{
	DispPutCMW(K_PushCursor);
	os_disp_textc(COLOR_VISABLUE);
	DispLineMW("LAN", MW_LINE7 + 5, MW_SMFONT);
	DispLineMW("<", MW_LINE7+9, MW_SMFONT);
	os_disp_textc(COLOR_GREEN);
	DispLineMW("ENTER", MW_LINE7+10, MW_SMFONT+MW_REVERSE);
	os_disp_textc(COLOR_VISABLUE);
	DispLineMW(">", MW_LINE7+14, MW_SMFONT);
	DispLineMW("COM1", MW_LINE8+5, MW_SMFONT);
	DispLineMW("<", MW_LINE8+9, MW_SMFONT);
	os_disp_textc(COLOR_YELLOW);		//kt-091012
	DispLineMW("CLEAR", MW_LINE8+10, MW_SMFONT+MW_REVERSE);
	os_disp_textc(COLOR_VISABLUE);
	DispLineMW(">", MW_LINE8+15, MW_SMFONT);
	DispPutCMW(K_PopCursor);
	return;
}

/***********************
 * **SR**
 ************************/
void DispSelecPage (WORD aNumPage, WORD aMaxPage, WORD aLine){

	if(aNumPage - 1 == 0)
		TextColor("sig >>>", aLine, COLOR_RED, MW_SMFONT | MW_RIGHT, 0);
	else if(aNumPage + 1 == aMaxPage)
		TextColor("<<< ant", aLine, COLOR_RED, MW_SMFONT | MW_LEFT, 0);
	else
		TextColor("<<< ant      sig >>>", aLine, COLOR_RED, MW_SMFONT | MW_CENTER, 0);

	return;
}

/******************************************************************************
 *  Function        : TransPassAdmin
 *  Description     : Pide clave de administrador descargada de la tabla cero y limpia desde la linea indicada
 *  Input           : line_Clean
 *  Note            : N/A
 *  Modified   by         : Manuel Barbaran
 *  Date : 15/07/2013
 *  Globals Changed : N/A
 ******************************************************************************/
BOOLEAN TransPassAdmin(DWORD line_Clean) //kt-141112
{
	struct TABLA_CERO tablaCero;
	BYTE kbdbuf[9];
	BYTE PassAdmin[4];

	GetTablaCero(&tablaCero);
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(PassAdmin, 0x00, sizeof(PassAdmin));


	while(TRUE)
	{
		DispClrBelowMW(line_Clean);

		TextColor("INGRESE PASSWORD", MW_LINE4, COLOR_VISABLUE, MW_SMFONT
				| MW_LEFT, 0);
		if (!APM_GetKbdSpectra(HIDE_NUMERIC + MW_SMFONT + MW_LINE6, IMAX(4),
				kbdbuf))
		{
			return FALSE;
		}

		compress(PassAdmin, &kbdbuf[1], 2);
		if (memcmp(&PassAdmin[0], &gTablaCero.b_pass_super[0], 2) != 0)
		{
			DispClrBelowMW(line_Clean);
			LongBeep();
			TextColor("PASSWORD INVALIDO", MW_LINE4, COLOR_RED, MW_SMFONT
					| MW_CENTER, 0); //kt-220413
			TextColor("RE INTENTAR", MW_LINE5, COLOR_RED, MW_SMFONT
					| MW_CENTER, 3);
			os_beep_close();
		}
		else
			break;

	}

	return TRUE;

}

/******************************************************************************
 *  Function        : DumpMemory
 *  Description     : Hace un volcado de memoria de 60 Car max.
 *  Input           : void *address -> Direccion de memoria
 *  Note            : en fuente spfont 26 caracteres * 7 Columnas (T1000)
 *  Globals Changed : By Jorge Numa
 ******************************************************************************/
void DumpMemory(void *address)
{
	BYTE key;
	DWORD i;
	const BYTE *screenAddress = address;

	do
	{
		DispLineMW("DUMP MEMORY", MW_LINE1,
				MW_SPFONT | MW_CENTER | MW_REVERSE | MW_CLRDISP);
		DispCtrlMW(MW_REV_OFF);

		for (i = 0; i < 60; i++)// 60 en el num de caracteres que se van a mostrar por página
		{
			printf("%02X", *(screenAddress + i));
		}

		key = WaitKey(9000, 0);

		if (key == MWKEY_CANCL)
		{
			break;
		}
	} while (key != MWKEY_CANCL);
}

void OpenUSB(void)
{
	BYTE filename[32];

	// Open USB file
	strcpy(filename, DEV_USBS);
	gIdUsb = OpenMW(filename, MW_RDWR);
	if (gIdUsb < 0)
	{
		DispLineMW("Error Abriendo USB!", MW_LINE4,
				MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);
		APM_WaitKey(300, 0);
		return;
	}
}

/******************************************************************************
 *  Function        : WriteUSB
 *  Description     : Abre el puerto USB y escribe lo que esté en aBuf2Send
 *  Input           : BYTE *aBuf2Send -> Buffer a enviar
 *  Note            : N/A
 *  Globals Changed : By Jorge Numa
 ******************************************************************************/

void WriteUSB(BYTE *aBuf2Send, DWORD aLen)
{
	DWORD status = 0;

	// Verifica el estado
	status = StatMW(gIdUsb, MW_USBS_STATUS, NULL);
	if (status & MW_USB_TXRDY)	// Si está listo? -> escriba en el puerto
	{
		// Escribe en el puesrto USB
		//printf("ESCIBE USB\n");
		//APM_WaitKey(9000,0);
		WriteMW(gIdUsb, aBuf2Send, aLen);
	}

	//CloseMW(gIdUsb);

}

//*****************************************************************************
//  Function        : TextColor
//  Description     : Muestra Texto con Color.
//  Input           : BYTE *Text, DWORD Line, DWORD Color, DWORD Comands, DWORD timeSec
//  Return          : Nothing
//  Note            : By Manuel Barbaran.
//  Globals Changed : N/A
//*****************************************************************************
void TextColor(BYTE *Text, DWORD Line, DWORD Color, DWORD Comands,
		DWORD timeSec)  //MFBC/23/11/12
{
	DispPutCMW(K_PushCursor);
	os_disp_textc(Color);
	DispLineMW(Text, Line, Comands);
	DispPutCMW(K_PopCursor);
	//	Delay10ms(timeSec);
	Delay1Sec(timeSec, 0);
}

//*****************************************************************************
//  Function        : Estado_Init
//  Description     : Verifica si ya se realizo la inicializacion
//  Input           : Nothing
//  Return          : Nothing
//  Note            : By Manuel Barbaran.
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN Estado_Init(void) //MFBC/14/01/13
{
	if (gAppDat.estadoInit == FALSE)
	{
		LongBeep();
		TextColor("INICIALICE !! ", MW_LINE5, COLOR_VISABLUE,
				MW_CLRDISP | MW_BIGFONT | MW_CENTER, 3);
		RefreshDispAfter(0);
		return FALSE;
	}
	return TRUE;
}

/******************************************************************************
 *  Function        : FormatAmount
 *  Description     : Formatea el monto con separador de miles y millones hasta
 9 digitos
 *  Input           : DDWORD aAmt, struct DISP_AMT *aDispAmt, DWORD aDecPos, 
 BYTE aCurrSym
 *  Note            : N/A
 *  Globals Changed : By Jorge Numa
 ******************************************************************************/
BOOLEAN FormatAmount(DDWORD aAmt, struct DISP_AMT *aDispAmt, DWORD aDecPos,
		BYTE aCurrSym)
{
	BYTE buffer[21];
	BYTE lead0len;
	BOOLEAN overflow;
	WORD len = 0;
	WORD i;
	BOOLEAN isNegative = FALSE;

	overflow = FALSE;
	memset(buffer, 0x00, sizeof(buffer));

	dbin2asc(buffer, aAmt);

	if (memcmp(buffer, "00000000", 8) != 0)
	{
		overflow = TRUE;
		memset(&buffer[8], '9', 12);
	}

	// Jorge Numa =>>> Chequea si el valor es negativo --> requerido por Credibanco 19-02-13
	if(aCurrSym !=0)
	{
		if (buffer[8] == '9')
		{
			for (i = 9; i < strlen(buffer); i++)
			{
				if (buffer[i] != '0')
				{
					buffer[8] = '0';	// Es un valor negativo (set 0)
					isNegative = TRUE;
					break;
				}
			}
		}
	}
	lead0len = (BYTE) skpb(buffer, '0', 17); /* leading zero count */



	if(aCurrSym !=0 || aDecPos == 0 || aAmt == 0)
	{
		len = strlen(buffer) - lead0len - 2;	// - 2 por lo decimales
	}
	else{
		len = strlen(buffer) - lead0len;
	}

	memmove(&buffer[0], &buffer[lead0len], len);

	switch (len)
	{
	case 4:
		memmove(&buffer[2], &buffer[1], len - 1);
		buffer[1] = ',';
		len += 1;
		break;
	case 5:
		memmove(&buffer[3], &buffer[2], len - 2);
		buffer[2] = ',';
		len += 1;
		break;

	case 6:
		memmove(&buffer[4], &buffer[3], len - 3);
		buffer[3] = ',';
		len += 1;
		break;
	case 7:
		memmove(&buffer[2], &buffer[1], len - 1);
		buffer[1] = '\'';
		memmove(&buffer[6], &buffer[5], 3);
		buffer[5] = ',';
		len += 2;
		break;
	case 8:
		memmove(&buffer[3], &buffer[2], len - 2);
		buffer[2] = '\'';
		memmove(&buffer[7], &buffer[6], 3);
		buffer[6] = ',';
		len += 2;
		break;
	case 9:		//123456789 -> 123'456.789
		memmove(&buffer[4], &buffer[3], len - 3);
		buffer[3] = '\'';
		memmove(&buffer[8], &buffer[7], 3);
		buffer[7] = ',';
		len += 2;
		break;
	case 10:	//1234567890 -> 1.234'567.890
		memmove(&buffer[2], &buffer[1], len - 1);
		buffer[1] = '.';
		memmove(&buffer[6], &buffer[5], len - 4);
		buffer[5] = '\'';
		memmove(&buffer[10], &buffer[9], 3);
		buffer[9] = ',';
		len += 3;
		break;
	case 11:	// 12345678901 -> 12.345'678901
		memmove(&buffer[3], &buffer[2], len - 2);
		buffer[2] = '.';
		memmove(&buffer[7], &buffer[6], len - 5);
		buffer[6] = '\'';
		memmove(&buffer[11], &buffer[10], 3);
		buffer[10] = ',';
		len += 3;
		break;
	case 12:	// 123456789012 -> 123.456'789,012
		memmove(&buffer[4], &buffer[3], len - 3);
		buffer[3] = '.';
		memmove(&buffer[8], &buffer[7], len - 6);
		buffer[7] = '\'';
		memmove(&buffer[12], &buffer[11], 3);
		buffer[11] = ',';
		len += 3;
		break;
	}

	if(aCurrSym !=0)
	{
		if (isNegative)
		{
			aDispAmt->content[0] = '-';
			aDispAmt->content[1] = aCurrSym;
			//aDispAmt->len = 19 -lead0len;          /* excluding decimal digits */
			aDispAmt->len = len + 2;
			memcpy(&aDispAmt->content[2], buffer, aDispAmt->len - 2);

		}
		else
		{
			aDispAmt->content[0] = aCurrSym;
			//aDispAmt->len = 19 -lead0len;          /* excluding decimal digits */
			aDispAmt->len = len + 1;
			memcpy(&aDispAmt->content[1], buffer, aDispAmt->len - 1);
		}
	}
	else
	{
		aDispAmt->len = len;
		memcpy(&aDispAmt->content[0], buffer, aDispAmt->len);

	}

	/*
	 if (aDecPos != 0) {
	 buffer[17] = '.';
	 memcpy(&aDispAmt->content[aDispAmt->len], &buffer[17], 3);
	 aDispAmt->len += 3;
	 }
	 */
	return overflow;
}

//*****************************************************************************
//  Function        : ShowStatus
//  Description     : Show message on status line.
//  Input           : aMsg;     // pointer to string message
//                    aMode;    // Display Mode
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ShowStatus(char *aMsg, DWORD aMode)
{
	DispLineMW(aMsg, MW_MAX_LINE, MW_REVERSE|MW_CENTER|MW_SPFONT);
}

BOOLEAN no_paper(void)  //MFBC/26/02/13
{
	DWORD status = 0, i = 0;
	gLptHandle = -1;
	os_lpt_open(NULL);
	for (i = 0; i < 3; i++)
	{

		status = os_lpt_status(0);
		Delay10ms(5);
	}

	if ((status & K_LptPaperout) || (status & MW_TMLPT_SPAPEROUT))
	{
		IOCtlMW(gLptHandle, IO_LPT_RESET, NULL);
		LongBeep();
		return TRUE;
	}
	else
	{
		IOCtlMW(gLptHandle, IO_LPT_RESET, NULL);
		return FALSE;
	}
}

BOOLEAN Papel(void)  //MFBC/26/02/13
{
	if (no_paper())
	{
		LongBeep();
		TextColor("Impresora", MW_LINE3, COLOR_RED,
				MW_BIGFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("sin papel", MW_LINE5, COLOR_RED, MW_BIGFONT | MW_CENTER, 2);
		os_lpt_close();
		RefreshDispAfter(0);
		ResetTerm(); //MFBC/04/03/13
		return FALSE;
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : BCDtoDEC
//  Description     : Pasa un valor en BCD (0 - 9) a su equivalente en decimal
//  Input           : b_Value;  // Valor en BCD
//                    d_Len;    // Tama�o del valor en BCD (no mayor a 5 bytes)
//  Return          : Valor en Decimal  : -1 si d_Len es mayor de 5
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************

DDWORD BCDtoDEC(BYTE *b_Value, DWORD d_Len)  //MFBC/11/03/13
{
	DDWORD d_Product = 1;
	DDWORD d_ValDec = 0, d_ValdecAux = 0;
	if (d_Len > 5)
		return -1;

	while (TRUE)
	{
		d_Len--;
		d_ValdecAux = (b_Value[d_Len] & 0x0F) * d_Product;
		d_Product *= 10;
		d_ValDec += d_ValdecAux;
		d_ValdecAux = (b_Value[d_Len] & 0xF0);
		d_ValdecAux = (d_ValdecAux >> 4) * d_Product;
		d_ValDec += d_ValdecAux;
		d_Product *= 10;
		if (d_Len <= 0)
			break;
	}
	return d_ValDec;
}


//*****************************************************************************
//  Function        : Verf_fecha
//  Description     : Verifica que una fecha sea valida
//  Input           : el dia, el mes y el anio es opcional(si es -1 toma el anio de la terminal)
//  Return          : TRUE/FALSE
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************

BOOLEAN Verf_fecha(DWORD d_Dia, DWORD d_Mes, DWORD d_anio)
{
	struct DATETIME dtg;
	DWORD diasTemp;
	BYTE b_anio[2];
	memset(b_anio, 0x00, sizeof(b_anio));

	ReadRTC(&dtg);
	if (d_anio == -1)
	{
		memcpy(b_anio, &dtg.b_century, 2);
		d_anio = BCDtoDEC(b_anio, 2);
	}

	if (d_Mes <= 0 || d_Mes > 12 || d_Dia <= 0 || d_Dia > 31)
	{
		LongBeep();
		return FALSE;
	}

	switch (d_Mes)
	{
	case 2:
		if (leap_year(d_anio))
			diasTemp = 29;
		else
			diasTemp = 28;
		break;
	case 4:
	case 6:
	case 9:
	case 11:
		diasTemp = 30;
		break;
	default:
		diasTemp = 31;
		break;

	}

	if (d_Dia > diasTemp)
	{
		LongBeep();
		return FALSE;
	}
	return TRUE;
}


//*****************************************************************************
//  Function        : Cajas
//  Description     : Valida si esta habilitado integracion a cajas
//  Input           : N/A
//  Return          : TRUE/FALSE
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN Cajas(void) //MFBC/18/04/13
{
	struct TABLA_CERO tabla_Cero; //MFBC/02/05/13
	GetTablaCero(&tabla_Cero);

	if ((gTablaCero.b_dir_ciudad[43] == '1'
			|| gTablaCero.b_dir_ciudad[43] == '3')	//kt-viernes
			&& gConfigComercio.flagCaja == TRUE)
		return TRUE;
	return FALSE;
}


//*****************************************************************************
//  Function        : text_screen
//  Description     : Muestra en pantalla  principal el mensaje
//  Input           : N/A
//  Return          : TRUE/FALSE
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
void text_screen (void)
{
#define MAX_LINESIZE      (20)
	BYTE mensaje[20];
	BYTE mensajeCompleto[79];
	int lenMsg = 0;

	// JORGE NUMA 04/10/2013
#if defined FULL_PRUEBA || defined PARCIAL_PRUEBA
	/*******MENSAJES PARA DESARROLLO*******/

	lenMsg = 36;
	BYTE KMensajePantalla[] =    // mensaje para desarrollo
	{ "  TERMINAL  DE  PRUEBA   " }; //25 bytes
	memset(mensajeCompleto, 0x00, lenMsg);
	strcat(mensajeCompleto, KMensajePantalla);
	strcat(mensajeCompleto , VERCB); // 10 bytes
	/******************************************/

#else
#if defined FULL_PRODUCCION || defined PARCIAL_PRODUCCION
	if (os_rc531_open() == K_ERR_RC531_OK)
	{
		/*******MENSAJES PARA PRODUCCION CTLS*******/

		lenMsg = 78;

		BYTE KMensajePantalla[] =    // mensaje para desarrollo
		{ "   INSERTE/DESLICE TARJETA  TARJETA CTLS PRESIONE ENTER  " }; //57 bytes
		BYTE KMensajePantallaComercio[] =
		{ " COMERCIO " }; //10 bytes

		memset(mensajeCompleto, 0x00, lenMsg);
		strcat(mensajeCompleto, KMensajePantallaComercio);
		strcat(mensajeCompleto , VERCB); // 10 bytes
		strcat(mensajeCompleto, KMensajePantalla);
		/*****************************************/

	}
	else
	{
		/*******MENSAJES PARA PRODUCCION*******/

		lenMsg = 49;

		BYTE KMensajePantalla[] =    // mensaje para desarrollo
		{ "   INSERTE/DESLICE TARJETA  " }; //28 bytes
		BYTE KMensajePantallaComercio[] =
		{ " COMERCIO " }; //10 bytes

		memset(mensajeCompleto, 0x00, lenMsg);
		strcat(mensajeCompleto, KMensajePantallaComercio);
		strcat(mensajeCompleto , VERCB); // 10 bytes
		strcat(mensajeCompleto, KMensajePantalla);
		/*****************************************/

	}
#endif  // #if defined FULL_PRODUCCION || defined PARCIAL_PRODUCCION
#endif  // #if defined FULL_PRUEBA || defined PARCIAL_PRUEBA

	g_count_text++;

	if (g_count_text == 2)
	{
		memset(mensaje, 0x00, sizeof(mensaje));
		g_count_text = 0;
		g_count_ind++;
		DispClrLineMW(MW_LINE2);

		if(g_count_ind > MAX_LINESIZE)
		{
			memcpy(mensaje, &mensajeCompleto[g_count_ind - MAX_LINESIZE ], MAX_LINESIZE );
			TextColor(mensaje, MW_LINE2 , COLOR_VISABLUE,
					MW_SMFONT  , 0);
		}
		else
		{
			memcpy(mensaje, mensajeCompleto, g_count_ind);
			TextColor(mensaje, MW_LINE2 + ( MAX_LINESIZE - g_count_ind), COLOR_VISABLUE,
					MW_SMFONT  , 0);
		}

		if(g_count_ind == (lenMsg + MAX_LINESIZE) - 1 )
			g_count_ind = 1;
	}

}

BOOLEAN passAdminComercio (DWORD lineClean)
{
	BYTE kbdbuf[20];
	BYTE PassTecnico[10];

	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
	gIdTabla4 = OpenFile(KTabla4File);
	GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(PassTecnico, 0x00, sizeof(PassTecnico));

	while(TRUE)
	{
		DispClrBelowMW(lineClean);
		TextColor("INGRESE PASSWORD", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);

		if (!APM_GetKbdSpectra(HIDE_NUMERIC + MW_SMFONT + MW_LINE7, IMAX(9), kbdbuf)) //kt-140912
		{
			CloseFile(gIdTabla4);
			FreeMW(p_tabla4);
			return FALSE;
		}

		memcpy(PassTecnico, &gTablaCuatro.b_cod_estable[4], 9);
#if 0
		printf("\fPass:%s", PassTecnico);
		APM_WaitKey(9000,0);
#endif

		if (gAppDat.estadoInit)
		{
			if (StrCmp(&kbdbuf[1], PassTecnico) != 0) //MFBC/14/11/12
			{
				DispClrBelowMW(lineClean);
				LongBeep();
				TextColor("PASSWORD INVALIDO", MW_LINE4, COLOR_RED, MW_SMFONT
						| MW_CENTER, 0); //kt-220413
				TextColor("RE INTENTAR", MW_LINE5, COLOR_RED, MW_SMFONT
						| MW_CENTER, 3);
				os_beep_close();
			}
			else
				break;

		}
		else if (StrCmp(&kbdbuf[1], "000000") != 0) //MFBC/14/11/12
		{
			DispClrBelowMW(lineClean);
			LongBeep();
			TextColor("PASSWORD INVALIDO", MW_LINE4, COLOR_RED, MW_SMFONT
					| MW_CENTER, 0); //kt-220413
			TextColor("RE INTENTAR", MW_LINE5, COLOR_RED, MW_SMFONT
					| MW_CENTER, 3);
			os_beep_close();
		}
		else
			break;

	}

	CloseFile(gIdTabla4);
	FreeMW(p_tabla4);
	return TRUE;
}


BOOLEAN check_lote (void)
{
	int rec_cnt = 0;

	rec_cnt = APM_GetRecCount();

	if(rec_cnt != 0)
	{
		return TRUE;
	}
	return FALSE;
}

int Hour2Min( WORD aHour, WORD aMin)
{
	WORD min;

	min = aHour * 60;
	min += aMin;

	return min;
}


int Hour2Min_2( char *aHour)
{
	short min, hora, minuto;
	char hh[3];
	char mm[3];

	memset(hh, 0x00, sizeof(hh));
	memset(mm, 0x00, sizeof(mm));

	memcpy(hh, aHour, 2);
	memcpy(mm, &aHour[2], 2);

	hora = atoi(hh);
	minuto = atoi(mm);

	min = hora * 60;
	min += minuto;

	return min;
}

int tabla_meses(int meses, int febrero)
{
	switch (meses){

	case 1:

		return 31;
		break;

	case 2:

		if( febrero == 0)
			return 60;
		else
			return 59;
		break;

	case 3:

		if( febrero == 0)
			return 91;
		else
			return 90;
		break;

	case 4:

		if( febrero == 0)
			return 121;
		else
			return 120;
		break;

	case 5:

		if( febrero == 0)
			return 152;
		else
			return 151;
		break;

	case 6:

		if( febrero == 0)
			return 182;
		else
			return 181;
		break;

	case 7:

		if( febrero == 0)
			return 213;
		else
			return 212;
		break;

	case 8:

		if( febrero == 0)
			return 244;
		else
			return 243;
		break;

	case 9:

		if( febrero == 0)
			return 274;
		else
			return 273;
		break;

	case 10:

		if( febrero == 0)
			return 305;
		else
			return 304;
		break;

	case 11:

		if( febrero == 0)
			return 335;
		else
			return 334;
		break;

	case 12:

		if( febrero == 0)
			return 366;
		else
			return 365;
		break;


	default:
		return 0;
	}


}

short Bisiesto( short year )
{
	int res = year % 4;

	if (year % 100 == 0 && year % 400!= 0)
		return 0;   // No es bisiesto
	else if((res == 0 && year % 100 != 0 ) || (year % 400 == 0 && year % 100 == 0 ))
		return 1;   // bisiesto
	else
		return 0;   // No es bisiesto

}


int Date2Min(char *buffer)
{
	char seg[ 3 ], min[ 3 ], hora[ 3 ], dia[ 3 ],mes[ 3 ],year[ 3 ];
	int actualYear = 0;
	int min_t = 0,hor_t = 0,dia_t = 0,mes_t = 0, dia_mes = 0, febrero = 0, minutos = 0;
	int year2min = 0;
	char dateTime[14];
	char sysYear[5];

	memset(dateTime, 0x00, sizeof(dateTime));
	memset(sysYear, 0x00, sizeof(sysYear));

	memcpy(sysYear, "13", 2);

	actualYear = atoi(sysYear);

	memcpy(year, &buffer[2], 2);
	memcpy(mes, &buffer[4], 2);
	memcpy(dia, &buffer[6], 2);
	memcpy(hora, &buffer[8], 2);
	memcpy(min, &buffer[10], 2);
	memcpy(seg, &buffer[12], 2);

	febrero = atoi(year) % 4;

	min_t = atoi(min);
	hor_t = atoi(hora) * 60;
	dia_t = (atoi(dia) - 1) * 24 * 60;
	dia_mes = tabla_meses(atoi(mes) - 1, febrero);
	mes_t = (dia_mes * 24 * 60);

	if( Bisiesto(atoi(year)) ) {
		year2min = ((atoi(year) - actualYear) * 366 * 24 * 60);
	}
	else{
		year2min = ((atoi(year) - actualYear) * 365 * 24 * 60);
	}

	minutos = min_t + hor_t + dia_t + mes_t + year2min;

	return minutos;
}

void GetIP( BYTE *aIP )
{
	struct MW_NIF_INFO netinfo;
	BYTE dhcp;

	GetSysCfgMW(MW_SYSCFG_DHCP_ENABLE, &dhcp);
	dhcp = (dhcp != 0) ? 1 : 0;
	if (dhcp)
	{
		NetInfoMW(MW_NIF_ETHERNET, &netinfo);
		memcpy(aIP, (BYTE *) &netinfo.d_ip, 4);
	}
	else
	{
		GetSysCfgMW(MW_SYSCFG_IP, &aIP[0]);
	}
}

void convertIPv4 ( BYTE *destIP, BYTE *IP, BYTE *separator )
{
	WORD index = 0;

	BYTE *IPPointer = NULL;

	BYTE tempIP[ 4 ] = "";

	if ( ( IPPointer = strtok( IP, separator ) ) != NULL )
		do
			tempIP[ index++ ] = atoi( IPPointer );
		while( ( IPPointer = strtok( NULL, separator ) ) != NULL );

	memcpy( destIP, tempIP, 4 );

	return;
}




//*****************************************************************************
//  Function        : codigoAgencia
//  Description     : Busca el nombre de la agencia que baja de la inicializacion
//  Input           : N/A
//  Return          : FALSE
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN codigoAgencia (void)
{
	DWORD CodTemp = 0;
	WORD reg_idx;
	BYTE kbdbuf[25];
	BYTE CodAgencia[3];
	//BOOLEAN flag_Nacional = 0;
	BYTE *p_tabla5 = (void *) MallocMW(sizeof(struct TABLA_CINCO));
	WORD max_reg = GetTabla5Count();
	gIdTabla5 = OpenFile(KTabla5File);


	memset(CodAgencia, 0x00, sizeof(CodAgencia));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));


	while (TRUE)
	{
		memset(g_TransCodeAgencia, 0x00, sizeof(g_TransCodeAgencia));

		if(!Cajas()){
			DispClrBelowMW(MW_LINE2);
			CodTemp = 0; //kt-291112
			memset(kbdbuf, 0x00, sizeof(kbdbuf));
			TextColor("Codigo Agencia:", MW_LINE3, COLOR_VISABLUE,
					MW_SMFONT | MW_LEFT, 0);
			if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE5
					+ RIGHT_JUST, IMIN(1) + IMAX(2), kbdbuf)) //MFBC/20/03/13 cambiar el minimo
			{
				CloseFile(gIdTabla5);
				FreeMW(p_tabla5);
				return FALSE;
			}

			if (kbdbuf[0] == 1)
			{
				memcpy(&CodAgencia[0], "0", kbdbuf[0]);
				memcpy(&CodAgencia[1], &kbdbuf[1], kbdbuf[0]);

			}
			else
			{
				memcpy(&CodAgencia[0], &kbdbuf[1], kbdbuf[0]);

			}
		}
		else{
			memset(&CodAgencia[0], 0x00, sizeof(CodAgencia));
			memset(&kbdbuf[0], 0x00, sizeof(&kbdbuf[1]));
			memcpy(&CodAgencia[0], gCajas.codAgencia_98, 2);			// Codigo de la agencia Enviado por la Caja **SR** 02/09/13
			kbdbuf[0] = strlen(gCajas.codAgencia_98);
			memcpy(&kbdbuf[1], gCajas.codAgencia_98, kbdbuf[0]);		// Se copia al Buffer para que realice la busqueda **SR** 02/09/13
		}

		//compress(&g_aeroTransCode[0], &CodAgencia[0], 1);
		compress(&g_TransCodeAgencia[0], &CodAgencia[0], 1);

		for (reg_idx = 0; reg_idx < max_reg; reg_idx++)
		{
			if (!GetTablaCinco(reg_idx, (struct TABLA_CINCO*) p_tabla5))
				continue;

			CodTemp = decbin8b(&kbdbuf[1], kbdbuf[0]);

			if ((CodTemp % 2) == 0) //kt-291112
			{
				CodTemp -= 1;
			}

			if (CodTemp == ((struct TABLA_CINCO*) p_tabla5)->b_cant_datos)
				break;
		}

		if (reg_idx == max_reg) //kt-301012 probar kt
		{
			LongBeep(); //MFBC/01/04/13
			DispClrBelowMW(MW_LINE2);
			TextColor("No existe codigo", MW_LINE5, COLOR_RED,MW_SMFONT | MW_CENTER, 3);
			if(Cajas()){			// Si el codigo no existe con Cajas termina la transaccion **SR** 02/09/13
				CloseFile(gIdTabla5);
				FreeMW(p_tabla5);
				return FALSE;
			}

			continue;
		}
		else
		{
			memcpy(INPUT.b_CodAgencia, &gTablaCinco.b_cod_ean[0], 15);
			if(Cajas()){			// No pide en pantalla la confirmacion **SR** 02/09/13
				CloseFile(gIdTabla5);
				FreeMW(p_tabla5);
				return TRUE;
			}
			TextColor(INPUT.b_CodAgencia, MW_LINE7, COLOR_VISABLUE,MW_SMFONT | MW_LEFT, 0);
			displaySI_NO();
		}

		if(!Cajas()){		// No pide en pantalla la confirmacion **SR** 02/09/13
			switch (APM_YesNo())
			{
			case 1:
				continue;
			case 2:
				break;
			default:
				continue;
			}
			break;
		}
	}

	CloseFile(gIdTabla5);
	FreeMW(p_tabla5);
	return TRUE;

}

//*****************************************************************************
//  Function        : campo63
//  Description     : Guarda el campo 63
//  Input           : el serial del pos
//  Return          : FALSE
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
void campo63 (BYTE *serialPos)
{
	if ( (referencia() == TRUE   && TX_DATA.b_terminal != 2) && (TX_DATA.b_trans == SALE_SWIPE || TX_DATA.b_trans == SALE_ICC ||  TX_DATA.b_trans == SUPERCUPO
			|| TX_DATA.b_trans == SALE_CTL) ) //kt-180413
	{
		TX_DATA.w_lenField63 += 17;
		memcpy(TX_DATA.sb_field63, "00", 2),memcpy(TX_DATA.sb_field63 + 2, TX_DATA.sb_num_ref, 15 );
	}
	else if ( ( referencia() == FALSE && TX_DATA.b_terminal != 2 ) && (TX_DATA.b_trans == CONSULTA_COSTO || TX_DATA.b_trans == LOGON
			|| TX_DATA.b_trans == SALE_CTL ||  TX_DATA.b_trans == SUPERCUPO ) )
	{
		TX_DATA.w_lenField63 += 17;
		memcpy(TX_DATA.sb_field63, "00000000000000000", 17);
	}
	if ( TX_DATA.b_terminal != 2 )
	{
		//memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, VERCB_SEND, 10);
		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, TX_DATA.sb_terminal_id, 8);
		TX_DATA.w_lenField63 += 8;
		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, &gTablaCuatro.b_cod_estable[4], 9);
		TX_DATA.w_lenField63 += 9;
		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, "A", 1);
		TX_DATA.w_lenField63 += 1;
		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, "  ", 2);
		TX_DATA.w_lenField63 += 2;
//		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, VERCB, 10);	 //MFBC/05/09/13
//		TX_DATA.w_lenField63 += 10;
//		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, "0000000000", 10 );
//		TX_DATA.w_lenField63 += 10; //MFBC/18/03/13
	}

	if (TX_DATA.b_terminal == 2)
	{
		memcpy(TX_DATA.sb_field63 , gAppDat.TermNo, 8);
		TX_DATA.w_lenField63 += 8;
		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63  , &gTablaCuatro.b_cod_estable[4], 11);
		TX_DATA.w_lenField63 += 11;
		memcpy(TX_DATA.sb_field63 + TX_DATA.w_lenField63, " ", 1);

	}

	INPUT.w_lenField63 = TX_DATA.w_lenField63;
	memcpy(INPUT.sb_field63, TX_DATA.sb_field63, TX_DATA.w_lenField63);

}

/*********************************************
 * Descripcion: Si el buffer contiene valores diferntes en nuemro setea todo el buffer en CEROS ASCII
 * Autor: Manuel Barbaran
 *******************************************/
void checkbuffNum (BYTE *buffNume, int len)
{
	int i;
	DWORD numero ;

	for (i = 0; i < len; i ++ )
	{
		numero = 0x30;

		while (TRUE)
		{
			if(buffNume[i] != numero)
			{
				if (numero == 0x39)
				{
					memset(buffNume, 0x30, len);
					return;
				}
				numero += 0x01;
				continue;
			}
			break;
		}

	}
	return;
}


//*****************************************************************************
//  Function        : confirmarMonto
//  Description     : Muestra el mensaje de confirmar monto
//  Input           : monto a mostrar
//  Return          : FALSE
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN confirmarMonto (DDWORD dd_monto)
{
	DWORD keyin = 0;

	while (TRUE)
	{
		DispClrBelowMW(MW_LINE2);
		TextColor("CONFIRMAR MONTO:", MW_LINE3, COLOR_VISABLUE, MW_LEFT
				| MW_BIGFONT, 0);
		DispAmnt(dd_monto, MW_LINE5, MW_BIGFONT);

		displaySI_NO_2();
		keyin = APM_YesNo();

		if (keyin == 2)
			return TRUE;
		else if(keyin == -1)
		{
			BeepMW(30, 9, 1);
			TextColor("Operacion Terminada", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor("Por Time-Out,", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 0);
			TextColor("intente", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 0);
			TextColor("Nuevamente", MW_LINE6, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 3);
			return FALSE;
		}
		else if(keyin == 0)
		{
			return FALSE;
		}
		else
		{
			continue;
		}

	}

}

//*****************************************************************************
//  Function        : WaitKey
//  Description     : Wait a key Input.
//  Input           : N/A
//  Return          : Key Code.
//  Note            : Jorge Numa 18/09/2013
//  Globals Changed : N/A
//*****************************************************************************
DWORD WaitKey2(void)
{
	DWORD ch;

	// Clear key buffer
	KbdFlushMW();

	// Wait key
	do
	{
		SleepMW();
		ch = GetCharMW();
	} while (ch == 0);

	return ch;
}

//*****************************************************************************
//  Function        : GetString
//  Description     : Get a String input.
//  Input           : aDest;      // output buffer
//                    aPos;       // line pos.
//                    aMaxLen;    // max len;
//  Return          : -1;         // Cancel;
//                    other;      // len of input.
//  Note            : Jorge Numa 18/09/2013
//  Globals Changed : N/A
//*****************************************************************************
int GetOTP(char *aDest, DWORD aPos, int aMinLen, int aMaxLen)
{
	int cursor;
	char *mask = MallocMW( aMaxLen + 1);

	memset(mask, 0, sizeof(mask));
	//    cursor = strlen (aDest);
	cursor = 0;

	memset(aDest, 0, sizeof(aDest));

	while (1)
	{
		DWORD ch;

		aDest[cursor] = 0;
		mask[cursor] = 0;
		DispClrLineMW(aPos);
		//        DispClrLineMW(MW_LINE4);
		//        DispLineMW(aDest, MW_LINE4, MW_RIGHT|MW_SMFONT);
		DispLineMW(mask, aPos, MW_RIGHT|MW_SMFONT);

		ch = WaitKey2();

		//        if ((ch >= '0') && (ch <= '9'))

		if ( ( (ch >= MWKEY_0) && (ch <= MWKEY_9) ) || ((ch == MWKEY_ASTERISK) || (ch == MWKEY_SHARP) ) )
		{
			if ( ch == MWKEY_ASTERISK )
			{
				ch = '*';
			}
			else if (ch == MWKEY_SHARP)
			{
				ch = '#';
			}
			if (cursor < (aMaxLen))
			{
				aDest[cursor] = (char)ch;
				mask[cursor] = '*';
				cursor ++;
			}
		}

		if (ch == MWKEY_CANCL)
		{
			FreeMW(mask);
			return -1;
		}

		if (ch == MWKEY_ENTER)
		{
			//            DispClrLineMW(aPos);
			//            DispLineMW(aDest, aPos, MW_SMFONT);
			//            APM_WaitKey(9000,0);
			if (strlen(aDest) < aMinLen) {
				continue;
			}
			FreeMW(mask);
			return cursor;
		}

		if (ch == MWKEY_CLR)
		{
			if (cursor)
				cursor --;
		}
	}
}
/**********************************************************************
 * Descripcion: Configuracion GPRS
 * **SR**  20/09/13
 ********************************************************************/

BOOLEAN ConfigGPRS (void){

	BYTE tmp[32 + 1];
	struct CNGPRS *AuxGPRS = (void *) MallocMW(sizeof(struct CNGPRS));

	DispClrBelowMW(MW_LINE1);
	DispPutCMW(K_PushCursor);
	os_disp_textc(COLOR_VISAYELLOW);
	os_disp_backc(COLOR_BLACK);
	DispLineMW("GPRS SetUp", MW_LINE1, MW_SPFONT | MW_CENTER);
	DispPutCMW(K_PopCursor);

	TextColor("APN:", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT,  0);

	memset(tmp, 0x00, sizeof(tmp));

	tmp[0] = strlen(gConfigGPRS.sb_Apn);
	memcpy(&tmp[1], gConfigGPRS.sb_Apn, tmp[0]);

	if (!APM_GetKbdSpectra(ALPHA_INPUT_NEW+ECHO+MW_LINE7+MW_BIGFONT+RIGHT_JUST, IMAX(30), tmp)){	// se modifica de GetKbd a GetKbdSpectra por motivos de funcionalidad  **SR**  08/10/13
		FreeMW(AuxGPRS);
		return FALSE;
	}

	memset(AuxGPRS->sb_Apn ,0x00,sizeof(AuxGPRS->sb_Apn));
	memcpy(AuxGPRS->sb_Apn,&tmp[1],tmp[0]);
	UpdConfigGPRS(AuxGPRS, sizeof(gConfigGPRS.sb_Apn));
	AcceptBeep();

	TextColor("Password:",MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);

	memset(tmp,0x00,sizeof(tmp));

	tmp[0] = strlen(gConfigGPRS.sb_Pass);
	memcpy(&tmp[1],gConfigGPRS.sb_Pass, tmp[0]);


	if (!APM_GetKbdSpectra(ALPHA_INPUT_NEW+ECHO+MW_LINE7+MW_BIGFONT+RIGHT_JUST, IMAX(30), tmp)){   // se modifica de GetKbd a GetKbdSpectra por motivos de funcionalidad  **SR**  08/10/13
		FreeMW(AuxGPRS);
		return FALSE;
	}

	memset(AuxGPRS->sb_Pass, 0x00, sizeof(AuxGPRS->sb_Pass));
	memcpy(AuxGPRS->sb_Pass,&tmp[1],tmp[0]);
	UpdConfigGPRS(AuxGPRS, sizeof(gConfigGPRS.sb_Pass) * 2);
	AcceptBeep();

	TextColor("User Name:",MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);

	memset(tmp,0x00,sizeof(tmp));

	tmp[0] = strlen(gConfigGPRS.sb_User);
	memcpy(&tmp[1], gConfigGPRS.sb_User ,tmp[0]);


	if (!APM_GetKbdSpectra(ALPHA_INPUT_NEW+ECHO+MW_LINE7+MW_BIGFONT+RIGHT_JUST, IMAX(30), tmp)){				// se modifica de GetKbd a GetKbdSpectra por motivos de funcionalidad  **SR**  08/10/13
		FreeMW(AuxGPRS);
		return FALSE;
	}

	memset(AuxGPRS->sb_User, 0x00, sizeof(AuxGPRS->sb_User));
	memcpy(AuxGPRS->sb_User,&tmp[1],tmp[0]);

	UpdConfigGPRS(AuxGPRS, sizeof(struct CNGPRS));
	AcceptBeep();

	Delay10ms(30);			// Se le coloca tiempo para que el beep no genere conflicto con el FreeMW();  **SR**  20/09/13
	FreeMW(AuxGPRS);
	return TRUE;
}


void cambioSuperPass (void)
{
	BYTE filename[32];
	BYTE kbdbuf[9];
	BYTE PassAdmin[4];
	BYTE PassAdminTemp[5];

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(PassAdmin, 0x00, sizeof(PassAdmin));
	memset(PassAdminTemp, 0x00, sizeof(PassAdminTemp));
	memset(filename, 0x00, sizeof(filename));

	strcpy(filename, KTabla0File);
	gIdTabla0 = 0;

	struct TABLA_CERO tabla_cero;
	GetTablaCero(&tabla_cero);

	DispClrBelowMW(MW_LINE1);

	TextColor("INGRESE PASSWORD", MW_LINE4, COLOR_VISABLUE, MW_SMFONT
			| MW_LEFT, 0);
	if (!APM_GetKbdSpectra(HIDE_NUMERIC + MW_SMFONT + MW_LINE6,  IMIN(4) + IMAX(4),
			kbdbuf))
	{
		return ;
	}

	compress(PassAdmin, &kbdbuf[1], kbdbuf[0]);

	if (memcmp(&PassAdmin[0], &gTablaCero.b_pass_super[0], 2) != 0)
	{
		DispClrBelowMW(MW_LINE1);
		LongBeep();
		TextColor("PASSWORD INVALIDO", MW_LINE4, COLOR_RED, MW_SMFONT
				| MW_CENTER, 0);
		TextColor("RE INTENTAR", MW_LINE5, COLOR_RED, MW_SMFONT
				| MW_CENTER, 3);
		os_beep_close();
		return ;
	}




	while (TRUE)
	{

		DispClrBelowMW(MW_LINE1);
		memset(kbdbuf, 0x00, sizeof(kbdbuf));

		TextColor("INGRESE PASSWORD", MW_LINE4, COLOR_VISABLUE, MW_SMFONT
				| MW_LEFT, 0);

		if (!APM_GetKbdSpectra(HIDE_NUMERIC + MW_SMFONT + MW_LINE7,  IMIN(4) + IMAX(4),
				kbdbuf))
		{
			return ;
		}

		memcpy(PassAdminTemp, &kbdbuf[1], kbdbuf[0]);
		//
		DispClrBelowMW(MW_LINE1);
		memset(kbdbuf, 0x00, sizeof(kbdbuf));
		memset(PassAdmin, 0x00, sizeof(PassAdmin));

		TextColor("REINGRESE PASSWORD", MW_LINE4, COLOR_VISABLUE, MW_SMFONT
				| MW_LEFT, 0);

		if (!APM_GetKbdSpectra(HIDE_NUMERIC + MW_SMFONT + MW_LINE7,   IMIN(4) + IMAX(4),
				kbdbuf))
		{
			return ;
		}
		memcpy(PassAdmin, &kbdbuf[1], kbdbuf[0]);


		if (memcmp(PassAdmin, PassAdminTemp, 4) != 0)
		{
			DispClrBelowMW(MW_LINE1);
			LongBeep();
			TextColor("PASSWORD INVALIDO", MW_LINE4, COLOR_RED, MW_SMFONT
					| MW_CENTER, 0); //kt-220413
			TextColor("RE INTENTAR", MW_LINE5, COLOR_RED, MW_SMFONT
					| MW_CENTER, 3);
			os_beep_close();
		}
		else
			break;


	}

	compress(tabla_cero.b_pass_super, PassAdmin, 2);

	gIdTabla0 = fOpenMW(filename);
	UpdTablaCero(gIdTabla0,&tabla_cero);
	fCloseMW(gIdTabla0);

	DispClrBelowMW(MW_LINE1);
	AcceptBeep();
	TextColor("Modificado", MW_LINE4, COLOR_VISABLUE, MW_BIGFONT
			| MW_CENTER, 0); //kt-220413
	TextColor("exitosamente", MW_LINE6, COLOR_VISABLUE, MW_BIGFONT
			| MW_CENTER, 2); //kt-220413
}


void configReferencia (void)
{
	BYTE kbdbuf[30];
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	kbdbuf[0] = strlen(gConfigComercio.Referencia);
	memcpy(&kbdbuf[1], gConfigComercio.Referencia, strlen(gConfigComercio.Referencia) );

	DispClrBelowMW(MW_LINE1);

	TextColor("NUM. REFERENCIA", MW_LINE4, COLOR_VISABLUE, MW_SMFONT
			| MW_LEFT, 0);

	if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE7, IMAX(15),
			kbdbuf))
	{
		FreeMW(conf_comer);
		return ;
	}

	if(atoi(&kbdbuf[1]) == 0)
	{
		memset(((struct CONFIG_COMERCIO*) conf_comer)->Referencia, 0x00, sizeof(((struct CONFIG_COMERCIO*) conf_comer)->Referencia));
	}
	else
	{
		memcpy( ((struct CONFIG_COMERCIO*) conf_comer)->Referencia, &kbdbuf[1], kbdbuf[0]);
	}

	AcceptBeep();

	SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
	FreeMW(conf_comer);
	os_beep_close();
}

BOOLEAN IsGRPS( void )
{
	DWORD cfg;

	cfg = os_hd_config();

	if ((cfg & K_HdGprs) == K_HdWismo)
	{
		return TRUE;
	}

	return FALSE;
}


void Ping(void)
{
	int  handle;
	int  send_cnt, rcvd_cnt, ret_time;
	int  i, j, rxlen;
	BYTE tmp[MW_MAX_LINESIZE+1];
	DWORD ip, config;
	WORD buf_size;
	struct MW_ICMP_PORT icmp_port;
	BYTE IpTerm[12];
	BYTE *gTxBuf = NULL;
	BYTE *gRxBuf = NULL;
	BYTE dhcp;
	struct MW_NIF_INFO netinfo;


	if(IsGRPS() == TRUE){				// Cuando la terminal Es GPRS muestra la ip asignada por el operador **SR** 09/10/13
		IPGprs();
		return;
	}

	gTxBuf =(BYTE*)MallocMW(100);
	gRxBuf =(BYTE*)MallocMW(100);

	GetSysCfgMW(MW_SYSCFG_DHCP_ENABLE, &dhcp);
	dhcp = (dhcp != 0) ? 1 : 0;
	if (dhcp)
	{
		NetInfoMW(MW_NIF_ETHERNET, &netinfo);
		memcpy(IpTerm, (BYTE *) &netinfo.d_ip, 12);
	}
	else
	{
		GetSysCfgMW(MW_SYSCFG_IP, &IpTerm[0]);
		GetSysCfgMW(MW_SYSCFG_NETMASK, &IpTerm[4]);
		GetSysCfgMW(MW_SYSCFG_GATEWAY, &IpTerm[8]);
	}

	buf_size = 50;

	memcpy(&ip, &IpTerm[8], 4); // Se copia el gateway
	config = 0;

	if (((config & MW_ICMP_PPP_MODE)==0) && !LanCableInsertedMW()) {
		TextColor("Verifique el cable!", MW_LINE3, COLOR_RED, MW_CLRDISP|MW_CENTER|MW_SPFONT, 3);
		FreeMW(gTxBuf);
		FreeMW(gRxBuf);
		return;
	}

	strcpy(tmp, DEV_ICMP);
	handle = OpenMW(tmp, 0);
	icmp_port.d_ip     = ip;
	icmp_port.b_option = config;
	IOCtlMW(handle, IO_ICMP_CONFIG, &icmp_port);
	IOCtlMW(handle, IO_ICMP_CONNECT, &icmp_port);

	send_cnt = rcvd_cnt = 0;

	DispClrBelowMW(MW_LINE2);
	sprintf(tmp, "%d.%d.%d.%d", IpTerm[8], IpTerm[9], IpTerm[10], IpTerm[11]);
	DispLineMW(tmp, MW_LINE2, MW_SPFONT);
	DispGotoMW(MW_LINE3, MW_SPFONT);
	for (i = 0; i < 5; i++)
	{
		for (j = i; j < buf_size+4; j++)   // include icmp header
			gTxBuf[j-i] = j;
		gTxBuf[0] = 0x08;   // Echo Message
		gTxBuf[1] = 0;
		WriteMW(handle, gTxBuf, buf_size+4);
		send_cnt++;

		ret_time = 0;
		rxlen = 0;
		while (ret_time++ < RESP_TIMEOUT)  {  // less than 1 sec
			rxlen += ReadMW(handle, &gRxBuf[rxlen], (100 - rxlen));
			if (rxlen >= buf_size+8) {  // remote ip(4) + icmp header (4) + icmp message
				break;
			}
			SleepMW();
		}
		j = 20;
		while (j--)
			SleepMW();
		if (ret_time >= RESP_TIMEOUT)
			PrintfMW("Request Timeout\n");
		else {
			if (memcmp(&gTxBuf[4], &gRxBuf[8], rxlen-8) == 0) {
				PrintfMW("Reply received[%d]\n", rxlen-8);
				rcvd_cnt++;
			}
			else
				PrintfMW("Data Error[%d]\n", rxlen-8);
		}
	}
	PrintfMW("\nEnvia: %d  Recibe: %d", send_cnt, rcvd_cnt);
	while (GetCharMW() == 0x00)
		SleepMW();

	IOCtlMW(handle, IO_ICMP_DISCONNECT, NULL);
	CloseMW(handle);
	FreeMW(gTxBuf);
	FreeMW(gRxBuf);
}

void IPGprs (void)
{

	struct MW_NIF_INFO nif_info;

	NetInfoMW(MW_NIF_PPP, &nif_info);
	DispTermIP((BYTE *)&nif_info);
	TextColor("Presione una tecla ",MW_LINE8, COLOR_VISABLUE, MW_SMFONT | MW_LEFT,0);
	TextColor("Para continuar ...",MW_LINE9, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
	WaitKey(1000,0);

	return;
}

/******************************************************************************
 *  Function        : debugHex
 *  Description     : Debug HEX data.
 *  Input           : N/A
 *  Return          : N/A
 *  Note            : John Cheung
 *  Globals Changed : N/A
 ******************************************************************************
 */
void debugHex(BYTE *aTitle, BYTE *aData, DWORD aLen)
{
	BYTE buf[30];
	WORD i;

	if (aLen == 0)
		return;

	// Print out TLV
	LptOpen(NULL);
	LptPutS("\x1B""F1\n");
	sprintf(buf, "%s(%d):\n", aTitle, aLen);
	LptPutS(buf);
	for (i=0; i<aLen; i++) {
		sprintf(buf, "%02X ", aData[i]);
		LptPutN(buf, 3);
	}
	LptPutS("\n");
	LptPutS("-- END --\n");
	LptClose();
}

int GetCashback(void)
{
	if (memcmp(&gTablaCero.b_dir_ciudad[41], "\x30\x30", 2) != 0 &&
			memcmp(&gTablaCero.b_dir_ciudad[41], "\x20\x20", 2) != 0)
	{
		if (memcmp(&gTablaCero.b_dir_ciudad[41], "\x39\x39", 2) == 0) {
			return 99;
		}
		else if (memcmp(&gTablaCero.b_dir_ciudad[41], "\x39\x38", 2) == 0) {
			return 98;
		}
	}

	return 0;
}



//*****************************************************************************
//  Function        : GetStr
//  Description     : Get a String input.
//  Input           : aDest;      // output buffer
//                    aPos;       // line pos.
//                    aMaxLen;    // max len;
//  Return          : -1;         // Cancel;
//                    other;      // len of input.
//  Note            : Copia de GetOTP sin mascara
//  Globals Changed : N/A
//*****************************************************************************
int GetStr(char *aDest, DWORD aPos, int aMinLen, int aMaxLen)
{
	int cursor;
	// char *mask = MallocMW( aMaxLen + 1);

	memset(aDest, 0x00, sizeof(aDest));
	//    cursor = strlen (aDest);
	cursor = 1;

	while (1)
	{
		DWORD ch;

		aDest[cursor] = 0;
		//   mask[cursor] = 0;
		DispClrLineMW(aPos);
		//        DispClrLineMW(MW_LINE4);
		//        DispLineMW(aDest, MW_LINE4, MW_RIGHT|MW_SMFONT);
		DispLineMW(&aDest[1], aPos, MW_RIGHT|MW_SMFONT);

		ch = WaitKey2();

		//        if ((ch >= '0') && (ch <= '9'))

		if ( ( (ch >= MWKEY_0) && (ch <= MWKEY_9) ) || ((ch == MWKEY_ASTERISK) || (ch == MWKEY_SHARP) ) )
		{
			if ( ch == MWKEY_ASTERISK )
			{
				ch = '*';
			}
			else if (ch == MWKEY_SHARP)
			{
				ch = '#';
			}
			if (cursor < (aMaxLen))
			{
				aDest[cursor] = (char)ch;
				//  mask[cursor] = '*';
				cursor ++;
			}
		}

		if (ch == MWKEY_CANCL)
		{
			// FreeMW(mask);
			return -1;
		}

		if (ch == MWKEY_ENTER)
		{
			//            DispClrLineMW(aPos);
			//            DispLineMW(aDest, aPos, MW_SMFONT);
			//            APM_WaitKey(9000,0);
			if (strlen(aDest) < aMinLen) {
				continue;
			}
			// FreeMW(mask);
			cursor --;
			aDest[0] = cursor;
			return cursor;
		}

		if (ch == MWKEY_CLR)
		{
			if (cursor)
				cursor --;
		}
	}
}

//*****************************************************************************
//  Function         : NuevoPIN
//  Description     : Solicita el PIN por segunda vez
//  Input               : N/A
//  Return             : Estado de la transaccion TRANS_REJ, TRANS_ACP....
//  By                    : Manuel Barbaran
//*****************************************************************************
BYTE NuevoPIN (void)
{
	BYTE panBCD[10];

	if (INPUT.b_tipo_cuenta == 0x10 || INPUT.b_tipo_cuenta == 0x20
			|| !SeleccionCuenta())
	{
		if (PinDebito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				RSP_DATA.w_rspcode = '0' * 256 + '5'; // Declinada
				ErrorTransCajas(CA_CANCEL);
				APM_ResetComm();
				return TRANS_REJ;
			}
		}
	}
	else
	{
		if (PinCredito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				RSP_DATA.w_rspcode = '0' * 256 + '5'; // Declinada
				ErrorTransCajas(CA_CANCEL);
				APM_ResetComm();
				return TRANS_REJ;
			}
		}
	}

	IncTraceNo();
	memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3);
	memcpy(TX_DATA.sb_pin, INPUT.sb_pin, 8);
	PackHostMsg();
	if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
			== COMM_OK)
	{
		RSP_DATA.b_response = CheckHostRsp();
		return RSP_DATA.b_response;
		//return CheckHostRsp();
	}
	else
	{
		RSP_DATA.w_rspcode = '0' * 256 + '5';  // Declinada
		return LINE_FAIL;
	}

}


