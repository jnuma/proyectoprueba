//-----------------------------------------------------------------------------
//  File          : EMVtrans.h
//  Module        :
//  Description   : Declrartion & Defination for EMVtrans.C
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _EMVTRANS_H_
#define _EMVTRANS_H_
#include "apm.h"

//-----------------------------------------------------------------------------
//      Functions Handle the data
//-----------------------------------------------------------------------------
extern BYTE EMV_PAGODIV;
extern BOOLEAN PINOFFLINE;
extern DWORD DD_AMOUNT;
extern DWORD DD_AMOUNT_DIVIDIDO;
extern DWORD DD_AMOUNT_TIP;
extern DWORD DD_DONACION;
extern DWORD DD_TIP_DIVIDIDO;
extern DWORD DD_IVA;
extern DWORD DD_IAC;
extern BYTE PAN_DIV[10];
extern BYTE AUTHCODE_DIV[6];
extern WORD NUM_CLIENTES;
extern WORD NUM_CLIENTES_TIP;
extern DWORD DD_DONACION;

extern BOOLEAN IS_MASTERCARD;
extern BOOLEAN WITHOUT_55;
// Jorge Numa 10/09/2013
#define EMV_PAGODIV_DEFAULT         0x01
#define EMV_PAGODIV_TYPE            0x02
#define EMV_PAGODIV_ACP             0x04
#define EMV_PAGODIV_REJ             0x08
#define EMV_PAGODIV_FALLBACK        0x10
#define EMV_PAGODIV_ACP_FALLBACK    0x20
#define EMV_PAGODIV_REJ_FALLBACK    0x40


extern BOOLEAN ValidEMVData(void);
extern void EMVFatal(void);
extern DWORD EMVTrans(DWORD aTrans);
extern DWORD EMVTransPagoFactura(DWORD aTrans);
extern void EMVInit(void);
extern BYTE *PackTagsData(BYTE *aBuf, WORD *aTags);
extern BYTE DeAss(TLIST *aList, BYTE *pbMsg, WORD wLen);
extern BYTE *TagData(TLIST *aSrc, WORD aTag);
extern TLIST *TagSeek(TLIST *aSrc, WORD aTag);
extern BYTE * TagPut(BYTE *aBuf, WORD aTag);
extern void CheckPinOffline( void );
extern void SpecialVoid(void);
//extern void debugEMV(BYTE aState, BYTE aIn, sIO_t *pEMV);
#endif

