/*
 * files.c
 *
 *  Created on: 30/07/2012
 *      Author: Jorge Numa
 */
#include <string.h>
#include <stdio.h>
#include "util.h"
#include "sysutil.h"
#include "message.h"
#include "constant.h"
#include "corevar.h"
#include "chkoptn.h"
#include "input.h"
#include "record.h"
#include "tranutil.h"
#include "files.h"

struct TABLA_CERO *gTablaCeroOffset = 0;
struct TABLA_DOS *gTablaDosOffset = 0;
struct TABLA_TRES *gTablaTresOffset = 0;
struct TABLA_CUATRO *gTablaCuatroOffset = 0;
struct TABLA_CINCO *gTablaCincoOffset = 0;
struct TABLA_SEIS *gTablaSeisOffset = 0;
struct TABLA_12 *gTabla12Offset = 0;
struct TABLA_0C *gTabla0COffset = 0;
struct TABLA_0D *gTabla0DOffset = 0;
struct TABLA_0F *gTabla0FOffset = 0;
TABLA_0E *gTabla0EOffset = 0;

int gIdTabla0;
int gIdTabla2;
int gIdTabla3;
int gIdTabla4;
int gIdTabla5;
int gIdTabla6;
int gIdTabla12;
int gIdTabla0C;
int gIdTabla0D;
int gIdTabla0F;
int gIdTabla0E;

int gIdUsb;

struct PAGO_DIVIDIDO *gPagoDivOffset = 0;
struct CONFIG_COMERCIO *gConfigComerOffset = 0; //MFBC/08/11/12
struct TABLA_IVAS *gTablaIvaOffset = 0;

//-----------------------------------------------------------------------------
//      Defines
//-----------------------------------------------------------------------------
typedef struct
{
	BYTE s_agent_id[8];
	BYTE s_filename[32];
	BYTE s_checking[8];
	BYTE b_last_file;
	DWORD d_filelength;
	WORD w_crc;
} __attribute__((packed)) /* DO NOT REMOVED */
DLL_HDR; /* received packet from serial port, cannot be changed */

typedef struct
{
	BYTE b_state; /* state of download */
	BOOLEAN b_last_hdr; /* bit for checking if last file */
	WORD w_rxlen; /* current length of received message (including filename, checksum, etc...) */
	BYTE * p_data_start; /* pointer for SDRAM location to store data */
	BYTE * p_data; /* current data pointer */
	BYTE * p_message; /* pointer for message displayed on screen */
	BYTE * p_rxbuf; /* pointer of receive buffer */
	DWORD d_total_len; /* current length of file received */
	WORD w_crc; /* checksum of one single file */
	DWORD d_filelength; /* total length for one single file */
	BYTE s_name[32]; /* file name for one single file */
} __attribute__((packed)) /* DO NOT REMOVED */
DLL_VAR; /* customized structure, can be changed */

#define MAX_NUM_OF_FILES  300		// for testing only
#define ITEM_PER_PAGE     6

// aMode
#define DISPLAY           0
#define	READ              1
#define	WRITE             2
#define	DELETE            3

//-----------------------------------------------------------------------------
//      Globals
//-----------------------------------------------------------------------------
static BYTE b_port;
DWORD dll_timer;

// handler for COM port
static int gAux1;
static int gAux2;
static int gUsb;

//-----------------------------------------------------------------------------
//      Constants
//-----------------------------------------------------------------------------
// AUX PORT
#define ANYPORT     0xFF
#define AUX1        0
#define AUXDBG      1
#define USB         2

// State of download (b_state)
#define WaitHeaderState   0
#define WaitDataState     1

// Single file size limit
#define size_1MB     0x00100000
#define size_2MB     0x00200000
#define size_3MB     0x00300000
#define size_4MB     0x00400000

// Message to display (p_message)
const BYTE Msg_header[] = "Descarga de Archivos";
const BYTE Msg_download[] = "Descarga";
const BYTE Msg_wait_for_download[] = "Esperando Archivos..";
const BYTE Msg_complete[] = "Completa";

const BYTE Msg_hdr_crc_err[] = "Hdr CRC Error";
const BYTE Msg_file_create_err[] = "Error Creando Archivo";
const BYTE Msg_aux_open_err[] = "Error abriendo Aux";
const BYTE Msg_total_check_err[] = "Total CS Error";
const BYTE Msg_write_err[] = "Error escribiendo";
const BYTE Msg_timeout_err[] = "Timeout Error";
const BYTE Msg_abort[] = "Cancelada";
const BYTE Msg_port_err[] = "Error Abriendo Puerto";

//*****************************************************************************
//  Function        : CreateEmptyFile
//  Description     : Create Empty File
//  Input           : aFilename;      // file name.
//  Return          : file handle;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int CreateEmptyFile(const char *aFilename)
{
	char filename[20];
	int file_handle;

	memset(filename, 0x00, sizeof(filename));

	strcpy(filename, aFilename);
	fDeleteMW(filename);
	file_handle = fCreateMW(filename, 0);
	return file_handle;
}

//*****************************************************************************
//  Function        : OpenFile
//  Description     : Open File
//  Input           : aFilename;      // file name.
//  Return          : file handle;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int OpenFile(const char *aFilename)
{
	char filename[20];
	int file_handle;

	memset(filename, 0x00, sizeof(filename));

	strcpy(filename, aFilename);
	file_handle = fOpenMW(filename);
	return file_handle;
}

//*****************************************************************************
//  Function        : CloseFile
//  Description     : Close File
//  Input           : aFilename;      // file name.
//  Return          : file handle;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int CloseFile(int aIdFile)
{
	if (aIdFile >= 0)
		return fCloseMW(aIdFile);

	return -2;
}

//*****************************************************************************
//  Function        : ReadSFile
//  Description     : Read from structure data file.
//  Input           : aFileHandle;      // file handle
//                    aOffset;          // file offset from beginning
//                    aBuf;             // data buffer
//                    aLen;             // len to read.
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ReadSFile(DWORD aFileHandle, DWORD aOffset, void *aBuf, DWORD aLen)
{
	DWORD len;

	fSeekMW(aFileHandle, aOffset);
	len = fReadMW(aFileHandle, aBuf, aLen);
	return (len == aLen) ? TRUE : FALSE;
}

//*****************************************************************************
//  Function        : WriteSFile
//  Description     : Write to structure data file.
//  Input           : aFileHandle;      // file handle
//                    aOffset;          // file offset from beginning
//                    aBuf;             // data buffer
//                    aLen;             // len to read.
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN WriteSFile(DWORD aFileHandle, DWORD aOffset, const void *aBuf,
		DWORD aLen)
{
	DWORD len;

	fSeekMW(aFileHandle, aOffset);
	len = fWriteMW(aFileHandle, aBuf, aLen);
	if (len != aLen)
	{
		DispLineMW("File WR Err!", MW_LINE3, MW_CLRDISP | MW_BIGFONT);
		ErrorDelay();
	}
	return (len == aLen) ? TRUE : FALSE;
}

BOOLEAN WriteSFile2(DWORD aFileHandle, DWORD aOffset, void *aBuf, DWORD aLen)
{
	DWORD len;
	BYTE *ptr;

	ptr = (BYTE*) MallocMW(aLen);
	memcpy(ptr, aBuf, aLen);

	fSeekMW(aFileHandle, aOffset);
	len = fWriteMW(aFileHandle, ptr, aLen);
	if (len != aLen)
	{
		DispLineMW("File WRITE Error!", MW_LINE3, MW_CLRDISP | MW_BIGFONT);
		ErrorDelay();
	}

	FreeMW(ptr);
	return (len == aLen) ? TRUE : FALSE;
}

//*****************************************************************************
//  Function        : UpdTablaCero
//  Description     : Update Tabla Cero
//  Input           : aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTablaCero(int aFileHandle, struct TABLA_CERO *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla Cero", MW_LINE4, MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) gTablaCeroOffset, aDat,
			sizeof(struct TABLA_CERO));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTablaDos
//  Description     : Update Tabla Dos
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTablaDos(int aFileHandle, DWORD aIdx, struct TABLA_DOS *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla Dos", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTablaDosOffset[aIdx], aDat,
			sizeof(struct TABLA_DOS));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTablaTres
//  Description     : Update Tabla Tres
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTablaTres(int aFileHandle, DWORD aIdx, struct TABLA_TRES *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla Tres", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTablaTresOffset[aIdx], aDat,
			sizeof(struct TABLA_TRES));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTablaCuatro
//  Description     : Update Tabla Cuatro
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTablaCuatro(int aFileHandle, DWORD aIdx, struct TABLA_CUATRO *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla Cuatro", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTablaCuatroOffset[aIdx], aDat,
			sizeof(struct TABLA_CUATRO));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTablaCinco
//  Description     : Update Tabla Cinco
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTablaCinco(int aFileHandle, DWORD aIdx, struct TABLA_CINCO *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla Cinco", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTablaCincoOffset[aIdx], aDat,
			sizeof(struct TABLA_CINCO));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTablaSeis
//  Description     : Update Tabla Seis
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTablaSeis(int aFileHandle, DWORD aIdx, struct TABLA_SEIS *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla Cinco", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTablaSeisOffset[aIdx], aDat,
			sizeof(struct TABLA_SEIS));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTabla12
//  Description     : Update Tabla 12
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTabla12(int aFileHandle, DWORD aIdx, struct TABLA_12 *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla 12", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTabla12Offset[aIdx], aDat,
			sizeof(struct TABLA_12));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTabla0C
//  Description     : Update Tabla 0C
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTabla0C(int aFileHandle, DWORD aIdx, struct TABLA_0C *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla 0C", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTabla0COffset[aIdx], aDat,
			sizeof(struct TABLA_0C));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTabla0D
//  Description     : Update Tabla 0D
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTabla0D(int aFileHandle, DWORD aIdx, struct TABLA_0D *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla 0D", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTabla0DOffset[aIdx], aDat,
			sizeof(struct TABLA_0D));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTabla0F
//  Description     : Update Tabla 0F
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTabla0F(int aFileHandle, DWORD aIdx, struct TABLA_0F *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla 0F", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTabla0FOffset[aIdx], aDat,
			sizeof(struct TABLA_0F));
	return TRUE;
}

//*****************************************************************************
//  Function        : UpdTabla0E
//  Description     : Update Tabla 0E
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTabla0E(int aFileHandle, DWORD aIdx, TABLA_0E *aDat)
{
	if (aFileHandle < 0)
	{
		DispLineMW("Error Actualizando", MW_LINE3, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("Tabla 0E", MW_LINE4, MW_CENTER | MW_SMFONT);
		ErrorDelay();
		return FALSE;
	}

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat);
	WriteSFile(aFileHandle, (DWORD) & gTabla0EOffset[aIdx], aDat,
			sizeof(TABLA_0E));
	return TRUE;
}

//*****************************************************************************
//  Function        : GetTermCfg
//  Description     : Get Terminal Config Data.
//  Input           : aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaCero(struct TABLA_CERO *aDat)
{
	int IdFile;
	BYTE filename[32];

	strcpy(filename, KTabla0File);

	CheckPointerAddr(aDat);

	IdFile = fOpenMW(filename);

	if (!ReadSFile(IdFile, (DWORD) gTablaCeroOffset, aDat,
			sizeof(struct TABLA_CERO)))
	{
		fCloseMW(IdFile);
		return FALSE;
	}

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		memcpy(&gTablaCero, aDat, sizeof(struct TABLA_CERO));
		fCloseMW(IdFile);
		return TRUE;
	}

	//memcpy(aDat, &KDefaultTermCfg, sizeof(struct TABLA_CERO));
	//aDat->w_crc = (WORD) cal_crc((BYTE *)aDat, (BYTE *) &aDat->w_crc - (BYTE *) aDat);
	memset(aDat, 0xFF, sizeof(struct TABLA_CERO));
	fCloseMW(IdFile);

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTablaDos
//  Description     : Get TablaDos Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaDos(DWORD aIdx, struct TABLA_DOS *aDat)
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla2, (DWORD) & gTablaDosOffset[aIdx], aDat,
			sizeof(struct TABLA_DOS)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		memcpy(&gTablaDos, aDat, sizeof(struct TABLA_DOS));
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(struct TABLA_DOS));
	return FALSE;
}

//*****************************************************************************
//  Function        : GetTablaTres
//  Description     : Get TablaTres Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaTres(DWORD aIdx, struct TABLA_TRES *aDat)
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla3, (DWORD) & gTablaTresOffset[aIdx], aDat,
			sizeof(struct TABLA_TRES)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		memset(&gTablaTres, 0x00, sizeof(struct TABLA_TRES)); //MFBC/22/04/13
		memcpy(&gTablaTres, aDat, sizeof(struct TABLA_TRES));
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(struct TABLA_TRES));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTablaCuatro
//  Description     : Get TablaTres Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaCuatro(DWORD aIdx, struct TABLA_CUATRO *aDat)
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla4, (DWORD) & gTablaCuatroOffset[aIdx], aDat,
			sizeof(struct TABLA_CUATRO)))
	{
		return FALSE;
	}

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		memcpy(&gTablaCuatro, aDat, sizeof(struct TABLA_CUATRO));
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(struct TABLA_CUATRO));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTablaCinco
//  Description     : Get TablaTres Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaCinco(DWORD aIdx, struct TABLA_CINCO *aDat)
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla5, (DWORD) & gTablaCincoOffset[aIdx], aDat,
			sizeof(struct TABLA_CINCO)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		memcpy(&gTablaCinco, aDat, sizeof(struct TABLA_CINCO));
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(struct TABLA_CINCO));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTablaSeis
//  Description     : Get TablaSeis Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaSeis(DWORD aIdx, struct TABLA_SEIS *aDat)	//NM-29/04/13 Agregue esta funcion
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla6, (DWORD) &gTablaSeisOffset[aIdx], aDat, sizeof(struct TABLA_SEIS)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc - (BYTE *) aDat))
	{
		memcpy(&gTablaSeis, aDat, sizeof(struct TABLA_SEIS));
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(struct TABLA_SEIS));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTablaDoce
//  Description     : multicomercio virtual.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaDoce(DWORD aIdx, struct TABLA_12 *aDat) //kt-211112
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla12, (DWORD) & gTabla12Offset[aIdx], aDat,
			sizeof(struct TABLA_12)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		memcpy(&gTablaDoce, aDat, sizeof(struct TABLA_12));
		return TRUE;
	}

	//printf("\fsalio");
	//APM_WaitKey(9000,0);

	memset(aDat, 0xFF, sizeof(struct TABLA_12));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTabla2Count
//  Description     : Get Number of Tabla dos data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla2Count(void)
{
	int ret;

	gIdTabla2 = OpenFile(KTabla2File);

	if (gIdTabla2 == -1)
		return 0;

	ret = fLengthMW(gIdTabla2) / sizeof(struct TABLA_DOS);
	CloseFile(gIdTabla2);

	return ret;
}

//*****************************************************************************
//  Function        : GetTabla3Count
//  Description     : Get Number of Tabla dos data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla3Count(void)
{
	int ret;

	gIdTabla3 = OpenFile(KTabla3File);

	if (gIdTabla3 == -1)
		return 0;

	ret = fLengthMW(gIdTabla3) / sizeof(struct TABLA_TRES);
	CloseFile(gIdTabla3);
	return ret;
}

//*****************************************************************************
//  Function        : GetTabla4Count
//  Description     : Get Number of Tabla dos data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla4Count(void)
{
	int ret;

	gIdTabla4 = OpenFile(KTabla4File);

	if (gIdTabla4 == -1)
		return 0;

	ret = fLengthMW(gIdTabla4) / sizeof(struct TABLA_CUATRO);
	CloseFile(gIdTabla4);

	return ret;
}

//*****************************************************************************
//  Function        : GetTabla5Count
//  Description     : Get Number of Tabla dos data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla5Count(void)
{
	int ret;

	gIdTabla5 = OpenFile(KTabla5File);

	if (gIdTabla5 == -1)
		return 0;

	ret = fLengthMW(gIdTabla5) / sizeof(struct TABLA_CINCO);
	CloseFile(gIdTabla5);

	return ret;
}

//*****************************************************************************
//  Function        : GetTabla5Count
//  Description     : Get Number of Tabla dos data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla6Count(void)	//NM-29/04/13 Agregue esta funcion.
{
	int ret;

	gIdTabla6 = OpenFile(KTabla6File);

	if (gIdTabla6 == -1)
		return 0;

	ret = fLengthMW(gIdTabla6) / sizeof(struct TABLA_SEIS);
	CloseFile(gIdTabla6);

	return ret;
}


//*****************************************************************************
//  Function        : GetTabla12Count
//  Description     : Get Number of Tabla doce data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla12Count(void) //kt-211112
{
	int ret;

	gIdTabla12 = OpenFile(KTabla12File);

	if (gIdTabla12 == -1)
		return 0;

	ret = fLengthMW(gIdTabla12) / sizeof(struct TABLA_12);
	CloseFile(gIdTabla12);

	return ret;
}

//*****************************************************************************
//  Function        : FilesVisaInit
//  Description     : Init. Data file.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void FilesVisaInit(void)
{
	struct TABLA_CERO tabla_cero;
	struct TABLA_DOS tabla_dos;
	struct TABLA_TRES tabla_tres;
	struct TABLA_CUATRO tabla_cuatro;
	struct TABLA_CINCO tabla_cinco;
	struct TABLA_SEIS tabla_seis;
	//  struct TABLA_SIETE tabla_siete;
	struct TABLA_12 tabla_12;
	struct TABLA_0C tabla_0C;
	struct TABLA_0D tabla_0D;
	struct TABLA_0F tabla_0F;
	TABLA_0E tabla_0E;

	OpenFilesInit();

	if (gIdTabla0 < 0 && gIdTabla2 < 0 && gIdTabla3 < 0 && gIdTabla4 < 0
			&& gIdTabla5 < 0 && gIdTabla6 < 0 && gIdTabla12 < 0 && gIdTabla0C
			< 0 && gIdTabla0D < 0 && gIdTabla0F < 0 && gIdTabla0E < 0)
	{
		/*		DispLineMW("Creando Tablas", MW_LINE4, MW_CENTER | MW_CLRDISP
				| MW_SMFONT);
		DispLineMW("de Inicializacion", MW_LINE5, MW_CENTER | MW_SMFONT);
		WaitKey(3000, 0);*/
		//gAppDatHdl = fCreateMW(filename, FALSE);

		gIdTabla0 = CreateEmptyFile(KTabla0File);
		gIdTabla2 = CreateEmptyFile(KTabla2File);
		gIdTabla3 = CreateEmptyFile(KTabla3File);
		gIdTabla4 = CreateEmptyFile(KTabla4File);
		gIdTabla5 = CreateEmptyFile(KTabla5File);
		gIdTabla6 = CreateEmptyFile(KTabla6File);
		//gIdTabla7 = CreateEmptyFile(KTabla7File);
		gIdTabla12 = CreateEmptyFile(KTabla12File);
		gIdTabla0C = CreateEmptyFile(KTabla0CFile);
		gIdTabla0D = CreateEmptyFile(KTabla0DFile);
		gIdTabla0F = CreateEmptyFile(KTabla0FFile);
		gIdTabla0E = CreateEmptyFile(KTabla0EFile);

		//memset(&app_data, 0, sizeof(struct APPDATA));
		memset(&tabla_cero, 0, sizeof(struct TABLA_CERO));
		memset(&tabla_dos, 0, sizeof(struct TABLA_DOS));
		memset(&tabla_tres, 0, sizeof(struct TABLA_TRES));
		memset(&tabla_cuatro, 0, sizeof(struct TABLA_CUATRO));
		memset(&tabla_cinco, 0, sizeof(struct TABLA_CINCO));
		memset(&tabla_seis, 0, sizeof(struct TABLA_SEIS));
		//memset( &tabla_siete, 0, sizeof(struct TABLA_SIETE) );
		memset(&tabla_12, 0, sizeof(struct TABLA_12));
		memset(&tabla_0C, 0, sizeof(struct TABLA_0C));
		memset(&tabla_0D, 0, sizeof(struct TABLA_0D));
		memset(&tabla_0F, 0, sizeof(struct TABLA_0F));
		memset(&tabla_0E, 0, sizeof(TABLA_0E));

		//fWriteMW(gAppDatHdl, &app_data, sizeof(struct APPDATA));
		fWriteMW(gIdTabla0, &tabla_cero, sizeof(struct TABLA_CERO));
		fWriteMW(gIdTabla2, &tabla_dos, sizeof(struct TABLA_DOS));
		fWriteMW(gIdTabla3, &tabla_tres, sizeof(struct TABLA_TRES));
		fWriteMW(gIdTabla4, &tabla_cuatro, sizeof(struct TABLA_CUATRO));
		fWriteMW(gIdTabla5, &tabla_cinco, sizeof(struct TABLA_CINCO));
		fWriteMW(gIdTabla6, &tabla_seis, sizeof(struct TABLA_SEIS));
		//fWriteMW(gIdTabla7 , &tabla_siete, sizeof(struct TABLA_SIETE));
		fWriteMW(gIdTabla12, &tabla_12, sizeof(struct TABLA_12));
		fWriteMW(gIdTabla0C, &tabla_0C, sizeof(struct TABLA_0C));
		fWriteMW(gIdTabla0D, &tabla_0D, sizeof(struct TABLA_0D));
		fWriteMW(gIdTabla0F, &tabla_0F, sizeof(struct TABLA_0F));
		fWriteMW(gIdTabla0E, &tabla_0E, sizeof(TABLA_0E));

		fCommitAllMW();
	}
	//else
	//fReadMW(gAppDatHdl, &app_data, sizeof(struct APPDATA));
	//memcpy(&gAppDat, &app_data, sizeof(struct APPDATA));

	CloseFilesInit();
}
void VisaInitSet(BOOLEAN flag_init)
{
	struct TABLA_CERO tabla_cero;
	struct TABLA_DOS tabla_dos;
	struct TABLA_TRES tabla_tres;
	struct TABLA_CUATRO tabla_cuatro;
	struct TABLA_CINCO tabla_cinco;
	struct TABLA_SEIS tabla_seis;
	//  struct TABLA_SIETE tabla_siete;
	struct TABLA_12 tabla_12;
	struct TABLA_0C tabla_0C;
	struct TABLA_0D tabla_0D;
	struct TABLA_0F tabla_0F;

	if (flag_init == TRUE)
	{
		if (check_lote() == TRUE)
		{
			LongBeep();
			TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
			return ;
		}
	}

	TABLA_0E tabla_0E;
	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA));//MFBC/02/05/13
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA) ); //MFBC/02/05/13

	OpenFilesInit();

	gIdTabla0 = CreateEmptyFile(KTabla0File);
	gIdTabla2 = CreateEmptyFile(KTabla2File);
	gIdTabla3 = CreateEmptyFile(KTabla3File);
	gIdTabla4 = CreateEmptyFile(KTabla4File);
	gIdTabla5 = CreateEmptyFile(KTabla5File);
	gIdTabla6 = CreateEmptyFile(KTabla6File);
	//gIdTabla7 = CreateEmptyFile(KTabla7File);
	gIdTabla12 = CreateEmptyFile(KTabla12File);
	gIdTabla0C = CreateEmptyFile(KTabla0CFile);
	gIdTabla0D = CreateEmptyFile(KTabla0DFile);
	gIdTabla0F = CreateEmptyFile(KTabla0FFile);
	gIdTabla0E = CreateEmptyFile(KTabla0EFile);

	//memset(&app_data, 0, sizeof(struct APPDATA));
	memset(&tabla_cero, 0, sizeof(struct TABLA_CERO));
	memset(&tabla_dos, 0, sizeof(struct TABLA_DOS));
	memset(&tabla_tres, 0, sizeof(struct TABLA_TRES));
	memset(&tabla_cuatro, 0, sizeof(struct TABLA_CUATRO));
	memset(&tabla_cinco, 0, sizeof(struct TABLA_CINCO));
	memset(&tabla_seis, 0, sizeof(struct TABLA_SEIS));
	//memset( &tabla_siete, 0, sizeof(struct TABLA_SIETE) );
	memset(&tabla_12, 0, sizeof(struct TABLA_12));
	memset(&tabla_0C, 0, sizeof(struct TABLA_0C));
	memset(&tabla_0D, 0, sizeof(struct TABLA_0D));
	memset(&tabla_0F, 0, sizeof(struct TABLA_0F));
	memset(&tabla_0E, 0, sizeof(TABLA_0E));

	//fWriteMW(gAppDatHdl, &app_data, sizeof(struct APPDATA));
	fWriteMW(gIdTabla0, &tabla_cero, sizeof(struct TABLA_CERO));
	fWriteMW(gIdTabla2, &tabla_dos, sizeof(struct TABLA_DOS));
	fWriteMW(gIdTabla3, &tabla_tres, sizeof(struct TABLA_TRES));
	fWriteMW(gIdTabla4, &tabla_cuatro, sizeof(struct TABLA_CUATRO));
	fWriteMW(gIdTabla5, &tabla_cinco, sizeof(struct TABLA_CINCO));
	fWriteMW(gIdTabla6, &tabla_seis, sizeof(struct TABLA_SEIS));
	//fWriteMW(gIdTabla7 , &tabla_siete, sizeof(struct TABLA_SIETE));
	fWriteMW(gIdTabla12, &tabla_12, sizeof(struct TABLA_12));
	fWriteMW(gIdTabla0C, &tabla_0C, sizeof(struct TABLA_0C));
	fWriteMW(gIdTabla0D, &tabla_0D, sizeof(struct TABLA_0D));
	fWriteMW(gIdTabla0F, &tabla_0F, sizeof(struct TABLA_0F));
	fWriteMW(gIdTabla0E, &tabla_0E, sizeof(TABLA_0E));

	((struct APPDATA*) conf_AppData)->estadoInit = FALSE; //MFBC/02/05/13
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	fCommitAllMW();

	CloseFilesInit(); //MFBC/05/06/13  habilite el cerrado de archivos

	if (flag_init == TRUE)
	{
		AcceptBeep();
		TextColor("INICIALIZACION ", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT | MW_CLRDISP, 0);
		TextColor("BORRADA", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 3);
	}

}

void OpenFilesInit(void)
{
	gIdTabla0 = OpenFile(KTabla0File);
	gIdTabla2 = OpenFile(KTabla2File);
	gIdTabla3 = OpenFile(KTabla3File);
	gIdTabla4 = OpenFile(KTabla4File);
	gIdTabla5 = OpenFile(KTabla5File);
	gIdTabla6 = OpenFile(KTabla6File);
	//gIdTabla7 = OpenFile(KTabla7File);
	gIdTabla12 = OpenFile(KTabla12File);
	gIdTabla0C = OpenFile(KTabla0CFile);
	gIdTabla0D = OpenFile(KTabla0DFile);
	gIdTabla0F = OpenFile(KTabla0FFile);
	gIdTabla0E = OpenFile(KTabla0EFile);
}

void CloseFilesInit(void)
{
	gIdTabla0 = CloseFile(gIdTabla0);
	gIdTabla2 = CloseFile(gIdTabla2);
	gIdTabla3 = CloseFile(gIdTabla3);
	gIdTabla4 = CloseFile(gIdTabla4);
	gIdTabla5 = CloseFile(gIdTabla5);
	gIdTabla6 = CloseFile(gIdTabla6);
	//gIdTabla7 = CloseFile(gIdTabla7);
	gIdTabla12 = CloseFile(gIdTabla12);
	gIdTabla0C = CloseFile(gIdTabla0C);
	gIdTabla0D = CloseFile(gIdTabla0D);
	gIdTabla0F = CloseFile(gIdTabla0F);
	gIdTabla0E = CloseFile(gIdTabla0E);
}

void SetFL63_BILL_TX(void)
{
	memset(gfl63_bill_tx.b_Cod_Facturadora, 0x30,
			sizeof(gfl63_bill_tx.b_Cod_Facturadora));
	memset(gfl63_bill_tx.b_Num_Factura, 0x30,
			sizeof(gfl63_bill_tx.b_Num_Factura));
	memset(gfl63_bill_tx.b_Nombre_Facturadora, 0x20,
			sizeof(gfl63_bill_tx.b_Nombre_Facturadora));
	memset(gfl63_bill_tx.b_IAC_NIT, 0x30, sizeof(gfl63_bill_tx.b_IAC_NIT));
	//	memset(gfl63_bill_tx.bCod_EUEF, 0x30, sizeof(gfl63_bill_tx.bCod_EUEF)); // Jorge Numa 25/10/2013
	memset(gfl63_bill_tx.bCod_EUEF, 0x30, 7);   // Jorge Numa 25/10/2013 -> Así lo hace Verifone
	memset(gfl63_bill_tx.bCod_EUEF+7, 0x20, 8); // Jorge Numa 25/10/2013
	memset(gfl63_bill_tx.b_Valor_Factura, 0x00,
			sizeof(gfl63_bill_tx.b_Valor_Factura));
	memset(gfl63_bill_tx.b_Vencimiento_Factura, 0x30,
			sizeof(gfl63_bill_tx.b_Vencimiento_Factura));
	gfl63_bill_tx.b_Id_PagoFactura = 0x20;
}


//*****************************************************************************
//  Function        : GetTabla0C
//  Description     : Get Tabla0C Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTabla0C(DWORD aIdx, struct TABLA_0C *aDat)
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla0C, (DWORD) & gTabla0COffset[aIdx], aDat,
			sizeof(struct TABLA_0C)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		//memcpy( &gTablaCuatro, aDat, sizeof(struct TABLA_0C) );
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(struct TABLA_0C));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTabla0D
//  Description     : Get Tabla0D Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTabla0D(DWORD aIdx, struct TABLA_0D *aDat)
{

	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla0D, (DWORD) & gTabla0DOffset[aIdx], aDat,
			sizeof(struct TABLA_0D)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		//memcpy( &gTablaCuatro, aDat, sizeof(struct TABLA_0C) );
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(struct TABLA_0D));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTabla0CCount
//  Description     : Get Number of Tabla 0C data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla0CCount(void)
{
	int ret;

	gIdTabla0C = OpenFile(KTabla0CFile);

	if (gIdTabla0C == -1)
		return 0;

	ret = fLengthMW(gIdTabla0C) / sizeof(struct TABLA_0C);
	CloseFile(gIdTabla0C);

	return ret;
}

//*****************************************************************************
//  Function        : GetTabla0DCount
//  Description     : Get Number of Tabla 0D data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla0DCount(void)
{
	int ret;

	gIdTabla0D = OpenFile(KTabla0DFile);

	if (gIdTabla0D == -1)
		return 0;

	ret = fLengthMW(gIdTabla0D) / sizeof(struct TABLA_0D);
	CloseFile(gIdTabla0D);

	return ret;
}

//*****************************************************************************
//  Function        : GetTabla0E
//  Description     : Get Tabla0D Data.
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTabla0E(DWORD aIdx, TABLA_0E *aDat)
{
	CheckPointerAddr(aDat);

	if (!ReadSFile(gIdTabla0E, (DWORD) & gTabla0EOffset[aIdx], aDat,
			sizeof(TABLA_0E)))
		return FALSE;

	if (aDat->w_crc == (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc
			- (BYTE *) aDat))
	{
		//memcpy( &gTablaCuatro, aDat, sizeof(struct TABLA_0C) );
		return TRUE;
	}

	memset(aDat, 0xFF, sizeof(TABLA_0E));

	return FALSE;
}

//*****************************************************************************
//  Function        : GetTabla0ECount
//  Description     : Get Number of Tabla 0C data in the table.
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTabla0ECount(void)
{
	int ret;

	gIdTabla0E = OpenFile(KTabla0EFile);

	if (gIdTabla0E == -1)
		return 0;

	ret = fLengthMW(gIdTabla0E) / sizeof(TABLA_0E);
	CloseFile(gIdTabla0E);

	return ret;
}

//kt-130912

//*****************************************************************************
//  Function        : SetPendingVISA
//  Description     : Set Acquirer Pending Flag.
//  Input           : aIdx;         // Table Index , -1 => All Table
//                    aVal;         // Flag
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SetPendingVISA(int aIdx, BYTE aVal)
{
	//struct TABLA_CUATRO tabla_cuatro;
	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
	int start_idx, end_idx;
	int acq_count = GetTabla4Count();

	if (aIdx >= acq_count)
	{
		FreeMW(p_tabla4);
		return;
	}

	if (aIdx == -1)
	{
		start_idx = 0;
		end_idx = acq_count - 1;
	}
	else
	{
		start_idx = aIdx;
		end_idx = aIdx;
	}

	gIdTabla4 = OpenFile(KTabla4File);

	for (aIdx = start_idx; aIdx <= end_idx; aIdx++)
	{
		if (!GetTablaCuatro(aIdx, (struct TABLA_CUATRO*) p_tabla4))
		{
			CloseFile(gIdTabla4);
			FreeMW(p_tabla4);
			return;
		}

		((struct TABLA_CUATRO*) p_tabla4)->b_pending = aVal;
		UpdTablaCuatro(gIdTabla4, aIdx, (struct TABLA_CUATRO*) p_tabla4);
		CloseFile(gIdTabla4);
		FreeMW(p_tabla4);
	}
}

//*****************************************************************************
//  Function        : GetPendingVISA
//  Description     : Get Acquirer Pending Flag.
//  Input           : Idx;         // Acquirer Table Index
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BYTE GetPendingVISA(int aIdx)
{
	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));

	gIdTabla4 = OpenFile(KTabla4File);

	if (!GetTablaCuatro(aIdx, (struct TABLA_CUATRO*) p_tabla4))
	{
		CloseFile(gIdTabla4);
		FreeMW(p_tabla4);
		return NOT_LOADED;
	}

	CloseFile(gIdTabla4);
	FreeMW(p_tabla4);
	return ((struct TABLA_CUATRO*) p_tabla4)->b_pending;
}


/**********************************************
 * Descripcion: acutualiza en el archivo la configuracion GPRS
 *  **SR** 20/09/13
 **********************************************/
BOOLEAN UpdConfigGPRS (struct CNGPRS *aData, DWORD aLen){

	BYTE FileName[32];
	int fd =  -1;

	memset(FileName, 0x00, sizeof(FileName));

	strcpy(FileName, KConfigGPRS);

	fd = fOpenMW(FileName);

	if(fd < 0){
		TextColor("FILE",COLOR_VISABLUE, MW_LINE4, MW_CENTER | MW_REVERSE | MW_SMFONT, 0);
		TextColor("ERROR ABRIENDO ",COLOR_VISABLUE, MW_LINE4, MW_CENTER | MW_SMFONT, 0);
		TextColor("EL ARCHIVO",COLOR_VISABLUE , MW_LINE5, MW_CENTER | MW_SMFONT, 3);
		return FALSE;
	}

	fWriteMW(fd, aData, aLen);

	memcpy(&gConfigGPRS.sb_Apn[0], aData, aLen);

	fCommitAllMW();
	fCloseMW(fd);

	return TRUE;
}


//*****************************************************************************
//  Function        : UpdTablaIvas
//  Description     :
//  Input           : aIdx;       // Table Index
//                    aDat;       // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTablaIvas(int aFileHandle, DWORD aIdx, struct TABLA_IVAS *aDat) //kt-311012
{
	if (aFileHandle < 0)
	{
		DispLineMW("APPDATA File ERROR!", MW_MAX_LINE, MW_CLRDISP | MW_REVERSE
				| MW_CLREOL | MW_SPFONT);
		ErrorDelay();
		return FALSE;
	}

	WriteSFile(aFileHandle, (DWORD) & gTablaIvaOffset[aIdx], aDat,
			sizeof(struct TABLA_IVAS));
	return TRUE;
}

//*****************************************************************************
//  Function        : GetPagoDividido
//  Description     :
//  Input           : aIdx;         // Table Index
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetTablaIvas(DWORD Id, DWORD aIdx, struct TABLA_IVAS *aDat) //kt-311012
{
	CheckPointerAddr(aDat);

	if (!ReadSFile(Id, (DWORD) & gTablaIvaOffset[aIdx], aDat,
			sizeof(struct TABLA_IVAS)))
		return FALSE;

	gTablaIvas.w_estadoBaseIva = aDat->w_estadoBaseIva;
	memcpy(&gTablaIvas.b_ivas, aDat->b_ivas, 2);

	return TRUE;

}

//*****************************************************************************
//  Function        : GetTablaIvasCount
//  Description     :
//  Input           : N/A
//  Return          : count;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int GetTablaIvasCount(void) //kt-311012
{
	int ret;
	int fd;

	fd = OpenFile(KTablaIvasFile);

	if (fd == -1)
		return 0;

	ret = fLengthMW(fd) / sizeof(struct TABLA_IVAS);
	CloseFile(fd);

	return ret;
}

//fin-kt


//*****************************************************************************
//  Function        : ReadSConfigFile
//  Description     : Leer archivo ConfigFile
//  Input           : aIdx;         // Table Index N/A Cero
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ReadSConfigFile(struct CONFIG_COMERCIO *aConfig_data) //MFBC/09/11/12
{
	int fd;
	char filename[32];

	CheckPointerAddr(&aConfig_data);
	strcpy(filename, KConfiComercioFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		DispLineMW("CONFIG Open ERROR!", MW_MAX_LINE, MW_CLRDISP | MW_REVERSE
				| MW_CLREOL | MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		fCloseMW(fd);
		return FALSE;
	}

	if (!ReadSFile(fd, (DWORD) & gConfigComerOffset[0], aConfig_data,
			sizeof(struct CONFIG_COMERCIO)))
	{
		fCloseMW(fd);
		return FALSE;
	}
	memcpy(&gConfigComercio, &aConfig_data, sizeof(struct CONFIG_COMERCIO)); //MFBC/21/11/12
	fCloseMW(fd);

	return TRUE;
}

//*****************************************************************************
//  Function        : SaveConfigFile
//  Description     : Save CONFIG_COMERCIO.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SaveConfigFile(struct CONFIG_COMERCIO *aConfig_data) //MFBC/08/11/12
{
	int fd;
	char filename[32];
	CheckPointerAddr(aConfig_data);
	strcpy(filename, KConfiComercioFile);

	fd = fOpenMW(filename);

	if (fd < 0)
	{
		DispLineMW("CONFIG Open ERROR!", MW_MAX_LINE, MW_CLRDISP | MW_REVERSE
				| MW_CLREOL | MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		return;
	}
	//	WriteSFile2(fd, MW_FSEEK_SET, aConfig_data, sizeof(struct CONFIG_COMERCIO));//Daniel Cajas TCP
	WriteSFile(fd, (DWORD) & gConfigComerOffset, aConfig_data,
			sizeof(struct CONFIG_COMERCIO));//MFBC/11/12/12
	fCommitAllMW();
	memcpy(&gConfigComercio, aConfig_data, sizeof(struct CONFIG_COMERCIO)); //MFBC/27/11/12 Actualizo el config comercio global

	fCloseMW(fd);
}

//=============================================================================


//*****************************************************************************
//  Function        : ComOpen
//  Description     : Open the COM port for downloading
//  Input           : N/A
//  Return          : port_opened;    // bit0=aux1, bit1=aux2, bit2=usb
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
DWORD ComOpen(void)
{
	struct MW_AUX_CFG aux_cfg;
	char dev_name[24];
	DWORD port_opened; // bit0=aux1, bit1=aux2, bit2=usb

	port_opened = 0;
	aux_cfg.b_len = sizeof(struct MW_AUX_CFG) - 1;
	aux_cfg.b_mode = 5; // transparent
	aux_cfg.b_speed = MW_AUX_115200 | 0x10; // 115200, 2-byte length data
	aux_cfg.b_rx_gap = 5; // 50ms qualify time
	aux_cfg.b_retry = 0xFF;

	// AUX1
	strcpy(dev_name, DEV_AUX1);
	if (((gAux1 = OpenMW(dev_name, MW_RDWR)) >= 0) && (IOCtlMW(gAux1,
			IO_AUX_CFG, &aux_cfg) >= 0))
		port_opened |= 1 << AUX1;

	// AUXDBG
	strcpy(dev_name, DEV_AUX0);
	if (((gAux2 = OpenMW(dev_name, MW_RDWR)) >= 0) && (IOCtlMW(gAux2,
			IO_AUX_CFG, &aux_cfg) >= 0))
		port_opened |= 1 << AUXDBG;

	// USB
	strcpy(dev_name, DEV_USBS);
	if (((gUsb = OpenMW(dev_name, MW_RDWR)) >= 0))
		port_opened |= 1 << USB;

	return port_opened;
}

//*****************************************************************************
//  Function        : ComClose
//  Description     : Close the COM port for downloading
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ComClose(void)
{
	CloseMW(gAux1);
	CloseMW(gAux2);
	CloseMW(gUsb);
}

//*****************************************************************************
//  Function        : ComRxRdy
//  Description     : Return which Com is ready for receiving
//  Input           : N/A
//  Return          : return which port is Rx Rdy
//                    0 = None Ready
//                    1 = Aux1 Ready
//                    2 = Aux2 Ready
//                    3 = USB Ready
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
DWORD ComRxRdy(void)
{
	if (b_port == ANYPORT)
	{
		if (IOCtlMW(gAux1, IO_AUX_RX_RDY, NULL) > 0)
		{
			b_port = AUX1;
			return (AUX1 + 1);
		}
		if (IOCtlMW(gAux2, IO_AUX_RX_RDY, NULL) > 0)
		{
			b_port = AUXDBG;
			return (AUXDBG + 1);
		}
		if (StatMW(gUsb, MW_USBS_STATUS, NULL) & MW_USB_RXRDY)
		{
			b_port = USB;
			return (USB + 1);
		}
		return 0;
	}

	switch (b_port)
	{
	case AUX1:
		if (IOCtlMW(gAux1, IO_AUX_RX_RDY, NULL) > 0)
			return (AUX1 + 1);
		return 0;

	case AUXDBG:
		if (IOCtlMW(gAux2, IO_AUX_RX_RDY, NULL) > 0)
			return (AUXDBG + 1);
		return 0;

	case USB:
		if (StatMW(gUsb, MW_USBS_STATUS, NULL) & MW_USB_RXRDY)
			return (USB + 1);
		return 0;
	}

	return 0;
}

//*****************************************************************************
//  Function        : ComRead
//  Description     : Get the data from the selected COM
//  Input           : ptr;      // received data buffer
//  Return          : ptr;      // received data
//                    length of received data
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
DWORD ComRead(BYTE * ptr)
{
	switch (b_port)
	{
	case AUX1:
		return ReadMW(gAux1, ptr, K_T4BufSize);
	case AUXDBG:
		return ReadMW(gAux2, ptr, K_T4BufSize);
	case USB:
		return ReadMW(gUsb, ptr, K_T4BufSize);
	}
	return 0;
}

//*****************************************************************************
//  Function        : RemoveDir
//  Description     : To remove the folder name as whole file name stored in terminal
//  Input           : aFilename;        // file name
//  Return          : T/F
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BOOLEAN RemoveDir(BYTE* aFilename)
{
	BYTE i = 32;
	BYTE temp[32];
	memset(temp, ' ', sizeof(temp));
	while (i > 0)
	{
		i--;
		if (aFilename[i] == 0x5C)
		{ // search character '\'
			memcpy(temp, &aFilename[i + 1], 32 - i - 1);
			memcpy(aFilename, temp, sizeof(temp));
			return TRUE; // folder found
		}
	}
	return FALSE; // folder not found
}

//*****************************************************************************
//  Function        : DLL_header_crc
//  Description     : Check if header crc is right
//  Input           : a_hdr;        // file header
//  Return          : T/F
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static DWORD DLL_header_crc(DLL_HDR * a_hdr)
{
	WORD crc;
	crc = cal_crc((BYTE*) a_hdr, sizeof(DLL_HDR) - 2);
	crc = swap_w(crc);
	return (crc == a_hdr->w_crc);
}

//*****************************************************************************
//  Function        : DLL_header_blk
//  Description     : Routine of download header block
//  Input           : a_var;        // download variable
//  Return          : T/F
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static DWORD DLL_header_blk(DLL_VAR *a_var)
{
	DLL_HDR * phdr;
	phdr = (DLL_HDR *) a_var->p_rxbuf;
	a_var->d_filelength = swap_l(phdr->d_filelength);
	a_var->b_last_hdr = (phdr->b_last_file == 'L');

	memcpy(a_var->s_name, phdr->s_filename, sizeof(a_var->s_name));

	// check header crc
	if (DLL_header_crc(phdr) == FALSE)
	{
		a_var->p_message = (BYTE *) Msg_hdr_crc_err;
		return FALSE;
	}

	a_var->d_total_len = 0;
	a_var->w_crc = 0;
	a_var->b_state = WaitDataState;

	return TRUE;
}

//*****************************************************************************
//  Function        : DLL_data_blk
//  Description     : Routine of download data block
//  Input           : a_var;        // download variable
//  Return          : T/F
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static DWORD DLL_data_blk(DLL_VAR *a_var)
{
	WORD crc;
	int file_handle;
	char filename[48];
	BYTE kbdbuf[36]; // displaying information of downloading

	TimerSetMW(dll_timer, 800);
	RemoveDir(a_var->s_name); // remove the folder name first

	// showing information of downloading
	DispLineMW("Cargando...", MW_LINE2, MW_LEFT | MW_SMFONT | MW_CLREOL);
	sprintf((char *) kbdbuf, "%s", a_var->s_name);
	DispLineMW(kbdbuf, MW_LINE3, MW_LEFT | MW_SMFONT | MW_CLREOL);
	sprintf((char *) kbdbuf, "%06d/%06d", a_var->d_total_len,
			a_var->d_filelength);
	DispLineMW(kbdbuf, MW_LINE4, MW_LEFT | MW_SMFONT | MW_CLREOL);

	// writing received message into SRAM buffer
	if (a_var->d_total_len < a_var->d_filelength)
	{
		memcpy(a_var->p_data, a_var->p_rxbuf, a_var->w_rxlen);
		memset(a_var->p_rxbuf, 0, a_var->w_rxlen);
		a_var->w_crc = cal_crcx(a_var->p_data, a_var->w_rxlen, a_var->w_crc);
		a_var->p_data += a_var->w_rxlen;
		a_var->d_total_len += a_var->w_rxlen;
		return TRUE;
	}

	crc = a_var->p_rxbuf[0] * 256 + a_var->p_rxbuf[1];
	if (a_var->w_crc != crc)
	{ // invalid total checksum
		a_var->p_message = (BYTE *) Msg_total_check_err;
		return FALSE;
	}

	//Save file here
	strcpy(filename, (char *) a_var->s_name);
	fDeleteMW(filename);

	if ((file_handle = fCreateMW(filename, MW_FCREATE_SHARE)) < 0)
	{
		a_var->p_message = (BYTE *) Msg_file_create_err;
		return FALSE;
	}

	if (fWriteMW(file_handle, a_var->p_data_start, a_var->d_filelength)
			!= a_var->d_filelength)
	{
		fCloseMW(file_handle);
		fDeleteMW(filename);
		a_var->p_message = (BYTE *) Msg_write_err;
		return FALSE;
	}

	fCloseMW(file_handle);

	// check if last file loaded
	if (a_var->b_last_hdr)
	{
		a_var->p_message = (BYTE *) Msg_complete;
	}
	else
	{
		DispClrBelowMW(MW_LINE2);
		DispLineMW(Msg_wait_for_download, MW_LINE2, MW_LEFT | MW_SMFONT
				| MW_CLREOL);
		a_var->b_state = WaitHeaderState;
		a_var->w_crc = 0;
		a_var->p_data = a_var->p_data_start;
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : ComFlush
//  Description     : Flush the data from the selected COM
//  Input           : ptr;      // received data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ComFlush(BYTE * ptr)
{
	if(b_port & (1<<AUX1)) {
		while (IOCtlMW(gAux1, IO_AUX_RX_RDY, NULL) > 0)
			ReadMW(gAux1, ptr, K_T4BufSize);
	}
	if(b_port & (1<<AUXDBG)) {
		while (IOCtlMW(gAux2, IO_AUX_RX_RDY, NULL) > 0)
			ReadMW(gAux2, ptr, K_T4BufSize);
	}
	if(b_port & (1<<USB)) {
		while (StatMW(gUsb, MW_USBS_STATUS, NULL) & MW_USB_RXRDY)
			ReadMW(gUsb, ptr, K_T4BufSize);
	}
}

//*****************************************************************************
//  Function        : DownloadFile
//  Description     : Routine of download file
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DownloadFile(void)
{
	DLL_VAR var;
	DWORD port;
	BYTE kbdbuf[20];
	DispLineMW(Msg_header, MW_LINE1, MW_CENTER | MW_SMFONT | MW_REVERSE
			| MW_CLRDISP);
	DispLineMW(Msg_wait_for_download, MW_LINE2, MW_LEFT | MW_SMFONT | MW_CLREOL);

	b_port = ANYPORT;
	memset(&var, 0, sizeof(var)); // init Download variable
	var.p_data_start = MallocMW(size_3MB); // allocate the maximum file size has to be sent
	var.p_data = var.p_data_start;
	dll_timer = TimerOpenMW();
	var.p_rxbuf = MallocMW(K_T4BufSize); // allocate receive buffer size (fixed)

	var.p_message = (BYTE *) Msg_aux_open_err;
	port = ComOpen();
	ComFlush(var.p_rxbuf);                      // flush Rx Buf

	if (port == 0x07)
	{ // port open successfully
		do
		{
			var.b_state = WaitHeaderState;
			TimerSetMW(dll_timer, 4000);
			while (TRUE)
			{
				if (ComRxRdy())
				{
					var.w_rxlen = ComRead(var.p_rxbuf);
					if (var.b_state == WaitHeaderState)
					{
						if (DLL_header_blk(&var) == FALSE)
						{
							var.b_last_hdr = TRUE;
							break;
						}
					}
					else
					{
						if (DLL_data_blk(&var) == FALSE)
						{
							var.b_last_hdr = TRUE;
							break;
						}
						if (var.p_message == Msg_complete) // All Completed
							break;
					}
				}

				if (TimerGetMW(dll_timer) == 0)
				{
					var.p_message = (BYTE *) Msg_timeout_err;
					var.b_last_hdr = TRUE;
					break;
				}
				if (os_kbd_getkey() == K_KeyCancel)
				{
					var.p_message = (BYTE *) Msg_abort;
					var.b_last_hdr = TRUE;
					break;
				}
			}
		} while (var.b_last_hdr == FALSE);
	}
	else
	{ // at least one port fail to open
		DispClrBelowMW(MW_LINE2);
		DispLineMW(Msg_port_err, MW_LINE2, MW_LEFT | MW_SMFONT | MW_CLREOL);
		sprintf((char *) kbdbuf, "%08X", port);
		DispLineMW(kbdbuf, MW_LINE3, MW_LEFT | MW_SMFONT | MW_CLREOL);
	}

	TimerCloseMW(dll_timer);
	DispClrBelowMW(MW_LINE2);
	DispLineMW(Msg_download, MW_LINE2, MW_LEFT | MW_SMFONT | MW_CLREOL);
	DispLineMW((BYTE *) var.p_message, MW_LINE3, MW_LEFT | MW_SMFONT
			| MW_CLREOL);
	ComClose();
	memset(var.p_data_start, 0, size_3MB);
	FreeMW(var.p_data_start);
	FreeMW(var.p_rxbuf);
	Delay10ms(200); // displaying message for 1 second
}

// ============================================================================

//*****************************************************************************
//  Function        : GetFileList
//  Description     : Get file list inside FLASH and store it in global
//  Input           : aList;		// pointer to file list
//  Return          : alistnum;		// number of file inside the list
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static int GetFileList(struct FILELIST *aList)
{
	int i = 0;
	int alistnum = 0;
	int file_handle;
	struct MW_FILE_HDR header;

	// initialize FILELIST
	for (i = 0; i < MAX_NUM_OF_FILES; i++)
		memset(&(aList[i]), 0, sizeof(struct FILELIST));

	file_handle = fDSearchMW(1, &header);

	if (file_handle < 0)
	{
		return alistnum;
	}
	else // at least 1 file in the list
	{
		// store list
		memcpy(&(aList[0].filename), header.sb_name, sizeof(header.sb_name));
		aList[0].len = header.d_filelen;
		alistnum++;

		for (i = 1; i < MAX_NUM_OF_FILES; i++)
		{
			if (fDSearchMW(0, &header) < 0) // search second file
				break;
			else
			{
				memcpy(&(aList[i].filename), header.sb_name,
						sizeof(header.sb_name));
				aList[i].len = header.d_filelen;
				alistnum++;
			}
		}
		return alistnum;
	}
}

//*****************************************************************************
//  Function        : DisplayList
//  Description     : Display file list inside FLASH into LCD
//  Input           : aMode;		// display mode
//					  aListNum;		// number of file in the list
//					  aList;		// pointer to file list
//					  aCurrPage;	// pointer to the current page shown
//  Return          : ch;			// character entered by the user
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static DWORD DisplayList(int aMode, int aListNum, struct FILELIST *aList,
		int *aCurrPage)
{
	int i = 0;
	int page = 0;
	DWORD ch = 0;
	BYTE buffer[27];
	float filesize = 0;

	while (1)
	{
		if (aMode == DISPLAY)
		{
			memset(buffer, ' ', sizeof(buffer));
			SprintfMW(buffer, "Size   Name            P%d", page + 1);
			DispLineMW(buffer, MW_LINE2, MW_LEFT | MW_SPFONT | MW_CLREOL);
		}
		else
		{
			memset(buffer, ' ', sizeof(buffer));
			SprintfMW(buffer, "File List              P%d", page + 1);
			DispLineMW(buffer, MW_LINE2, MW_LEFT | MW_SPFONT | MW_CLREOL);
		}
		DispLineMW("--------------------------", MW_LINE3, MW_LEFT | MW_SPFONT);
		if (aListNum > ITEM_PER_PAGE)
		{
			for (i = 0; i < ITEM_PER_PAGE; i++)
			{
				if (aMode == DISPLAY)
				{
					memset(buffer, ' ', sizeof(buffer));
					if (aList[i + page * ITEM_PER_PAGE].len > 1024 * 1024)
					{
						filesize
						= (float) (aList[i + page * ITEM_PER_PAGE].len)
						/ (1024 * 1024);
						SprintfMW(buffer, "%.2f" "MB %s", filesize, aList[i
						                                                  + page * ITEM_PER_PAGE].filename);
					}
					else if ((aList[i + page * ITEM_PER_PAGE].len > 1024)
							&& (aList[i + page * ITEM_PER_PAGE].len <= 1024
									* 1024))
					{
						filesize
						= (float) (aList[i + page * ITEM_PER_PAGE].len)
						/ (1024);
						SprintfMW(buffer, "%.2f" "kB %s", filesize, aList[i
						                                                  + page * ITEM_PER_PAGE].filename);
					}
					else
					{
						SprintfMW(buffer, "%5dB" " %s", aList[i + page
						                                      * ITEM_PER_PAGE].len, aList[i + page
						                                                                  * ITEM_PER_PAGE].filename);
					}
					DispLineMW(buffer, MW_LINE4 + (i << 8), MW_LEFT | MW_SPFONT
							| MW_CLREOL);
				}
				else
				{
					memset(buffer, ' ', sizeof(buffer));
					SprintfMW(buffer, "%d>%s", i + 1, aList[i + page
					                                        * ITEM_PER_PAGE].filename);
					DispLineMW(buffer, MW_LINE4 + (i << 8), MW_LEFT | MW_SPFONT
							| MW_CLREOL);
				}
			}
			ch = APM_WaitKey(2000, 0);
			if (ch != MWKEY_CANCL)
			{
				if (aMode == DISPLAY)
				{
					aListNum -= 6;
					page++;
				}
				else
				{
					if ((ch >= '1') && (ch <= '6')) // selection
						break;
					else // continue to show list
					{
						aListNum -= 6;
						page++;
					}
				}
			}
			else
				break;
		}
		else // last page
		{
			for (i = 0; i < aListNum; i++)
			{
				if (aMode == DISPLAY)
				{
					memset(buffer, ' ', sizeof(buffer));
					if (aList[i + page * ITEM_PER_PAGE].len > 1024 * 1024)
					{
						filesize
						= (float) (aList[i + page * ITEM_PER_PAGE].len)
						/ (1024 * 1024);
						SprintfMW(buffer, "%.2f" "MB %s", filesize, aList[i
						                                                  + page * ITEM_PER_PAGE].filename);
					}
					else if ((aList[i + page * ITEM_PER_PAGE].len > 1024)
							&& (aList[i + page * ITEM_PER_PAGE].len <= 1024
									* 1024))
					{
						filesize
						= (float) (aList[i + page * ITEM_PER_PAGE].len)
						/ (1024);
						SprintfMW(buffer, "%.2f" "kB %s", filesize, aList[i
						                                                  + page * ITEM_PER_PAGE].filename);
					}
					else
					{
						SprintfMW(buffer, "%5dB" " %s", aList[i + page
						                                      * ITEM_PER_PAGE].len, aList[i + page
						                                                                  * ITEM_PER_PAGE].filename);
					}
					DispLineMW(buffer, MW_LINE4 + (i << 8), MW_LEFT | MW_SPFONT
							| MW_CLREOL);
				}
				else
				{
					memset(buffer, ' ', sizeof(buffer));
					SprintfMW(buffer, "%d>%s", i + 1, aList[i + page
					                                        * ITEM_PER_PAGE].filename);
					DispLineMW(buffer, MW_LINE4 + (i << 8), MW_LEFT | MW_SPFONT
							| MW_CLREOL);
				}
			}
			if (aListNum != 6) // for not enough 6 items
				DispClrBelowMW((aListNum << 8) + 0x400);
			ch = APM_WaitKey(2000, 0);
			break;
		}
	}
	*aCurrPage = page; // update current page
	return ch;
}

//*****************************************************************************
//  Function        : ListFile
//  Description     : Show file list in file system
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ListFile(void)
{
	int list_num = 0;
	int *temp_page;
	DWORD key = 0;
	struct FILELIST list[MAX_NUM_OF_FILES];
	temp_page = MallocMW(sizeof(int));

	list_num = GetFileList(&list[0]);

	if (list_num == 0)
	{
		DispLineMW("No file inside!", MW_LINE2, MW_LEFT | MW_SPFONT | MW_CLREOL);
		APM_WaitKey(2000, 0);
	}
	else
	{
		if (list_num == 0)
			return;

		// display list
		while (1)
		{
			key = DisplayList(DISPLAY, list_num, &list[0], temp_page);
			if (key == MWKEY_CANCL)
				break;
		}
	}
	FreeMW(temp_page);
}

#if 0
//*****************************************************************************
//  Function        : CreateFile
//  Description     : Create empty file under T800 file system
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void CreateFile(void)
{
	char filename[20][MAX_NUM_OF_FILES];
	int file_handle;
	int i=0, fail=0, succ=0;
	char num_str[3];
	BYTE num=0;
	BYTE buffer[21];
	DispLineMW("No. of files:(0-99)", MW_LINE3, MW_LEFT|MW_SPFONT|MW_CLREOL);

	// 2 digits
	if (!APM_GetKbd(NUMERIC_INPUT+MW_LINE5+RIGHT_JUST, 2, num_str))
		return;

	// num_str = {length of data(1 byte), data entered(n bytes)}
	num = atoi(&num_str[1]);

	for (i=0; i<num; i++)
	{
		memset(filename[i], 0, sizeof(filename[i]));

		memcpy(filename[i], "file", 4);
		SprintfMW(&(filename[i][4]), "%d", i+1);

		file_handle = fCreateMW(filename[i], MW_FCREATE_SHARE);
		if (file_handle < 0)
			fail++;
		else
			succ++;
		fCloseMW(file_handle);
	}
	memset(buffer, ' ', sizeof(buffer));
	SprintfMW(buffer, "Created success: %d", succ);
	DispLineMW(buffer, MW_LINE6, MW_LEFT|MW_SPFONT|MW_CLREOL);
	memset(buffer, ' ', sizeof(buffer));
	SprintfMW(buffer, "Created fail: %d", fail);
	DispLineMW(buffer, MW_LINE7, MW_LEFT|MW_SPFONT|MW_CLREOL);
	WaitKey();
}
#endif

//*****************************************************************************
//  Function        : DeleteFile
//  Description     : Delete file under T800 file system
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DeleteFile(void)
{
	int page = 0;
	int *temp_page;
	int list_num = 0;
	DWORD key = 0;
	BYTE deletefile[20]; // stored filename to be deleted locally
	int delete_filehdr;

	struct FILELIST list[MAX_NUM_OF_FILES];
	temp_page = MallocMW(sizeof(int));

	// initialize deletefile
	memset(deletefile, 0, sizeof(deletefile));

	list_num = GetFileList(&list[0]);

	if (list_num == 0)
	{
		DispLineMW("No file inside!", MW_LINE2, MW_LEFT | MW_SPFONT | MW_CLREOL);
		APM_WaitKey(2000, 0);
	}
	else
	{
		// display list
		while (1)
		{
			key = DisplayList(DELETE, list_num, &list[0], temp_page);
			if (key == MWKEY_CANCL)
				break;

			page = *temp_page; // update current page

			if (key >= '1' && key <= '6')
			{
				// copy dedicated file to the local variable "deletefile"
				memcpy(deletefile,
						list[(key - '1') + page * ITEM_PER_PAGE].filename,
						sizeof(deletefile));
				delete_filehdr = fDeleteMW(deletefile);
				DispClrBelowMW(MW_LINE2);
				if (delete_filehdr < 0)
				{
					DispLineMW("delete fail!", MW_LINE2, MW_LEFT | MW_SPFONT
							| MW_CLREOL);
					APM_WaitKey(2000, 0);
					break;
				}
				else
				{
					if (list_num < (key - '0')) // entered num not exists
					{
						DispLineMW("No such file!", MW_LINE2, MW_LEFT
								| MW_SPFONT | MW_CLREOL);
						APM_WaitKey(2000, 0);
						break;
					}
					else
					{
						DispLineMW("delete success!", MW_LINE2, MW_LEFT
								| MW_SPFONT | MW_CLREOL);
						APM_WaitKey(2000, 0);
						break;
					}
				}
			}
		}
	}
	FreeMW(temp_page);
}

//*****************************************************************************
//  Function        : ReadFile
//  Description     : Read a file under T800 file system
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ReadFile(void)
{
	char content[7 * 8];
	int content_len = 0;
	char temp[56 * 2];
	char buffer[21];

	int i, j, line;
	int page = 0;
	int *temp_page;
	int list_num = 0;
	DWORD key = 0;
	BYTE openfile[20]; // stored filename to be read locally
	int read_filehdr = -1;

	struct FILELIST list[MAX_NUM_OF_FILES];
	temp_page = MallocMW(sizeof(int));

	// initialize
	memset(openfile, 0, sizeof(openfile));
	memset(content, 0, sizeof(content));
	memset(temp, 0, sizeof(temp));

	list_num = GetFileList(&list[0]);

	if (list_num == 0)
	{
		DispLineMW("No file inside!", MW_LINE2, MW_LEFT | MW_SPFONT | MW_CLREOL);
		APM_WaitKey(2000, 0);
	}
	else
	{
		// display list
		while (1)
		{
			key = DisplayList(READ, list_num, &list[0], temp_page);
			if (key == MWKEY_CANCL)
				break;

			page = *temp_page; // update current page

			if (key >= '1' && key <= '6')
			{
				// copy dedicated filename to the local variable "openfile"
				memcpy(openfile,
						list[(key - '1') + page * ITEM_PER_PAGE].filename,
						sizeof(openfile));
				read_filehdr = fOpenMW(openfile);
				DispClrBelowMW(MW_LINE2);
				if (read_filehdr < 0)
				{
					DispLineMW("Open fail!", MW_LINE2, MW_LEFT | MW_SPFONT
							| MW_CLREOL);
					fCloseMW(read_filehdr);
					APM_WaitKey(2000, 0);
					break;
				}
				else
				{
					if (list_num < (key - '0')) // entered num not exists
					{
						DispLineMW("No such file!", MW_LINE2, MW_LEFT
								| MW_SPFONT | MW_CLREOL);
						fCloseMW(read_filehdr);
						APM_WaitKey(2000, 0);
						break;
					}
					else
					{
						content_len = fReadMW(read_filehdr, content,
								sizeof(content));
						DispClrBelowMW(MW_LINE2);

						split(temp, content, 56);

						i = 0;
						j = 0;
						line = 0;
						while (i < content_len * 2)
						{
							buffer[j++] = temp[i++];
							// i%2==0: 2 character as 1 byte
							// i!=0: starting
							// i%14==0: end of line
							if ((i % 2 == 0) && (i != 0) && (i % 14 != 0))
								buffer[j++] = ' ';
							if (i == content_len * 2) // only one line
							{
								DispLineMW(buffer, MW_LINE2 + (line << 8),
										MW_LEFT | MW_SMFONT | MW_CLREOL);
							}
							else // more than one line
							{
								if (j == 20)
								{
									DispLineMW(buffer, MW_LINE2 + (line << 8),
											MW_LEFT | MW_SMFONT | MW_CLREOL);
									j = 0;
									line++;
									memset(buffer, 0, sizeof(buffer));
								}
							}
						}
						fCloseMW(read_filehdr);
						APM_WaitKey(2000, 0);
						break;
					}
				}
			}
		}
	}
	FreeMW(temp_page);
}

#if 0
//*****************************************************************************
//  Function        : WriteFile
//  Description     : Write a file with random data under T800 file system
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void WriteFile(void)
{
	char content[7*8]; // store content of data to be written
	char temp[8]; // store random data

	int i=0;
	int page=0;
	int *temp_page;
	int list_num=0;
	DWORD key=0;
	BYTE openfile[20]; // stored filename to be written locally
	int write_filehdr = -1;

	struct FILELIST list[MAX_NUM_OF_FILES];
	temp_page = MallocMW(sizeof(int));

	// initialize
	memset(openfile, 0, sizeof(openfile));
	memset(content, 0, sizeof(content));

	list_num = GetFileList(&list[0]);

	if (list_num == 0)
	{
		DispLineMW("No file inside!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
		WaitKey();
	}
	else
	{
		// display list
		while(1)
		{
			key = DisplayList(WRITE, list_num, &list[0], temp_page);
			if (key == MWKEY_CANCL)
				break;

			page = *temp_page; // update current page

			if (key >= '1' && key <= '6')
			{
				// copy dedicated filename to the local variable "openfile"
				memcpy(openfile, list[(key-'1')+page*ITEM_PER_PAGE].filename, sizeof(openfile));
				write_filehdr = fOpenMW(openfile);
				DispClrBelowMW(MW_LINE2);
				if (write_filehdr < 0)
				{
					DispLineMW("Write fail!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
					fCloseMW(write_filehdr);
					WaitKey();
					break;
				}
				else
				{
					if (list_num < (key-'0')) // entered num not exists

					{
						DispLineMW("No such file!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
						fCloseMW(write_filehdr);
						WaitKey();
						break;
					}
					else
					{
						for (i=0; i<7; i++)
						{
							RandMW(temp);
							memcpy(&content[i*8], temp, 8);
						}

						fWriteMW(write_filehdr, content, sizeof(content));
						DispClrBelowMW(MW_LINE2);
						DispLineMW("Write success!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
						fCommitAllMW();
						fCloseMW(write_filehdr);
						WaitKey();
						break;
					}
				}
			}
		}
	}
	FreeMW(temp_page);
}

void ParamFile(void)
{
	T_Dir dirLink;
	T_DirEntry *entry;
	struct FILELIST list[MAX_NUM_OF_FILES];
	char dirname[] = "/";

	int i;
	char content[20*8]; // show character only
	char buffer[20];
	int *temp_page;
	int page=0;
	DWORD key=0;
	BYTE openfile[20]; // stored filename to be read locally
	int pfile_num = 0;
	int read_filehdr = -1;

	DispClrBelowMW(MW_LINE2);
	temp_page = MallocMW(sizeof(int));

	memset(openfile, 0, sizeof(openfile));
	memset(content, 0, sizeof(content));

	if (os_file_init_search(dirname, &dirLink) < 0) // read number of files
		return;

	do
	{
		entry = (T_DirEntry *)os_file_readdir(&dirLink); // read files one by one
		if (entry != NULL)
		{
			if (strstr((char const *)entry->s_name, "PARAM"))
			{ // read para files only
				memcpy(list[pfile_num].filename, entry->s_name, sizeof(list[pfile_num].filename));
				list[pfile_num].len = entry->d_filelen;
				pfile_num++;
			}
		}
	}while (entry != NULL);

	if (pfile_num == 0)
	{
		DispLineMW("No para file inside!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
		WaitKey();
	}
	else
	{
		// display list
		while(1)
		{
			key = DisplayList(READ, pfile_num, &list[0], temp_page);
			if (key == MWKEY_CANCL)
				break;

			page = *temp_page; // update current page

			if (key >= '1' && key <= '6')
			{
				// make full path to open file
				memcpy(openfile, "/", 1);
				strcat(openfile, list[(key-'1')+page*ITEM_PER_PAGE].filename);
				read_filehdr = os_file_open(openfile, K_O_RDONLY);
				DispClrBelowMW(MW_LINE2);
				if (read_filehdr < 0)
				{
					DispLineMW("Open fail!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
					os_file_close(read_filehdr);
					WaitKey();
					break;
				}
				else
				{
					if (pfile_num < (key-'0')) // entered num not exists

					{
						DispLineMW("No such file!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
						os_file_close(read_filehdr);
						WaitKey();
						break;
					}
					else
					{
						os_file_read(read_filehdr, content, sizeof(content));
						DispClrBelowMW(MW_LINE2);

						for(i=0; i<8; i++)
						{
							memcpy(buffer, &content[20*i], sizeof(buffer));
							DispLineMW(buffer, MW_LINE2 + (i<<8), MW_LEFT|MW_SMFONT|MW_CLREOL);
							memset(buffer, 0, sizeof(buffer));
						}

						os_file_close(read_filehdr);
						WaitKey();
						break;
					}
				}
			} // end of selection
		} // end while loop
	}
}

//--------------------------------------------------------------------------
void dummyMW(void)
{
	int delete_filehdr;
	char filename[20];

	memcpy(filename, "file", 20);

	delete_filehdr = fDeleteMW(filename);
	if (delete_filehdr < 0)
	{
		DispLineMW("delete fail!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
		WaitKey();
	}
	else
	{
		DispLineMW("delete success!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
		WaitKey();
	}
}

void dummyOS(void)
{
	int delete_filehdr;
	char filename[20];

	memcpy(filename, "file", 20);

	delete_filehdr = os_file_delete(filename);
	if (delete_filehdr < 0)
	{
		DispLineMW("delete fail!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
		WaitKey();
	}
	else
	{
		DispLineMW("delete success!", MW_LINE2, MW_LEFT|MW_SPFONT|MW_CLREOL);
		WaitKey();
	}
}
#endif

//*****************************************************************************
//  Function        : GetPARAMFile
//  Description     : Get content from param file
//  Input           : aptr: aFileName
//  Return          : N/A
//  Note            : Modified by Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetPARAMFile(BYTE *buff, BYTE *aFileName, BOOLEAN aShowError)
{
	T_Dir dirLink;
	T_DirEntry *entry;
	struct FILELIST list[32];
	char dirname[] = "/";

	//	int result;
	BYTE *content;
	BYTE filename[20];				// stored filename to be read locally
	int pfile_num = 0;
	int filesize = 0;
	int readlen = 0;
	int read_filehdr = -1;
	//	BYTE buf[32];

	memset(filename, 0, sizeof(filename));
	//    memcpy(filename, aFileName, sizeof(filename));

	if (os_file_init_search(dirname, &dirLink) < 0)				// read number of files
	{
		if( aShowError )
		{
			LongBeep();
			DispLineMW("CERO ARCHIVOS", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
			APM_WaitKey(300, 0);
		}
		return FALSE;
	}

	do {
		entry = (T_DirEntry *)os_file_readdir(&dirLink);		// read files one by one

		if (entry != NULL) {
			//            printf("\fName:<%s>",entry->s_name );
			//            APM_WaitKey(9000,0);
			if (strstr((char const *)entry->s_name, aFileName)) {	// read para files only
				memcpy(list[pfile_num].filename, entry->s_name, sizeof(list[pfile_num].filename));
				list[pfile_num].len = entry->d_filelen;
				pfile_num++;
				break;
			}
		}
	} while (entry != NULL);

	if (pfile_num == 0)
	{
		if( aShowError )
		{
			LongBeep();
			DispLineMW("NO EXISTE ARCHIVO", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
			APM_WaitKey(300, 0);
		}
	}
	else
	{
		pfile_num--;
		memcpy(filename, dirname, 1);
		strcat(filename, list[pfile_num].filename);
		if ((read_filehdr = os_file_open(filename, K_O_RDONLY)) < 0)
		{
			if( aShowError )
			{
				LongBeep();
				DispLineMW("Open fail!", MW_LINE2, MW_LEFT|MW_SMFONT|MW_CLRDISP);
				os_file_close(read_filehdr);
				APM_WaitKey(300, 0);
			}

			return FALSE;
		}
		else
		{
			/* getting file content */
			filesize = os_file_length(read_filehdr);
			content = (BYTE*) MallocMW(filesize);
			if (content == NULL) {
				os_file_close(read_filehdr);
				return FALSE;
			}
			readlen = os_file_read(read_filehdr, content, filesize);
			os_file_close(read_filehdr);
		}

		if (readlen != filesize) {
			FreeMW(content);
			if (aShowError) {
				LongBeep();
				DispLineMW("NO EXISTE ARCHIVO", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
				APM_WaitKey(300, 0);
			}
			return FALSE;
		}

		memcpy(buff, content, readlen);
		FreeMW(content);
		return TRUE;
	}
	return FALSE;
}

//*****************************************************************************
//  Function        : IsThereFile
//  Description     : Get content from param file
//  Input           : aptr: aFileName
//  Return          : N/A
//  Note            : Modified by Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN IsThereFile(BYTE *aFileName, BOOLEAN aShowError)
{
	T_Dir dirLink;
	T_DirEntry *entry;
	struct FILELIST list[32];
	char dirname[] = "/";


	int pfile_num = 0;

	if (os_file_init_search(dirname, &dirLink) < 0)				// read number of files
	{
		if( aShowError )
		{
			LongBeep();
			DispLineMW("CERO ARCHIVOS", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
			APM_WaitKey(300, 0);
		}
		return FALSE;
	}

	do {
		entry = (T_DirEntry *)os_file_readdir(&dirLink);		// read files one by one
		if (entry != NULL) {
			if (strstr((char const *)entry->s_name, aFileName)) {	// read para files only
				memcpy(list[pfile_num].filename, entry->s_name, sizeof(list[pfile_num].filename));
				list[pfile_num].len = entry->d_filelen;
				pfile_num++;
				break;
			}
		}
	} while (entry != NULL);

	if (pfile_num == 0)
	{
		if( aShowError )
		{
			LongBeep();
			DispLineMW("NO EXISTE ARCHIVO", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
			APM_WaitKey(300, 0);
		}
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

//*****************************************************************************
//  Function        : CSParamFile
//  Description     : Get checksum from param file
//  Input           : aptr: aFileName
//  Return          : N/A
//  Note            : Modified by Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN CSParamFile(BYTE *aChecksum, BYTE *aFileName, BOOLEAN aShowError)
{
	T_Dir dirLink;
	T_DirEntry *entry;
	char dirname[] = "/";

	int pfile_num = 0;

	if (os_file_init_search(dirname, &dirLink) < 0)				// read number of files
	{
		if( aShowError )
		{
			LongBeep();
			DispLineMW("CERO ARCHIVOS", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
			APM_WaitKey(300, 0);
		}
		return FALSE;
	}

	do {
		entry = (T_DirEntry *)os_file_readdir(&dirLink);		// read files one by one
		if (entry != NULL) {
			if (strstr((char const *)entry->s_name, aFileName)) {	// read para files only
				//				memcpy(list[pfile_num].filename, entry->s_name, sizeof(list[pfile_num].filename));
				//				list[pfile_num].len = entry->d_filelen;
				pfile_num++;
				break;
			}
		}
	} while (entry != NULL);

	if (pfile_num == 0)
	{
		if( aShowError )
		{
			LongBeep();
			DispLineMW("NO EXISTE ARCHIVO", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
			APM_WaitKey(300, 0);
		}
		return FALSE;
	}
	else
	{
		memset(aChecksum, 0, sizeof(aChecksum));
		memcpy(aChecksum, &entry->s_name[strlen(entry->s_name) - 4], 4);

		//        printf("\fCS:<%s>", aChecksum );
		//        APM_WaitKey(9000,0);

		return TRUE;
	}
}

// Jorge Numa 30/09/2013
void ContactlessInit( void )
{
	BYTE filename[32];
	BYTE cs_paramc[5];
	BYTE cs_paramp[5];
	BYTE cs_paramw[5];
	BOOLEAN retPARAMC = FALSE;
	BOOLEAN retPARAMP = FALSE;
	BOOLEAN retPARAMW = FALSE;
	struct CTLS_FILE ctls_file;
	struct CTLGEN ctl_gen;
	int id = -1;
	int idCTLGEN = -1;
	int idCTLPPASS = -1;
	int idCTLPWAVE = -1;
	BYTE *buff;

	memset(filename, 0x00, sizeof(filename));
	strcpy(filename, "CTLSFILE");
	id = fOpenMW(filename);
	if (id < 0)
	{
		//        printf("\fDEBUG 1 IF");
		//        APM_WaitKey(9000,0);

		// CTLSGEN.tms
		retPARAMC = IsThereFile("PARAMC", FALSE);

		// CTLSPPASS.tms
		retPARAMP = IsThereFile("PARAMP", FALSE);

		// CTLSPWAVE.tms
		retPARAMW = IsThereFile("PARAMW", FALSE);
		if (retPARAMC == FALSE || retPARAMP == FALSE || retPARAMW == FALSE)
		{
			LongBeep();
			TextColor("CARGUE ARCHIVOS", MW_LINE4, COLOR_RED, MW_CENTER
					| MW_CLRDISP | MW_SMFONT, 0);
			TextColor("CONTACTLESS", MW_LINE5, COLOR_RED, MW_CENTER
					| MW_SMFONT, 3);

			fCloseMW(id);
			return;
		}
		else
		{
			memset(filename, 0x00, sizeof(filename));
			strcpy(filename, "CTLSFILE");

			id = fCreateMW(filename, FALSE);
			memset(&ctls_file, 0, sizeof(struct CTLS_FILE));

			memset(cs_paramc, 0, sizeof(cs_paramc));
			memset(cs_paramp, 0, sizeof(cs_paramp));
			memset(cs_paramw, 0, sizeof(cs_paramw));

			// CTLSGEN.tms
			CSParamFile(cs_paramc, "PARAMC", false);
			memcpy(ctls_file.cs_paramc, cs_paramc, 4);

			// CTLSPPASS.tms
			CSParamFile(cs_paramp, "PARAMP", false);
			memcpy(ctls_file.cs_paramp, cs_paramp, 4);

			// CTLSPWAVE.tms
			CSParamFile(cs_paramw, "PARAMW", false);
			memcpy(ctls_file.cs_paramw, cs_paramw, 4);

			fWriteMW(id, &ctls_file, sizeof(struct CTLS_FILE));

			buff = (BYTE*)MallocMW(100);
			memset(buff, 0, sizeof(buff));

			idCTLGEN = OpenFile(KCTLGEN);
			if (idCTLGEN < 0)
			{
				idCTLGEN = CreateEmptyFile(KCTLGEN);
				//                printf("\fDEBUG 1");
				//                APM_WaitKey(9000,0);
				if(GetPARAMFile(buff, "PARAMC", TRUE) == TRUE)
				{
					upackedCTLGEN(buff);

					memcpy(&ctl_gen, &gCTLGEN, sizeof(struct CTLGEN));

					fWriteMW(idCTLGEN, &ctl_gen, sizeof(struct CTLGEN));
					fCommitAllMW();
				}
				else
				{
					fCloseMW(idCTLGEN);
					FreeMW(buff);
					return;
				}
			}


			memset(buff, 0, sizeof(buff));

			idCTLPPASS = OpenFile(KCTLPPASS);
			if (idCTLPPASS < 0)
			{
				idCTLPPASS = CreateEmptyFile(KCTLPPASS);
				//                printf("\fDEBUG 2");
				//                APM_WaitKey(9000,0);
				if(GetPARAMFile(buff, "PARAMP", TRUE) == TRUE)
				{
					fWriteMW(idCTLPPASS, buff, strlen(buff));
					fCommitAllMW();
				}
				else
				{
					fCloseMW(idCTLPPASS);
					FreeMW(buff);
					return;
				}
			}

			memset(buff, 0, sizeof(buff));

			idCTLPWAVE = OpenFile(KCTLPWAVE);
			if (idCTLPWAVE < 0)
			{
				idCTLPWAVE = CreateEmptyFile(KCTLPWAVE);
				//                printf("\fDEBUG 3");
				//                APM_WaitKey(9000,0);
				if(GetPARAMFile(buff, "PARAMW", TRUE) == TRUE)
				{
					fWriteMW(idCTLPWAVE, buff, strlen(buff));
					fCommitAllMW();
				}
				else
				{
					fCloseMW(idCTLPWAVE);
					FreeMW(buff);
					return;
				}
			}

			fCommitAllMW();
			fCloseMW(idCTLGEN);
			fCloseMW(idCTLPPASS);
			fCloseMW(idCTLPWAVE);

		}
	}
	else
	{
		fSeekMW(id, 0);
		buff = (BYTE*)MallocMW(100);
		fReadMW(id, &ctls_file, sizeof(struct CTLS_FILE));

		memset(cs_paramc, 0, sizeof(cs_paramc));
		memset(cs_paramp, 0, sizeof(cs_paramp));
		memset(cs_paramw, 0, sizeof(cs_paramw));

		// Si el Checksum es diferente se crea de nuevo
		// CTLSGEN.tms
		CSParamFile(cs_paramc, "PARAMC", false);

		//        printf("\fDEBUG1 else");
		//        APM_WaitKey(9000,0);
		if( memcmp(ctls_file.cs_paramc, cs_paramc, 4) != 0)
		{
			memcpy(ctls_file.cs_paramc, cs_paramc, 4);
			memset(buff, 0, sizeof(buff));

			idCTLGEN = CreateEmptyFile(KCTLGEN);
			if(GetPARAMFile(buff, "PARAMC", TRUE) == TRUE)
			{
				//                printf("\fDEBUG1");
				//                APM_WaitKey(9000,0);
				upackedCTLGEN(buff);

				memcpy(&ctl_gen, &gCTLGEN, sizeof(struct CTLGEN));

				fWriteMW(idCTLGEN, &ctl_gen, sizeof(struct CTLGEN));

				fCommitAllMW();
				fCloseMW(idCTLGEN);
			}
			else
			{
				fSeekMW(id, 0);
				fWriteMW(id, &ctls_file, sizeof(struct CTLS_FILE));
				fCommitAllMW();
				fCloseMW(idCTLGEN);
				fCloseMW(id);
				FreeMW(buff);
				return;
			}
		}

		// Si el Checksum es diferente se crea de nuevo
		// CTLSPPASS.tms
		CSParamFile(cs_paramp, "PARAMP", false);

		if( memcmp(ctls_file.cs_paramp, cs_paramp, 4) != 0)
		{
			memcpy(ctls_file.cs_paramp, cs_paramp, 4);
			memset(buff, 0, sizeof(buff));

			idCTLPPASS = CreateEmptyFile(KCTLPPASS);
			if(GetPARAMFile(buff, "PARAMP", TRUE) == TRUE)
			{
				//                printf("\fDEBUG2");
				//                APM_WaitKey(9000,0);
				fWriteMW(idCTLPPASS, buff, strlen(buff));
				fCommitAllMW();
				fCloseMW(idCTLPPASS);
			}
			else
			{
				fSeekMW(id, 0);
				fWriteMW(id, &ctls_file, sizeof(struct CTLS_FILE));
				fCommitAllMW();
				fCloseMW(idCTLPPASS);
				fCloseMW(id);
				FreeMW(buff);
				return;
			}
		}

		// Si el Checksum es diferente se crea de nuevo
		// CTLSPWAVE.tms
		CSParamFile(cs_paramw, "PARAMW", false);

		if( memcmp(ctls_file.cs_paramw, cs_paramw, 4) != 0)
		{
			memcpy(ctls_file.cs_paramw, cs_paramw, 4);
			memset(buff, 0, sizeof(buff));

			idCTLPWAVE = CreateEmptyFile(KCTLPWAVE);
			if(GetPARAMFile(buff, "PARAMW", TRUE) == TRUE)
			{
				//                printf("\fDEBUG3");
				//                APM_WaitKey(9000,0);

				fWriteMW(idCTLPWAVE, buff, strlen(buff));
				fCloseMW(idCTLPWAVE);
			}
			else
			{
				fSeekMW(id, 0);
				fWriteMW(id, &ctls_file, sizeof(struct CTLS_FILE));
				fCloseMW(idCTLPWAVE);
				fCloseMW(id);
				FreeMW(buff);
				return;
			}
		}
	}
	fSeekMW(id, 0);
	fWriteMW(id, &ctls_file, sizeof(struct CTLS_FILE));
	fCommitAllMW();
	fCloseMW(id);
	FreeMW(buff);

}

void upackedCTLGEN(BYTE *buff)
{
	BYTE token[2];
	BYTE *ptr1;
	BYTE tmpFileGen[13];
	WORD x, y;
	struct CTLGEN aCTLGEN;

	token[0] = 0x0A;

	memset(&aCTLGEN, 0, sizeof(struct CTLGEN));

	ptr1 = strtok(buff, token);

	x = 1;
	while (ptr1 != NULL)
	{
		//ptr1 = strtok(fdata + idx1, token);
		//idx1 += strlen(ptr1) + 1;
		//idx1 += strlen(ptr1) + 1;

		switch (x)
		{
		case 1:
			for (y = 0; y < strlen(ptr1); y++)
			{
				if (ptr1[y] == '=')
				{

					memset(tmpFileGen, 0x00, sizeof(tmpFileGen));

					memcpy(tmpFileGen, &ptr1[y + 1], strlen( &ptr1[y + 1]));

					LSetStr(tmpFileGen, 12, '0');

					memcpy( aCTLGEN.trans_limit, tmpFileGen, 12 );

					//                        compress(sb_TRANS_LIMIT, tmpFileGen, 6);
					//                         printf("\fsb_TRANS_LIMIT<%s>\n", tmpFileGen);
					//                         APM_WaitKey(9000,0);
					break;
				}
			}
			break;

		case 2:
			for (y = 0; y < strlen(ptr1); y++)
			{
				if (ptr1[y] == '=')
				{
					memset(tmpFileGen, 0x00, sizeof(tmpFileGen));
					memcpy(tmpFileGen, &ptr1[y + 1], strlen(
							&ptr1[y + 1]));
					LSetStr(tmpFileGen, 12, '0');
					memcpy( aCTLGEN.cvm_limit, tmpFileGen, 12 );
					//                        compress(sb_CVM_LIMIT, tmpFileGen, 6);
					//                        printf("\fsb_CVM_LIMIT<%s>\n", tmpFileGen);
					//                        APM_WaitKey(9000,0);
					break;
				}
			}
			break;

		case 3:
			for (y = 0; y < strlen(ptr1); y++)
			{
				if (ptr1[y] == '=')
				{
					memset(tmpFileGen, 0x00, sizeof(tmpFileGen));
					memcpy(tmpFileGen, &ptr1[y + 1], strlen(
							&ptr1[y + 1]));
					LSetStr(tmpFileGen, 12, '0');
					memcpy( aCTLGEN.floor_limit, tmpFileGen, 12 );
					//                        compress(sb_FLOOR_LIMIT, tmpFileGen, 6);
					//                        printf("\fsb_FLOOR_LIMIT<%s>\n", tmpFileGen);
					//                        APM_WaitKey(9000,0);
					break;
				}
			}
			break;
		}
		ptr1 = strtok(NULL, token);
		x++;
	}

	memcpy(&gCTLGEN, &aCTLGEN, sizeof(struct CTLGEN));

}


