/*
************************************
*       Module : x509.c            *
*       Name   : TF                *
*       Date   : 11-01-2008        *
************************************
*/
/*==========================================================================*/
/* Naming conventions                                                       */
/* ~~~~~~~~~~~~~~~~~~                                                       */
/*         Class define : Leading C                                         */
/*        Struct define : Leading T                                         */
/*               Struct : Leading s                                         */
/*               Class  : Leading c                                         */
/*             Constant : Leading K                                         */
/*      Global Variable : Leading g                                         */
/*    Function argument : Leading a                                         */
/*       Local Variable : All lower case                                    */
/*            Byte size : Leading b                                         */
/*            Word size : Leading w                                         */
/*           Dword size : Leading d                                         */
/*              Pointer : Leading p                                         */
/*==========================================================================*/

#include <string.h>
#include "x509.h"

#define K_AsnPrimitive            0x00
#define K_AsnConstructed          0x20

#define K_AsnUniversal            0x00
#define K_AsnApplication          0x40
#define K_AsnContextSpecific      0x80
#define K_AsnPrivate              0xC0

#define K_AsnBoolean              1
#define K_AsnInteger              2
#define K_AsnBitString            3
#define K_AsnOctetString          4
#define K_AsnNull                 5
#define K_AsnOId                  6
#define K_AsnUtf8String           12
#define K_AsnSequence             16
#define K_AsnSet                  17
#define K_AsnPrintableString      19
#define K_AsnT61String            20
#define K_AsnIa5String            22
#define K_AsnUtcTime              23
#define K_AsnGeneralizedTime      24

#define K_AttribCountryName       6
#define K_AttribLocality          7
#define K_AttribOrganization      10
#define K_AttribOrgUnit           11
#define K_AttribDnQualifier       46
#define K_AttribStateProvince     8
#define K_AttribCommonName        3

#define K_ImplicitIssuerId        1
#define K_ImplicitSubjectId       2
#define K_ExplicitExtension       3

#define K_ExtCertPolicies         146
#define K_ExtBasicConstraints     133
#define K_ExtAuthKeyId            149
#define K_ExtKeyUsage             129
#define K_ExtAltSubjectName       131
#define K_ExtAltIssuerName        132
#define K_ExtSubjKeyId            128

#define K_OIdRsaMd2               646
#define K_OIdRsaMd5               648
#define K_OIdRsaSha1              649

#define K_OIdSha1                 88
#define K_OIdMd2                  646
#define K_OIdMd5                  649

/*---------------------------------------------------------------------*/
static int x509_get_asn_length(BYTE ** a_p,DWORD a_size,int * a_valLen)
{
  BYTE  *c, *end;
  int   len, olen;

  c = *a_p;
  end = c + a_size;
  if (end - c < 1)
    return 0;

  /*
    If the length byte has high bit only set, it's an indefinite length
    We don't support this!
  */
  if (*c == 0x80)
  {
    *a_valLen = -1;
    return 0;
  }
  /*
    If the high bit is set, the lower 7 bits represent the number of
    bytes that follow and represent length
    If the high bit is not set, the lower 7 represent the actual length
  */
  len = *c & 0x7F;
  if ((*(c++) & 0x80) != 0)
  {
    /*
      Make sure there aren't more than 4 bytes of length specifier,
      and that we have that many bytes left in the buffer
    */
    if (len > 4 || len == 0x7f || (end - c) < len)
      return 0;

    olen = 0;
    while (len > 0)
    {
      olen = (olen << 8) | *c;
      c++; len--;
    }
    if (olen < 0 || olen > 0x7FFFFF)
    {
      return 0;
    }
    len = olen;
  }

  *a_p = c;
  *a_valLen = len;
  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_integer(BYTE ** a_pp,int a_len,int * a_val)
{
  BYTE  *p, *end;
  DWORD ui;
  int   vlen;

  p = *a_pp;
  end = p + a_len;
  if (a_len < 1 || *(p++) != K_AsnInteger ||
      x509_get_asn_length(&p, a_len - 1, &vlen) == 0)
    return 0;

  /*
    This check prevents us from having a big positive integer where the
    high bit is set because it will be encoded as 5 bytes (with leading
    blank byte).  If that is required, a getUnsigned routine should be used
  */
  if (vlen > sizeof(int) || end - p < vlen)
    return 0;

  ui = 0;
  /*
    If high bit is set, it's a negative integer, so perform the two's compliment
    Otherwise do a standard big endian read (most likely case for RSA)
  */
  if (*p & 0x80)
  {
    while (vlen-- > 0)
    {
      ui = (ui << 8) | (*p ^ 0xFF);
      p++;
    }
    vlen = (int)ui;
    vlen++;
    vlen = -vlen;
    *a_val = vlen;
  }
  else
  {
    while (vlen-- > 0)
    {
      ui = (ui << 8) | *p;
      p++;
    }
    *a_val = (int)ui;
  }
  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_set(BYTE ** a_pp,int a_len,int * a_setlen)
{
  BYTE * p;

  p = *a_pp;
  if (a_len<1 || *(p++)!=(K_AsnSet|K_AsnConstructed) ||
      x509_get_asn_length(&p,a_len-1,a_setlen)==0 || a_len<*a_setlen)
    return 0;

  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
/* x509_get_sequence -- Find Sequence Object From Buffer
 * pp     - Pointer to Buffer
 * len    - Size of Buffer
 * seqlen - Return the Length of Sequence Object
 */
static int x509_get_sequence(BYTE ** a_pp,DWORD a_len,int * a_seqlen)
{
  BYTE * p;

  p = *a_pp;
  if (a_len<1 || *(p++)!=(K_AsnSequence|K_AsnConstructed) ||
      x509_get_asn_length(&p,a_len-1,a_seqlen)==0 || a_len<*a_seqlen)
    return 0;

  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_dn_attrib(BYTE ** a_pp,int a_len)
{
/* sslSha1Context_t hash; */
  BYTE *  p;
  BYTE *  dnEnd;
  int     llen,setlen,arcLen,id,stringType;

  p = *a_pp;
  if (x509_get_sequence(&p,a_len,&llen) == 0)
    return 0;

  dnEnd = p + llen;

/* matrixSha1Init(&hash); */
  while (p < dnEnd)
  {
    if (x509_get_set(&p,(int)(dnEnd - p),&setlen) == 0)
      return 0;
    if (x509_get_sequence(&p,(int)(dnEnd - p),&llen) == 0)
      return 0;
    if (dnEnd <= p || (*(p++) != K_AsnOId) ||
        x509_get_asn_length(&p,(int)(dnEnd - p),&arcLen) == 0 ||
        (dnEnd - p) < arcLen)
      return 0;
/*
  id-at                        OBJECT IDENTIFIER ::= {joint-iso-ccitt(2) ds(5) 4}
  id-at-commonName             OBJECT IDENTIFIER ::= {id-at 3}
  id-at-countryName            OBJECT IDENTIFIER ::= {id-at 6}
  id-at-localityName           OBJECT IDENTIFIER ::= {id-at 7}
  id-at-stateOrProvinceName    OBJECT IDENTIFIER ::= {id-at 8}
  id-at-organizationName       OBJECT IDENTIFIER ::= {id-at 10}
  id-at-organizationalUnitName OBJECT IDENTIFIER ::= {id-at 11}
*/
    *a_pp = p;
/*
  FUTURE: Currently skipping OIDs not of type {joint-iso-ccitt(2) ds(5) 4}
  However, we could be dealing with an OID we MUST support per RFC.
  domainComponent is one such example.
*/
    if (dnEnd - p < 2)
      return 0;
    if ((*p++ != 85) || (*p++ != 4) )
    {
      p = *a_pp;
/*
  Move past the OID and string type, get data size, and skip it.
  NOTE: Have had problems parsing older certs in this area.
*/
      if (dnEnd - p < arcLen + 1)
        return 0;
      p += arcLen + 1;
      if (x509_get_asn_length(&p,(int)(dnEnd - p),&llen) == 0 ||
          dnEnd - p < llen)
        return 0;
      p = p + llen;
      continue;
    }
/*
  Next are the id of the attribute type and the ASN string type
*/
    if (arcLen != 3 || dnEnd - p < 2)
      return 0;
    id = (int)*p++;
    /*
      Done with OID parsing
    */
    stringType = (int)*p++;

    x509_get_asn_length(&p,(int)(dnEnd - p),&llen);
    if (dnEnd - p < llen)
      return 0;
    switch (stringType)
    {
      case K_AsnPrintableString:
      case K_AsnUtf8String:
      case K_AsnT61String:
        p = p + llen;
        break;
      default:
        return 0;
    }

    switch (id)
    {
      case K_AttribCountryName:
/*       if (attribs->country) { */
/*         psFree(attribs->country); */
/*       } */
/*       attribs->country = stringOut; */
        break;
      case K_AttribStateProvince:
/*       if (attribs->state) { */
/*         psFree(attribs->state); */
/*       } */
/*       attribs->state = stringOut; */
        break;
      case K_AttribLocality:
/*       if (attribs->locality) { */
/*         psFree(attribs->locality); */
/*       } */
/*       attribs->locality = stringOut; */
        break;
      case K_AttribOrganization:
/*       if (attribs->organization) { */
/*         psFree(attribs->organization); */
/*       } */
/*       attribs->organization = stringOut; */
        break;
      case K_AttribOrgUnit:
/*       if (attribs->orgUnit) { */
/*         psFree(attribs->orgUnit); */
/*       } */
/*       attribs->orgUnit = stringOut; */
        break;
      case K_AttribCommonName:
/*       if (attribs->commonName) { */
/*         psFree(attribs->commonName); */
/*       } */
/*       attribs->commonName = stringOut; */
        break;
/*
  Not a MUST support
*/
      default:
/*       psFree(stringOut); */
        return 0;
    }
/*
  Hash up the DN.  Nice for validation later
*/
/*     if (stringOut != NULL) { */
/*       matrixSha1Update(&hash, (BYTE*)stringOut, llen); */
/*     } */
  }
/*   matrixSha1Final(&hash, (BYTE*)attribs->hash); */
  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_explicit_version(BYTE ** a_pp,int a_len,int a_expVal,int * a_val)
{
  BYTE *  p;
  int     exLen;

  if (a_len < 1)
    return 0;

  p = * a_pp;

  /*
    This is an optional value, so don't error if not present.  The default
    value is version 1
  */
  if (*p != (K_AsnContextSpecific | K_AsnConstructed | a_expVal))
  {
    *a_val = 0;
    return -1;
  }
  p++;

  if (x509_get_asn_length(&p,a_len-1,&exLen)==0 || (a_len-1)<exLen)
    return 0;

  if (x509_get_integer(&p,exLen,a_val) == 0)
    return 0;

  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_SerialNum(BYTE ** a_pp,int a_len)
{
  BYTE *  p;
  int     vlen;

  p = *a_pp;
  if (a_len < 1 || *(p++) != K_AsnInteger ||
      x509_get_asn_length(&p,a_len-1,&vlen) == 0)
    return 0;

   p += vlen;
  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_algorithm_identifier(BYTE ** a_pp,int a_len,int * a_oi,int a_isPubKey)
{
  BYTE *  p;
  BYTE *  end;
  int     arcLen,llen;

  p = *a_pp;
  end = p + a_len;

  if (a_len < 1 || x509_get_sequence(&p,a_len,&llen) == 0)
    return 0;

  if (end - p < 1)
    return 0;

  if (*(p++) != K_AsnOId || x509_get_asn_length(&p,(int)(end - p),&arcLen) == 0 ||
      llen < arcLen)
    return 0;
  /*
    List of expected (currently supported) OIDs
    algorithm               summed  length  hex
    s_x509_sha1             88      05      2b0e03021a
    md2                     646     08      2a864886f70d0202
    s_x509_md5              649     08      2a864886f70d0205
    rsaEncryption           645     09      2a864886f70d010101
    md2WithRSAEncryption    646     09      2a864886f70d010102
    md5WithRSAEncryption    648     09      2a864886f70d010104
    sha-1WithRSAEncryption  649     09      2a864886f70d010105

    Yes, the summing isn't ideal (as can be seen with the duplicate 649),
    but the specific implementation makes this ok.
  */
  if (end - p < 2)
    return 0;

  if (a_isPubKey && (*p != 0x2a) && (*(p + 1) != 0x86))
    /*
      Expecting DSA here if not RSA, but OID doesn't always match
    */
    return 0;

  *a_oi = 0;
  while (arcLen-- > 0)
    *a_oi += (int)*p++;
  /*
    Each of these cases should have a trailing NULL parameter.  Skip it
  */
  if (end - p < 2)
    return 0;
  if (*p != K_AsnNull)
    return 0;
  *a_pp = p + 2;
  return -1;
}

/*---------------------------------------------------------------------*/
static void x509_generalize_time(BYTE a_timeFormat, int a_timeLen, BYTE* a_inTime, BYTE* a_outTime)
{
  if (a_timeFormat == K_AsnUtcTime)
  {
    if (a_inTime[0] >= '5')
    {
      a_outTime[0] = '1';
      a_outTime[1] = '9';
    }
    else
    {
      a_outTime[0] = '2';
      a_outTime[1] = '0';
    }
    memcpy(&a_outTime[2], a_inTime, a_timeLen);
    a_outTime[a_timeLen+1] = 0;
  }
  else
  {
    memcpy(a_outTime, a_inTime, a_timeLen);
    a_outTime[a_timeLen-1] = 0;
  }
}

/*---------------------------------------------------------------------*/
static int x509_get_validity(BYTE ** a_pp,int a_len)
{
  BYTE *  p;
  BYTE *  end;
  int     seqLen,timeLen;
  BYTE    timeFormat;
  BYTE    currTime[15];
  BYTE    notBefore[15];
  BYTE    notAfter[15];

  p = *a_pp;
  end = p + a_len;
  if (a_len<1 || *(p++)!=(K_AsnSequence|K_AsnConstructed) ||
      x509_get_asn_length(&p,a_len-1,&seqLen)==0 || (end-p)<seqLen)
    return 0;
/*
  Have notBefore and notAfter times in UTCTime or GeneralizedTime formats
*/
  if ((end-p)<1 || ((*p!=K_AsnUtcTime) && (*p!=K_AsnGeneralizedTime)))
    return 0;

  timeFormat = *p;

  p++;
/*
  Allocate them as null terminated strings
*/
  if (x509_get_asn_length(&p,seqLen,&timeLen)==0 || (end-p)<timeLen)
    return 0;

  os_rtc_get(currTime);
  currTime[14] = 0;
  x509_generalize_time(timeFormat, timeLen, p, notBefore);
  if (memcmp(currTime, notBefore, 14) < 0)
    return 0;

  p = p + timeLen;
  if ((end-p)<1 || ((*p!=K_AsnUtcTime) && (*p!=K_AsnGeneralizedTime)))
    return 0;

  timeFormat = *p;

  p++;
  if (x509_get_asn_length(&p,seqLen-timeLen,&timeLen)==0 || (end-p)<timeLen)
    return 0;

  x509_generalize_time(timeFormat, timeLen, p, notAfter);
  if (memcmp(currTime, notAfter, 14) > 0)
    return 0;

  p = p + timeLen;
  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_big(BYTE ** a_pp,int a_len,BYTE * a_big,DWORD * a_keylen)
{
  BYTE *  p;
  int     vlen;

  p = *a_pp;
  if (a_len < 1 || *(p++) != K_AsnInteger ||
      x509_get_asn_length(&p,a_len-1,&vlen) == 0)
    return 0;

  if (vlen >= *a_keylen)
    return 0;

  if (*p == 0)
  {
    p++;
    vlen--;
  }

  *a_keylen = vlen;
  memcpy(a_big,(void*)p,vlen);

  p += vlen;
  *a_pp = p;

  return -1;
}

/*---------------------------------------------------------------------*/
static int x509_get_pubkey(BYTE** a_pp,int a_len,T_KEY * a_pubKey)
{
  BYTE *  p;
  BYTE    value[4];
  int     pubKeyLen,ignore_bits,seqLen;
  DWORD   keyLen,expLen,count;

  p = *a_pp;
  if (a_len<1 || (*(p++)!=K_AsnBitString) ||
      x509_get_asn_length(&p,a_len-1,&pubKeyLen)==0 || (a_len-1)<pubKeyLen)
    return 0;

  ignore_bits = *p++;
/*
  We assume this is always zero
*/
  if (ignore_bits != 0)
    return 0;

  expLen = 4;
  keyLen = 258;
  if (x509_get_sequence(&p,pubKeyLen,&seqLen) == 0 ||
      x509_get_big(&p,seqLen,(BYTE*)a_pubKey->s_key,&keyLen) == 0 ||
      x509_get_big(&p,seqLen,(BYTE*)value,&expLen) == 0)
    return 0;

#ifdef _PCI_  /* Accept only 2048 bits key or above */
  if (keyLen < 256)
    return 0;
#endif

  a_pubKey->d_keysize  = keyLen;
  a_pubKey->d_exponent = 0;
  for (count=0;count<expLen;count++)
    a_pubKey->d_exponent = (a_pubKey->d_exponent<<8) | value[count];

  *a_pp = p;
  return -1;
}

/*---------------------------------------------------------------------*/
DWORD x509_parse_cert(BYTE * a_ptr,DWORD a_size,T_KEY * a_pubKey)
{
  BYTE *p;
  BYTE *end;
  int  len;
  int  certVersion,certAlgorithm,pubKeyAlgorithm;

  p   = a_ptr;
  len = 0;
  end = p + a_size;

/*
  Certificate ::= SEQUENCE  {
                    tbsCertificate      TBSCertificate,
                    signatureAlgorithm  AlgorithmIdentifier,
                    signatureValue      BIT STRING  }
*/
  if (x509_get_sequence(&p,a_size,&len) == 0)
    return K_X509ErrCertSequence;

/*
  TBSCertificate ::=  SEQUENCE  {
      version[0] EXPLICIT Version DEFAULT v1,
      serialNumber CertificateSerialNumber,
      signature AlgorithmIdentifier,
      issuer Name,
      validity Validity,
      subject Name,
      subjectPublicKeyInfo SubjectPublicKeyInfo,
      issuerUniqueID[1] IMPLICIT UniqueIdentifier OPTIONAL,
        -- If present, version shall be v2 or v3
      subjectUniqueID[2] IMPLICIT UniqueIdentifier OPTIONAL,
        -- If present, version shall be v2 or v3
      extensions[3] EXPLICIT Extensions OPTIONAL
        -- If present, version shall be v3  }
*/
  if (x509_get_sequence(&p,(DWORD)(end - p),&len) == 0)
    return K_X509ErrMainSequence;

/*
  Version ::= INTEGER  {  v1(0), v2(1), v3(2)  }
*/
  if (x509_get_explicit_version(&p,(int)(end - p),0,&certVersion) == 0)
    return K_X509ErrExplicit;

/*
  CertificateSerialNumber ::= INTEGER
*/
  if (x509_get_SerialNum(&p,(int)(end - p)) == 0)
    return K_X509ErrSerialNum;

/*
  AlgorithmIdentifier ::= SEQUENCE  {
                  algorithm   OBJECT IDENTIFIER,
                  parameters  ANY DEFINED BY algorithm OPTIONAL }
*/
  if (x509_get_algorithm_identifier(&p,(int)(end - p),&certAlgorithm,0) == 0)
    return K_X509ErrCertAlg;

/*
  Name ::= CHOICE {
    RDNSequence }

    RDNSequence ::= SEQUENCE OF RelativeDistinguishedName

    RelativeDistinguishedName ::= SET OF AttributeTypeAndValue

    AttributeTypeAndValue ::= SEQUENCE {
      type  AttributeType,
      value AttributeValue }

    AttributeType ::= OBJECT IDENTIFIER

    AttributeValue ::= ANY DEFINED BY AttributeType
*/
  if (x509_get_dn_attrib(&p,(int)(end - p)) == 0)
    return K_X509ErrDnAttrib;

/*
  Validity ::= SEQUENCE {
    notBefore Time,
    notAfter  Time  }
*/
  if (x509_get_validity(&p,(int)(end - p)) == 0)
    return K_X509ErrValId;

/*
  Subject DN
*/
  if (x509_get_dn_attrib(&p,(int)(end - p)) == 0)
    return K_X509ErrSubDn;

/*
  SubjectPublicKeyInfo  ::=  SEQUENCE  {
    algorithm   AlgorithmIdentifier,
    subjectPublicKey  BIT STRING  }
*/
  if (x509_get_sequence(&p,(int)(end - p),&len) == 0)
    return K_X509ErrPubSeq;

  if (x509_get_algorithm_identifier(&p,(int)(end - p),&pubKeyAlgorithm,1) == 0)
    return K_X509ErrPubAlg;

  if (x509_get_pubkey(&p,(int)(end - p),a_pubKey) == 0)
    return K_X509ErrPubKey;

  /* End here as we only need to get the CA public key */
  return K_X509Ok;
}
