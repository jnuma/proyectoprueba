//-----------------------------------------------------------------------------
//  File          : tranutil.c
//  Module        :
//  Description   : Utilities function for transactions.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  10 May  2006  Lewis       Initial Version for new CONTROL
//  15 Jan  2009  Lewis       Bug Fixed.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "corevar.h"
#include "chkoptn.h"
#include "constant.h"
#include "input.h"
#include "message.h"
#include "print.h"
#include "util.h"
#include "record.h"
#include "ecr.h"
#include "lptutil.h"
#include "tranutil.h"
#include "menu.h"
#include "files.h"
#include "Sysutil.h"
#include "keytrans.h"
#include "key2dll.h"
#include "cajas.h"
#include "ctltrans.h"
#include "toggle.h"		//NM-22/04/13
#include "IpConfig.h"		//NM-22/04/13
#include "settle.h"		//NM-22/04/13


void displayAuthCode(void);

//-----------------------------------------------------------------------------
//      Globals Variables
//-----------------------------------------------------------------------------
BYTE gbCommMode;

//---------------------------------------------
//static const struct MENU_ITEM KCommItems[] =
//{
//		{ 0, "TCP               " },
//		{ 1, "DIAL              " },
//		{ -1, NULL }, };
//
//static const struct MENU_DAT KCommMenu =
//{ "Comunicacion", KCommItems, };

struct TRANS_DATA gGTS;

//-----------------------------------------------------------------------------
//      Constant
//-----------------------------------------------------------------------------
const BYTE KAuthKeydel[16] = {
		0x72,0x85,0xF5,0x2B,0xB5,0xFB,0x97,0xA3,0xCF,0x3E,0x74,0x13,0x3A,0x5E,0x0F,0x23	//KEY CREDIBANCO
};


#define TYPE_VISA1       0x00
#define TYPE_SDLC        0x01
#define TYPE_ASYN        0x02
#define TYPE_VOICE       0x03

#define MODE_BELL        0x00
#define MODE_CCITT       0x01

#define BPS_300          0x00
#define BPS_1200         0x01
#define BPS_2400         0x02
#define BPS_9600         0x03
#define BPS_AUTO         0x04

static const struct
{
	BYTE b_protocol;
	BYTE b_mode;
	BYTE b_speed;
} KCommParam[8] =
{
		{ TYPE_SDLC, MODE_BELL, BPS_1200 },
		{ TYPE_ASYN, MODE_BELL, BPS_1200 },
		{ TYPE_SDLC, MODE_CCITT, BPS_1200 },
		{ TYPE_ASYN, MODE_CCITT, BPS_1200 },
		{ TYPE_ASYN, MODE_BELL, BPS_300 },
		{ TYPE_ASYN, MODE_CCITT, BPS_300 },
		{ TYPE_SDLC, MODE_CCITT, BPS_2400 },
		{ TYPE_ASYN, MODE_CCITT, BPS_2400 } };

static const struct MDMSREG KDefaultSReg =
{ sizeof(struct MW_MDM_PORT), // len
		{ 0x01, // Protocol = SDLC
				0x01, // Modem Type = CCITT
				0x01, // Speed = 1200
				12, // Inactivity Timeout in unit of 2.5 sec, total 30sec
				15, // Redial Pause time in 100 msec, total 1.5secs */
				0x00, // Primary Tel Len
				{ 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F',
						'F',
						'F', // Primary Phone #
						'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F',
						'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F',
						'F', 'F' }, 0x03, // Primary Dial Attempts
						20, // Primary Connection Time
						0x00, // Secondary Tel Len
						{ 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F',
								'F',
								'F', // Secondary Phone #
								'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F',
								'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F',
								'F', 'F' }, 0x03, // Secondary Dial Attempts
								20, // Secondary Connection Time
								3, // CD on qualify time
								4, // CD off qualify time
								10, // Async Message qualify time in 10msec, total 100ms
								15, // Wait Online time in sec, total 15 secs */
								10, // (DTMF) tone duration in 10msec, total 100ms */
								5, // hold line time in minutes */
								MW_SHORT_DIALTONE, // busy tone + check line + blink dialing + short dial tone */
		} };



int gPPPHandle = -1;	//NM-22/04/13 Agregue desde aca hasta la linea 197
struct MW_PPP_DATA *gPPPData;

static const char * const KPPPState[] = {
		"      Dead      ",
		"  Establishing  ",
		" Authenticating ",
		"    Network     ",
		"  Terminating   ",
		"   PPP Ready    "
};


static const struct MW_MDM_PORT KLineDialParam = {
		2,                      /* Protocol: ASYNC */
		1,                      /* CCITT */
		5,                      /* auto speed with V.42 */
		72,                     /* Drop after idle, unit in 2.5s, 3 min */
		15,                     /* Pause between re-dial, unit in 100ms */
		0,                      /* Pri-phone number length */
		{'F'},
		3,                      /* Pri-phone# Dial attempts */
		40,                     /* Pri-phone# Time to wait CD, unit in 2s */
		0,                      /* Sec-phone number length */
		{'F'},
		3,                      /* Sec-phone# Dial attempts */
		40,                     /* Sec-phone# Time to wait CD, unit in 2s */
		3,                      /* CD on qualify time, unit in 250ms*/
		4,                      /* CD off delay, unit in 250ms */
		5,                      /* Message qualify time, unit in 10ms */
		15,                     /* Wait time for link up after CD, in sec */
		0xff,                   /* DTMF duration, unit in 10ms */
		0,                      /* Hold line after drop, in sec */
		0x00                    /* Line Detection mode */
};


// User Define Dialup Scirpts
static const char *KExpModemOK[] = {
		"OK", "Ok", NULL
};
static const char *KExpModemConnected[] = {
		"CONNECT", NULL
};

static const T_EXPECT_SEND KDefaultDialScript[] = {
		{NULL, 0, 0, (signed char*)"ATZ", NULL, 10, 1, 0},
		{(char **)KExpModemOK, 2, 2, (signed char*)"ATDT\\T", "BUSY", 10, 1, 0},
		{(char **)KExpModemConnected, 7, 7, NULL, "BUSY", 120, 1, 0}
};

//



//*****************************************************************************
//  Function        : OverMargin
//  Description     : Check for over adjustment margin.
//  Input           : N/A
//  Return          : TRUE;     // over limit;
//                    FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN OverMargin(void)
{
	DDWORD amount;

	if (STIS_ACQ_TBL(0).sb_reserved[2] == 0) /* zero for no limit */
		return (FALSE);

	amount = INPUT.dd_amount * bcd2val(STIS_ACQ_TBL(0).sb_reserved[2]);
	amount /= 100;
	return (INPUT.dd_tip > amount);
}
//*****************************************************************************
//  Function        : PackDTGAA
//  Description     : Pack date/time & accept response code.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackDTGAA(void)
{
	ReadRTC(&RSP_DATA.s_dtg);
	RSP_DATA.w_rspcode = 'A' * 256 + 'P';
	memset(RSP_DATA.sb_rrn, ' ', 12);
}
//*****************************************************************************
//  Function        : PackDateTime
//  Description     : Pack DATE/TIME on receipt.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void PackDateTime(struct DATETIME *aDtg)
{
	BYTE tmp[32];

	ConvDateTime(tmp, aDtg, 1);
	pack_mem("Date/Time:", 10);
	pack_space(1);
	pack_mem(tmp, 12);
	pack_space(1);
	if (PrintTimeReqd())
		pack_mem(&tmp[12], 6);
	pack_lf();
}
#endif

void PackDateTimeVisa(struct DATETIME *aDtg)
{
	BYTE tmp[49]; //NM-25/04/13 Estaba "22"
	memset(tmp, 0x00, sizeof(tmp));

	ConvDateTimeVisa(tmp, aDtg);
	pack_mem(tmp, 48); //NM-25/04/13 Estaba "22"
	pack_lf();

}

//*****************************************************************************
//  Function        : PackContent
//  Description     : Pack receipt contents.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
//static void PackContent(void)		//kt-190413
//{
//	DDWORD no_tips = -1;
//	BYTE trans, offset;
//	BYTE tmp[32];
//	struct DISP_AMT disp_amt;
//	struct DESC_TBL prod;
//
//	//PackLogo();             // Header line
//	//pack_lf();
//	PackRcptHeader();
//
//	// Card Name
//	pack_space(9);
//	pack_mem(STIS_ISS_TBL(0).sb_card_name, 10);
//	pack_lf();
//
//	// PAN
//	split(tmp, P_DATA.sb_pan, 10);
//	if (MaskCardNo())
//		memset(&tmp[6], 'X', 6); // mask PAN
//	offset = fndb(tmp, 'F', 19);
//	pack_mem(tmp, offset);
//	pack_byte('(');
//	pack_byte(P_DATA.b_entry_mode);
//	pack_byte(')');
//	pack_lf();
//
//	// Card Holder Name
//	pack_mem(P_DATA.sb_holder_name, 26);
//	pack_lf();
//
//	// Trans Name & Expiry date
//	trans = (P_DATA.b_trans_status & VOIDED) ? VOID : P_DATA.b_trans;
//	memcpy(tmp, GetConstMsg(KTransHeader[trans]), 16);
//	// locate start pos of English transaction name
//	for (offset = 4; offset < 13; offset++)
//	{
//		if (tmp[offset] != ' ')
//			break;
//	}
//	pack_mem((unsigned char *) tmp, 4);
//	pack_mem((unsigned char *) &tmp[offset], (BYTE) (16 - offset));
//	if (P_DATA.sb_exp_date[0] != 0)
//	{
//		pack_space(MAX_CHAR_LINE_NORMAL - 29 + offset);
//		pack_mem("Exp:", 4);
//		split_data((BYTE *) &P_DATA.sb_exp_date[1], 1);
//		pack_byte('/');
//		split_data((BYTE *) &P_DATA.sb_exp_date[0], 1);
//	}
//	pack_lf();
//
//	// Batch & Trace
//	pack_mem("Batch:", 6);
//	split_data(STIS_ACQ_TBL(0).sb_curr_batch_no, 3);
//	pack_space(MAX_CHAR_LINE_NORMAL - 24);
//	pack_mem("Trace:", 6);
//	split_data(P_DATA.sb_trace_no, 3);
//	pack_lf();
//	PackDateTime(&P_DATA.s_dtg);
//
//	// RRN & APPV CODE
//	pack_mem("RRN:", 4);
//	pack_mem(P_DATA.sb_rrn, 12);
//	pack_space(MAX_CHAR_LINE_NORMAL - 27);
//	pack_mem("APPV:", 5);
//	pack_mem(P_DATA.sb_auth_code, 6);
//
//	// Product descriptor
//	offset = 0;
//	while ((offset < APM_GetDescCount() ) && (P_DATA.sb_product[offset] != 0xFF))
//	{
//		APM_GetDescTbl(P_DATA.sb_product[offset], &prod);
//		pack_lf();
//		pack_mem((BYTE *) &prod.sb_text, 20);
//		offset += 1;
//	}
//	for (; offset < 4; offset++)
//		pack_lf();
//
//	// Amount
//	if (P_DATA.dd_tip != no_tips)
//	{
//		pack_mem("   BASE", 7);
//		pack_space(4);
//		PackAmt(P_DATA.dd_base_amount, MAX_CHAR_LINE_NORMAL - 11);
//		pack_lf();
//		pack_mem("    TIP", 7);
//		if (P_DATA.dd_tip)
//		{
//			pack_space(4);
//			PackAmt(P_DATA.dd_tip, MAX_CHAR_LINE_NORMAL - 11);
//		}
//		else
//		{
//			pack_space(4);
//			pack_mem(STIS_TERM_CFG.sb_currency_name, 3);
//		}
//		pack_lf();
//		pack_space(MAX_CHAR_LINE_NORMAL - 13);
//		pack_nbyte('-', 13);
//		pack_lf();
//	}
//
//	pack_mem("TOTAL", 5);
//	pack_space(MAX_CHAR_LINE_NORMAL - 30);
//	if ((P_DATA.dd_tip == 0) && ((P_DATA.b_trans_status & VOIDED) == 0))
//	{
//		pack_mem(STIS_TERM_CFG.sb_currency_name, 3);
//	}
//	else
//	{
//		ConvAmount(P_DATA.dd_amount, &disp_amt, STIS_TERM_CFG.b_decimal_pos,
//				STIS_TERM_CFG.b_currency);
//		pack_space((BYTE) (22 - disp_amt.len));
//		pack_mem(STIS_TERM_CFG.sb_currency_name, 3);
//		pack_mem(disp_amt.content, disp_amt.len);
//	}
//	pack_lf();
//	pack_space(MAX_CHAR_LINE_NORMAL - 19);
//	pack_nbyte('=', 19);
//	pack_lf();
//
//	// EMV Trans data
//	if (P_DATA.b_entry_mode == ICC)
//	{
//		pack_mem("AID    : ", 9);
//		split_data(P_DATA.s_icc_data.sb_aid, P_DATA.s_icc_data.b_aid_len); // !2009-01-15
//		pack_lf();
//	}
//	pack_lfs(2);
//
//#if (TMLPT_SUPPORT)
//	if ((RSP_DATA.w_rspcode == 'V'*256+'S')||(P_DATA.b_entry_mode!=ICC))
//	{
//		pack_lfs(4);
//		pack_nbyte('_', MAX_CHAR_LINE_NORMAL-2);
//		pack_lf();
//		pack_mem("X CardHolder Signature", 22);
//	}
//#endif
//}
//*****************************************************************************
//  Function        : PackTxnSlip
//  Description     : Ready transaction receipts.
//  Input           : aReprint;     // TRUE => pack Reprint Line
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static void PackTxnSlip(BOOLEAN aReprint, BOOLEAN aDuplicado) //kt-021012
{

	/*if (!ImprimaRecibo())			//kt-101012 revisar con Oscar
	 return;*/
	//printf("\fe debug uno"); WaitKey(3000, 0); //debugmanuel
	MsgBufSetup();

	//switch (TX_DATA.b_trans)

	PackVoucher(aReprint, aDuplicado);

	//	//PackFF();
	PackMsgBufLen(); //MFBC/21/02/13 se comento por que no se esta usando
	//	if (aReprint == FALSE && TX_DATA.b_trans != SALDO_MULTIBOL && TX_DATA.b_trans != SALDO
	//			&& TX_DATA.b_trans != CHEQUES_AL_DIA && TX_DATA.b_trans != CHEQUES_POSTFECHADOS )
	//		SaveLastRcpt(MSG_BUF.sb_content, MSG_BUF.d_len);

}
//*****************************************************************************
//  Function        : PackRecordP
//  Description     : Pack data from RECORD_BUF to p_data & ready receipt.
//  Input           : aReprint;     // TRUE=>for reprint.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : p_data;
//*****************************************************************************
void PackRecordP(BOOLEAN aReprint, BOOLEAN aDuplicado)
{
	//DDWORD no_tips = -1;

	ByteCopy((BYTE *) &P_DATA, (BYTE *) &RECORD_BUF, (BYTE *) &P_DATA.b_type
			- (BYTE *) &P_DATA.b_trans);
	ByteCopy((BYTE *) &P_DATA.s_icc_data, (BYTE *) &RECORD_BUF.s_icc_data,
			sizeof(struct ICC_DATA));

	P_DATA.dd_base_amount = P_DATA.dd_amount;

	RSP_DATA.w_rspcode = 'A' * 256 + 'P';
	// !2007-05-23++
	P_DATA.w_rspcode = RECORD_BUF.w_rspcode;
	P_DATA.b_PinVerified = RECORD_BUF.b_PinVerified;
	// !2007-05-23--


	PackTxnSlip(aReprint, aDuplicado);
}
//*****************************************************************************
//  Function        : PackInputP
//  Description     : Pack data from input to p_data & ready receipt.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : p_data;
//*****************************************************************************
void PackInputP(void)
{
	//DDWORD no_tips = -1;
	//printf("\f entro segunda debug");WaitKey(3000, 0);
	ByteCopy((BYTE *) &P_DATA.b_trans, (BYTE *) &INPUT.b_trans, (DWORD)((DWORD)
			& P_DATA.b_type - (DWORD) & P_DATA.b_trans));
	ByteCopy((BYTE *) &P_DATA.s_icc_data, (BYTE *) &INPUT.s_icc_data,
			sizeof(struct ICC_DATA));
	ByteCopy((BYTE *) &P_DATA.s_dtg, (BYTE *) &RSP_DATA.s_dtg,
			sizeof(struct DATETIME));
	memcpy(P_DATA.sb_rrn, &RSP_DATA.sb_rrn, sizeof(P_DATA.sb_rrn));
	memcpy(P_DATA.sb_auth_code, &RSP_DATA.sb_auth_code,
			sizeof(P_DATA.sb_auth_code));
	P_DATA.w_rspcode = RSP_DATA.w_rspcode;
	P_DATA.b_PinVerified = RSP_DATA.b_PinVerified;
	P_DATA.b_type = CUS_SLIP;
	P_DATA.dd_base_amount = INPUT.dd_amount; /* base amount */
	P_DATA.dd_tip = INPUT.dd_tip;

	PackTxnSlip(FALSE, FALSE);
}
//*****************************************************************************
//  Function        : DispHeader
//  Description     : Display transaction header.
//  Input           : aCardName;  // pointer to card name
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispHeader(BYTE *aCardName)
{
	BYTE tmpbuf[MW_MAX_LINESIZE + 1];

	memset(tmpbuf, 0x20, sizeof(tmpbuf));

	ClearDispMW();
	DispPutCMW(K_PushCursor); // MFBC 19/10/2012
	os_disp_backc(COLOR_VISAYELLOW);

	DispLineMW(GetConstMsg(KTransHeader[INPUT.b_trans]), MW_LINE1, MW_REVERSE
			| MW_CENTER | MW_SMFONT);
	//DispLineMW( tmpbuf, MW_LINE1, MW_REVERSE | MW_CENTER | MW_SMFONT);

	DispPutCMW(K_PopCursor);

	if (aCardName != NULL)
	{
		memset(tmpbuf, 0x00, sizeof(tmpbuf));
		memcpy(tmpbuf, aCardName, 10);
		DispLineMW(tmpbuf, MW_LINE3, MW_SMFONT);
	}
}


//*****************************************************************************
//  Function        : DispHeaderTrans
//  Description     : Mustra el tipo de transaccion a realizar.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispHeaderTrans(void)
{
	BYTE tmpbuf[MW_MAX_LINESIZE + 1];

	memset(tmpbuf, 0x20, sizeof(tmpbuf));

	ClearDispMW();
	DispPutCMW(K_PushCursor); // MFBC 19/10/2012
	os_disp_backc(COLOR_VISAYELLOW);
	DispLineMW(GetConstMsg(KTransHeader[INPUT.b_trans]), MW_LINE1, MW_REVERSE
			| MW_CENTER | MW_SMFONT);

	DispPutCMW(K_PopCursor);

}
//*****************************************************************************
//  Function        : SetRspCode
//  Description     : Update the rspdata.w_rspcode with INPUT.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : hst_rsp_data;
//*****************************************************************************
void SetRspCode(WORD aRspCode)
{
	RSP_DATA.w_rspcode = aRspCode;
}
//*****************************************************************************
//  Function        : ClearResponse
//  Description     : Clear host gGTS.b_response data.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : hst_rsp_data;
//*****************************************************************************
void ClearResponse(void)
{
	RSP_DATA.text[0] = 0;
	memset(&RSP_DATA.text[1], ' ', 69);
	memset(RSP_DATA.sb_rrn, ' ', 12);
	RSP_DATA.b_response = TRANS_FAIL;
	RSP_DATA.w_rspcode = 'N' * 256 + 'C';
}
//*****************************************************************************
//  Function        : ConfirmCard
//  Description     : Prompt user to comfirm card data.
//  Input           : N/A
//  Return          : TRUE;   // confirmed
//                    FALSE;  // CANCEL
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ConfirmCard(void)
{
	BYTE buffer[4];
	BYTE tmpbuf[MW_MAX_LINESIZE + 1];
	DWORD i, keyin;
	if ((INPUT.b_entry_mode == MANUAL) /*|| !DispMSRReqd()*/)
		return TRUE;

	DispClrBelowMW(MW_LINE3);
	memset(tmpbuf, 0, sizeof(tmpbuf));
	memcpy(tmpbuf, STIS_ISS_TBL(0).sb_card_name, 10);
	DispLineMW(tmpbuf, MW_LINE4, MW_SMFONT);

	split(&tmpbuf[1], INPUT.sb_pan, 10);
	tmpbuf[0] = (BYTE) fndb(&tmpbuf[1], 'F', 19);
	tmpbuf[1 + tmpbuf[0]] = 0;
	DispLineMW(&tmpbuf[1], MW_LINE5, MW_SPFONT);

	if (ExpDateReqd())
	{
		split(buffer, INPUT.sb_exp_date, 2);
		sprintf(tmpbuf, "EXP DATE : %c%c/%c%c", buffer[2], buffer[3],
				buffer[0], buffer[1]);
		DispLineMW(tmpbuf, MW_LINE6, MW_SMFONT);
	}

	for (i = 20; i; i--)
	{
		if (INPUT.sb_holder_name[i] != ' ')
			break;
	}
	memset(tmpbuf, 0, sizeof(tmpbuf));
	memcpy(tmpbuf, INPUT.sb_holder_name, (BYTE)(i + 1));
	DispLineMW(tmpbuf, MW_LINE7, MW_BIGFONT);

	do
	{
		keyin = APM_WaitKey(KBD_TIMEOUT, 0);
		if (keyin == MWKEY_CANCL)
			return (FALSE);
	} while (keyin != MWKEY_ENTER);

	return (TRUE);
}
//*****************************************************************************
//  Function        : TransAllowed
//  Description     : Check Whether Transaction is allowed
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
BOOLEAN TransAllowed(DWORD aInputMode)
{
	BOOLEAN tips_req;

	if (STIS_TERM_DATA.b_stis_mode < TRANS_MODE)
	{
		DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
		return FALSE;
	}

	switch (INPUT.b_trans)
	{
	case AUTH_SWIPE:
	case AUTH_MANUAL:
		if (AuthBlocked())
		{
			DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
			return FALSE;
		}
		break;
	case OFFLINE:
		if (!OfflineAllowed())
		{
			DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
			return FALSE;
		}
		break;
	case REFUND:
		if (RefundBlocked())
		{
			DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
			return FALSE;
		}
		break;
	}

	if (BlockLocalTrans())
	{
		DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
		return FALSE;
	}
	if ((aInputMode == MANUAL) && (BlockLocalManual() || !ManualEntryAllow()))
	{
		DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
		return FALSE;
	}
	if ((aInputMode == SWIPE) && BlockLocalSwipe())
	{
		DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
		return FALSE;
	}
	if (HostSettlePending() || APM_BatchFull(INPUT.w_host_idx))
	{
		DispErrorMsg(GetConstMsg(EDC_TU_CLOSE_BATCH));
		return FALSE;
	}

	if (INPUT.b_trans != SALE_OFFLINE)
	{
		PackComm(INPUT.w_host_idx, FALSE);
		APM_PreConnect();
	}

	while (TRUE)
	{
		if (INPUT.b_entry_mode != MANUAL)
			Short1Beep();

		if (!ConfirmCard())
			break;

		if (!GetExpDate())
			break;

		//if ((INPUT.b_entry_mode == MANUAL) && ManualPSWReqd()) {
		//  if (A21_PSWGet(NULL, P_MANUAL) != PASSWORD_OK)
		//    break;
		//  DispHeader(NULL);
		//}

		if (INPUT.b_trans == REFUND)
			tips_req = 0;
		else
			tips_req = TIPsReqd();

		if (!GetAmount(tips_req))
			break;

		if (INPUT.b_trans != AUTH_SWIPE)
		{
			if (!GetProductCode())
				break;
			DispHeader(NULL);
		}
		else
			memset(INPUT.sb_product, 0xFF, sizeof(INPUT.sb_product));

		if ((Prompt4DBC()) && (INPUT.b_trans != SALE_OFFLINE))
		{
			if (!Get4DBC())
				break;
		}
		else
			memset(INPUT.sb_amex_4DBC, 0x00, sizeof(INPUT.sb_amex_4DBC));

		GetEcrRef();

		DispHeader(NULL);
		return TRUE;
	}

	APM_ResetComm();
	return FALSE;
}

#endif

//*****************************************************************************
//  Function        : PackRspText
//  Description     : Update response text base on rspcode.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackRspText(void)
{
	BYTE var_i;
	BYTE *pMsg;

	if (RSP_DATA.text[0] != 0)
		return;

	var_i = 0;
	while ((KRspText[var_i].w_idx != RSP_DATA.w_rspcode)
			&& (KRspText[var_i].w_idx != '*' * 256 + '*'))
		var_i += 1;

	memset(&RSP_DATA.text[1], ' ', 40);

	pMsg = GetConstMsg(KRspText[var_i].d_text_id);
	memcpy(&RSP_DATA.text[1], pMsg, strlen(pMsg)); //kt-111012
	//memcpy(&RSP_DATA.text[21], &pMsg[16], 16);
	if (RSP_DATA.w_rspcode == 'A' * 256 + 'P' || RSP_DATA.w_rspcode == 'T' * 256 + 'A') //kt-111012
		memcpy(&RSP_DATA.text[14], RSP_DATA.sb_auth_code, 6); //kt-111012
	else if (RSP_DATA.w_rspcode == '0' * 256 + '2')
		memcpy(&RSP_DATA.text[22], RSP_DATA.sb_auth_code, 6);

	RSP_DATA.text[0] = 30; //kt-111012
}

//*****************************************************************************
//  Function        : DispRspText
//  Description     : Display Response text.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispRspText(BOOLEAN aDispHdr)
{
	BYTE tmpbuf[MW_MAX_LINESIZE + 1];
	PackRspText();
	DispClrBelowMW(MW_LINE2);
	if (aDispHdr)
		DispHeader(STIS_ISS_TBL(0).sb_card_name);
	memset(tmpbuf, 0, sizeof(tmpbuf));
	memcpy(tmpbuf, &RSP_DATA.text[1], 30); //kt-111012
	DispLineMW(tmpbuf, MW_LINE3, MW_CENTER | MW_SMFONT);
	//memcpy(tmpbuf, &RSP_DATA.text[21], 6);
	//DispLineMW(tmpbuf, MW_LINE6, MW_CENTER | MW_SMFONT);
}

void DispRspText2(BOOLEAN aDispHdr)
{
	BYTE tmpbuf[MW_MAX_LINESIZE + 1];
	PackRspText();

	DispClrBelowMW(MW_LINE2);
	memset(tmpbuf, 0, sizeof(tmpbuf));
	memcpy(tmpbuf, &RSP_DATA.text[1], 30); //kt-111012

	if (RSP_DATA.w_rspcode == 'J' * 256 + 'E')
	{
		Short1Beep();
		TextColor("INSERTE / DESLICE", MW_LINE4, COLOR_VISABLUE, MW_CLRDISP
				| MW_CENTER | MW_SMFONT, 0);
		TextColor("TARJETA", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT,
				3);
	}
	else if (RSP_DATA.w_rspcode == 'C' * 256 + 'P')
	{
		ClearDispMW();
		return;
	}
	else
	{
		DispLineMW(tmpbuf, MW_LINE3, MW_CENTER | MW_BIGFONT);
	}
	//memcpy(tmpbuf, &RSP_DATA.text[21], 6);
	//DispLineMW(tmpbuf, MW_LINE6, MW_CENTER | MW_SMFONT);
}
//*****************************************************************************
//  Function        : DispErrorMsg
//  Description     : Display Error Message on MW_LLINE5 and pause 3 sec.
//  Input           : aMsg;     // pointer to constant message.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispErrorMsg(const BYTE *aMsg)
{
	//DispClrBelowMW(MW_LINE1);
	Disp2x16Msg(aMsg, MW_LINE5, MW_BIGFONT);
	Delay1Sec(2, 1);
}
//*****************************************************************************
//  Function        : TransEnd
//  Description     : Transaction end process.
//  Input           : aShowCard;  // Show card name when TRUE
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void TransEnd(BOOLEAN aShowCard) //kt-170413
{
	BYTE tmpbuf[31];
	int respuesta =  RSP_DATA.b_response;
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	memset(tmpbuf, 0x00, sizeof(tmpbuf));

	if (RSP_DATA.b_response != TRANS_ACP) //MFBC/12/06/13
		LongBeep();

	//APM_ResetComm();

	if (TX_DATA.b_trans != INIT && MSG_BUF.d_len == 0 && TX_DATA.b_trans  //MFBC/20/05/13  se cambiaron INPUT por TX_DATA
			!= TEST_MSG && TX_DATA.b_trans != LOGON  && TX_DATA.b_trans != CONSULTA_COSTO)//&& RSP_DATA.b_response != TRANS_ACP ) //MFBC/17/05/13  agregue la validacion de "aShowCard"
	{
		/*	if (INPUT.b_trans != INIT && MSG_BUF.d_len == 0 )//&& RSP_DATA.b_response != TRANS_ACP )
	{*/
		if (TX_DATA.b_trans != SETTLEMENT)
		{

			DispRspText2(FALSE);
			APM_WaitKey(100, 0);
		}


		INPUT.b_flag_Consulta_Costo = FALSE; //MFBC/24/04/13
		EcrResp();
		APM_ClearKeyin(); // flush key to avoid display refresh caused by key event
		MsgBufSetup();
		PackMsgBufLen();
		fCommitAllMW();
		APM_ResetComm();//MFBC/02/05/13
		RefreshDispAfter(0);
		return;
	}

	if (TX_DATA.b_trans == SALE_CTL && CTL_PRINT == FALSE)
	{
		MSG_BUF.d_len = 0;
	}

	if (aShowCard)
	{
		// if (RSP_DATA.w_rspcode == '0' * 256 + '0' || RSP_DATA.w_rspcode == 'A' * 256 + 'P' || RSP_DATA.w_rspcode == 'T' * 256 + 'A' || RSP_DATA.w_rspcode == 'V' * 256 + 'S') //kt-111012 TA es para EMV
		if (MSG_BUF.d_len != 0)
		{
			if (RSP_DATA.b_response == TRANS_ACP) //MFBC/12/06/13  baje esta validacion
			{

				AcceptBeep();
				memcpy( ((struct APPDATA*) conf_AppData)->RocNo,  &P_DATA.sb_roc_no[0], 3);
				SaveDataFile((struct APPDATA*) conf_AppData);

			}

#if (LPT_SUPPORT|TMLPT_SUPPORT)    // Conditional Compile for Printer support
			if( TX_DATA.b_trans != TEST_MSG
					&& TX_DATA.b_trans != LOGON  && TX_DATA.b_trans != CONSULTA_COSTO ) //MFBC/16/05/13 agregue consulta recarga
			{
				DispRspText2(FALSE);
			}
			PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
			if (TX_DATA.b_trans != INIT) //MFBC/30/01/13
			{
				DispHeaderTrans();
				DispRspText2(FALSE);
				APM_WaitKey(200,0);
			}


			if ( respuesta != TRANS_REJ)
			{
				DispClrBelowMW( MW_LINE5 );// JANM 01-02-13
				//DispLineMW("GENERAR COPIA ?", MW_LINE7, MW_CENTER|MW_SMFONT);
				//DispLineMW("Si=ENTER    No=CNCL", MW_LINE9, MW_CENTER|MW_SMFONT|MW_REVERSE);
				TextColor("GENERAR COPIA ?", MW_LINE7, COLOR_VISABLUE,
						MW_SMFONT | MW_CENTER, 0);
				displaySI_NO_2();
				if(APM_YesNo() == 2)
				{
					PackTxnSlip(TRUE, FALSE);
					PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
				}
			}
#endif                    // PRINTER_SUPPORT
		}
	}
	//	if (INPUT.b_trans != INIT && INPUT.b_trans != LOGON && INPUT.b_trans != TEST_MSG )		//kt-301012
	/*if (TX_DATA.b_trans != SALE_ICC)
	 {
	 memcpy(((struct APPDATA*) conf_AppData)->RocNo, TX_DATA.sb_roc_no, 3);
	 SaveDataFile((struct APPDATA*) conf_AppData);
	 }*/

	//FreeMW(conf_AppData);
	INPUT.b_flag_Consulta_Costo = FALSE; //MFBC/24/04/13
	EcrResp();
	APM_ClearKeyin(); // flush key to avoid display refresh caused by key event
	MsgBufSetup();
	PackMsgBufLen();
	APM_ResetComm();//MFBC/02/05/13
	fCommitAllMW();
#if (T800)
	os_disp_bl_control(10); // off disp after 10 seconds
#endif
	RefreshDispAfter(0);
}
//*****************************************************************************
//  Function        : DispAmount
//  Description     : Display the formated amount line_nbr. (right justify)
//  Input           : aAmount;    // DDWORD amount
//                    aLineNo;    // aLineNo & 0xF0 is the line number
//                                // to be displayed.
//                                // Bit 0 : 1 - minus sign needed
//                                // Bit 1 : 1 - OVERWRITE
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispAmount(DDWORD aAmount, DWORD aLineNo, DWORD aFont)
{
	struct DISP_AMT disp_amt;
	BOOLEAN minus = aLineNo & 0x0001;
	BOOLEAN clr_eol = aLineNo & 0x0002;
	BYTE tmpbuf[MW_MAX_LINESIZE + 1], *ptr;

	aLineNo &= ~0x0003; // remove ctrl flag.

	ConvAmount(aAmount, &disp_amt, STIS_TERM_CFG.b_decimal_pos,
			STIS_TERM_CFG.b_currency);

	memset(tmpbuf, 0, sizeof(tmpbuf));
	ptr = tmpbuf;
	if (minus)
		*ptr++ = '-';
	memcpy(ptr, disp_amt.content, disp_amt.len);
	if (clr_eol)
		aFont |= MW_CLREOL;
	DispLineMW(tmpbuf, aLineNo, MW_RIGHT | aFont);
}

//*****************************************************************************
//  Function        : DispAmnt
//  Description     : Display the formated amount line_nbr. (right justify)
//  Input           : aAmount;    // DDWORD amount
//                    aLineNo;    // aLineNo & 0xF0 is the line number
//                                // to be displayed.
//                                // Bit 0 : 1 - minus sign needed
//                                // Bit 1 : 1 - OVERWRITE
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispAmnt(DDWORD aAmount, DWORD aLineNo, DWORD aFont)
{
	struct DISP_AMT disp_amt;
	//  BOOLEAN minus   = aLineNo & 0x0001;
	BOOLEAN clr_eol = aLineNo & 0x0002;
	BYTE tmpbuf[MW_MAX_LINESIZE + 1], *ptr;

	aLineNo &= ~0x0003; // remove ctrl flag.

	FormatAmount(aAmount, &disp_amt, bcd2bin(gTablaCero.b_num_decimales), gTablaCero.b_simb_moneda); //kt-110912

	memset(tmpbuf, 0, sizeof(tmpbuf));
	ptr = tmpbuf;

	if (INPUT.b_trans != VOID ) {
		if (flagAnul == TRUE) {
			memcpy(&tmpbuf[0], "-", 1);
			memcpy(&tmpbuf[1], disp_amt.content, disp_amt.len);
		}
		else
			memcpy(ptr, disp_amt.content, disp_amt.len);
	}
	else
		memcpy(ptr, disp_amt.content, disp_amt.len);

	if (clr_eol)
		aFont |= MW_CLREOL;


	DispLineMW(tmpbuf, aLineNo, MW_RIGHT | aFont);
}


//*****************************************************************************
//  Function        : AddVoidedTotals
//  Description     : Add RECORD_BUF.dd_amount & count to global voided_totals.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void AddTotal(struct TOTAL *aTot, DDWORD aAmount)
{
	aTot->dd_amount += aAmount;
	aTot->w_count++;
}



//*****************************************************************************
//  Function        : AddTotals
//  Description     : Add RECORD_BUF amount to term_tot.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void AddToTotals(BOOLEAN print) //LFGD 03/15/2013
{
	if (RECORD_BUF.b_trans_status & ADJUSTED)
		TERM_TOT.d_adjust_count++;

	if (RECORD_BUF.b_trans_status & VOIDED)
	{
		//if (SaleType(RECORD_BUF.b_trans))

		AddTotal(&TERM_TOT.s_void_sale, RECORD_BUF.dd_amount); //kt-domingo
		AddTotal(&TERM_TOT.s_void_iva_tot     , RECORD_BUF.dd_iva     );	// se adiciona para el reporte de Cierre integrado **SR** 03/09/13
		AddTotal(&TERM_TOT.s_void_tips_tot    , RECORD_BUF.dd_tip     );	// se adiciona para el reporte de Cierre integrado **SR** 03/09/13
		AddTotal(&TERM_TOT.s_void_donacion_tot, RECORD_BUF.dd_donacion); // se adiciona para el reporte de Cierre integrado **SR** 03/09/13
		AddTotal(&TERM_TOT.s_void_base_tot    , RECORD_BUF.dd_base    ); // se adiciona para el reporte de Cierre integrado **SR** 03/09/13

		//else
		//AddTotal(&TERM_TOT.s_void_refund, RECORD_BUF.dd_amount);
		//return;
	}
	if (TaxesType(RECORD_BUF.b_trans)) //LFGD 03/13/2013
	{
		//AddTotal(&TERM_TOT.s_imp_tot, RECORD_BUF.dd_amount);
		return;
	}

	if (SaleType(RECORD_BUF.b_trans))
	{

		if (print == TRUE)
		{
			AddTotal(&TERM_TOT.s_sale_tot, RECORD_BUF.dd_net_mount );
		}
		else
			AddTotal(&TERM_TOT.s_sale_tot, RECORD_BUF.dd_amount );
		//AddTotal(&TERM_TOT.s_sale_tot, RECORD_BUF.dd_amount + RECORD_BUF.dd_valor_efectivo);
		//	AddTotal(&TERM_TOT.s_sale_tot, RECORD_BUF.dd_net_mount + RECORD_BUF.dd_IAC);    // Jorge Numa 13-06-13 ++


		if (RECORD_BUF.dd_IAC != 0) //kt-090413
			AddTotal(&TERM_TOT.s_iac_tot, RECORD_BUF.dd_IAC);


		if (RECORD_BUF.b_tipo_cuenta == 0x40) //LFGD 02/27/2013
			AddTotal(&TERM_TOT.s_cr_tot, RECORD_BUF.dd_amount);


		if (RECORD_BUF.dd_valor_efectivo != 0)
			AddTotal(&TERM_TOT.s_cash_back, RECORD_BUF.dd_valor_efectivo   );

		if (RECORD_BUF.dd_iva != 0)
			AddTotal(&TERM_TOT.s_iva_tot, RECORD_BUF.dd_iva   );

		if (RECORD_BUF.dd_base != 0)
			AddTotal(&TERM_TOT.s_base_tot, RECORD_BUF.dd_base  );

		if (RECORD_BUF.dd_tip != 0)
			AddTotal(&TERM_TOT.s_tips_tot, RECORD_BUF.dd_tip   );
		if ((RECORD_BUF.dd_donacion != 0) && (RECORD_BUF.b_trans_status & VOIDED) != 0x10)
			AddTotal(&TERM_TOT.s_donacion_tot, RECORD_BUF.dd_donacion);
		if (RECORD_BUF.dd_tasa_admin != 0)
			AddTotal(&TERM_TOT.s_tAdm_tot, RECORD_BUF.dd_tasa_admin);
	}
	else
		AddTotal(&TERM_TOT.s_refund_tot, RECORD_BUF.dd_amount);
}
//*****************************************************************************
//  Function        : CalTotals
//  Description     : Calculate to transaction totals.
//  Input           : all;      // TRUE : total for all transaction.
//                              // FALSE: acquirer[0] ONLY.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : ...
//*****************************************************************************
void CalTotals(BOOLEAN aAll)
{
	DWORD i = 0;
	DWORD flagBono = 0x00;
	BYTE flagSett[16]; // Jorge Numa

	memset(flagSett, 0, sizeof(flagSett)); // Jorge Numa

	flagSett[1] = 0x31; // Jorge Numa
	flagSett[1] -= '0'; // Jorge Numa


	memset(&TERM_TOT, 0, sizeof(struct TOTAL_STRUCT));
	while (TRUE)
	{
		if (!APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF)))
			break;
		i++;
		//		if ((aAll || (RECORD_BUF.w_host_idx == INPUT.w_host_idx))
		//				&& RECORD_BUF.b_trans != BONO_OBSEQUIO)	//kt-220413

		if ((aAll || (RECORD_BUF.w_host_idx == INPUT.w_host_idx)) )//RECORD_BUF.b_trans != ANULACION_PUNTOS) //ktkt
		{
			flagBono |= 0x10;

			AddToTotals(FALSE);

		}


	}

	if(flagBono == 0x01 )
	{
		CloseBatchVisa(); // 19-09-12 Jorge Numa ++
		os_config_write(K_CF_Settled, &flagSett[1]); // Jorge Numa
		os_config_update(); // Jorge Numa
		fCommitAllMW();
		RefreshDispAfter(0);
	}
}
//*****************************************************************************
//  Function        : CalIssuerSum
//  Description     : Calculate transaction totals by issuer.
//  Input           : aIssuerIdx;   // issuer index.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : TERM_TOT;
//*****************************************************************************
#if (NO_USED)
static void CalIssuerSum(WORD aIssuerIdx)
{
	DWORD i = 0;

	while (TRUE)
	{
		if ((!APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF))))
			break;

		i++;
		if (RECORD_BUF.b_trans == CHEQUES_AL_DIA //kt-190413
				|| RECORD_BUF.b_trans == CHEQUES_POSTFECHADOS)
			continue;

		if (RECORD_BUF.w_issuer_idx == aIssuerIdx)
			AddToTotals(FALSE);
	}
}
#endif
//*****************************************************************************
//  Function        : ValidAcquirer
//  Description     : Validate whether the Acquirer is Support.
//  Input           : aAcqID;
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
static BOOLEAN ValidAcquirer(BYTE aAcqID)
{
	BYTE i;
	WORD max_acq = APM_GetAcqCount();

	for (i = 0; i < max_acq; i++)
	{
		if (!APM_GetAcqTbl(i, &STIS_ACQ_TBL(1)))
			continue;
		if (STIS_ACQ_TBL(1).b_status == NOT_LOADED)
			return (FALSE);
		if (aAcqID == STIS_ACQ_TBL(1).b_id)
			return (CorrectHost(GetHostType(1)));
	}
	return (FALSE);
}
#endif

//*****************************************************************************
//  Function        : PackIssuerTotals
//  Description     : Pack total information by issuer.
//  Input           : aIssuerIdx;   // issuer index.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void PackIssuerTotals(WORD aIssuerIdx)
{
	CalIssuerSum(aIssuerIdx);
	//pack_mem(STIS_ISS_TBL(1).sb_card_name, 10);
	//pack_lf();
	PackTotals(&TERM_TOT);
}
#endif

//*****************************************************************************
//  Function        : PackIssuerTotalsVISA
//  Description     : Pack total information by issuer.
//  Input           : aIssuerIdx;   // issuer index.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN PackIssuerTotalsVISA(WORD aIssuerIdx, BOOLEAN GranTotal, BOOLEAN display)//kt-290413
{
	DWORD i = 0;
	BYTE tipoCuenta;
	int Aux = 0;

	memset(&TERM_TOT, 0, sizeof(struct TOTAL_STRUCT));

	while (TRUE) //kt-100413
	{
		if ((!APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF))))
			break;
		i++;

		if (!GranTotal)
		{
			if ((memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
					&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id,
							8) == 0))
				AddToTotals(TRUE);
		}
		else
		{
			if ((memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
					&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id,
							8) == 0))
				AddToTotals(TRUE);
		}
	}

	if (!GranTotal)
		tipoCuenta = 0x60;
	else
		tipoCuenta = 0x50;

	if (display)
	{
		Aux = DispTransTotal(&TERM_TOT, tipoCuenta);
		return Aux; //MFBC/26/12/13 optimice codigo
	}
	else
		PackTotalsVISA(&TERM_TOT, tipoCuenta);

	return TRUE;

}

//*****************************************************************************
//  Function        : PackIssueTotalsMultibol
//  Description     : Pack total information by issuer.
//  Input           : aIssuerIdx;   // issuer index.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int PackIssueTotalsMultibol(BOOLEAN display)
{
	DWORD i = 0;
	BYTE tipoCuenta;
	int aux = 1;

	memset(&TERM_TOT, 0, sizeof(struct TOTAL_STRUCT));

	while (TRUE)
	{
		if ((!APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF))))
			break;
		i++;

		if ((memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0)
				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id,
						8) == 0) )
			AddToTotals(TRUE);

	}

	tipoCuenta = 0x60;

	if (display)
		aux = DispTransTotal(&TERM_TOT, tipoCuenta);
	else
		PackTotalsVISA(&TERM_TOT, tipoCuenta);

	return aux;

}
//*****************************************************************************
//  Function        : PackVoidTotals
//  Description     : Pack void transaction totals by ISSUER.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
static void PackVoidTotals(void)
{
	static const BYTE c_voided_sales[] =
	{ "VOIDED SALES" };
	static const BYTE c_voided_refund[] =
	{ "VOIDED REFUND" };

	pack_mem((BYTE *) c_voided_sales, 12);
	pack_space(1);
	bindec_data(TERM_TOT.s_void_sale.w_count, 3);
	PackAmt(TERM_TOT.s_void_sale.dd_amount, 14);
	pack_lf();
	pack_mem((BYTE *) c_voided_refund, 13);
	bindec_data(TERM_TOT.s_void_refund.w_count, 3);
	PackAmt(TERM_TOT.s_void_refund.dd_amount, 14);
	pack_lf();
}
#endif

//*****************************************************************************
//  Function        : PackIssuerSum
//  Description     : Pack ISSUER transaction sum.
//  Input           : all;        // TRUE :- pack for all issuers.
//                                // FALSE:- ONLY pack for STIS_ISS_TBL(0).
//  Return          : N/A
//  Note            : vico
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void PackIssuerSum(BOOLEAN aAll)
{
	WORD issuer_idx, card_idx;
	WORD max_issuer = APM_GetIssuerCount();
	WORD max_card = APM_GetCardCount();

	PackSeperator('-');
	pack_str("\x1bW1 SUMMARY REPORT \x1bW0");
	pack_lf();
	PackSeperator('-');

	memset(&TERM_TOT, 0, sizeof(struct TOTAL_STRUCT));
	for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
	{
		if (!APM_GetIssuer(issuer_idx, &STIS_ISS_TBL(1)))
			continue;
		if (STIS_ISS_TBL(1).b_id == 0xff)
			continue;

		for (card_idx = 0; card_idx < max_card; card_idx++)
		{
			if (!APM_GetCardTbl(card_idx, &STIS_CARD_TBL(0)))
				continue;
			if (STIS_CARD_TBL(0).b_issuer_id != STIS_ISS_TBL(1).b_id)
				continue;
			if (!ValidAcquirer(STIS_CARD_TBL(0).b_acquirer_id))
				continue;
			if (aAll
					|| (STIS_CARD_TBL(0).b_acquirer_id == STIS_ACQ_TBL(0).b_id))
			{
				PackIssuerTotals(issuer_idx);
				break;
			}
		}
		// clear total except voided sum
		memset(&TERM_TOT, 0, (BYTE *) &TERM_TOT.s_void_sale
				- (BYTE *) &TERM_TOT.d_adjust_count);
	}

	PackVoidTotals();
	PackSeperator('-');
	pack_lf();
	PackEndOfRep();
}
#endif

//*****************************************************************************
//  Function        : FormatHolderName
//  Description     : Format the card holder name as per AMEX request.
//  Input           : aDest;        // pointer to output buffer.
//                    aName;        // pointer to name buffer from CARD.
//                    len;          // len of name buffer.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : input.sb_holder_name;
//*****************************************************************************
void FormatHolderName(BYTE *aDest, BYTE *aName, BYTE aLen)
{
	BYTE i, tIdx, mIdx;

	memset(aDest, ' ', sizeof(INPUT.sb_holder_name));
	tIdx = aLen;
	mIdx = aLen;
	// locate title
	for (i = 0; i < aLen; i++)
	{
		if (aName[i] == '.')
		{
			tIdx = i;
			break;
		}
		if (aName[i] == '/')
			mIdx = i;
	}
	// Exception case handle for only exist title or title come first than middle
	if (tIdx < mIdx)
		tIdx = aLen;
	if (aLen > tIdx)
	{ // title exist
		memcpy(aDest, &aName[tIdx + 1], aLen - tIdx - 1);
		aDest += (aLen - tIdx);
	}
	if (tIdx > mIdx)
	{ // Middle name exist
		memcpy(aDest, &aName[mIdx + 1], tIdx - mIdx - 1);
		aDest += (tIdx - mIdx - 1);
	}
	memcpy(aDest, aName, mIdx);
}
//*****************************************************************************
//  Function        : IncTraceNo
//  Description     : Increment the system trace no & save to input & tx_data.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void IncTraceNo(void)
{
    struct APPDATA *Global;
    Global= (void *) MallocMW(sizeof(struct APPDATA));
    memcpy(Global,&gAppDat,sizeof(struct APPDATA));

    memcpy(INPUT.sb_trace_no, Global->NumTrans, 3);
	//memcpy(INPUT.sb_roc_no, INPUT.sb_trace_no, 3);
    bcdinc(Global->NumTrans, 3);
    if (memcmp(Global->NumTrans, "\x00\x00\x00", 3) == 0)
        bcdinc(Global->NumTrans, 3);
    SaveDataFile(Global);
    FreeMW(Global);
    //    IncAPMTraceNo();

}

void IncRocNo(void)
{
    struct APPDATA *Global;
    Global= (void *) MallocMW(sizeof(struct APPDATA));
    memcpy(Global,&gAppDat,sizeof(struct APPDATA));
	// memcpy(INPUT.sb_roc_no, STIS_TERM_DATA.sb_roc_no, 3);
    bcdinc(Global->NumRoc, 3);
    if (memcmp(Global->NumRoc, "\x00\x00\x00", 3) == 0)
        bcdinc(Global->NumRoc, 3);
    SaveDataFile(Global);
    FreeMW(Global);
}
//*****************************************************************************
//  Function        : ClearRspData
//  Description     : Clear RSP_DATA.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals         : RSP_DATA;
//*****************************************************************************
void ClearRspData(void)
{
	RSP_DATA.b_response = TRANS_FAIL;
	RSP_DATA.w_rspcode = 'C' * 256 + 'N';
	RSP_DATA.text[0] = 0;
	memset(&RSP_DATA.text[1], ' ', 69);
	memset(RSP_DATA.sb_rrn, ' ', 12);
	memset(RSP_DATA.sb_auth_code, ' ', 6);
}
//*****************************************************************************
//  Function        : ResetTerm
//  Description     : Reset terminal globals & hardware.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals         : gRspData;
//                    gInput.
//*****************************************************************************
void ResetTerm(void)
{
	IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
	gGDS->b_disp_chgd = TRUE;
	MSG_BUF.d_len = 0;
	ClearRspData();
	memset(&INPUT, 0, sizeof(INPUT));
#if (T800)
	os_disp_bl_control(0x10);
#endif
	APM_ClearKeyin();
}
//******************************************************************************
//  Function        : ConvB2Comma
//  Description     : Convert the special character in buffer.
//  Input           : aPtr;    // pointer to buffer need to be convert.
//  Return          : N/A
//  Note            : 'B' => ','
//                    'D' => '*'
//                    'E' => '#'
//  Globals Changed : N/A
//******************************************************************************
void ConvB2Comma(BYTE *aPtr)
{
	while (*aPtr)
	{
		if (*aPtr == 'B')
			*aPtr = ',';
		else if (*aPtr == 'D')
			*aPtr = '*';
		else if (*aPtr == 'E')
			*aPtr = '#';
		aPtr++;
	}
}
//******************************************************************************
//  Function        : PackTelNo
//  Description     : Pack telephone number together AT cmd & pabx.
//  Input           : aTermData
//                    aTelBuf;       // pointer to result buffer.
//                    aTel;          // pointer to tel num with 'F' packed.
//  Return          : length of packed buffer.
//  Note            : N/A
//  Globals Changed : N/A
//******************************************************************************
static BYTE PackTelNo(struct TERM_DATA aTermData, BYTE *aTelBuf, BYTE *aTel)
{
	BYTE count;

	memset(aTelBuf, 0, 34);
	count = 0;
	aTelBuf[count++] = aTermData.b_dial_mode == 0 ? 'T' : 'P';
	split(&aTelBuf[count], aTermData.sb_pabx, 4);
	count = (BYTE) fndb(aTelBuf, 'F', 9);
	split(&aTelBuf[count], aTel, 12);
	count = (BYTE) fndb(aTelBuf, 'F', (BYTE)(count + 24));
	aTelBuf[count] = 0;
	ConvB2Comma(aTelBuf);

	return (count);
}

//******************************************************************************
//  Function        : PackTelNo
//  Description     : Pack telephone number together AT cmd & pabx.
//  Input           : aTermData
//                    aTelBuf;       // pointer to result buffer.
//                    aTel;          // pointer to tel num with 'F' packed.
//  Return          : length of packed buffer.
//  Note            : N/A
//  Globals Changed : N/A
//******************************************************************************
static BYTE PackTelNoTest(BYTE *aTelBuf, BYTE *aTel)
{
	BYTE count;

	memset(aTelBuf, 0, 34);
	count = 0;
	if (gAppDat.TipoMarcado == TRUE)
		aTelBuf[count++] = 'T'; //P = Pulse, T = Tone

	else
		aTelBuf[count++] = 'P'; //P = Pulse, T = Tone
	//split(&aTelBuf[count], aTermData.sb_pabx, 4);
	split(&aTelBuf[count], gAppDat.Prefijo, 5);
	count = (BYTE)fndb(aTelBuf,'F', 10);
	split(&aTelBuf[count], aTel, 12);
	count = (BYTE) fndb(aTelBuf, 'F', (BYTE)(count + 24));
	aTelBuf[count] = 0;

	/*printf("\faTelBuf:<%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x>", aTelBuf[0], aTelBuf[1], aTelBuf[2], aTelBuf[3], aTelBuf[4],
	 aTelBuf[5], aTelBuf[6], aTelBuf[7], aTelBuf[8], aTelBuf[9], aTelBuf[10], aTelBuf[11]);
	 APM_WaitKey(9000, 0);*/
	ConvB2Comma(aTelBuf);

	return (count);
}

//*****************************************************************************
//  Function        : PackComm
//  Description     : Pack EDC host communication parameter
//  Input           : aHostIdx;     // host index
//                    aSettle;      // TRUE => Settlement 
//  Return          : N/A
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
void PackComm(DWORD aHostIdx, BOOLEAN aForSettle)
{
	struct COMMPARAM comm_param;
	struct ACQUIRER_TBL acq_tbl;
	struct TERM_DATA term_data;

	// get terminal data and acquirer table
	APM_GetTermData(&term_data);
	APM_GetAcqTbl(aHostIdx, &acq_tbl);

	memset(&comm_param, 0, sizeof(comm_param));
	comm_param.wLen = sizeof(comm_param);
	comm_param.bCommMode = APM_COMM_AUX;
	comm_param.bHostConnTime = 3; // 3 Sec
	comm_param.bTimeoutVal = 3; // 3 Sec

	if (memcmp(acq_tbl.sb_ip, "\x00\x00\x00\x00", 4) == 0)
	{ // ip = 0.0.0.0
		comm_param.bCommMode = APM_COMM_SYNC;
	}
	else if (memcmp(acq_tbl.sb_ip, "\xFF\xFF\xFF\xFF", 4) == 0)
	{ // ip = 255.255.255.255
		comm_param.bCommMode = APM_COMM_AUX;
	}
	else
		comm_param.bCommMode = APM_COMM_TCPIP;

	// modem setting
	memcpy((BYTE *) &comm_param.sMdm, (BYTE *) &KDefaultSReg,
			sizeof(struct MDMSREG));
	// common parameters
	if ((comm_param.bCommMode == APM_COMM_SYNC) || (comm_param.bCommMode
			== APM_COMM_ASYNC))
	{
		comm_param.bAsyncDelay = term_data.b_async_delay;
		comm_param.bHostConnTime = 20;
		comm_param.bTimeoutVal = acq_tbl.b_timeout_val;
	}
	else if (comm_param.bCommMode == APM_COMM_TCPIP)
	{
		comm_param.bHostConnTime = 20;
		comm_param.bTimeoutVal = acq_tbl.b_timeout_val;
	}
	// settlement host
	if (aForSettle)
	{
		memcpy(&comm_param.sMdm.sMdmCfg.b_protocol,
				&KCommParam[acq_tbl.b_settle_mdm_mode].b_protocol, 3);
		comm_param.sMdm.sMdmCfg.b_ptel_len = PackTelNo(term_data,
				comm_param.sMdm.sMdmCfg.s_ptel, acq_tbl.sb_pri_settle_tel);
		comm_param.sMdm.sMdmCfg.b_pconn_tval = acq_tbl.b_pri_settle_conn_time;
		comm_param.sMdm.sMdmCfg.b_pconn_limit = acq_tbl.b_pri_settle_redial;
		comm_param.sMdm.sMdmCfg.b_stel_len = PackTelNo(term_data,
				comm_param.sMdm.sMdmCfg.s_stel, acq_tbl.sb_sec_settle_tel);
		comm_param.sMdm.sMdmCfg.b_sconn_tval = acq_tbl.b_sec_settle_conn_time;
		comm_param.sMdm.sMdmCfg.b_sconn_tval = acq_tbl.b_sec_settle_redial;
	}
	// transaction host
	else
	{
		memcpy(&comm_param.sMdm.sMdmCfg.b_protocol,
				&KCommParam[acq_tbl.b_trans_mdm_mode].b_protocol, 3);
		comm_param.sMdm.sMdmCfg.b_ptel_len = PackTelNo(term_data,
				comm_param.sMdm.sMdmCfg.s_ptel, acq_tbl.sb_pri_trans_tel);
		comm_param.sMdm.sMdmCfg.b_pconn_tval = acq_tbl.b_pri_trans_conn_time;
		comm_param.sMdm.sMdmCfg.b_pconn_limit = acq_tbl.b_pri_trans_redial;
		comm_param.sMdm.sMdmCfg.b_stel_len = PackTelNo(term_data,
				comm_param.sMdm.sMdmCfg.s_stel, acq_tbl.sb_sec_trans_tel);
		comm_param.sMdm.sMdmCfg.b_sconn_tval = acq_tbl.b_sec_trans_conn_time;
		comm_param.sMdm.sMdmCfg.b_sconn_tval = acq_tbl.b_sec_trans_redial;
	}
	if (comm_param.bCommMode == APM_COMM_SYNC)
	{
		if (comm_param.sMdm.sMdmCfg.b_protocol == TYPE_ASYN)
			comm_param.bCommMode = APM_COMM_ASYNC;
		else
			comm_param.bCommMode = APM_COMM_SYNC;
	}

	// tcp setting
	comm_param.sTcp.bLen = sizeof(comm_param.sTcp);
	memcpy(&comm_param.sTcp.sTcpCfg.d_ip, acq_tbl.sb_ip, 4);
	comm_param.sTcp.sTcpCfg.w_port = acq_tbl.sb_port[0] * 256
			+ acq_tbl.sb_port[1];
	comm_param.sTcp.sTcpCfg.b_option
	= (acq_tbl.b_ssl_key_idx & 0x7F) ? MW_TCP_SSL_MODE : 0;
	comm_param.sTcp.sTcpCfg.b_sslidx = acq_tbl.b_ssl_key_idx & 0x7F;
	comm_param.sTcp.bAdd2ByteLen = acq_tbl.b_reserved1 ? 1 : 0; // ExtraMsgLen
	if (acq_tbl.b_ssl_key_idx & 0x80)
	{
		comm_param.sTcp.sTcpCfg.b_option |= MW_TCP_PPP_MODE;
		if (comm_param.bCommMode == APM_COMM_TCPIP)
			comm_param.bCommMode = APM_COMM_GPRS;
	}

	// aux setting
	comm_param.sAux.bLen = sizeof(comm_param.sAux);
	comm_param.sAux.bPort = acq_tbl.sb_port[0];
	comm_param.sAux.sAuxCfg.b_len = sizeof(struct MW_AUX_CFG);
	comm_param.sAux.sAuxCfg.b_mode = MW_AUX_NO_PARITY;
	comm_param.sAux.sAuxCfg.b_speed = acq_tbl.sb_port[1];
	comm_param.sAux.sAuxCfg.b_rx_gap = 10; // 100 ms
	comm_param.sAux.sAuxCfg.b_rsp_gap = 10;
	comm_param.sAux.sAuxCfg.b_tx_gap = 10;
	comm_param.sAux.sAuxCfg.b_retry = 0;

	// Config PPP parameter
	comm_param.sPPP.bKeepAlive = TRUE; // Never disconnect
	if (comm_param.bCommMode == APM_COMM_GPRS)
		comm_param.sPPP.dDevice = MW_PPP_DEVICE_GPRS;
	else
		comm_param.sPPP.dDevice = MW_PPP_DEVICE_NONE;
	comm_param.sPPP.dSpeed = 230400;
	comm_param.sPPP.dMode = MW_PPP_MODE_NORMAL;
	memset(comm_param.sPPP.scUserID, 0, sizeof(comm_param.sPPP.scUserID));
	memset(comm_param.sPPP.scPwd, 0, sizeof(comm_param.sPPP.scPwd));
	comm_param.sPPP.psLogin = NULL;
	comm_param.sPPP.dLoginPair = 0;
	comm_param.sPPP.psDialUp = NULL;
	comm_param.sPPP.dDialupPair = 0;
	comm_param.sPPP.psDialParam = NULL;

	APM_PackComm(&comm_param);
}

void PackCommTest(DWORD aHostIdx, BOOLEAN aForSettle)
{
	struct COMMPARAM comm_param;
	BYTE AuxIP[5];
	BYTE APN[60];
	BYTE TelPrinc[6 + 1];
	BYTE TelSec[6 + 1];

	memset(AuxIP, 0x00, sizeof(AuxIP));
	memset(APN, 0x00, sizeof(APN));
	memset(TelPrinc, 0xFF, sizeof(TelPrinc));
	memset(TelSec, 0xFF, sizeof(TelSec));


	//compress(TelPrinc, gTablaCuatro.b_tel_pri, 6);
	//compress(TelSec, gTablaCuatro.b_tel_sec, 6);
	//printf("\f telprin %02x %02x %02x %02x %02x %02x", gTablaCuatro.b_tel_pri[0], gTablaCuatro.b_tel_pri[1], gTablaCuatro.b_tel_pri[2], gTablaCuatro.b_tel_pri[3],
	//gTablaCuatro.b_tel_pri[4], gTablaCuatro.b_tel_pri[5]); WaitKey(3000, 0);
	// get terminal data and acquirer table
	//APM_GetTermData(&term_data);
	//APM_GetAcqTbl(aHostIdx, &acq_tbl);

	memset(&comm_param, 0, sizeof(comm_param));
	comm_param.wLen = sizeof(comm_param);
	//comm_param.bCommMode      = APM_COMM_AUX;
	comm_param.bHostConnTime = 30; // 30 Sec
	comm_param.bTimeoutVal = 30; // 30 Sec

	//  if (memcmp(acq_tbl.sb_ip, "\x00\x00\x00\x00", 4) == 0) {  // ip = 0.0.0.0
	//    comm_param.bCommMode = APM_COMM_SYNC;
	//  } else if (memcmp(acq_tbl.sb_ip, "\xFF\xFF\xFF\xFF", 4) == 0) {  // ip = 255.255.255.255
	//    comm_param.bCommMode = APM_COMM_AUX;
	//  } else

	//memcpy(comm_param.sPPP.scAPN,"AT+CGDCONT=1,\"IP\",\"CREDIBANCO.COMCEL.COM.COM\"", 49);

	memcpy((BYTE *) &comm_param.sMdm, (BYTE *) &KDefaultSReg,
			sizeof(struct MDMSREG));

	//sel_opc = MenuSelect(&KCommMenu, 0);

	//if (sel_opc == -1)
	//	/return;

	//switch (sel_opc)
	//{
	//case 0:

	/////////////////////////////COMUNICACION LAN////////////////////////////////////////////

	if (gAppDat.ConexionLAN == TRUE)
	{
		//		printf("\f entro a conexion lan"); WaitKey(3000,0);
		comm_param.bCommMode = APM_COMM_TCPIP;
		gbCommMode = APM_COMM_TCPIP;
		comm_param.sTcp.sTcpCfg.b_option = 0;
	}

	//72.55.144.180
	//6161

	//printf("debugM"); WaitKey(3000, 0);
	//memcpy(APN,"CREDIBANCO.COMCEL.COM.CO", 24 );

	//memcpy(APN,"INTERNET.MOVISTAR.COM.CO", 24 );

	/////////////////////////////COMUNICACION GPRS////////////////////////////////////////////

	if (gAppDat.ConexionGPRS == TRUE) //kt-170413
	{
		//		printf("\fGPRS"); WaitKey(3000, 0);
		//memcpy(APN, "CREDIBAN.MOVISTAR.COM.CO", 24);
		comm_param.sTcp.sTcpCfg.b_option |= MW_TCP_PPP_MODE;
		comm_param.bCommMode = APM_COMM_GPRS;
		gbCommMode = APM_COMM_GPRS;
		//sprintf(comm_param.sPPP.scAPN,"AT+CGDCONT=1,\"IP\",\"%s\"",APN);
		//memset(comm_param.sPPP.scAPN, 0x00, sizeof(comm_param.sPPP.scAPN));
		//memcpy(comm_param.sPPP.scAPN, APN, strlen(APN));

	}
	//printf("debugA"); WaitKey(3000, 0);

	/////////////////////////////COMUNICACION DIAL////////////////////////////////////////////

	if (gAppDat.ConexionDIAL == TRUE)
	{
		if (gAppDat.ModoLlamada == TRUE)
		{
			gbCommMode = APM_COMM_SYNC;
			comm_param.bCommMode = APM_COMM_SYNC;
		}
		else
		{
			gbCommMode = APM_COMM_ASYNC;
			comm_param.bCommMode = APM_COMM_ASYNC;
		}

		memcpy(&comm_param.sMdm.sMdmCfg.b_protocol, &KCommParam[2].b_protocol,
				3);

		//		if ((prefijo[0] != 0xFF) && (prefijo[1] != 0xFF))  // comentado para pruebas de prefijo manuel barbaran /14/11/13
		//		{
		//			memcpy(TelPrincTemp, prefijo, strlen(prefijo));
		//			memcpy(TelSecTemp, prefijo, strlen(prefijo));
		//			Indice = strlen(prefijo);
		//			IndiceDos = 12 - strlen(prefijo);
		//		}
		//		else
		//			IndiceDos = 0;

		//		if (INPUT.b_trans == INIT)
		//if (TRUE)
		if (gAppDat.estadoInit == FALSE)
		{
			memcpy(TelPrinc, gAppDat.TelPrincipal, 6);
			memcpy(TelSec, gAppDat.TelSecundario, 6);
			comm_param.sMdm.sMdmCfg.b_ptel_len = PackTelNoTest(
					comm_param.sMdm.sMdmCfg.s_ptel, TelPrinc);
			comm_param.sMdm.sMdmCfg.b_stel_len = PackTelNoTest(
					comm_param.sMdm.sMdmCfg.s_stel, TelSec);
		}
		else
		{
			memcpy(TelPrinc, gTablaCuatro.b_tel_pri, 6);
			memcpy(TelSec, gTablaCuatro.b_tel_sec, 6);

			comm_param.sMdm.sMdmCfg.b_ptel_len = PackTelNoTest(
					comm_param.sMdm.sMdmCfg.s_ptel, TelPrinc);
			comm_param.sMdm.sMdmCfg.b_stel_len = PackTelNoTest(
					comm_param.sMdm.sMdmCfg.s_stel, TelSec);
		}
		//comm_param.sMdm.sMdmCfg.b_ptel_len    = PackTelNo(term_data, comm_param.sMdm.sMdmCfg.s_ptel, acq_tbl.sb_pri_trans_tel);
		//comm_param.sMdm.sMdmCfg.b_ptel_len    = PackTelNoTest( comm_param.sMdm.sMdmCfg.s_ptel, "\x03\x48\x02\x18\xFF");
		//comm_param.sMdm.sMdmCfg.b_ptel_len = PackTelNoTest(comm_param.sMdm.sMdmCfg.s_ptel, "\x34\x80\x21\x8F");
		comm_param.sMdm.sMdmCfg.b_pconn_tval = 30;
		comm_param.sMdm.sMdmCfg.b_pconn_limit = 2;
		//comm_param.sMdm.sMdmCfg.b_stel_len    = PackTelNo(term_data, comm_param.sMdm.sMdmCfg.s_stel, acq_tbl.sb_sec_trans_tel);
		//comm_param.sMdm.sMdmCfg.b_stel_len = PackTelNoTest(comm_param.sMdm.sMdmCfg.s_stel, "\x34\x80\x21\x8F");
		//comm_param.sMdm.sMdmCfg.b_sconn_tval  = acq_tbl.b_sec_trans_conn_time;
		comm_param.sMdm.sMdmCfg.b_sconn_tval = 30;
		//comm_param.sMdm.sMdmCfg.b_sconn_tval  = acq_tbl.b_sec_trans_redial;
		//printf("\f salgo dial"); WaitKey(3000,0);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//printf("\f sali dial"); WaitKey(3000,0);
	//comm_param.bCommMode = APM_COMM_TCPIP;

	/*
	 // modem setting
	 memcpy((BYTE *)&comm_param.sMdm, (BYTE *)&KDefaultSReg, sizeof(struct MDMSREG));
	 // common parameters
	 if ((comm_param.bCommMode == APM_COMM_SYNC) || (comm_param.bCommMode == APM_COMM_ASYNC)) {
	 comm_param.bAsyncDelay    = term_data.b_async_delay;
	 comm_param.bHostConnTime  = 20;
	 comm_param.bTimeoutVal    = acq_tbl.b_timeout_val;
	 }
	 else if (comm_param.bCommMode == APM_COMM_TCPIP) {
	 comm_param.bHostConnTime  = 20;
	 comm_param.bTimeoutVal    = acq_tbl.b_timeout_val;
	 }
	 // settlement host
	 if (aForSettle) {
	 memcpy(&comm_param.sMdm.sMdmCfg.b_protocol, &KCommParam[acq_tbl.b_settle_mdm_mode].b_protocol, 3);
	 comm_param.sMdm.sMdmCfg.b_ptel_len    = PackTelNo(term_data, comm_param.sMdm.sMdmCfg.s_ptel, acq_tbl.sb_pri_settle_tel);
	 comm_param.sMdm.sMdmCfg.b_pconn_tval  = acq_tbl.b_pri_settle_conn_time;
	 comm_param.sMdm.sMdmCfg.b_pconn_limit = acq_tbl.b_pri_settle_redial;
	 comm_param.sMdm.sMdmCfg.b_stel_len    = PackTelNo(term_data, comm_param.sMdm.sMdmCfg.s_stel, acq_tbl.sb_sec_settle_tel);
	 comm_param.sMdm.sMdmCfg.b_sconn_tval  = acq_tbl.b_sec_settle_conn_time;
	 comm_param.sMdm.sMdmCfg.b_sconn_tval  = acq_tbl.b_sec_settle_redial;
	 }
	 // transaction host
	 else {
	 memcpy(&comm_param.sMdm.sMdmCfg.b_protocol, &KCommParam[acq_tbl.b_trans_mdm_mode].b_protocol, 3);
	 comm_param.sMdm.sMdmCfg.b_ptel_len    = PackTelNo(term_data, comm_param.sMdm.sMdmCfg.s_ptel, acq_tbl.sb_pri_trans_tel);
	 comm_param.sMdm.sMdmCfg.b_pconn_tval  = acq_tbl.b_pri_trans_conn_time;
	 comm_param.sMdm.sMdmCfg.b_pconn_limit = acq_tbl.b_pri_trans_redial;
	 comm_param.sMdm.sMdmCfg.b_stel_len    = PackTelNo(term_data, comm_param.sMdm.sMdmCfg.s_stel, acq_tbl.sb_sec_trans_tel);
	 comm_param.sMdm.sMdmCfg.b_sconn_tval  = acq_tbl.b_sec_trans_conn_time;
	 comm_param.sMdm.sMdmCfg.b_sconn_tval  = acq_tbl.b_sec_trans_redial;
	 }
	 if (comm_param.bCommMode == APM_COMM_SYNC) {
	 if (comm_param.sMdm.sMdmCfg.b_protocol == TYPE_ASYN)
	 comm_param.bCommMode = APM_COMM_ASYNC;
	 else
	 comm_param.bCommMode = APM_COMM_SYNC;
	 }
	 */
	// tcp setting
	comm_param.sTcp.bLen = sizeof(comm_param.sTcp);

	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, acq_tbl.sb_ip, 4);
	//comm_param.sTcp.sTcpCfg.w_port     = acq_tbl.sb_port[0]*256+acq_tbl.sb_port[1];

	//printf("IP:%d.%d.%d.%d", gTcpIp.sb_ip[0], gTcpIp.sb_ip[1], gTcpIp.sb_ip[2], gTcpIp.sb_ip[3]);
	//GetChar();

	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, gTcpIp.sb_ip, 4);
	//comm_param.sTcp.sTcpCfg.w_port = gTcpIp.w_port;
	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, "\x0a\x00\x00\x7F", 4);
	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, "\xAC\x1B\x01\x34", 4); //172.27.1.52 ip de HOST
	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, "\xAC\x1A\x0D\x58", 4); //172.26.13.88 ip de HOST

	//printf("\f  IP: %02x/%02x/%02x/ %02x", AuxIP[0], AuxIP[1], AuxIP[2], AuxIP[3] ); WaitKey(3000,0);
	//printf("\f  IP: %02x/%02x/%02x/ %02x", gAppDat.IpHost[0], gAppDat.IpHost[1], gAppDat.IpHost[2], gAppDat.IpHost[3] ); WaitKey(3000,0);

	memcpy(&comm_param.sTcp.sTcpCfg.d_ip, gAppDat.IpHost, 4); //MFBC  27/09/19
	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, AuxIP, 4 );
	//	memcpy(&comm_param.sTcp.sTcpCfg.d_ip, gAppDat.IpHost, 4 );

	//printf("\f  IP2: %04x", comm_param.sTcp.sTcpCfg.d_ip ); WaitKey(3000,0);

	//printf("\f  IP Host = %02x/%02x/%02x/ %02x", gAppDat.IpHost[0], gAppDat.IpHost[1],
	//	gAppDat.IpHost[2], gAppDat.IpHost[3]); WaitKey(3000,0);

	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, "\xAC\x1A\x0D\x57", 4);
	//memcpy(&comm_param.sTcp.sTcpCfg.d_ip, "\x0a\x00\x00\x80", 4);

	//comm_param.sTcp.sTcpCfg.w_port = 6165;
	// comm_param.sTcp.sTcpCfg.w_port = 2101;
	comm_param.sTcp.sTcpCfg.w_port = gAppDat.PortHost; //MFBC  27/09/19
	//printf ("\f Puerto Host %d", comm_param.sTcp.sTcpCfg.w_port ); WaitKey(3000, 0);
	//	comm_param.sTcp.sTcpCfg.w_port = 2100;

	//comm_param.sTcp.sTcpCfg.b_option   = (acq_tbl.b_ssl_key_idx&0x7F)?MW_TCP_SSL_MODE:0;

	//comm_param.sTcp.sTcpCfg.b_option  |= MW_TCP_PPP_MODE;

	//comm_param.sTcp.sTcpCfg.b_sslidx   = acq_tbl.b_ssl_key_idx & 0x7F;
	//comm_param.sTcp.sTcpCfg.b_sslidx  |= 0x80;
	if (gConfigComercio.habSSL == TRUE)
	{ //Daniel SSL
		comm_param.sTcp.sTcpCfg.b_sslidx = gSSL_KEY;

		// Jorge Numa ->> Se agrega "|" para que funcione GPRS con SSL
		comm_param.sTcp.sTcpCfg.b_option |= (gSSL_KEY & 0x7F) ? MW_TCP_SSL_MODE : 0;
	}
	//comm_param.sTcp.bAdd2ByteLen       = acq_tbl.b_reserved1? 1 : 0;    // ExtraMsgLen
	comm_param.sTcp.bAdd2ByteLen = 1; // ExtraMsgLen
	/*
	 if (acq_tbl.b_ssl_key_idx&0x80)
	 {
	 comm_param.sTcp.sTcpCfg.b_option  |= MW_TCP_PPP_MODE;
	 if (comm_param.bCommMode == APM_COMM_TCPIP)
	 comm_param.bCommMode = APM_COMM_GPRS;
	 }
	 */
	// aux setting
	comm_param.sAux.bLen = sizeof(comm_param.sAux);
	//comm_param.sAux.bPort             = acq_tbl.sb_port[0];
	comm_param.sAux.sAuxCfg.b_len = sizeof(struct MW_AUX_CFG);
	comm_param.sAux.sAuxCfg.b_mode = MW_AUX_NO_PARITY;
	//comm_param.sAux.sAuxCfg.b_speed   = acq_tbl.sb_port[1];
	comm_param.sAux.sAuxCfg.b_rx_gap = 10; // 100 ms
	comm_param.sAux.sAuxCfg.b_rsp_gap = 10;
	comm_param.sAux.sAuxCfg.b_tx_gap = 10;
	comm_param.sAux.sAuxCfg.b_retry = 0;

	// Config PPP parameter
	comm_param.sPPP.bKeepAlive = TRUE; // Never disconnect
	if (comm_param.bCommMode == APM_COMM_GPRS)
		comm_param.sPPP.dDevice = MW_PPP_DEVICE_GPRS;
	else if (comm_param.bCommMode == APM_COMM_DIAL) //NM-19/04/13 Agregue este ELSE IF
		comm_param.sPPP.dDevice = MW_PPP_DEVICE_LINE;
	else
		comm_param.sPPP.dDevice = MW_PPP_DEVICE_NONE;

	comm_param.sPPP.dSpeed = 230400;
	comm_param.sPPP.dMode = MW_PPP_MODE_NORMAL;
	memset(comm_param.sPPP.scUserID, 0, sizeof(comm_param.sPPP.scUserID));
	memset(comm_param.sPPP.scPwd, 0, sizeof(comm_param.sPPP.scPwd));
	memset(comm_param.sPPP.scAPN, 0, sizeof(comm_param.sPPP.scAPN));

	if (gAppDat.ConexionGPRS == TRUE){					// **SR** 20/09/13
		memcpy(comm_param.sPPP.scAPN, gConfigGPRS.sb_Apn , strlen(gConfigGPRS.sb_Apn));
		memcpy(comm_param.sPPP.scUserID, gConfigGPRS.sb_User, strlen(gConfigGPRS.sb_User));
		memcpy(comm_param.sPPP.scPwd, gConfigGPRS.sb_Pass, strlen(gConfigGPRS.sb_Pass));
	}

	comm_param.sPPP.psLogin = NULL;
	comm_param.sPPP.dLoginPair = 0;
	comm_param.sPPP.psDialUp = NULL;
	comm_param.sPPP.dDialupPair = 0;
	comm_param.sPPP.psDialParam = NULL;
	//printf("\f voy al packComm"); WaitKey(3000,0);
	//FreeMW(conf_AppData);
	APM_PackComm(&comm_param);
}

void PackCommCaja(DWORD aHostIdx, BOOLEAN aForSettle) //Daniel Cajas TCP
{
	struct COMMPARAM comm_param;
	BYTE AuxIP[5];

	memset(AuxIP, 0x00, sizeof(AuxIP));
	memset(&comm_param, 0, sizeof(comm_param));
	comm_param.wLen = sizeof(comm_param);
	comm_param.bHostConnTime = 30; // 30 Sec
	comm_param.bTimeoutVal = 30; // 30 Sec

	memcpy((BYTE *) &comm_param.sMdm, (BYTE *) &KDefaultSReg,
			sizeof(struct MDMSREG));

	comm_param.bCommMode = APM_COMM_TCPIP;
	gbCommMode = APM_COMM_TCPIP;

	comm_param.sTcp.bLen = sizeof(comm_param.sTcp);

	memcpy(&comm_param.sTcp.sTcpCfg.d_ip, gAppDat.IpCaja, 4); //MFBC  27/09/19
	comm_param.sTcp.sTcpCfg.w_port = gAppDat.PortCaja; //MFBC  27/09/19

	comm_param.sTcp.sTcpCfg.b_option = 0;
	//comm_param.sTcp.sTcpCfg.b_sslidx   = acq_tbl.b_ssl_key_idx & 0x7F;
	//comm_param.sTcp.bAdd2ByteLen       = acq_tbl.b_reserved1? 1 : 0;    // ExtraMsgLen
	comm_param.sTcp.bAdd2ByteLen = 1; // ExtraMsgLen

	comm_param.sAux.bLen = sizeof(comm_param.sAux);
	//comm_param.sAux.bPort             = acq_tbl.sb_port[0];
	comm_param.sAux.sAuxCfg.b_len = sizeof(struct MW_AUX_CFG);
	comm_param.sAux.sAuxCfg.b_mode = MW_AUX_NO_PARITY;
	//comm_param.sAux.sAuxCfg.b_speed   = acq_tbl.sb_port[1];
	comm_param.sAux.sAuxCfg.b_rx_gap = 10; // 100 ms
	comm_param.sAux.sAuxCfg.b_rsp_gap = 10;
	comm_param.sAux.sAuxCfg.b_tx_gap = 10;
	comm_param.sAux.sAuxCfg.b_retry = 0;

	// Config PPP parameter
	comm_param.sPPP.bKeepAlive = TRUE; // Never disconnect
	if (comm_param.bCommMode == APM_COMM_GPRS)
		comm_param.sPPP.dDevice = MW_PPP_DEVICE_GPRS;
	else
		comm_param.sPPP.dDevice = MW_PPP_DEVICE_NONE;
	comm_param.sPPP.dSpeed = 230400;
	comm_param.sPPP.dMode = MW_PPP_MODE_NORMAL;
	memset(comm_param.sPPP.scUserID, 0, sizeof(comm_param.sPPP.scUserID));
	memset(comm_param.sPPP.scPwd, 0, sizeof(comm_param.sPPP.scPwd));
	comm_param.sPPP.psLogin = NULL;
	comm_param.sPPP.dLoginPair = 0;
	comm_param.sPPP.psDialUp = NULL;
	comm_param.sPPP.dDialupPair = 0;
	comm_param.sPPP.psDialParam = NULL;
	//printf("\f voy al packComm"); WaitKey(3000,0);
	//FreeMW(conf_AppData);
	APM_PackComm(&comm_param);
}

BOOLEAN getPinBlock(BYTE *panBCD, BOOLEAN clear) //El de los chinos :D //MFBC/15/04/13  cambie la funcion completa
{
	int retPin;
	BYTE msgPin[] = "INGRESE PIN";
	BYTE nomIss[11];

	if (!key_ready(EKEY_IDX))
	{
		LongBeep();
		TextColor("NO HAY WKEY", COLOR_RED, MW_LINE5, MW_CLRDISP | MW_CENTER
				| MW_BIGFONT, 3); //MFBC/24/02/13
		return FALSE;
	}

	//DispClrBelowMW(MW_LINE1);
	if (clear)
		DispClrBelowMW(MW_LINE2); //kt-060912
	memset(panBCD, 0x00, 10);
	memcpy(panBCD, INPUT.sb_pan, 10);
	panBCD[10] = 0x01; // PIN fix len 4
	panBCD[10] = PRM_PIN_BYPASS; // enable PIN bypass

	memset(nomIss, 0x00, sizeof(nomIss));
	memset(INPUT.sb_pin, 0x00, sizeof(INPUT.sb_pin));
	memcpy(nomIss, gTablaTres.b_nom_emisor, 10);

	os_beep_open();

	do
	{
		// PINBLOCK
		// DispClrBelowMW(MW_LINE1); //MFBC/24/02/13		
		DispLineMW(nomIss, MW_LINE1, MW_CLRDISP | MW_LEFT | MW_SMFONT);

		if (gOrg_rec.dd_amount != 0  && (INPUT.b_trans == VOID ) ) //MFBC/12/04/13
		{
			//gOrg_rec.dd_amount += gOrg_rec.dd_donacion; //MFBC/24/05/13
			// TextColor("MONTO:", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
			DispAmnt(gOrg_rec.dd_amount, MW_LINE1, MW_SMFONT); // 14/03/2013 - Jorge Numa
		}
		else if (INPUT.dd_amount != 0)
		{
			// TextColor("MONTO:", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
			//DispAmnt(INPUT.dd_amount + INPUT.dd_valor_efectivo, MW_LINE1, MW_SMFONT); // 14/03/2013 - Jorge Numa
			DispAmnt(INPUT.dd_amount , MW_LINE1, MW_SMFONT); // 14/03/2013 - Jorge Numa
		}


		os_beep_close();

		DispGotoMW(MW_LINE3 + 2, MW_SMFONT);
		DispPutCMW(K_PushCursor);
		os_disp_textc(COLOR_VISABLUE);
		retPin = KDLL_GetX98PIN((BYTE*) panBCD, (BYTE*) msgPin, EKEY_IDX);
		DispPutCMW(K_PopCursor);
		if (retPin == 2)
			return FALSE;
	} while (memcmp(panBCD, "\x00\x00\x00\x00\x00\x00\x00\x00", 8) == 0);

	memcpy(INPUT.sb_pin, panBCD, 8);
	//	if (gOrg_rec.dd_amount != 0  && (INPUT.b_trans == VOID || INPUT.b_trans == VOID_MULTIBOL) ) //MFBC/12/04/13
	//		gOrg_rec.dd_amount -= gOrg_rec.dd_donacion; //Jorge Numa

	return TRUE;
}

DWORD press1Or2Key(void)
{
	//BYTE key = 0x00;

	while (1)
	{
		os_sleep();
		//key = os_kbd_getkey();
		switch (WaitKey(2000, 0))
		{
		case MWKEY_2:
			return 2;
		case MWKEY_1:
			return 1;
		case K_KeyCancel:
			return 0;
		default:
			return 0;
			break;
		}
	}
	return 0;
}

void displayAuthCode(void)
{
	BYTE buffer[20];

	memset(buffer, 0x00, sizeof(buffer));
	//DispHeader(NULL);
	DispHeaderTrans();

	sprintf(buffer, "APROBADO: %s", RSP_DATA.sb_auth_code);
	DispLineMW(buffer, MW_LINE4, /*MW_CLRDISP|MW_REVERSE|*/
			MW_CENTER | MW_SMFONT);
	APM_WaitKey(300, 0);
}



//nohora
//*****************************************************************************
//  Function        : GetPPPStatus
//  Description     : Return current PPP status
//  Input           : N/A
//  Return          : PPP status;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
DWORD GetPPPStatus(void)
{
	return IOCtlMW(gPPPHandle, IO_PPP_GET_PHASE, NULL);
}


//*****************************************************************************
//  Function        : ShowPPPStatus
//  Description     : Show PPP Status.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ShowPPPStatus(void)
{
	DWORD status, ostatus, uptime;
	struct MW_NIF_INFO nif_info;
	BYTE tmp[MW_MAX_LINESIZE+1];

	ClearDispMW();
	ostatus = -2;
	status  = -1;
	while (GetCharMW() != MWKEY_CANCL) {
		SleepMW();

		// Show Status DWORD
		status = GetPPPStatus();
		if (ostatus != status) {
			// Show IP Info
			if (status == MW_PPP_PHASE_READY) {
				NetInfoMW(MW_NIF_PPP, &nif_info);
			}
			else {
				memset(&nif_info, 0, sizeof(nif_info));
			}
			DispTermIP((BYTE *)&nif_info);

			// Show Status
			if (status < 6) {
				DispLineMW(KPPPState[status], MW_LINE1, MW_REVERSE|MW_CENTER|MW_SMFONT);
			}
			else {
				DispLineMW("IDLE", MW_LINE1, MW_REVERSE|MW_CENTER|MW_SMFONT);
			}
			ostatus = status;
		}

		if (status == MW_PPP_PHASE_READY)
			uptime = IOCtlMW(gPPPHandle, IO_PPP_UPTIME, NULL);
		else
			uptime = 0;
		sprintf(tmp, "Up Time: %05d Secs", uptime);
		DispLineMW(tmp, MW_MAX_LINE, MW_CENTER|MW_SPFONT);
	}
}



/*---------------------------------------------------------------------*/
/*
function : to release the allocated struct EXPECT_SEND
input    : struct EXPECT_SEND structure array ptr
           size of array
output   : none
 */
void ExpectSendRelease(T_EXPECT_SEND ** a_s, DWORD a_n)
{
	T_EXPECT_SEND * ps;
	char **p;

	ps = *a_s;
	if ((ps == NULL) || (a_n == 0))
		return;
	while (a_n--)
	{
		if (ps->p_send)
			FreeMW(ps->p_send);
		if (ps->p_abort)
			FreeMW(ps->p_abort);
		if (ps->p_expect)
		{
			p = ps->p_expect;
			while (*p)
			{
				FreeMW(*p);
				p++;
			}
			FreeMW(ps->p_expect);
		}
		ps++;
	}
	FreeMW(*a_s);
	*a_s = NULL;
}

/*---------------------------------------------------------------------*/
/*
function : to release the allocated struct EXPECT_SEND
input    : struct EXPECT_SEND structure array ptr
           size of array
output   : none
 */
T_EXPECT_SEND * ExpectSendAlloc(const T_EXPECT_SEND * a_s,DWORD a_n)
{
	T_EXPECT_SEND * ps,*ops;
	char  *p;
	char  **pp;
	char  **pt;
	DWORD i;
	DWORD size;
	DWORD n;

	if ((a_s == NULL) || (a_n == 0))
		return NULL;
	size = sizeof(T_EXPECT_SEND);
	size += 3;
	size &= ~3;
	size *= a_n;
	if ((ops=(T_EXPECT_SEND*)MallocMW(size * a_n)) == NULL)
		return NULL;
	ps = ops;
	n   = a_n;
	memcpy(ps,a_s,size);
	while (n--)
	{
		ps->p_expect = NULL;
		ps->p_send   = NULL;
		ps->p_abort  = NULL;
		ps++;
	}
	ps = ops;
	n   = a_n;

	while (n--)
	{
		if (a_s->p_abort)
		{
			if ((p=(char*)MallocMW(strlen(a_s->p_abort)+1))== NULL)
			{
				ExpectSendRelease(&ops,a_n);
				return NULL;
			}
			ps->p_abort = p;
			strcpy(p,a_s->p_abort);
		}

		if (a_s->p_send)
		{
			if ((p=(char*)MallocMW(strlen((char*)a_s->p_send)+1))== NULL)
			{
				ExpectSendRelease(&ops,a_n);
				return NULL;
			}
			ps->p_send = (signed char *)p;
			strcpy(p,(char*)a_s->p_send);
		}

		if (a_s->p_expect)
		{
			i = sizeof(char*);
			pp = a_s->p_expect;
			while (*pp++)
				i += sizeof(char*);

			if ((pt=(char**)MallocMW(i))== NULL)
			{
				ExpectSendRelease(&ops,a_n);
				return NULL;
			}
			memset(pt,0,i);   // set all null
			ps->p_expect = pt;

			pp = a_s->p_expect;
			while (*pp)
			{
				if ((p=(char*)MallocMW(strlen(*pp)+1))== NULL)
				{
					ExpectSendRelease(&ops,a_n);
					return NULL;
				}
				*pt++ = p;
				strcpy(p,*pp);
				pp++;
			}
			*pt = NULL;
		}
		a_s++;
		ps++;
	}
	return ops;
}


//*****************************************************************************
//  Function        : PPPConnect
//  Description     : Make a PPP connection.
//  Input           : N/A
//  Return         : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PPPConnect(void)
{
	//int option;
	//int org_val;
	int handle;
	char tmp[32], username[32], userpwd[32], phone_no[32];
	DWORD status;
	BOOLEAN connecting;
	int gPPPport;

	DispLineMW("PPP CONNECTION", MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);

	//org_val = gPPPport == -1 ? MW_PPP_DEVICE_LINE : gPPPport;
	//option = ToggleOption("PPP Port:", KPPPport, org_val-MW_PPP_DEVICE_LINE);
	//  if (option == -1)
	//    return;
	//  gPPPport = option + MW_PPP_DEVICE_LINE;
	gPPPport = MW_PPP_DEVICE_LINE;

	// setup gprs
	//  if (gPPPport == MW_PPP_DEVICE_GPRS) { // PPP GPRS
	//    if (SetGPRS() == FALSE) {
	//      return;
	//    }
	//    //map gprs user and password to PPP user and password
	//    strcpy(gUserName, gGPRSDat.scUserName);
	//    strcpy(gUserPsw, gGPRSDat.scPassword);
	//  }
	//  else {  // Get User Name
	DispClrBelowMW(MW_LINE3);
	//strcpy(tmp, gUserName);
	memset(tmp, 0x00, sizeof(tmp));
	memcpy(&tmp[1], "12345", 5);
	tmp[0] = 5;
	DispLineMW("UserName: ", MW_LINE5, MW_BIGFONT);
	/*if (GetString (tmp, MW_LINE7, 16) < 0) {
      return;
    }*/
	if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE7, IMIN(4)+IMAX(10), tmp))
		return;

	//strcpy(gUserName, tmp);
	memset(username,0x00,sizeof(username));
	memcpy(username, &tmp[1], tmp[0]);

	// Get User Password
	DispClrBelowMW(MW_LINE3);
	//strcpy(tmp, gUserPsw);
	memset(tmp, 0x00, sizeof(tmp));
	memcpy(&tmp[1], "12345", 5);
	tmp[0] = 5;
	DispLineMW("Password: ", MW_LINE5, MW_BIGFONT);
	/*if (GetString (tmp, MW_LINE7, 16) < 0) {
    	return;
    }*/
	if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE7, IMIN(4)+IMAX(10), tmp))
		return;

	//strcpy(gUserPsw, tmp);
	memset(userpwd,0x00,sizeof(userpwd));
	memcpy(userpwd, &tmp[1], tmp[0]);
	//  }

	if (gPPPport == MW_PPP_DEVICE_LINE) {   // PPP on modem
		// Get User Phone
		DispClrBelowMW(MW_LINE3);
		//strcpy(tmp, gPhoneNo);
		memset(tmp, 0x00, sizeof(tmp));
		memcpy(&tmp[1], "3212152", 7);
		tmp[0] = 7;
		DispLineMW("Phone No: ", MW_LINE5, MW_BIGFONT);
		/*if (GetString (tmp, MW_LINE7, 16) < 0) {
      return;
    }*/
		if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE7, IMIN(4)+IMAX(10), tmp))
			return;

		//strcpy(gPhoneNo, tmp);
		memset(phone_no, 0x00, sizeof(phone_no));
		memcpy(phone_no, &tmp[1], tmp[0]);
		//18-12-12 JC ++
		//Close modem handle opened by APM
		handle = APM_GetHwHandle(APM_SDEV_MDM);
		if (handle != -1)
			CloseMW(handle);
		//18-12-12 JC --
	}

	strcpy(tmp, DEV_PPP1);
	if ((gPPPHandle = OpenMW(tmp, 0)) == -1) {
		return;
	}

	gPPPData = MallocMW(sizeof(struct MW_PPP_DATA));
	if (gPPPData == NULL)
		return;

	connecting = FALSE;
	DispClrBelowMW(MW_LINE3);
	IOCtlMW(gPPPHandle, IO_PPP_DISCONNECT, NULL);
	while (GetCharMW() != MWKEY_CANCL) {
		SleepMW();

		// Show Status DWORD
		status = GetPPPStatus();
		if (status != -1) {
			DispLineMW(KPPPState[status], MW_LINE7, MW_CENTER|MW_SMFONT);
			sprintf(tmp, "Status: %08X", status);
			DispLineMW(tmp, MW_LINE8, MW_CENTER|MW_SMFONT);

		}
		else {
			if (connecting){
				DispLineMW("Connecting", MW_LINE7, MW_CENTER|MW_SMFONT);
			}
			else {
				DispLineMW("Idle", MW_LINE7, MW_CENTER|MW_SMFONT);
			}
			Delay10ms(200);
		}

		if ((status == MW_PPP_PHASE_DEAD) || (status == MW_PPP_PHASE_IDLE)) {
			connecting = TRUE;
			if (gPPPport == MW_PPP_DEVICE_LINE) {   // Modem Line
				gPPPData->device    = gPPPport;
				gPPPData->dev_speed = 115200;
				gPPPData->id        = 0;
				IOCtlMW(gPPPHandle, IO_PPP_SET_DEVICE, gPPPData);
				//bs_ppp_set_device(gPPPport, 115200, 0);

				memcpy(&gPPPData->line_config, &KLineDialParam, sizeof(gPPPData->line_config));
				IOCtlMW(gPPPHandle, IO_PPP_SET_DIAL_PARAM, gPPPData);
				//bs_ppp_set_dial_param(&KLineDialParam);

				//strcpy(phone_no, gPhoneNo);
				gPPPData->phone1 = phone_no;
				gPPPData->phone2 = phone_no;
				IOCtlMW(gPPPHandle, IO_PPP_SET_PHONE, gPPPData);
				//bs_ppp_set_phones(gPhoneNo,gPhoneNo);

				gPPPData->mode = MW_PPP_MODE_NORMAL;
				IOCtlMW(gPPPHandle, IO_PPP_SET_MODE, gPPPData);
				//bs_ppp_set_mode(MW_PPP_MODE_NORMAL);


				// For window PPP connection
				gPPPData->login_scripts = 0;
				gPPPData->login_pair    = 0;
				IOCtlMW(gPPPHandle, IO_PPP_SET_LOGIN_SCRIPT, gPPPData);

				// For Unix PPP Connection
				//gPPPData->login_pair    = 3;
				//gPPPData->login_scripts = (struct MW_EXPECT_SEND *) ExpectSendAlloc(KDefaultLoginScript, gPPPData->login_pair);
				//IOCtlMW(gPPPHandle, IO_PPP_SET_LOGIN_SCRIPT, gPPPData);
				//ExpectSendRelease((T_EXPECT_SEND **)&gPPPData->login_scripts, gPPPData->login_pair);
				//bs_ppp_set_login_script(0, 0);

				gPPPData->dial_pair    = 3;
				gPPPData->dial_scripts = (struct MW_EXPECT_SEND *) ExpectSendAlloc(KDefaultDialScript, gPPPData->dial_pair);
				IOCtlMW(gPPPHandle, IO_PPP_SET_DIAL_SCRIPT, gPPPData);
				ExpectSendRelease((T_EXPECT_SEND **)gPPPData->dial_scripts, gPPPData->dial_pair);

				//strcpy(username, gUserName);
				//strcpy(userpwd,  gUserPsw);
				gPPPData->username = username;
				gPPPData->userpwd  = userpwd;
				IOCtlMW(gPPPHandle, IO_PPP_SET_LOGINID, gPPPData);
				//bs_ppp_set_login_id(gUserName, gUserPsw);
			}

			if (!IOCtlMW(gPPPHandle, IO_PPP_CONNECT, NULL)) {
				DispLineMW("Connection Fail!", MW_LINE7, MW_CENTER|MW_SMFONT);
				APM_WaitKey(5000, 0);
				return;
			}
			else
				break;
		}
	}
	ShowPPPStatus();
}

/*******************************************
 * Descripcion: Cierra la coneccion que esta actualmente y abre la que comunica con la Caja
 * **SR** 17/09/13
 *********************************************/
void OpenCommCajas (void){

	Delay10ms(15);
	APM_ResetComm();
	PackCommCaja(INPUT.w_host_idx, FALSE);
	Delay10ms(15);

	return;
}


/*******************************************
 * Descripcion: Cierra la coneccion con la Caja y abre nuevamente al que estaba abierta
 * **SR** 17/09/13
 *********************************************/

void CloseCommCajas (void){

	APM_ResetComm();
	Delay10ms(15);
	PackCommTest(INPUT.w_host_idx, false);
	APM_PreConnect();
	Delay10ms(15);

	return;
}
//******************************************************************************
//  Function        : ConvCaracter
//  Description     : Convert the special character in buffer.
//  Input           : aPtr;    // pointer to buffer need to be convert.
//  Return          : N/A
//  Note            : ',' => 'B'
//                        '*' => 'D'
//                        '#' => 'E'
//  Copia de la funcion ConvB2Comma
//  Globals Changed : N/A
// By Manuel barbaran
//******************************************************************************
void ConvCaracter(BYTE *aPtr)
{
	while (*aPtr)
	{
		if (*aPtr == ',')
			*aPtr = 'B';
		else if (*aPtr ==  '*')
			*aPtr = 'D';
		else if (*aPtr == '#' )
			*aPtr = 'E';
		aPtr++;
	}
}



//******************************************************************************
//  Function        : encryptPinBlock
//  Description     : encripta el PIN pasado por parametro
//  Input           : aPIN = PIN a encriptar, aLenPIN = longitud del PIN.
//  Return          : TRUE/FALSE
//  Note            : N/A
//  Globals Changed : N/A
// By Manuel barbaran
//******************************************************************************
BOOLEAN encryptPinBlock(BOOLEAN *aPIN, DWORD aLenPIN)
{
	int retPin;
	BYTE panBCD[10];
	BYTE msgPin[28];
	BYTE msgPinBCD[9];


	if (!key_ready(EKEY_IDX))
	{
		LongBeep();
		TextColor("NO HAY WKEY", COLOR_RED, MW_LINE5, MW_CLRDISP | MW_CENTER
				| MW_BIGFONT, 3);
		return FALSE;
	}


	DispClrBelowMW(MW_LINE2);
	memset(msgPin,  'F', sizeof(msgPin));
	memset(panBCD, 0x00, sizeof(panBCD));
	memset(msgPinBCD, 0x00, sizeof(msgPinBCD));

	memcpy(&msgPin[2], aPIN, aLenPIN );

	compress(msgPinBCD, msgPin, 9);

	//memset(&msgPinBCD[9], 0x00, 1);
	memcpy(panBCD, INPUT.sb_pan, 10);

	//panBCD[11] = PRM_PIN_BYPASS; // enable PIN bypass

	memset(INPUT.sb_pin, 0x00, sizeof(INPUT.sb_pin));
	os_beep_close();

	retPin = KDLL_GetX98PIN((BYTE*) panBCD, (BYTE*) msgPinBCD, EKEY_IDX);
	if (retPin == 2)
		return FALSE;

	memcpy(INPUT.sb_pin, panBCD, 8);

	return TRUE;
}


//*****************************************************************************
//  Function        : DownloadAuth
//  Description     : Auth for app download.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int DownloadAuthDel(void)
{
	BYTE challenge[8];
	DWORD crypto;

	os_get_challenge(challenge);
	os_3deskey(KAuthKeydel);
	os_3des(challenge);

	crypto = conv_bl(challenge);
	if (os_auth(K_AuthAppDll, crypto) == FALSE){
		return -1;
	}

	return 0;
}



//*****************************************************************************
//  Function        : DeletApp
//  Description     : Elimina la plicacion despues del proceso exitoso.
//  Input           : aOn;        // TRUE => show ':'
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DeletApp (void){
	int ret;
	BYTE AppName[10 + 1];
#define Name "TERMLOADER"

	if (DownloadAuthDel() < 0) {
		// ShowMsg(Call_Ent);
		return;
	}

	memset(AppName,0x00, sizeof(AppName));
	strcpy(AppName, Name);

	ret = os_appdl_delete_app(AppName);
	if (ret < 0){
		//ShowMsg(Call_Ent);
	}

	STIS_TERM_DATA.b_tkl_ProcesOk = FALSE;
	APM_TermDataUpd(&STIS_TERM_DATA);
	TextColor("EL POS SE REINICIARA",MW_LINE3, COLOR_VISABLUE, MW_SMFONT| MW_CLRDISP | MW_CENTER, 0);
	TextColor("PARA GUARDAR CAMBIOS",MW_LINE4, COLOR_VISABLUE, MW_SMFONT| MW_CENTER, 0);
	TextColor("DE SEGURIDAD",MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
	ResetMW();
	return;
}


void location_name (void)
{
	BYTE var_data [40 + 1];
	BYTE nombre[23 + 1];
	DWORD indicaNombre = 0;

	memset (nombre, 0x00, sizeof(nombre));
	memset (var_data, 0x00, sizeof(var_data));

	memcpy(nombre, gTablaCero.b_nom_establ, 23);
	LTrim(nombre, ' ');
	RTrim(nombre, ' ');
	indicaNombre = 40 - strlen (nombre);

	memcpy(var_data, nombre, strlen (nombre));
	memcpy(&var_data[strlen (nombre)], gTablaCero.b_dir_ciudad, indicaNombre );
	indicaNombre =  40 - indicaNombre;

	if (indicaNombre != 0)
		pack_space(indicaNombre);

	pack_str(var_data);

}






