//-----------------------------------------------------------------------------
//  File          : sysutil.h
//  Module        :
//  Description   : Declrartion & Defination for SYSUTIL.C
//  Author        : Lewis
//  Notes         : No UI is include in the routines, call util lib
//                  Only.
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  01 Apr  2009  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _INC_SYSUTIL_H_
#define _INC_SYSUTIL_H_
#include "common.h"

//-----------------------------------------------------------------------------
//  System Timer Usage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//  Beep & delay Functions
//-----------------------------------------------------------------------------
extern void LongBeep(void);
extern void AcceptBeep(void);
extern void Short2Beep(void);
extern void Short1Beep(void);
extern void Delay1Sec(DWORD aTime, BOOLEAN aBeepReqd);
extern void ErrorDelay(void);
extern void Delay10ms(DWORD aCnt);
extern BYTE FechaHoraRec[17];

//-----------------------------------------------------------------------------
//  Display Handling Functions.
//-----------------------------------------------------------------------------
#if (T1000)
// RRRRRGGGGGIBBBBB
#define RGB(r,g,b)      (((r&0xF8)<<8) | ((g&0xF8)<<3) | ((b&0xF8)>>3) | ((r&g&b&4)<<3))
#define T800_TO_T1000(x) ((((x&0x001F)<<11) | ((x&0x03E0)<<1) | ((x&0x7C00)>>10) | ((x&0x8000)>>10)))
#else  // T800
// IBBBBBGGGGGRRRRR
#define RGB(r,g,b)      (((r&0xF8)>>3) | ((g&0xF8)<<2) | ((b&0xF8)<<7) | ((r&g&b&4)<<13))
#endif

#define RGB_BACKGR      RGB(0xB0, 0xC1, 0xF0)
#define RGB_WHITE       RGB(255,255,255)

//  Color Defines (2 bytes)
#define COLOR_BLACK    RGB(0x00, 0x00, 0x00)
#define COLOR_WHITE    RGB(0xFF, 0xFF, 0xFF)
#define COLOR_RED      RGB(0xFF, 0x00, 0x00)
#define COLOR_YELLOW   RGB(0xFF, 0xFF, 0x00)
#define COLOR_GREEN    RGB(0x00, 0x80, 0x00)
#define COLOR_BLUE     RGB(0x00, 0xFF, 0x00)
#define COLOR_BACKGR   RGB(0xB0, 0xC1, 0xF0)
#define COLOR_DARKBLUE  RGB(0x0F, 0x87, 0xFF)
#define COLOR_VISABLUE  RGB(0x00, 0x00, 0x9F)
#define COLOR_VISABLUE2  RGB(0xA4, 0xA4, 0xFF)
#define COLOR_VISAYELLOW  RGB(0xD2, 0xD2, 0x00)

//-----------------------------------------------------------------------------
//  Date/Time Handling Functions
//-----------------------------------------------------------------------------
struct DATETIME {
  BYTE  b_century;
  BYTE  b_year;
  BYTE  b_month;
  BYTE  b_day;
  BYTE  b_hour;
  BYTE  b_minute;
  BYTE  b_second;
};
extern void SetRTC(struct DATETIME *aTDtg);
extern void ReadRTC(struct DATETIME *aTDtg);
extern void ConvDateTime(BYTE *aBuf, struct DATETIME *aTDtg, BOOLEAN aLongYear);
extern BYTE SetDTG(BYTE *aDateTime);

//-----------------------------------------------------------------------------
//  Display Handling Functions.
//-----------------------------------------------------------------------------
extern void Disp2x16Msg(const BYTE *aMsg, DWORD aOffset, DWORD aCtrl);

//-----------------------------------------------------------------------------
//    Data Buffer related Functions
//-----------------------------------------------------------------------------
extern void ByteCopy(BYTE *dest, BYTE *src, DWORD len);
extern void CompressInputData(void *aDest, BYTE *aSrc, BYTE aLen);
extern void CompressInputFData(void *aDest, BYTE *aSrc,  BYTE aLen);

//-----------------------------------------------------------------------------
//    Misc Functions
//-----------------------------------------------------------------------------
extern BYTE *GetMonthText(BYTE aMM);
extern void MemFatalErr(void *aMemPtr);
extern void DebugPoint(char *aStr);
extern void displaySI_NO (void);
extern void displaySI_NO_2 (void);
extern void DisplayHabPAN (void);
extern BOOLEAN TransPassAdmin(DWORD line_Clean); //kt-141112
extern BOOLEAN passAdminComercio (DWORD lineClean);
extern BOOLEAN check_lote (void);
//-----------------------------------------------------------------------------
//     Amount related functions.
//-----------------------------------------------------------------------------
struct DISP_AMT {
  BYTE len;
  BYTE content[21];
};
extern BOOLEAN ConvAmount(DDWORD aAmt, struct DISP_AMT *aDispAmt, DWORD aDecPos, BYTE aCurrSym);

extern void CheckPointerAddr(void *aPtr);
extern BYTE *RTrim(BYTE *string, BYTE junk);
extern BYTE *LTrim(BYTE *string, BYTE junk);
extern int StrCmp(char* aStr1, char* aStr2);
extern void RSetStr(BYTE *aStr, WORD aLen, BYTE aCar);
extern void LSetStr(BYTE *aStr, WORD aLen, BYTE aCar);
extern void worToBcd(int valor, BYTE * buffer);
extern int bcd2ToInt(BYTE * buffer, BYTE len);
extern void DumpMemory( void *address );
extern void WriteUSB( BYTE *aBuf2Send, DWORD aLen );
extern void OpenUSB( void );
extern void TextColor (BYTE *Text, DWORD Line, DWORD Color, DWORD Comands, DWORD timeSec );  //MFBC/23/11/12
extern BOOLEAN Estado_Init (void);  //MFBC/14/01/13
extern void displayTCP_RS232(void);
extern BOOLEAN FormatAmount(DDWORD aAmt, struct DISP_AMT *aDispAmt, DWORD aDecPos, BYTE aCurrSym);
extern void ConvDateTimeVisa(BYTE *aBuf, struct DATETIME *aTDtg);
extern void ShowStatus(char *aMsg, DWORD aMode);
extern BOOLEAN Papel (void);
extern BOOLEAN no_paper(void);
extern  DDWORD BCDtoDEC (BYTE *b_Value, DWORD d_Len);
extern BOOLEAN Verf_fecha(DWORD d_Dia, DWORD d_Mes, DWORD d_anio);
extern  BOOLEAN cambioClaveBono(void) ;
extern BOOLEAN Cajas(void); //MFBC/18/04/13
extern void text_screen (void);
extern BOOLEAN superviPassword (DWORD line_Clean);

extern int Hour2Min( WORD aHour, WORD aMin);
extern int Hour2Min_2( char *aHour);
extern int tabla_meses(int meses, int febrero);
extern short Bisiesto( short year );
extern int Date2Min(char *buffer);
extern void CalDateTimeReq( void );

extern void GetIP( BYTE *aIP );
extern void convertIPv4 ( BYTE *destIP, BYTE *IP, BYTE *separator );
extern BOOLEAN codigoAerolinea (BOOLEAN cajas);
extern BOOLEAN codigoAgencia (void);
extern void campo63 (BYTE *serialPos);

extern void checkbuffNum (BYTE *buffNume, int len);
extern BOOLEAN confirmarMonto (DDWORD dd_monto);

extern void SetChequesCajas (void);
extern DWORD WaitKey2(void);
extern int GetOTP(char *aDest, DWORD aPos, int aMinLen, int aMaxLen);

extern BOOLEAN ConfigGPRS (void);

extern void cambioSuperPass (void);
extern void configReferencia (void);
extern BOOLEAN IsGRPS( void );
extern void Ping(void);

extern void IPGprs (void);
extern void displaySI_NOLine(WORD aLine);

extern void DispSelecPage (WORD aNumPage, WORD aMaxPage, WORD aLine);

extern void debugHex(BYTE *aTitle, BYTE *aData, DWORD aLen);
extern int GetStr(char *aDest, DWORD aPos, int aMinLen, int aMaxLen);
extern int GetCashback(void);
extern BYTE NuevoPIN (void);

#endif // _SYSUTIL_H_

