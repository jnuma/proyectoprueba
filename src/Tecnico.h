/*
 * Tecnico.h
 *
 *  Created on: 25/09/2012
 *      Author: Manuel F. Barbaran Conde
 */

#ifndef TECNICO_H_
#define TECNICO_H_


extern void MenuDevices (void);
extern void MenuCargaLlaves(void);
extern void MenuTecnico(void);
extern void ConfigConexion(void);
extern void habilitarTarjetaExito(void);
extern void MenuConfigComercio(void);
extern void habilitarPinAvance(void);
extern void habilitarCuotasEnAvance(void);
extern void habilitarEnvioSerial(void);
extern void habilitarAerolineas(void);
extern void habilitarEnvioTracks(void);
extern void EditarPorcentajePropina(void);
extern void habilitarEnvioPAN(void);
extern void AcumulaPuntos(void);
extern void RemidirPuntos(void);
extern void SugerirCierre(void);
extern int AutoCierre(struct CONFIG_COMERCIO* conf_comer);  //MFBC/23/11/12
extern void InformePolar(void);
extern void RecargaTrans(void);
extern void MenuTipoConexion(void);
extern void ConexionTipoDial (void);
extern void ConexionTipoLAN (void);
extern void ConexionTipoGPRS(void);
extern void MenuConfHost(void);
extern void ConfigIpHost (void);
extern void ConfigPortHost (void);
extern void EditNII(void);
extern void EditNII_Movil(void);
extern void Edit_Terminal(void);
extern void TelEdit(BOOLEAN PrinOrSecTel);
extern void MenuParIniciales (void);
extern void  ImpresionParam (void);
extern void EstadoSI(void); //kt-311012
extern void EstadoNO(void); //kt-311012
extern void configKIN(void);
extern void configTarjetaVirtual(void);
extern void Edit_Autocierre(void); //MFBC/23/11/12
extern BOOLEAN VerificaAutoCierre(void); //MFBC/23/11/12
extern void ConfigIpCaja(void);//Daniel Cajas TCP
extern void ConfigPortCaja(void);//Daniel Cajas TCP
extern void MenuConfCaja(void);//Daniel Cajas TCP
extern void configSSL(void);//Daniel SSL
extern void Prefijo(void);  //MFBC/19/02/13
extern void TipoLlamada (void);
extern void TipoMarcado (void);
extern void editarNumCaja(void);//nora
extern void MenuFtp(void);//nohora
extern BOOLEAN  editar_Recibo (void);
extern void MenuEditCTLS (void);
extern void edit_Trans_limit (void);
extern void edit_CVM_limit (void);
extern void edit_Floor_limit (void);
extern void posFijo (void);
extern void MenuParamEspecial (void);		//**SR**

#endif /* TECNICO_H_ */
