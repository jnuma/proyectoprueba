//-----------------------------------------------------------------------------
//  File          : ctltrans.c
//  Module        :
//  Description   : Include routines for Contactless transactions.
//  Author        : John
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  21 Oct 2010   John        Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "apm.h"
#include "util.h"
#include "sysutil.h"
#include "message.h"
#include "corevar.h"
#include "constant.h"
#include "chkoptn.h"
#include "input.h"
#include "print.h"
#include "hostmsg.h"
#include "record.h"
#include "reversal.h"
#include "offline.h"
#include "sale.h"
#include "tranutil.h"
#include "tlvutil.h"
#include "ctltrans.h"
#include "files.h"
#include "emvtrans.h"
#include "TLVUtil.h"

//-----------------------------------------------------------------------------
//    Defines
//-----------------------------------------------------------------------------
#define bAction         gGDS->s_CTLOut.pbMsg[1]

// contactless function return
#define CL_OK             (int)0
#define CL_ERR            (int)-1
#define CL_TIMEOUT        (int)-2
#define CL_MORE_CARDS     (int)-3
#define CL_WTX_TIMEOUT    (int)-4

#define CTL_COMPLETE      0

// contactless transaction amount limit
#define CTL_TRANS_LIMIT   3500000           // 35,000.00
#define CLCMD_ERROR (int)-1

#define PWAVE_DEBUG	1

BOOLEAN CTL_SIGN;
BOOLEAN CTL_PRINT;

//-----------------------------------------------------------------------------
//    Constant
//-----------------------------------------------------------------------------

// const WORD wCTLOnlineTags[] = { 
//   0x5F2A,                                 /* Transaction Currency Code */
//   0x5F34,                                 /* PAN Sequence Number */
//   0x82,                                   /* AIP */
//   0x84,                                   /* Dedicated File Name */
//   0x95,                                   /* TVR */
//   0x9A,                                   /* Transaction Date */
//   0x9C,                                   /* Transaction Type */
//   0x9F02,                                 /* Amount Authorised */
//   0x9F10,                                 /* Issuer Application Data */
//   0x9F1A,                                 /* Terminal Country Code */
//   0x9F21,                                 /* Trans Time */
//   0x9F26,                                 /* Application Cryptogram */
//   0x9F27,                                 /* CID */
//   0x9F34,                                 /* CVM Result */
//   0x9F36,                                 /* Application Transaction Counter */
//   0x9F37,                                 /* Unpredicatable Number */
//   0x9F66,                                 /* Terminal Trans Qualifiers (TTQ) */
//   0xDFAE,                                 /* Payment Scheme */
//   0xDFB5,                                 /* Trans CVM */
//   0
// };

const WORD wCTLOnlineTags[] =
{ 0x5F2A, /* Transaction Currency Code */
		0x5F34, /* PAN Sequence Number */
		0x5F24, /*AppExpirationDate             ++++++*/
		0x5F25, /*AppEffectiveDate              ++++++*/
		0x5F28, /*xxxxxxxxxxxxxxxxx             ++++++*/
		0x9F57, /* 0x5F28 IssuerCountryCode            ++++++*/
		0x57, /*Track 2                       ++++++*/
		0x82, /* AIP */
		//0x84, /* Dedicated File Name */
		0x95, /* TVR */
		0x9A, /* Transaction Date */
		0x9F21, /* TxnTime                      +++++*/
		0x9B, /* Transaction Status Info      +++++*/
		0x9C, /* Transaction Type */
		0x9F02, /* Amount Authorised */
		0x9F03, /* Amount Other Num             ++++++*/
		0x9F07, /* AppUsageControl              ++++++*/
		//0x9F09, /* Application Version # */
		0x9F10, /* Issuer Application Data */
		0x9F16, /* MerchantId                   ++++++*/
		0x9F1A, /* Terminal Country Code */
		0x9F1E, /* IFD Serial Number */
		0x9F26, /* Application Cryptogram */
		0x9F27, /* CID */
		0x9F33, /* Terminal Capabilities */
		0x9F34, /* CVM Result */
		0x9F35, /* Terminal Type */
		0x9F36, /* Application Transaction Counter */
		0x9F39, /* POSEntryMode                 ++++++*/
		0x9F37, /* Unpredicatable Number */
		//0x9F41, /* Transaction Sequence Counter */
		//0x9F53, /* Transaction Category code */
		0 };

// pack zero data for compliance if not returned by EMVCLDLL
const WORD wCTLDummyTags[][2] =
{ 0x9F07, 4, /* AppUsageControl */
		0x9F34, 3, /* CVM Result */
		0x9F03, 6, 0, 0, };

TLIST *CTLTagSeek(WORD aTag);
static TLIST gTagList;
static BYTE gTagBuf[256];

//*****************************************************************************
//  Function        : KernelReady
//  Description     : Check Whether EMV/CTL Kernel exist
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BOOLEAN KernelReady(BYTE aAppID)
{
	if (!IsAppIdExistMW(aAppID))
	{
		return FALSE;
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : CTLFatal
//  Description     : CTL error handler.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : Modified by Jorge Numa
//*****************************************************************************
static void CTLFatal(DWORD aCmd)
{
	if (aCmd == CLCMD_KEY_LOADING)
	{
		DispLineMW("CTL error - Keys", MW_LINE5,
				MW_CLRDISP | MW_CENTER | MW_SMFONT);
	}
	else if (aCmd == CLCMD_LOAD_AID)
	{
		DispLineMW("CTL error - AID's", MW_LINE5,
				MW_CLRDISP | MW_CENTER | MW_SMFONT);
	}
	else
		DispLineMW("CTL error handler", MW_LINE5,
				MW_CLRDISP | MW_CENTER | MW_SMFONT);

	while (APM_WaitKey((DWORD) - 1, 0) != MWKEY_CANCL)
		SleepMW();
}

//*****************************************************************************
//  Function        : PackCTLDummyData
//  Description     : Prepare CTL dummy data (filled with '0')
//  Input           : aBuf;         // pointer to dest buffer.
//                    aTags;        // list of tags
//  Return          : End of buffer pointer;
//  Note            : N/A
//  Globals Changed : N/a
//*****************************************************************************
BYTE *PackCTLDummyData(BYTE *aBuf, WORD aTag)
{
	WORD i;

	i = 0;
	while (wCTLDummyTags[i][0] != 0)
	{
		// printf("\fbefore if ->tag:<%04x>", aTag);
		// APM_WaitKey(9000,0);
		if (wCTLDummyTags[i][0] == aTag)
		{
			// printf("\fif->tag:<%04x>", aTag);
			// APM_WaitKey(9000,0);
			aBuf = TagPut(aBuf, aTag);
			*aBuf++ = wCTLDummyTags[i][1];
			memset(aBuf, 0, wCTLDummyTags[i][1]);
			aBuf += wCTLDummyTags[i][1];
			break;
		}
		i++;
	}
	return aBuf;
}

//*****************************************************************************
//  Function        : PackCTLTagsData
//  Description     : Prepare CTL tag data
//  Input           : aBuf;         // pointer to dest buffer.
//                    aTags;        // list of tags
//  Return          : End of buffer pointer;
//  Note            : N/A
//  Globals Changed : N/a
//*****************************************************************************
#if 0
BYTE *PackCTLTagsData(BYTE *aBuf, WORD *aTags)
{
	WORD tag;
	TLIST *psLp;
	BYTE sCtlBuf[256];
	CL_IO s_CTLOut;
	//  int len = 0;

	while ((tag = *aTags) != 0)
	{
		if ((psLp = TagSeek(gGDS->s_TagList, tag)) != NULL)
		{
			aBuf = TagPut(aBuf, tag);
			*aBuf++ = psLp->sLen; // assume byte len
			memcpy(aBuf, psLp->pbLoc, psLp->sLen);
			aBuf += psLp->sLen;
		}
		else if (tag == 0x9F03)
		{
			aBuf = TagPut(aBuf, 0x9F03);
			*aBuf++ = 6; // assume byte len
			memset(aBuf, 0, 6);
			aBuf += 6;
		}
		else
		{
			memset(gGDS->s_CTLIn.pbMsg, 0, 3);
			WPut(gGDS->s_CTLIn.pbMsg, tag);
			gGDS->s_CTLIn.wLen = 3;
			s_CTLOut.pbMsg = sCtlBuf;
			if (emvclSetup(CLCMD_GET_TAGS, &gGDS->s_CTLIn, &s_CTLOut))
			{
				//aBuf = TagPut(aBuf, tag);
				//*aBuf++ = s_CTLOut.wLen; // assume byte len
				if ((memcmp(&s_CTLOut.pbMsg[0], "\x5F", 1) == 0) || (memcmp(&s_CTLOut.pbMsg[0], "\x9F", 1) == 0))
				{
					if (&s_CTLOut.pbMsg[2] != 0x00)
					{
						memcpy(aBuf, s_CTLOut.pbMsg, s_CTLOut.wLen);
						aBuf += s_CTLOut.wLen;
					}
					else
					{
						aBuf = PackCTLDummyData(aBuf, tag); // pack zero data if need
					}
				}
				else
				{
					if (&s_CTLOut.pbMsg[1] != 0x00)
					{
						memcpy(aBuf, s_CTLOut.pbMsg, s_CTLOut.wLen);
						aBuf += s_CTLOut.wLen;
					}
					else
					{
						aBuf = PackCTLDummyData(aBuf, tag); // pack zero data if need
					}
				}
			}
		}
		aTags++; // next tag word
	}
	return aBuf;
}
#else
//*****************************************************************************
//  Function        : PackCTLTagsData
//  Description     : Prepare CTL tag data
//  Input           : aBuf;         // pointer to dest buffer.
//                    aTags;        // list of tags
//  Return          : End of buffer pointer;
//  Note            : N/A
//  Globals Changed : N/a
//*****************************************************************************
BYTE *PackCTLTagsData(BYTE *aBuf, WORD *aTags)
{
	WORD tag;
	TLIST *psLp;
	BYTE *pMsg, sCtlBuf[256];
	CL_IO s_CTLIn, s_CTLOut;

	while ((tag = *aTags) != 0)
	{
		if ((psLp = TagSeek(gGDS->s_TagList, tag)) != NULL)
		{
			aBuf = TagPut(aBuf, tag);
			*aBuf++ = psLp->sLen; // assume byte len
			memcpy(aBuf, psLp->pbLoc, psLp->sLen);
			aBuf += psLp->sLen;
		}
		else
		{
			s_CTLIn.pbMsg = s_CTLOut.pbMsg = sCtlBuf;
			memset(s_CTLIn.pbMsg, 0, sizeof(s_CTLIn.pbMsg));
			pMsg = s_CTLIn.pbMsg;
			pMsg = TagPut(pMsg, tag);
			s_CTLIn.wLen = pMsg - s_CTLIn.pbMsg;
			if (emvclSetup(CLCMD_GET_TAGS, &s_CTLIn, &s_CTLOut))
			{
				// get valid 'V' if return len longer than 'T+L'
				if (s_CTLOut.wLen > (s_CTLIn.wLen + 1))
				{
					memcpy(aBuf, s_CTLOut.pbMsg, s_CTLOut.wLen);
					aBuf += s_CTLOut.wLen;
				}
				else
					aBuf = PackCTLDummyData(aBuf, tag); // pack zero data if need
			}
		}
		aTags++; // next tag word
	}
	return aBuf;
}
#endif

//*****************************************************************************
//  Function        : PackCTLData
//  Description     : Put CTL related data to  ICC DATA buffer
//  Input           : aDat;     // pointer to ICC DATA struct
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//******************************************************************************
void PackCTLData(struct ICC_DATA *aDat)
{
	BYTE *ptr;

	aDat->w_misc_len = 0;
	ptr = PackCTLTagsData(aDat->sb_misc_content, (WORD *) wCTLOnlineTags);
	aDat->w_misc_len = (BYTE) ((DWORD) ptr - (DWORD) aDat->sb_misc_content);
}

//*****************************************************************************
//  Function        : CTLOnline
//  Description     : Online Auth
//  Input           : N/A
//  Return          : FALSE: fail to go online, else TRUE
//  Note            : Zero length response => online failure
//  Globals Changed : N/A;
//*****************************************************************************
static BOOLEAN CTLOnline(void)
{
	if (DeAss(gGDS->s_TagList, gGDS->s_CTLOut.pbMsg + 2,
			(WORD) (gGDS->s_CTLOut.wLen - 2)) == 0)
		CTLFatal(CLCMD_ERROR);
	DispHeader(NULL);
	INPUT.b_transAcep = FALSE; //MFBC/25/04/13

	//PackComm(INPUT.w_host_idx, FALSE);
	ClearResponse();
	PackCommTest(INPUT.w_host_idx, FALSE);

	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));
	gIdTabla12 = OpenFile(KTabla12File);
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);


	memset(INPUT.sb_comercio_v, 0x20, sizeof(INPUT.sb_comercio_v));
	memset(INPUT.sb_nom_comer_v, 0x20, sizeof(INPUT.sb_nom_comer_v));
	memset(INPUT.sb_terminal_id, 0x20, sizeof(INPUT.sb_terminal_id));
	memset(INPUT.sb_term_comer_v, 0x20, sizeof(INPUT.sb_term_comer_v));

	memcpy(INPUT.sb_comercio_v, gTablaDoce.b_establ_virtual,9);
	memcpy(INPUT.sb_nom_comer_v, gTablaDoce.b_nom_establ, 15);
	memcpy(INPUT.sb_term_comer_v, gTablaDoce.b_term_virtual, 8);
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);

	FreeMW(p_tabla12);
	CloseFile(gIdTabla12);

	//	memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-261112
	//	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8); //kt-261112

	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			//OfflineSent(FALSE);
			memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no,
					sizeof(TX_DATA.sb_trace_no));
			//memcpy(INPUT.sb_roc_no, INPUT.sb_trace_no, sizeof(TX_DATA.sb_trace_no));
			IncAPMTraceNo();
			if (INPUT.b_trans == VOID)
			{
				gOrg_rec.b_trans_void = gOrg_rec.b_trans;
				ByteCopy((BYTE *) &TX_DATA, (BYTE *) &gOrg_rec,
						TX_DATA.sb_pin - (BYTE *) &TX_DATA);
				ReadRTC(&TX_DATA.s_dtg);
				TX_DATA.dd_amount = TX_DATA.dd_org_amount;
				TX_DATA.b_trans = VOID;
				TX_DATA.sb_proc_code[0] = TX_DATA.sb_proc_code[0] | 0x02;
				//IncAPMTraceNo();
				//IncTraceNo();
				memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3);
				memcpy(TX_DATA.sb_pin, INPUT.sb_pin, sizeof(INPUT.sb_pin)); // online pin


				ByteCopy((BYTE *) &INPUT, (BYTE *) &gOrg_rec,
						INPUT.sb_pin - (BYTE *) &INPUT);
				ReadRTC(&INPUT.s_dtg);
				INPUT.dd_amount = INPUT.dd_org_amount;
				INPUT.b_trans = VOID;
				INPUT.sb_proc_code[0] = INPUT.sb_proc_code[0] | 0x02;
				memcpy(INPUT.sb_trace_no, TX_DATA.sb_trace_no, 3);
				memcpy(INPUT.sb_pin, TX_DATA.sb_pin, sizeof(TX_DATA.sb_pin)); // online pin
			}

			if (INPUT.b_trans != VOID)
			{
				MoveInput2Tx();
			}

			memcpy(TX_DATA.sb_pin, INPUT.sb_pin, sizeof(INPUT.sb_pin)); // online pin
			PackCTLData(&TX_DATA.s_icc_data); // field 55 ICC data
			PackProcCode(TX_DATA.b_trans, TX_DATA.b_acc_ind);
			PackHostMsg();
			UpdateHostStatus(REV_PENDING);
			ClearResponse();

			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, FALSE))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();

				if (RSP_DATA.w_rspcode == '5' * 256 + '5')  //MFBC/09/12/13
				{
					RSP_DATA.b_response = NuevoPIN();
				}

				if (RSP_DATA.b_response == TRANS_REJ)
					UpdateHostStatus(NO_PENDING);

				if (TX_DATA.b_trans == VOID)
				{
					if (RSP_DATA.b_response == TRANS_ACP)
					{
						gOrg_rec.b_trans_status |= VOIDED;
						memcpy(gOrg_rec.sb_trace_no, TX_DATA.sb_trace_no,
								sizeof(gOrg_rec.sb_trace_no));
						memcpy(gOrg_rec.sb_auth_code, RSP_DATA.sb_auth_code,
								sizeof(gOrg_rec.sb_auth_code));
						ReadRTC(&gOrg_rec.s_dtg);
						memcpy(&RECORD_BUF, &gOrg_rec,
								sizeof(struct TXN_RECORD));
						RECORD_BUF.b_trans = TX_DATA.b_trans;
						UpdateRecord(gRecIdx);
						PackRecordP(FALSE, FALSE);
					}
				}
			}
			//			if (RSP_DATA.b_response <= TRANS_REJ)
			//				UpdateHostStatus(NO_PENDING);
		}
		APM_ResetComm();
		return TRUE;
	}
	return FALSE; // unable to go online
}
//*****************************************************************************
//  Function        : CTLError
//  Description     : Handle CTL error response
//  Input           : CTL process next state.
//  Return          : CTL process next state.
//  Note            : N/A
//  Globals Changed : N/A;
//*****************************************************************************
static DWORD CTLError(DWORD aNextState)
{
	//	BYTE buf[64];

	DispHeader(NULL);
	switch (gGDS->s_CTLOut.pbMsg[0])
	{ //error message response
	case CLERR_SMCIO :
	case CLERR_CARDL1 :
	case CLERR_DATA :
		RSP_DATA.w_rspcode = 'R' * 256 + 'E';
		break;
	case CLERR_CANCEL :
		DispClrBelowMW(MW_LINE3);
		RSP_DATA.w_rspcode = 'C' * 256 + 'N';
		break;
	case CLERR_SEQ :
		RSP_DATA.w_rspcode = 'S' * 256 + 'C';
		break;
	case CLERR_NOMORE :
		RSP_DATA.w_rspcode = 'U' * 256 + 'C';
		break;
	case CLERR_NOAPPL : // not supported if no matching app
		//RSP_DATA.w_rspcode = 'M' * 256 + 'A'; // fallback
		RSP_DATA.w_rspcode = 'J' * 256 + 'E';
		break;
	case CLERR_BLOCKED :
		RSP_DATA.w_rspcode = 'S' * 256 + 'B';
		break;
	case CLERR_CONFIG :
	case CLERR_MEMORY :
		RSP_DATA.w_rspcode = 'J' * 256 + 'E';
		break;
	case CLERR_PP_OVER_LMT :
		RSP_DATA.w_rspcode = 'J' * 256 + 'E';
		break;
	default:
		RSP_DATA.w_rspcode = 'J' * 256 + 'E';
		//		RSP_DATA.w_rspcode = 'S' * 256 + 'E';
		break;
	}
	/*SprintfMW(buf, "CLERR = %02x", gGDS->s_CTLOut.pbMsg[0]);
	 DispLineMW(buf, MW_LINE5, MW_CLREOL | MW_CENTER | MW_BIGFONT);
	 APM_WaitKey(100, 0);*/

	LongBeep();
	aNextState = CTL_COMPLETE; // no fallback

	RSP_DATA.b_response = TRANS_REJ; // for print msg disable
	return aNextState;
}

//*****************************************************************************
//  Function        : CTLReferral
//  Description     : Voice Referral
//  Input           : N/A
//  Return          : FALSE => ERROR
//  Note            : Zero length response => decline
//  Globals Changed : N/A
//******************************************************************************
static BOOLEAN CTLReferral(void)
{
	PackRspText();
	DispLineMW(&RSP_DATA.text[1], MW_LINE1, MW_CENTER | MW_BIGFONT);

	APM_ResetComm();
	RSP_DATA.w_rspcode = 'C' * 256 + 'N';
	RSP_DATA.text[0] = 0;

	if (GetAuthCode())
	{
		INPUT.b_trans_status = OFFLINE;
		DispHeader(STIS_ISS_TBL(0).sb_card_name);
		RSP_DATA.b_response = TRANS_ACP;
	}
	else
		DispHeader(STIS_ISS_TBL(0).sb_card_name);

	if (RSP_DATA.b_response != TRANS_ACP)
	{
		LongBeep();
		return FALSE;
	}
	PackDTGAA();
	return TRUE;
}

//*****************************************************************************
//  Function        : CTLAccepted
//  Description     : Store CTL Data into Batch.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : gds.CTLdll, input;
//*****************************************************************************
static void CTLAccepted(void)
{
	TLIST *psLp;
	CL_IO s_CTLIn, s_CTLOut;
	BYTE sCtlBuf[256];
	BYTE bLen;

	s_CTLIn.pbMsg = s_CTLOut.pbMsg = sCtlBuf;

	RSP_DATA.b_response = TRANS_ACP;
	if ((bAction& CLACT_ONLINE) == 0)
	{
		memset(RSP_DATA.sb_auth_code, ' ', 6);
		memcpy(RSP_DATA.sb_auth_code, "Y1", 2);
		IncAPMTraceNo();
		memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no,
				sizeof(TX_DATA.sb_trace_no));
	}

	if ((psLp = TagSeek(gGDS->s_TagList, 0x9f26)) != NULL) // app Cryptogram
	{
		memcpy(INPUT.s_icc_data.sb_tag_9f26, psLp->pbLoc, psLp->sLen);
		INPUT.s_icc_data.b_tag_9f26_len = psLp->sLen;
	}
	memcpy(s_CTLIn.pbMsg, "\x4F\x00", 2); //AID for printing
	s_CTLIn.wLen = 2;
	if (emvclSetup(CLCMD_GET_TAGS, &s_CTLIn, &s_CTLOut) && (s_CTLOut.wLen > 2))
	{
		if (s_CTLOut.pbMsg[1] > 8)
		{
			memcpy(INPUT.s_icc_data.sb_aid, &s_CTLOut.pbMsg[2], 8);
			INPUT.s_icc_data.b_aid_len = 8;
		}
		else
		{
			memcpy(INPUT.s_icc_data.sb_aid, &s_CTLOut.pbMsg[2],
					s_CTLOut.pbMsg[1]);
			INPUT.s_icc_data.b_aid_len = s_CTLOut.pbMsg[1];
		}

	}

	psLp = CTLTagSeek(0x9F12); // Application Preferred Name - Jorge Numa
	if (psLp != NULL)
	{
		memset(INPUT.s_icc_data.sb_tag_9f12, 0x00,
				sizeof(INPUT.s_icc_data.sb_tag_9f12));
		if ((bLen = psLp->sLen) > 20)
			bLen = 20;
		memcpy(INPUT.s_icc_data.sb_tag_9f12, psLp->pbLoc, bLen);
		INPUT.s_icc_data.b_tag_9f12_len = bLen;

		//        printf( "\fAPN:<%s>", INPUT.s_icc_data.sb_tag_9f12 );
		//        APM_WaitKey(9000,0);
	}

	// if ((psLp = TagSeek(gGDS->s_TagList, 0xDFAE)) != NULL)
	// { // payment scheme
	// 	INPUT.s_icc_data.sb_aid[0] = psLp->pbLoc[0]; // reuse AID field
	// 	INPUT.s_icc_data.b_aid_len = 1;
	// }

	SetRspCode('0' * 256 + '0');
	PackCTLData(&INPUT.s_icc_data); // field 55 ICC data
	if (bAction& ACT_SIGNATURE)
		RSP_DATA.w_rspcode = 'V' * 256 + 'S';
	else
		RSP_DATA.w_rspcode = 'T' * 256 + 'A';

	if (TX_DATA.b_trans != VOID)
	{
		SaveRecord();
		IncRocNo(); // Jorge Numa 05-02-2013
		PackInputP();
		if (CTL_SIGN)
		{
			CTL_PRINT = TRUE;
		}
		else
		{
			TextColor("Desea imprimir recibo?:", MW_LINE4, COLOR_VISABLUE,
					MW_SPFONT | MW_CENTER | MW_CLRDISP, 0);
			DispLineMW("Si=ENTER    No=CNCL", MW_LINE9,
					MW_CENTER | MW_SMFONT | MW_REVERSE);

			if (APM_YesNo() == 2)
			{
				CTL_PRINT = TRUE;
			}
			else
			{
				CTL_PRINT = FALSE;
			}
		}

		MoveInput2Tx();
	}
	// if (!APM_BatchFull(INPUT.w_host_idx))
	// 	SaveRecord();
}

//*****************************************************************************
//  Function        : CTLComplete
//  Description     : Handle CTL COMPLETE response
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static void CTLComplete(void)
{
	BYTE buf[10], sCtlBuf[256];
	CL_IO s_CTLIn, s_CTLOut;
	BOOLEAN CTQ = FALSE;
	BYTE panBCD[10];

	memset(panBCD, 0x00, sizeof(panBCD));

	if (DeAss(gGDS->s_TagList, gGDS->s_CTLOut.pbMsg + 2,
			(WORD) (gGDS->s_CTLOut.wLen - 2)) == 0)
		CTLFatal(CLCMD_ERROR);
	//debugAuxCTL(gGDS->s_CTLOut.pbMsg, gGDS->s_CTLOut.wLen);

	ClearResponse();
	memcpy(&RSP_DATA.s_dtg.b_year, TagData(gGDS->s_TagList, 0x9A), 3);
	memcpy(&RSP_DATA.s_dtg.b_hour, TagData(gGDS->s_TagList, 0x9F21), 3);
	memset(RSP_DATA.sb_auth_code, ' ', sizeof(RSP_DATA.sb_auth_code));
	//RSP_DATA.w_rspcode = '5' * 256 + '1';
	RSP_DATA.w_rspcode = 'J' * 256 + 'E';
	RSP_DATA.b_response = TRANS_FAIL;

	DispHeader(NULL);
	//Disp2x16Msg(GetConstMsg(EDC_CTL_CARD_READ_OK), MW_LINE3, MW_BIGFONT);
	//DispLineMW("LECTURA OK", MW_LINE4, MW_CLREOL | MW_CENTER | MW_SMFONT);
	// DispLineMW("RETIRE TARJETEA", MW_LINE6, MW_CLREOL | MW_CENTER | MW_SMFONT);
	AcceptBeep();
	TextColor("LECTURA OK", COLOR_GREEN, MW_LINE4,
			MW_CLRDISP | MW_CENTER | MW_SMFONT, 0);
	TextColor("RETIRE TARJETA", COLOR_GREEN, MW_LINE6, MW_CENTER | MW_SMFONT,
			1);

	ValidCTLData(TRUE);

	// Seek tag (CTQ)--> JORGE NUMA
	CTL_SIGN = FALSE; // 03-04-13 Jorge Numa

	s_CTLIn.pbMsg = s_CTLOut.pbMsg = sCtlBuf;
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "CTQ=", 4);
	memcpy(s_CTLIn.pbMsg, "\x9F\x6C\x00", 3);
	s_CTLIn.wLen = 3;
	if (emvclSetup(CLCMD_GET_TAGS, &s_CTLIn, &s_CTLOut) && (s_CTLOut.wLen > 3))
	{
		split(buf + 4, s_CTLOut.pbMsg, s_CTLOut.wLen); // Esta retornando el TAG
		CTQ = TRUE;
	}

	// DEBUG
#if 0
	{
		DispLineMW(buf, MW_LINE7, MW_SPFONT);
		printf("\nLen:<%d>", s_CTLOut.wLen);
		APM_WaitKey(9000,0);
	}
#endif

	// NO RETORNA EL TAG O SI RETORNA Y PIDE PIN
	if (CTQ == FALSE || (s_CTLOut.pbMsg[3] & 0x80)) // 03-04-13 Jorge Numa
	{
		if (!getPinBlock(panBCD, TRUE)) // CVM Default -> Siempre pide PIN
		{
			RSP_DATA.w_rspcode = 'C' * 256 + 'P';   // Jorge Numa 23/10/2013
			return;
		}
	}
	else if (s_CTLOut.pbMsg[3] & 0x40)
	{
		// SIGN
		CTL_SIGN = TRUE;
	}
	else
	{
		// NO SIGN
		CTL_SIGN = FALSE;
	}

	// JJJ check offline approve here!!!
	if (bAction& CLACT_APPROVED)
	{
		//        bAction &= ~CLACT_APPROVED;    // clear the approved flag!!!
		//        bAction |= CLACT_ONLINE;        // force go online!!!
		LongBeep();
		RSP_DATA.w_rspcode = '5' * 256 + '1';
		return;
	}
	// JJJ

	if (bAction& CLACT_REFERRAL)
	{ // referral requested
		if (!CTLReferral())
			return;
		bAction |= CLACT_APPROVED;// so it will print & update batch
	}
	if (bAction& CLACT_ONLINE)
	{
		// online
		CL_LedSet(CTL_LED_ARQC);
		if (CTLOnline())
		{
			if (RSP_DATA.b_response == TRANS_ACP)
				bAction |= CLACT_APPROVED;
		}
	}

	if (bAction& CLACT_APPROVED)
	{
		INPUT.b_transAcep = TRUE; //NM-26/04/13 Agregue
		CTLAccepted();
	}
	else
	{
		// declined
		IncRocNo(); // Jorge Numa 04-06-13
		LongBeep();
	}
}

//*****************************************************************************
//  Function        : ePANExtract
//  Description     : Extract PAN data from CTL data.
//  Input           : N/A
//  Return          : FALSE => Error;
//  Note            : N/A
//  Globals Changed : gInput;
//*****************************************************************************
static BOOLEAN ePANExtract(BOOLEAN aCheckCard)
{
#define TRK2_SEPARATOR  'D'
	TLIST *psLp;
	BYTE bLen, bSepLen;

	if (DeAss(gGDS->s_TagList, gGDS->s_CTLOut.pbMsg + 2,
			gGDS->s_CTLOut.wLen - 2))
	{
		if ((psLp = TagSeek(gGDS->s_TagList, 0x57)) != NULL)
		{
			if ((bLen = psLp->sLen) * 2 <= sizeof(INPUT.s_trk2buf.sb_content))
			{
				memset(INPUT.s_trk2buf.sb_content, 'F',
						sizeof(INPUT.s_trk2buf.sb_content));
				split(INPUT.s_trk2buf.sb_content, psLp->pbLoc, bLen);

				INPUT.s_trk2buf.b_len = bLen * 2;
				INPUT.s_trk2buf.b_len = fndb(INPUT.s_trk2buf.sb_content, 'F',
						INPUT.s_trk2buf.b_len);

				bSepLen = fndb(INPUT.s_trk2buf.sb_content, TRK2_SEPARATOR,
						INPUT.s_trk2buf.b_len);

				if (bSepLen <= 19)
				{
					// Get PAN and expiry date from Track 2
					INPUT.s_trk2buf.sb_content[bSepLen] = 'F'; /* cater for odd len */
					memset(INPUT.sb_pan, 0xFF, 10);
					compress(INPUT.sb_pan, INPUT.s_trk2buf.sb_content,
							(BYTE) ((bSepLen + 1) / 2));
					INPUT.s_trk2buf.sb_content[bSepLen] = TRK2_SEPARATOR; /* cater for odd len */
					compress(INPUT.sb_exp_date,
							&INPUT.s_trk2buf.sb_content[1 + bSepLen], 2);
				}
				if ((INPUT.sb_pan[9] & 0x0F) == 0x0F)
				{
					if (!aCheckCard || (aCheckCard && InCardTableVisa(TRUE)))
						return TRUE;
				}
				RSP_DATA.w_rspcode = 'U' * 256 + 'C';
				return FALSE;
			}
		}
	}
	return FALSE;
}

//*****************************************************************************
//  Function        : ValidCTLData
//  Description     : Validate & Extract CTL Data
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : gGTS;
//*****************************************************************************
BOOLEAN ValidCTLData(BOOLEAN aCheckCard)
{
	TLIST * psLp;
	BYTE bLen;

	memset(INPUT.sb_holder_name, ' ', sizeof(INPUT.sb_holder_name)); // KC++ 29-05-2012

	if (ePANExtract(aCheckCard) == FALSE)
	{
		return FALSE;
	}

	if ((psLp = TagSeek(gGDS->s_TagList, 0x5F24)) != NULL)
	{ //App. Exp. Date
		memcpy(&INPUT.sb_exp_date, psLp->pbLoc, 2);
	}
	if ((psLp = TagSeek(gGDS->s_TagList, 0x5F20)) != NULL)
	{ //card holder name
		if ((bLen = psLp->sLen) > sizeof(INPUT.sb_holder_name))
			bLen = sizeof(INPUT.sb_holder_name);
		memcpy(INPUT.sb_holder_name, psLp->pbLoc, bLen);
	}

	memset(INPUT.s_icc_data.sb_label, ' ', sizeof(INPUT.s_icc_data.sb_label));
	psLp = TagSeek(gGDS->s_TagList, 0x50); // app label
	if (psLp != NULL)
	{
		if ((bLen = psLp->sLen) > 16)
			bLen = 16;
		memcpy(INPUT.s_icc_data.sb_label, psLp->pbLoc, bLen);
		INPUT.s_icc_data.b_tag_50_len = bLen;
	}

	return TRUE;
}

//*****************************************************************************
//  Function        : ClrCTLKey
//  Description     : Clear All Contactless Keys.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : Modified by Jorge Numa
//*****************************************************************************
void ClrCTLKey(void)
{
	if (!KernelReady(EMVCLDLL_ID))
		return;

	memset(gGDS->s_CTLIn.pbMsg, 0, 6);
	gGDS->s_CTLIn.wLen = 6;

	DoCTLSetup(CLCMD_KEY_LOADING); // clear all key
}

//*****************************************************************************
//  Function        : ClrCTLParam
//  Description     : Clear all Contactless dll's parameters.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : Modified by Jorge Numa
//*****************************************************************************
void ClrCTLParam(void)
{
	if (!KernelReady(EMVCLDLL_ID))
		return;

	os_config_read(K_CF_SerialNo, gGDS->s_CTLIn.pbMsg);
	gGDS->s_CTLIn.wLen = 8;

	DoCTLSetup(CLCMD_IFDSN_RW); // set IFDSN
	DoCTLSetup(CLCMD_CLR_AID); // clear AID parameters
}

//*****************************************************************************
//  Function        : DoSetup
//  Description     : Send the setup command to EMV Kernel.
//  Input           : aCmd;             // Setup Command
//  Return          : N/A
//  Note            :
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN DoCTLSetup(DWORD aCmd)
{
	if (emvclSetup(aCmd, &gGDS->s_CTLIn, &gGDS->s_CTLOut) == FALSE)
	{
		if (aCmd != CLCMD_READ_AIDIDX)
		{
			CTLFatal(aCmd);
		}
		else
			gGDS->s_CTLOut.wLen = 0;
		return FALSE;
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : CTLPreProcess
//  Description     : Pre-Processing for Contactless Txn.
//  Input           : N/A
//  Return          : next action
//  Note            : N/A
//  Globals Changed :
//*****************************************************************************
BOOLEAN CTLPreProcess(void)
{
	DWORD state;
	BYTE tmp[10];

	// init vars
	state = CLMSG_PROC_INIT;
	memcpy(gGDS->s_CTLIn.pbMsg + 1, "\x9F\x02\x06", 3);
	dbin2bcd(tmp, INPUT.dd_amount);
	memcpy(gGDS->s_CTLIn.pbMsg + 4, &tmp[4], 6);
	gGDS->s_CTLIn.wLen = 10;

	// card log from COM1
	/*
	 memcpy(&gGDS->s_CTLIn.pbMsg[gGDS->s_CTLIn.wLen], "\xDF\xF2\x01\x01\xDF\xF3\x01\x01", 8);
	 gGDS->s_CTLIn.wLen += 8;
	 */

	gGDS->s_CTLIn.pbMsg[0] = 0;
	if (emvclMsg(state, &gGDS->s_CTLIn, &gGDS->s_CTLOut) == FALSE)
	{
		CTLError(state);
		return FALSE;
	}
	//os_disp_backc(RGB_WHITE); //25-10-12 JC ++  // set white colour when reader active
	return TRUE;
}

//*****************************************************************************
//  Function        : CTLCompraTrans
//  Description     : Realiza la compra con Contactless
//  Input           : aCmd;             // Setup Command
//  Return          : N/A
//  Note            :
//  Globals Changed : N/A
//*****************************************************************************
DWORD CTLCompraTrans(BYTE aTrans)
{
	BYTE kbdbuf[31];
	//  DWORD ret = 0;
	DWORD keyin = 0;
	//BYTE panBCD[10];
	//	WORD idx_donacion;
	//BYTE tempDonacion[3];
	BYTE panAscii[20 + 1];

	INPUT.b_transAcep = FALSE; //NM-26/04/13 Agregue

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(panAscii, 0x00, sizeof(panAscii));
	memset(INPUT.sb_num_ref, 0x30, sizeof(INPUT.sb_num_ref)); //MFBC/29/04/13

	memset(&INPUT, 0, sizeof(INPUT));

	INPUT.b_trans = aTrans;

	ClearDispMW();

	//Captura monto
	if (!GetAmnt(true))
		return -1;

	if (iva_Comportamiento(INPUT.dd_amount) == FALSE)
	{
		APM_ResetComm(); //MFBC/22/05/13
		return -1;
	}

	//PROPINA
	INPUT.dd_amount += INPUT.dd_tip; //MFBC/22/05/13

	// NUMERO DE REFERENCIA
	if (!GetReferencia()) //MFBC/18/04/13
	{
		APM_ResetComm();
		return -1;
	}

	// DONACION
	if (GetDonacion() == FALSE) //MFBC/20/05/13 se valida donacion
	{
		APM_ResetComm();
		return -1;
	}

	//Ingresar cedula cliente
	if (!GetCedulaCliente()) //MFBC/18/04/13
	{
		APM_ResetComm();
		return -1;
	}

	//Ingresar cedula cajero
	if (!GetCedulaCajero()) //MFBC/18/04/13
	{
		APM_ResetComm();
		return -1;
	}

	INPUT.dd_amount += INPUT.dd_valor_efectivo; // Pendiente confirmar con Manuel


	if (!SelCuenta())
		return -1;

	if (CtlUnderTransLimit((DWORD) (INPUT.dd_amount))) 
	{
		if (CTLPreProcess() == FALSE)
		{
			if (RSP_DATA.w_rspcode == 'J' * 256 + 'E')
			{

				Short1Beep();
				TextColor("INSERTE / DESLICE", MW_LINE4, COLOR_VISABLUE,
						MW_CLRDISP | MW_CENTER | MW_SMFONT, 0);
				TextColor("TARJETA", MW_LINE5, COLOR_VISABLUE,
						MW_CENTER | MW_SMFONT, 3);
			}
			else
			{
				DispRspText(FALSE);
				APM_WaitKey(300, 0);
			}
			ResetTerm();
			return FALSE;
		}
	}
	else
	{
		LongBeep();
		TextColor("MONTO EXCEDIDO", MW_LINE4, COLOR_RED,
				MW_CLRDISP | MW_CENTER | MW_SMFONT, 0);
		TextColor("PARA CONTACTLESS", MW_LINE5, COLOR_RED,
				MW_CENTER | MW_SMFONT, 3);


		Short1Beep();
		TextColor("INSERTE / DESLICE", MW_LINE4, COLOR_VISABLUE,MW_CLRDISP | MW_CENTER | MW_SMFONT, 0);
		TextColor("TARJETA", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT,3);
		os_beep_close();
		ResetTerm();
		return FALSE;
	}
	// Contactless transaction
	// Set timer
	TimerSetMW(gTimerHdl[TIMER_DISP], 1500);

	if (INPUT.b_tipo_cuenta == 0x30 || INPUT.b_tipo_cuenta == 0x40) // Credito
	{
		if (!GetCuotas())
			return FALSE;
	}
	//os_disp_putc(K_ClrHome);
	// DispLineMW("ACERQUE LA TARJETA", MW_LINE1, MW_CENTER | MW_CLRDISP
	// 		| MW_SMFONT);
	TextColor("ACERQUE LA TARJETA", MW_LINE1, COLOR_VISABLUE,
			MW_CENTER | MW_CLRDISP | MW_SMFONT, 0);
	TextColor("MONTO:", MW_LINE8, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
	DispAmnt(INPUT.dd_amount, MW_LINE8, MW_SMFONT);
	do
	{
		SleepMW();
		if (keyin == MWKEY_CANCL) {
			return FALSE;
		}
		DispDefinedLogo(DP_LOGO_LANDINGZONE, FALSE);
		if (INPUT.dd_amount != 0
				&& CtlUnderTransLimit((DWORD) (INPUT.dd_amount)))
		{
			if (CTLWaitCard())
			{
				CTLTrans(aTrans);
				ResetTerm();
				CL_Close(0);
				return TRUE;
			}
		}
		keyin = APM_GetKeyin();

	} while (TimerGetMW(gTimerHdl[TIMER_DISP]) != 0);
	CL_Close(0);
	return FALSE;
}

//=============================================================================
//  Function        : CL_Led
//  Description     :
//  Input           : aIdOnOffCycle - Id | On | Off | Cycle
//  Return          :
//  Note            : N/A
//  Globals Changed : N/A
//=============================================================================
void CL_Led(BYTE *aIdOnOffCycle)
{
#ifndef WIN32 // simulator not supported yet
	BYTE bLedId;
	DWORD dOnOff, dCycle;
	switch (aIdOnOffCycle[0])
	{ //map to led id
	case 0: //blue
		bLedId = 1;
		break;
	case 1: //yellow
		bLedId = 2;
		break;
	case 2: //green
		bLedId = 0;
		break;
	case 3: //red
		bLedId = 3;
		break;
	default: //unknown, use red as default
		bLedId = 3;
		break;
	}
	if (aIdOnOffCycle[1] == 0xFF)
	{ //on
		os_rc531_led_set(bLedId);
	}
	else if (aIdOnOffCycle[2] == 0xFF)
	{ //off
		os_rc531_led_clear(bLedId);
	}
	else
	{ //flash
		dOnOff = ((DWORD) (aIdOnOffCycle[1] * 10) << 16)
																				| (DWORD) (aIdOnOffCycle[2] * 10); //change to 100ms unit
		dCycle = (aIdOnOffCycle[3] == 0xFF) ? 0xFFFFFF : aIdOnOffCycle[3];
		os_rc531_led_on(bLedId, dOnOff, dCycle);
	}

#endif // #ifndef WIN32 
}

//=============================================================================
//  Function        : CL_LedSet
//  Description     :
//  Input           : aSet
//                    
//  Return          :
//  Note            : N/A
//  Globals Changed : N/A
//=============================================================================
void CL_LedSet(DWORD aSet)
{
	switch (aSet)
	{
	case CTL_LED_IDLE:
		// all led off
		CL_Led("\x00\x00\xFF\x00");
		CL_Led("\x01\x00\xFF\x00");
		CL_Led("\x02\x00\xFF\x00");
		CL_Led("\x03\x00\xFF\x00");
		break;
	case CTL_LED_TAP_CARD:
		// blue led on
		CL_Led("\x00\xFF\x00\x00");
		CL_Led("\x01\x00\xFF\x00");
		CL_Led("\x02\x00\xFF\x00");
		CL_Led("\x03\x00\xFF\x00");
		break;
	case CTL_LED_PROC:
		// blue & yellow leds on
		CL_Led("\x00\xFF\x00\x00");
		CL_Led("\x01\xFF\x00\x00");
		CL_Led("\x02\x00\xFF\x00");
		CL_Led("\x03\x00\xFF\x00");
		break;
	case CTL_LED_TC:
		// blue & green leds on
		CL_Led("\x00\xFF\x00\x00");
		CL_Led("\x01\x00\xFF\x00");
		CL_Led("\x02\xFF\x00\x00");
		CL_Led("\x03\x00\xFF\x00");
		break;
	case CTL_LED_ARQC:
		// blue & green leds on
		CL_Led("\x00\xFF\x00\x00");
		CL_Led("\x01\x00\xFF\x00");
		CL_Led("\x02\xFF\x00\x00");
		CL_Led("\x03\x00\xFF\x00");
		break;
	case CTL_LED_AAC:
		// blue & red leds on
		CL_Led("\x00\xFF\x00\x00");
		CL_Led("\x01\x00\xFF\x00");
		CL_Led("\x02\x00\xFF\x00");
		CL_Led("\x03\xFF\x00\x00");
		break;
	case CTL_LED_RM_CARD:
		// blue & green leds on
		CL_Led("\x00\xFF\x00\x00");
		CL_Led("\x01\x00\xFF\x00");
		CL_Led("\x02\xFF\x00\x00");
		CL_Led("\x03\x00\xFF\x00");
		break;
	case CTL_LED_TRANS_IDLE:
		// all led off except blue led blinks
		CL_Led("\x00\x02\x32\xFF");
		CL_Led("\x01\x00\xFF\x00");
		CL_Led("\x02\x00\xFF\x00");
		CL_Led("\x03\x00\xFF\x00");
		break;
	default:
		break;
	}
}

/******************************************************************************
 *  Function        : debugCTL
 *  Description     : EMV debug printing routines.
 *  Input           : N/A
 *  Return          : N/A
 *  Note            : N/A
 *  Globals Changed : N/A
 ******************************************************************************
 */
void debugCTL(BYTE aState, BYTE aIn, CL_IO *pCTL)
{
	WORD i, len, max = 70;
	BYTE buf[64];

	SprintfMW(buf, "emvclMsg(%d)-%s\n", aState, aIn ? "In" : "Out");
	DispLineMW(buf, MW_LINE1, MW_CLRDISP | MW_SPFONT);
	len = pCTL->wLen;
	for (i = 1; i <= len; i++)
	{
		PrintfMW("%02X", (BYTE) pCTL->pbMsg[i - 1]);
		if ((i % max) == 0)
		{
			PrintfMW("..");
			APM_WaitKey(9000, 0);
			ClearDispMW();
			DispGotoMW(MW_LINE2, MW_SPFONT);
		}
	}
	APM_WaitKey(9000, 0);
}

//*****************************************************************************
//  Function        : CTLProcess
//  Description     : Contactless Processing state machine.
//  Input           : N/A
//  Return          : next action
//  Note            : N/A
//  Globals Changed :
//*****************************************************************************
DWORD CTLProcess(void)
{
	DWORD next_state;

	// turn on blue & yellow LEDs
	CL_LedSet(CTL_LED_PROC);

	// init vars
	struct APPDATA *Global;
	Global= (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(Global,&gAppDat,sizeof(struct APPDATA));

	memset(INPUT.sb_pin, 0, sizeof(INPUT.sb_pin));
	//	memcpy(INPUT.sb_trace_no, STIS_TERM_DATA.sb_trace_no, 3);
	memcpy(INPUT.sb_trace_no, Global->NumTrans, 3);

	FreeMW(Global);
	next_state = CLMSG_PROC_START | CLMSGX_AUTO_TO_FINISH;
	//next_state = CLMSG_PROC_START;
	gGDS->s_CTLIn.pbMsg[0] = 0;

	gGDS->s_CTLIn.wLen = 1;
	do
	{
		//				debugCTL(next_state, 1, &gGDS->s_CTLIn);
		if (emvclMsg(next_state, &gGDS->s_CTLIn, &gGDS->s_CTLOut) == FALSE)
		{
			CL_Close(0);
			next_state = CTLError(next_state);
			return next_state;
		}

		//debugCTL(next_state, 0, &gGDS->s_CTLOut);
		next_state = gGDS->s_CTLOut.pbMsg[0];

		switch (gGDS->s_CTLOut.pbMsg[0] & 0x7F)
		{
		case CTL_COMPLETE: /* completed */
			CL_Close(0);
			CTLComplete();
			// CTLDebug(); /* JJJ */
			return CTL_COMPLETE;
		case CLMSG_ENTER_SEL : // select application
			gGDS->s_CTLIn.pbMsg[0] = 0;
			gGDS->s_CTLIn.wLen = 1;
			break;
		case CLMSG_VALID_CARD : /* validate application */
			if (ValidCTLData(TRUE))
			{
				if (!ConfirmCard())
				{ // leave exp date chk to kernel
					CL_Close(0);
					RSP_DATA.w_rspcode = 'C' * 256 + 'N';
					return CTL_COMPLETE; // 25-04-05++ txn end
				}
			}
			gGDS->s_CTLIn.pbMsg[0] = 0;
			gGDS->s_CTLIn.wLen = 1;
			break;
		default:
			CTLFatal(CLCMD_ERROR);
		}
	} while (TRUE);
}

//*****************************************************************************
//  Function        : CTLTrans
//  Description     : CTL trans handler.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//******************************************************************************
DWORD CTLTrans(BYTE aTrans)
{
	//INPUT.b_trans = SALE_CTL;
	INPUT.b_trans = aTrans;
	/*
	 if (STIS_TERM_DATA.b_stis_mode < TRANS_MODE) {
	 DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
	 return FALSE;
	 }*/

	/*
	 if (BlockLocalTrans()) {
	 DispErrorMsg(GetConstMsg(EDC_TU_NOT_ALLOW));
	 return FALSE;
	 }
	 */

	//if (HostSettlePending() || APM_BatchFull(INPUT.w_host_idx)) {

	if (HostSettlePending())
	{
		DispErrorMsg(GetConstMsg(EDC_TU_CLOSE_BATCH));
		return FALSE;
	}

	INPUT.b_entry_mode = CONTACTLESS;
	RSP_DATA.b_response = TRANS_FAIL;
	RSP_DATA.w_rspcode = '5' * 256 + '1';
	GetEcrRef();

	DispHeader(NULL); // show correct header

	DispClrBelowMW(MW_LINE3);
	//Disp2x16Msg(GetConstMsg(EDC_CTL_PROCESSING), MW_LINE3, MW_BIGFONT);
	DispLineMW("PROCESANDO...", MW_LINE5, MW_CLRDISP | MW_CENTER | MW_SMFONT);

	INPUT.dd_amount -= INPUT.dd_donacion;

	CTLProcess();

	if (bAction& CLACT_APPROVED)
		CL_LedSet(CTL_LED_TC); // accept
	else
		CL_LedSet(CTL_LED_AAC);// decline

	if (RSP_DATA.w_rspcode != '0' * 256 + '0')
		TransEnd((BOOLEAN) (RSP_DATA.b_response == TRANS_ACP));

	return TRUE;
}
//=============================================================================
//  Function        : CL_WaitCard
//  Description     :
//  Input           : aTout10ms - timeout in 10ms
//                    aLvParam - LV param for open
//  Return          : >=0 - OK
//                    <0  - Err
//  Note            : N/A
//  Globals Changed : N/A
//=============================================================================
int CL_WaitCard(DWORD aTout10ms, BYTE *aLvParam)
{
#ifndef WIN32 // simulator not supported yet
	int iRet;
	BYTE bLen, abIStr[20];

	// turn on blue LED
	CL_LedSet(CTL_LED_TAP_CARD);

	//Param = [Len(1)] | [PollTime(2)] | [Timeout(2)] | [WtxTimeout(2) |..
	bLen = 0;
	abIStr[bLen++] = 0; // num of bytes - set to 0 first
	WPut(abIStr + bLen, aTout10ms); //poll time in 10ms
	bLen += 2;
	// set parameter if need
	if ((aLvParam != NULL ) && (aLvParam[0] > 0x02))
	{
		memcpy(abIStr + bLen, aLvParam + 3, aLvParam[0] - 2); //ignore Poll time
		bLen += (aLvParam[0] - 2);
	}
	abIStr[0] = bLen - 1; // update final len
	iRet = os_rc531_poll(abIStr);
	switch (iRet)
	{
	case K_ERR_RC531_OK:
		iRet = CL_OK;
		break;
	case K_ERR_RC531_NO_CARD:
		iRet = CL_TIMEOUT;
		break;
	case K_ERR_RC531_COLLISION:
		iRet = CL_MORE_CARDS;
		break;
	case K_ERR_RC531_TRANS:
	case K_ERR_RC531_PROTOCOL:
	case K_ERR_RC531_TIMEOUT:
	case K_ERR_RC531_INTERNAL:
	case K_ERR_RC531_NOT_READY:
	case K_ERR_RC531_INPUT:
	default:
		iRet = CL_ERR;
		break;
	}
	return iRet;

#endif // #ifndef WIN32 
}
//=============================================================================
//  Function        : CL_Close
//  Description     :
//  Input           : aWaitRemove10ms - If zero, no waiting and return ok.
//                      Other wait time, return ok if removed or err not yet remove
//  Return          : >=0 - OK
//                    <0  - Err
//  Note            : N/A
//  Globals Changed : N/A
//=============================================================================
int CL_Close(DWORD aWaitRemove10ms)
{
#ifndef WIN32 // simulator not supported yet
	int iRet;
	if (aWaitRemove10ms == 0)
	{ //PC1004 for reset command
		os_rc531_remove(1);
	}
	iRet = os_rc531_remove(aWaitRemove10ms);
	switch (iRet)
	{
	case K_ERR_RC531_OK:
		iRet = CL_OK;
		break;
	case K_ERR_RC531_NO_CARD:
	case K_ERR_RC531_COLLISION:
	case K_ERR_RC531_TRANS:
	case K_ERR_RC531_PROTOCOL:
	case K_ERR_RC531_TIMEOUT:
	case K_ERR_RC531_INTERNAL:
	case K_ERR_RC531_NOT_READY:
	case K_ERR_RC531_INPUT:
	default:
		iRet = CL_ERR;
		break;
	}
	//os_disp_backc(RGB_BACKGR);  //25-10-12 JC ++  // set backgd colour when close
	return iRet;

#endif // #ifndef WIN32 
}

//*****************************************************************************
//  Function        : CTLDebug
//  Description     : Debug contactless txn data
//  Input           : N/A
//  Return          : N/A
//  Note            : JJJ
//  Globals Changed :
//*****************************************************************************
void CTLDebug(void)
{
	BYTE buf[128], sCtlBuf[256];
	TLIST *psLp;
	CL_IO s_CTLIn, s_CTLOut;

	/*
	 if (INPUT.dd_amount != 123)   // only debug when amount is $1.23
	 return;
	 */

	SprintfMW(buf, "bAction=%02x", bAction);
	DispLineMW(buf, MW_LINE1, MW_CLRDISP | MW_SPFONT);
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "TVR=", 4);
	if ((psLp = TagSeek(gGDS->s_TagList, 0x95)) != NULL)
		split(buf + 4, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE2, MW_SPFONT);
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "TVR(Pwave)=", 11);
#if 1
	memcpy(gGDS->s_CTLIn.pbMsg, "\xDF\xC3\x00", 3); // Tag CLT_VT95
	gGDS->s_CTLIn.wLen = 3;
	s_CTLOut.pbMsg = sCtlBuf;
	if (emvclSetup(CLCMD_GET_TAGS, &gGDS->s_CTLIn, &s_CTLOut))
	{
		split(buf + 11, s_CTLOut.pbMsg, s_CTLOut.wLen);
		DispLineMW(buf, MW_LINE3, MW_SPFONT);
	}
#else
	if ((psLp = TagSeek(gGDS->s_TagList, 0xDFC3)) != NULL)
		split(buf+11, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE3, MW_SPFONT);
#endif

	// #ifdef PWAVE_DEBUG
#if 1
	// for Paywave only
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "TTQ=", 4);
	if ((psLp = TagSeek(gGDS->s_TagList, 0x9F66)) != NULL)
		split(buf + 4, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE7, MW_SPFONT);

	memset(buf, 0, sizeof(buf));
	memcpy(buf, "CTQ=", 4);
	memcpy(s_CTLIn.pbMsg, "\x9F\x6C\x00", 3);
	s_CTLIn.wLen = 3;

	if (emvclSetup(CLCMD_GET_TAGS, &s_CTLIn, &s_CTLOut) && (s_CTLOut.wLen > 3))
		split(buf + 4, s_CTLOut.pbMsg, s_CTLOut.wLen);

	DispLineMW(buf, MW_LINE8, MW_SPFONT);
	APM_WaitKey(9000, 0);
#else
	// for Paypass only
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "Txn.CVM=", 8);
	if ((psLp = TagSeek(gGDS->s_TagList, 0xDFB5)) != NULL)
		split(buf + 8, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE7, MW_SPFONT);
#endif
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "CID=", 4);
	if ((psLp = TagSeek(gGDS->s_TagList, 0x9F27)) != NULL)
		split(buf + 4, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE4, MW_SPFONT);
	// memset(buf, 0, sizeof(buf));
	// memcpy(buf, "TTQ=", 4);
	// if ((psLp = TagSeek(gGDS->s_TagList, 0x9F66)) != NULL)
	// 	split(buf + 4, psLp->pbLoc, psLp->sLen);
	// DispLineMW(buf, MW_LINE5, MW_SPFONT);
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "P.Sch=", 6);
	if ((psLp = TagSeek(gGDS->s_TagList, 0xDFAE)) != NULL)
		split(buf + 6, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE6, MW_SPFONT);
	// memset(buf, 0, sizeof(buf));
	// memcpy(buf, "Txn.CVM=", 8);
	// if ((psLp = TagSeek(gGDS->s_TagList, 0xDFB5)) != NULL)
	// 	split(buf + 8, psLp->pbLoc, psLp->sLen);
	// DispLineMW(buf, MW_LINE7, MW_SPFONT);

	memset(buf, 0, sizeof(buf));
	memcpy(buf, "Trk2=", 5);
	if ((psLp = TagSeek(gGDS->s_TagList, 0x57)) != NULL)
		split(buf + 5, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE8, MW_SPFONT);
	memset(buf, 0, sizeof(buf));
	memcpy(buf, "ExpDate=", 8);
	if ((psLp = TagSeek(gGDS->s_TagList, 0x5F24)) != NULL)
		split(buf + 8, psLp->pbLoc, psLp->sLen);
	DispLineMW(buf, MW_LINE9, MW_SPFONT);

	WaitKey(9000, 0);
}

//*****************************************************************************
//  Function        : CtlUnderTransLimit
//  Description     : CTL trans limit checking.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//******************************************************************************
BOOLEAN CtlUnderTransLimit(DWORD aAmount)
{
	DWORD idFileGen = -1;
	BYTE fileName[10];
	DWORD trans_limit = 0;
	struct CTLGEN ctl_gen;
	BYTE tmpAmount[13];

	memset(tmpAmount, 0x00, sizeof(tmpAmount));
	memset(&ctl_gen, 0, sizeof(struct CTLGEN));
	memset(fileName, 0x00, sizeof(fileName));
	memcpy(fileName, "CTLGEN", 6);
	idFileGen = fOpenMW(fileName);

	fSeekMW(idFileGen, 0);
	fReadMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN)); // Jorge Numa
	fCloseMW(idFileGen);

	memcpy(tmpAmount, ctl_gen.trans_limit, 12);
	LTrim(tmpAmount, '0');
	trans_limit = decbin8b(tmpAmount, strlen(tmpAmount));

	if (INPUT.dd_amount > trans_limit)
	{
		return FALSE;
	}

	return TRUE;
}

//*****************************************************************************
//  Function        : CTLWaitCard
//  Description     : Activate reader and wait for card.
//  Input           : N/A
//  Return          : next action
//  Note            : N/A
//  Globals Changed :
//*****************************************************************************
BOOLEAN CTLWaitCard(void)
{
#define TX_TIMEOUT    500         // 5 secs
	int iClRet;
	BYTE bRetry, abLvClParam[16];

	// init
	//Param = [Len(1)] | [PollTime(2)] | [Timeout(2)] | [WtxTimeout(2)] | ..
	memset(abLvClParam, 0x00, sizeof(abLvClParam));
	abLvClParam[0] = 6;
	//PollTime will be set by CL_WaitCard()!
	WPut(abLvClParam + 3, TX_TIMEOUT); //Timeout(2)
	WPut(abLvClParam + 5, TX_TIMEOUT); //WtxTimeout is min 5 sec
	//memcpy(abLvClParam, "\x06\x00\x00\x05\xDC\x01\xF4", 7);   // Pody

	// wait card tag
	bRetry = 0;
	// tag card
	iClRet = CL_WaitCard(0, abLvClParam);
	//iClRet = CL_WaitCard(0, NULL);
	if (iClRet == CL_OK)
	{ // card tagged
		return TRUE;
	}
	else if (iClRet == CL_MORE_CARDS)
	{
		if (bRetry++ >= 10)
		{
			DispLineMW("Too Many Cards!", MW_LINE5,
					MW_CLREOL | MW_CENTER | MW_BIGFONT);
			APM_WaitKey(9000, 0);
			return FALSE;
		}
	}
	else if (iClRet != CL_TIMEOUT)
	{
		if (bRetry++ >= 10)
		{
			return FALSE;
		}
	}

	return FALSE;
}

//=============================================================================
//  Function        : DispDefinedLogo
//  Description     : display and/or check pre-defined logo
//  Input           : 
//  Return          : -ve not support or err, 0 ok
//  Note            : N/A
//  Globals Changed : PC1205 new created
//=============================================================================
int DispDefinedLogo(DWORD aIdx, BOOLEAN aIsCheckAvailOnly)
{
	BYTE *pmLogo = NULL;

	switch (aIdx)
	{
	case DP_LOGO_LANDINGZONE:
#ifdef K_LandZoneSize
		if (aIsCheckAvailOnly)
		{
			return 0;
		}
		pmLogo = os_malloc(K_LandZoneSize+4);
		if (pmLogo == NULL)
		{
			return -1;
		}
		if (os_config_read(K_CF_LandingZone, pmLogo) == TRUE)
		{
			//os_disp_putc(K_ClrHome);
			os_disp_putg(pmLogo);
		}
		os_free(pmLogo);
		return 0;
#else
		break;
#endif
	default:
		break;
	}

	if (pmLogo != NULL)
	{
		os_free(pmLogo);
	}
	return -1;
}

//=============================================================================
//  Function        : CL_Init
//  Description     :
//  Input           : 
//  Return          : >=0 - OK
//                    <0  - Err
//  Note            : N/A
//  Globals Changed : N/A
//=============================================================================
BOOLEAN CL_Init(void)
{
#ifndef WIN32 // simulator not supported yet
	// clear all LEDs
	CL_LedSet(CTL_LED_IDLE);

	// open contactless interface
	if (os_rc531_open() == K_ERR_RC531_OK)
	{
		os_rc531_led_open();
		return TRUE;
	}
	else
	{
		//		DispLineMW("Terminal Sin ", MW_LINE5,
		//				MW_CLRDISP | MW_CENTER | MW_SMFONT);
		//		DispLineMW("Contactless", MW_LINE6, MW_CENTER | MW_SMFONT);
		Delay10ms(10);
		return FALSE;
	}

#endif // #ifndef WIN32 
}

//*****************************************************************************
//  Function        : CTLTagSeek
//  Description     : Search CTL tags from EMVCLDLL.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A;
//*****************************************************************************
TLIST *CTLTagSeek(WORD aTag)
{
	TLIST *psLp;
	BYTE *pMsg, sCtlBuf[256];
	CL_IO s_CTLIn, s_CTLOut;

	if ((psLp = TagSeek(gGDS->s_TagList, aTag)) != NULL)
		return psLp;

	s_CTLIn.pbMsg = s_CTLOut.pbMsg = sCtlBuf;
	memset(s_CTLIn.pbMsg, 0, sizeof(s_CTLIn.pbMsg));
	pMsg = s_CTLIn.pbMsg;
	pMsg = TagPut(pMsg, aTag);
	s_CTLIn.wLen = pMsg - s_CTLIn.pbMsg;
	if (emvclSetup(CLCMD_GET_TAGS, &s_CTLIn, &s_CTLOut))
	{
		// get valid 'V' if return len longer than 'T+L'
		if (s_CTLOut.wLen > (s_CTLIn.wLen + 1))
		{
			gTagList.wTag = aTag;
			gTagList.sLen = s_CTLOut.pbMsg[s_CTLIn.wLen];
			memcpy(gTagBuf, &s_CTLOut.pbMsg[s_CTLIn.wLen + 1],
					s_CTLOut.wLen - s_CTLIn.wLen - 1);
			gTagList.pbLoc = gTagBuf;
			return &gTagList;
		}
	}

	return NULL ;
}

