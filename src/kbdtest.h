//-----------------------------------------------------------------------------
//  File          : kbdtest.h
//  Module        :
//  Description   :
//  Author        : Lewis
//  Notes         :
//
//  Naming conventions
//  ~~~~~~~~~~~~~~~~~~
//             Constant : Leading K
//      Global Variable : Leading g
//    Function argument : Leading a
//       Local Variable : All lower case
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  04 Jun  2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _INC_KBDTEST_H_
#define _INC_KBDTEST_H_
#include <string.h>
#include "common.h"
#include "system.h"


//-----------------------------------------------------------------------------
//      Function API
//-----------------------------------------------------------------------------
extern void KeyPressTest(void);
extern void GetAuthPINTest(void);
extern void GetNonAuthPINTest(void);


#endif // _INC_KBDTEST_H_
