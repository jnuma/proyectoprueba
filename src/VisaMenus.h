/*
 * VisaMenus.h
 *
 *  Created on: 1/08/2012
 *      Author: Manuel Barbaran
 */

#ifndef VISAMENUS_H_
#define VISAMENUS_H_

#include "common.h"

extern void InitMenuVisa(void);
extern void MenuComercio(void);
extern int menuOperativoMaster (void);
extern int count_Items (const struct MENU_ITEM *ps_Menu);
extern void MenuAdministrativo(void);
extern BOOLEAN modoChequeMenu(BYTE *tipoCheque);
extern void tipoChequeMenu(void);

extern void habilitarEnvioPAN(void);
extern void habilitarEnvioSN(void);
extern void configurarEcrPos(void);

extern int MenuBancos(void);
extern void Pruebas (void);
extern void version(void);
extern DWORD menuOperRecarga(void);
extern BYTE menuOperTransporte(void);	//NM-15/05/13 Agregue esta funcion
extern void menuTransEspeciales (void);

#endif /* VISAMENUS_H_ */
