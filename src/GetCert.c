//-----------------------------------------------------------------------------
//  File          : GetCert.c
//  Module        :
//  Description   : Get Cert/key from file.
//  Author        : Kenneth Chan
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  10 Jul  2012  Kenneth     Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include "system.h"
#include "midware.h"
#include "apm.h"
#include "sysutil.h"
#include "util.h"
#include "x509.h"
#include "GetCert.h"

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Constant
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Globals
//-----------------------------------------------------------------------------


//*****************************************************************************
//  Function        : GetCAKey
//  Description     : Get CA key from file
//  Input           : aptr: CA key pointer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetCAKey(struct MW_KEY *aptr, BYTE *certSslName)
{
	T_Dir dirLink;
	T_DirEntry *entry;
	struct FILELIST list[10];
	char dirname[] = "/";

	int result;
	BYTE *content;
	BYTE filename[20];				// stored filename to be read locally
	int pfile_num = 0;
	int filesize = 0;
	int readlen = 0;
	int read_filehdr = -1;
	BYTE buf[32];

	memset(filename, 0, sizeof(filename));

	if (os_file_init_search(dirname, &dirLink) < 0)				// read number of files
		return FALSE;

	do {
		entry = (T_DirEntry *)os_file_readdir(&dirLink);		// read files one by one
		if (entry != NULL) {
			if (strstr((char const *)entry->s_name, certSslName)) {	// read para files only
				memcpy(list[pfile_num].filename, entry->s_name, sizeof(list[pfile_num].filename));
				list[pfile_num].len = entry->d_filelen;
				pfile_num++;
				break;
			}
		}
	} while (entry != NULL);

	if (pfile_num == 0)
	{
		LongBeep();
		DispLineMW("CERTIFICADO NO EXISTE", MW_LINE2, MW_CENTER|MW_SPFONT|MW_CLRDISP);
		APM_WaitKey(300, 0);
	}
	else
	{
		pfile_num--;
		memcpy(filename, dirname, 1);
		strcat(filename, list[pfile_num].filename);
		if ((read_filehdr = os_file_open(filename, K_O_RDONLY)) < 0)
		{
			LongBeep();
			DispLineMW("Open fail!", MW_LINE2, MW_LEFT|MW_SMFONT|MW_CLRDISP);
			os_file_close(read_filehdr);
			APM_WaitKey(300, 0);
			return FALSE;
		}
		else
		{
			/* getting file content */
			filesize = os_file_length(read_filehdr);
			content = (BYTE*) MallocMW(filesize);
			if (content == NULL) {
				os_file_close(read_filehdr);
				return FALSE;
			}
			readlen = os_file_read(read_filehdr, content, filesize);
			os_file_close(read_filehdr);
		}

		if (readlen != filesize) {
			FreeMW(content);
			return FALSE;
		}

		memset(aptr, 0, sizeof(struct MW_KEY));
		aptr->d_key_idx = 0xFF;              // extract cert index
		result = x509_parse_cert(content, filesize, (T_KEY *)aptr);
		if (result != K_X509Ok)
		{
			LongBeep();
			SprintfMW(buf, "X509 Key Parse Err: %d", result);
			DispLineMW(buf, MW_LINE2, MW_LEFT|MW_SMFONT|MW_CLRDISP);
			FreeMW(content);
			return FALSE;
		}

		FreeMW(content);
		return TRUE;
	}
	return FALSE;
}
