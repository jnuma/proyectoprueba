/*
 * Tecnico.c
 *
 *  Created on: 25/09/2012
 *      Author: Manuel Barbaran Conde
 */

#include "menu.h"
#include "Sysutil.h"
#include "Corevar.h"
#include "hardware.h"
#include <stdlib.h>
#include "util.h"
#include "tranutil.h"
#include "files.h"
#include "FUNC.H"
#include "IpConfig.h"
#include "Tecnico.h"
#include "input.h"
#include "Print.h"
#include "lptutil.h"
#include "testrans.h"
#include "init.h"
#include "settle.h"
#include "caKey.h" //Daniel SSL
#include "keytrans.h"

#ifdef FTPDL
#include "dl_ftp.h" //nohora
#endif


static const struct MENU_ITEM KConfComercioItems[] =
{
		{ 1, "Propina" },
		{ 2, "Numero de Caja" },
		{ 3, "Acumula Puntos" },
		{ 4, "Redimir Puntos" },
		{ 5, "Sugerir Cierre" },
		{ 6, "Auto Cierre" },
		{ 7, "Recarga Celular" },
		{ 8, "Hab. Aerolineas" },
		{ 9, "Hab. Envio de PAN" },
		{ 10, "Hab. Envio Tracks" },
		{ 11, "Hab. Avance Cuotas" },//
		{ 12, "Informe Polar" },
		{ 13, "Envio de Serial" },
		{ 14, "Recarga Trans." },
		{ 15, "Hab. Tarjeta Exito" },//
		{ 16, "Hab. PIN Avance" },//
		{ -1, NULL }, };

static const struct MENU_DAT KConfigComercioMenu =
{ "CONFIGURAR COMERCIO", KConfComercioItems, };


static const struct MENU_ITEM KConfItems[] =
{
		{ 1, "Conf. Host" },
		{ 2, "Conf. Pos" },
		{ 3, "Conf. Caja" },//Daniel Cajas TCP
		{ -1, NULL }, };

static const struct MENU_DAT KConfiMenu =
{ "CONEXION MENU", KConfItems, };

static const struct MENU_ITEM KParInitItems[] =
{
		{ 1, "Terminal ID" },
		{ 2, "Tel. Principal" },
		{ 3, "Tel. Alterno" },
		{ 4, "Prefijo" },
		{ 5, "Posfijo" },
		{ 6, "Tipo Marcado" },
		{ 7, "Tipo de Llamada" },
		{ -1, NULL }, };

static const struct MENU_DAT KParInitMenu =
{ "PARAM. INICIALES", KParInitItems, };



static const struct MENU_ITEM KCtlsItems[] =
{
		{ 1, "CTLS trans Limit" },
		{ 2, "CTLS CVM Limit" },
		//		{ 3, "Floor Limit" },
		{ -1, NULL }, };

static const struct MENU_DAT KCtlsMenu =
{ "EDITAR CTLS", KCtlsItems, };


static const struct MENU_ITEM KTPCallItems[] =
{
		{ 1, "NORMAL" },
		{ 2, "FORZADA" },
		{ -1, NULL }, };

static const struct MENU_DAT KTPCallMenu =
{ "TIPO DE LLAMADA", KTPCallItems, };

static const struct MENU_ITEM KTPMarcadotems[] =
{
		{ 1, "Tono" },
		{ 2, "Pulso" },
		{ -1, NULL }, };

static const struct MENU_DAT KTPMarcadoMenu =
{ "TIPO DE MARCADO", KTPMarcadotems, };

static const struct MENU_ITEM KConfHostItems[] =
{
		{ 1, "IP Host" },
		{ 2, "Puerto Host" },
		//{ 3, "Formato TX/RX" },
		//{ 4, "ID LAN" },
		{ 3, "Habilitar SSL" },//Daniel SSL
		{ -1, NULL }, };

static const struct MENU_DAT KConfiHostMenu =
{ "CONFIG HOST", KConfHostItems, };

static const struct MENU_ITEM KConfCajaItems[] = //Daniel Cajas TCP
{
		{ 1, "IP Caja" },
		{ 2, "Puerto Caja" },
		{ -1, NULL }, };

static const struct MENU_DAT KConfiCajaMenu = //Daniel Cajas TCP
{ "CONFIG CAJA", KConfCajaItems, };

static const struct MENU_ITEM KCargaLlavesItem[] =
{
		{ 1, "Llaves PIN y COMM" },
		{ 2, "Solo Llave PIN" },
		{ 3, "Solo Llave COMM" },
		{ -1, NULL }, };

static const struct MENU_DAT KCargaLlavesMenu =
{ "CARGA LLAVES 3DES", KCargaLlavesItem, };

static const struct MENU_ITEM KBorrarItem[] = //kt-161012
{
		{ 1, "Diario" },
		{ 2, "Reversos" },
		{ 3, "Inicializacion" },
		{ -1, NULL }, };

static const struct MENU_DAT KBorrarMenu = //kt-161012
{ "BORRAR", KBorrarItem, };

static const struct MENU_ITEM KConfiSSLItems[] = //Daniel SSL
{
		{ 1, "PARAM0" },
		{ 2, "PARAM1" },
		{ 3, "PARAM2" },
		{ 4, "PARAM3" },
		{ 5, "PARAM4" },
		{ -1, NULL }, };

static const struct MENU_DAT KSSLMenu = //Daniel SSL
{ "CERTIFICADOS SSL", KConfiSSLItems, };

static const struct MENU_ITEM KParamSpecial[] = //**SR** 17/12/13
{
		{ 1, "Num. Recibo" },
		{ 2, "Vel. Modem" },
		{ 3, "Paridad" },
		{ 4, "Vel. Caja" },
		{ -1, NULL }, };

static const struct MENU_DAT KMenuParamSpecial = //**SR** 17/12/13
{ "PARAMETROS ESPECIALES", KParamSpecial, };


//NM-22/04/13 Agregue este menu
#ifdef FTPDL
static const struct MENU_ITEM KMenuFtpItems[] =
{
		{ 1, "Rango Consulta" },
		/*
		{ 2, "Config. LAN" },
		{ 3, "Config. GPRS" },
		{ 4, "Config. DIAL" },
		{ 5, "DESCARGA FTP" },
		 */
		{ -1, NULL }, };


static const struct MENU_DAT KMenuFtp =
{ "MENU FTP", KMenuFtpItems, };

#endif

void EstadoSI(void) //kt-081012
{
	TextColor("ACTUAL: ", MW_LINE6, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("SI", MW_LINE6 + 8, COLOR_GREEN, MW_SMFONT, 0);
}

void EstadoNO(void) //kt-081012
{
	TextColor("ACTUAL: ", MW_LINE6, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("NO", MW_LINE6 + 8, COLOR_YELLOW, MW_SMFONT, 0);
}

void EstadoOpciones(BYTE *msj, BOOLEAN flagmsj) //kt-091012
{
	TextColor(msj, MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);

	if (flagmsj)
	{
		AcceptBeep();
		TextColor("Habilitado!!", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
	}
	else
	{
		LongBeep();
		TextColor("Deshabilitado!!", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
	}
}

void habilitarEnvioSerial(void)
{
	DWORD keyin;

	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	TextColor("Hab. Enviar Serial?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	displaySI_NO();

	if (gConfigComercio.habEnvioSerial == TRUE)
		EstadoSI();
	else
		EstadoNO();

	keyin = APM_YesNo();

	if (keyin == 2)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->habEnvioSerial = TRUE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Envio serial", TRUE);
		FreeMW(conf_comer);
	}
	else if (keyin == 1)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->habEnvioSerial = FALSE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Envio serial", FALSE);
		FreeMW(conf_comer);
		return;
	}
	else
	{
		FreeMW(conf_comer);
		return;
	}
}

void habilitarEnvioTracks(void)
{
	DWORD keyin;
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));
	TextColor("Hab. Envio Tracks?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);

	displaySI_NO();

	if (gConfigComercio.habEnvioTracks == TRUE)
		EstadoSI();
	else
		EstadoNO();

	keyin = APM_YesNo();

	if (keyin == 2)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->habEnvioTracks = TRUE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Envio de Tracks", TRUE);
		guardarBIN(4);
		FreeMW(conf_comer);
	}
	else if (keyin == 1)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->habEnvioTracks = FALSE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Envio de Tracks", FALSE);
		FreeMW(conf_comer);
		return;
	}
	else
	{
		FreeMW(conf_comer);
		return;
	}
}

void EditarPorcentajePropina(void)
{
	BYTE kbdbuff[3];
	BYTE Porcentaje[3];
	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(kbdbuff, 0x00, sizeof(kbdbuff));
	memset(Porcentaje, 0x00, sizeof(Porcentaje));

	TextColor("Editar Propina?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);

	displaySI_NO();

	sprintf(Porcentaje, "%02d", gAppDat.porcPropina);
	TextColor("Porcentaje:", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
	TextColor(Porcentaje, MW_LINE5 + 11, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("%", MW_LINE5 + 14, COLOR_VISABLUE, MW_SMFONT, 0);

	if (APM_YesNo() == 2)
	{
		TextColor("Digite Porcentaje", MW_LINE3, COLOR_VISABLUE, MW_BIGFONT | MW_LEFT | MW_CLRDISP, 0);

		if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE6, IMIN(1)
				+ IMAX(2), kbdbuff))
		{
			FreeMW(conf_AppData);
			return;
		}
		((struct APPDATA*) conf_AppData)->porcPropina = atoi(&kbdbuff[1]);

		SaveDataFile((struct APPDATA*) conf_AppData);
		FreeMW(conf_AppData);
		AcceptBeep();
		TextColor("Porcentaje Editado", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("Exitosamente!!", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
	}
	FreeMW(conf_AppData);
	return;
}

void habilitarEnvioPAN(void)
{

	struct CONFIG_COMERCIO *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(&conf_comer->habEnvioPan, &gConfigComercio.habEnvioPan, sizeof(struct CONFIG_COMERCIO));

	TextColor("Hab. Envio de PAN ?", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	TextColor("ACTUAL: ", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
	DisplayHabPAN();

	switch (gConfigComercio.habEnvioPan)
	{

	case 0:
		TextColor("NO", MW_LINE3 + 8, COLOR_RED, MW_SMFONT, 0);
		break;

	case 1:
		TextColor("SI", MW_LINE3 + 8, COLOR_YELLOW, MW_SMFONT, 0);
		break;

	case 2:
		TextColor("SI(*)", MW_LINE3 + 8, COLOR_GREEN, MW_SMFONT, 0);
		break;

	case 3:
		TextColor("SI(*)", MW_LINE3 + 8, COLOR_BLACK, MW_SMFONT, 0);
		break;

	default:
		break;

	}

	while(TRUE){
		switch (WaitKey(3000,0)) {

		case MWKEY_1:
			((struct CONFIG_COMERCIO*) conf_comer)->habEnvioPan = 1;
			AcceptBeep();
			TextColor("SI   ", MW_LINE3 + 8, COLOR_YELLOW, MW_SMFONT, 0);
			break;
		case MWKEY_2:
			((struct CONFIG_COMERCIO*) conf_comer)->habEnvioPan = 2;
			AcceptBeep();
			TextColor("SI(*)", MW_LINE3 + 8, COLOR_GREEN, MW_SMFONT, 2);
			break;
		case MWKEY_3:
			((struct CONFIG_COMERCIO*) conf_comer)->habEnvioPan = 3;
			AcceptBeep();
			TextColor("SI(*)", MW_LINE3 + 8, COLOR_BLACK, MW_SMFONT, 2);
			break;
		case MWKEY_0:
			((struct CONFIG_COMERCIO*) conf_comer)->habEnvioPan = 0;
			AcceptBeep();
			TextColor("NO   ", MW_LINE3 + 8, COLOR_RED, MW_SMFONT, 2);
			break;
		case MWKEY_CANCL:
			FreeMW(conf_comer);
			return;
			break;
		default:
			continue;
			break;
		}
		break;
	}

	SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);

	if (gConfigComercio.habEnvioPan == 1)
		guardarBIN(2);

	FreeMW(conf_comer);
	return;
}


////////////////////////////////////////////

void SugerirCierre(void)
{
	DWORD keyin;
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	TextColor("Sugerir Cierre?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	displaySI_NO();

	if (gConfigComercio.SugerirCierre == TRUE)
		EstadoSI();
	else
		EstadoNO();

	keyin = APM_YesNo();

	if (keyin == 2)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->SugerirCierre = TRUE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Sugerir Cierre", TRUE);
		FreeMW(conf_comer);
	}
	else if (keyin == 1)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->SugerirCierre = FALSE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Sugerir Cierre", FALSE);
		FreeMW(conf_comer);
		return;
	}
	else
	{
		FreeMW(conf_comer);
		return;
	}
}

////////////////////////////////////////////////////////

//MFBC/23/11/12
int AutoCierre(struct CONFIG_COMERCIO* conf_comer)
{
	int keyin;

	TextColor("Habilitar", MW_LINE3, COLOR_VISABLUE, MW_LEFT | MW_SMFONT | MW_CLRDISP, 0);
	TextColor("Autocierre?", MW_LINE4, COLOR_VISABLUE, MW_LEFT | MW_SMFONT, 0);
	displaySI_NO();

	keyin = APM_YesNo();

	if (keyin == 2)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->AutoCierre = TRUE;
		//SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Cierre automatico", TRUE);
	}
	else if (keyin == 1)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->AutoCierre = FALSE;
		//SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Cierre automatico", FALSE);
	}
	return keyin;
}


void InformePolar(void)
{
	DWORD keyin;
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	TextColor("Informe Polar?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	displaySI_NO();

	if (gConfigComercio.InformePolar == TRUE)
		EstadoSI();
	else
		EstadoNO();

	keyin = APM_YesNo();

	if (keyin == 2)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->InformePolar = TRUE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Informe polar", TRUE);
		FreeMW(conf_comer);
	}
	else if (keyin == 1)
	{
		((struct CONFIG_COMERCIO*) conf_comer)->InformePolar = FALSE;
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		EstadoOpciones("Informe polar", FALSE);
		FreeMW(conf_comer);
		return;
	}
	else
	{
		FreeMW(conf_comer);
		return;
	}
}


void BorrarLote(void) //kt-161012
{
	//struct TABLA_CERO tabla0;
	//	BYTE kbdbuf[9];
	//	BYTE PassAdmin[4];
	WORD rec_cnt;
	BYTE flagSett[16];         // Jorge Numa


	memset(flagSett, 0, sizeof(flagSett));  // Jorge Numa

	flagSett[1] = 0x31;  // Jorge Numa
	flagSett[1] -= '0';  // Jorge Numa

	//GetTablaCero(&tabla0);

	rec_cnt = APM_GetRecCount();
	if (rec_cnt == 0)
	{
		LongBeep();
		DispClrBelowMW(MW_LINE2);
		TextColor("NO EXISTEN TRANS", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
		return;

	}



	if(TransPassAdmin(MW_LINE2) == FALSE)
		return;

	//	DispClrBelowMW(MW_LINE2);
	//	TextColor("INGRESE PASSWORD", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
	//	if (!APM_GetKbdSpectra(HIDE_NUMERIC + MW_SMFONT + MW_LINE6, IMAX(4), kbdbuf))
	//		return;
	//
	//	compress(PassAdmin, &kbdbuf[1], 2);
	//	if (memcmp(&PassAdmin[0], &gTablaCero.b_pass_super[0], 2) != 0)
	//	{
	//		LongBeep();
	//		TextColor("PASSWORD INVALIDO", MW_LINE4, COLOR_RED, MW_SMFONT
	//				| MW_CENTER | MW_CLREOL, 0); //kt-220413
	//		TextColor("RE INTENTAR", MW_LINE5, COLOR_RED, MW_SMFONT
	//							| MW_CENTER | MW_CLREOL, 3);
	//		return;
	//	}

	CloseBatchVisa();
	DispLineMW("DIARIO", MW_LINE1, MW_SMFONT | MW_CENTER | MW_CLRDISP
			| MW_REVERSE);
	AcceptBeep();
	TextColor("Diario borrado", MW_LINE4, COLOR_GREEN, MW_SMFONT | MW_CENTER, 0);
	TextColor("Exitosamente!!", MW_LINE5, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);
	os_config_write(K_CF_Settled, &flagSett[1]); // Jorge Numa
	os_config_update(); // Jorge Numa
}

void BorrarReverso(void) //kt-161012
{
	struct TABLA_CERO tabla0;
	//	BYTE kbdbuf[9];
	//	BYTE PassAdmin[4];
	//WORD rec_cnt;

	GetTablaCero(&tabla0);
	DispClrBelowMW(MW_LINE2);

	if (GetPendingVISA(INPUT.w_host_idx) != REV_PENDING)
	{
		LongBeep();
		TextColor("NO HAY REVERSOS", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
		TextColor("PENDIENTES", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
		return;
	}



	if(TransPassAdmin(MW_LINE2) == FALSE)
		return;

	//	TextColor("INGRESE PASSWORD", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
	//
	//	if (!APM_GetKbdSpectra(HIDE_NUMERIC + MW_SMFONT + MW_LINE6, IMAX(4), kbdbuf))
	//		return;
	//
	//	compress(PassAdmin, &kbdbuf[1], 2);
	//
	//	DispClrBelowMW(MW_LINE2);
	//	if (memcmp(&PassAdmin[0], &gTablaCero.b_pass_super[0], 2) != 0)
	//	{
	//		LongBeep();
	//		TextColor("CLAVE INVALIDA", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER , 3);
	//		return;
	//	}
	DispClrBelowMW(MW_LINE2);
	SetPendingVISA(INPUT.w_host_idx, NO_PENDING);
	AcceptBeep();
	TextColor("Reverso borrado", MW_LINE4, COLOR_GREEN, MW_SMFONT | MW_CENTER, 0);
	TextColor("Exitosamente!!", MW_LINE5, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);
}

void MenuBorrar(void)
{
	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KBorrarMenu, select);

		if (select == -1)
			return;

		switch (select)
		{

		case 1:
			BorrarLote();
			break;

		case 2:
			BorrarReverso();
			break;

		case 3:
			VisaInitSet(TRUE);
			//CloseFilesInit();
			break;
		default:
			break;
		}
	}
}


static const struct MENU_ITEM KTecnicoItems[] = //kt-231112
{
		{ 1, "Conf. Conexion" },
		{ 2, "Param. Inicial" },
		{ 3, "Inicializacion" },
		{ 4, "Logon" },
		{ 5, "Interfaz Llaves" },
		{ 6, "Param. Especial" },
		{ 7, "Borrar" },
		{ 8, "Cfg. Comercio" },
		{ 9, "T. Virtual" },
		{ 10, "Key Index DIALUP" }, //Config. KIN
		{ 11, "Imp.Comer.Virt." },
		{ 12, "Bono obsequio" },
		{ 13, "Descargar Archivos" },
		{ 14, "Editar CTLS" },
		{ 15, "Descarga Remota" },
		{ -1, NULL }, };



//*****************************************************************************
//  Function        : MenuTecnico
//  Description     : Despliega Menu Tecnico.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

void MenuTecnico(void)
{
	BYTE *p_tabla12; //kt-231112
	BYTE idComerciotmp;
	int idxPrint = 0;
	int idxMaster = 0;
	BOOLEAN ActiveFTP = FALSE;
	static struct MENU_ITEM_DOS KAppsItems[20];
	static struct MENU_DAT_DOS KAppsMenu[1];

	memset(&KAppsItems, 0x00, 20);


	DispClrBelowMW(MW_LINE1);
	DispPutCMW(K_PushCursor);
	os_disp_textc(COLOR_VISAYELLOW);
	os_disp_backc(COLOR_BLACK);
	DispLineMW("TECNICO", MW_LINE1, MW_SPFONT | MW_CENTER);
	DispPutCMW(K_PopCursor);

	if(passAdminComercio(MW_LINE2) != TRUE)
	{
		IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
		return;
	}

	/*Esto es para saber si existe multicomercio*/
	/**************************************************************/
	p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12)); //kt-231112
	gIdTabla12 = OpenFile(KTabla12File);

	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);

	idComerciotmp = ((struct TABLA_12*) p_tabla12)->b_id;

	CloseFile(gIdTabla12);
	FreeMW(p_tabla12);
	/**************************************************************/

	while (TRUE)
	{
		if (KTecnicoItems[idxMaster].iID == 11 &&  idComerciotmp <= 0 )
			idxMaster++;

#ifdef FTPDL
		ActiveFTP = TRUE;
#endif

		if (KTecnicoItems[idxMaster].iID == 15 && ActiveFTP == FALSE )
			idxMaster++;

		if (KTecnicoItems[idxMaster].pcDesc == NULL)
			break;
		KAppsItems[idxPrint].iID = idxPrint + 1;
		KAppsItems[idxPrint].pcDesc = NULL;
		KAppsItems[idxPrint].iDX = KTecnicoItems[idxMaster].iID;
		memset(KAppsItems[idxPrint].scDescDat, 0x20, sizeof(KAppsItems[idxPrint].scDescDat));
		memcpy(KAppsItems[idxPrint].scDescDat, KTecnicoItems[idxMaster].pcDesc,	strlen(KTecnicoItems[idxMaster].pcDesc));
		idxPrint++;
		idxMaster++;
	}

	KAppsItems[idxPrint].iID = -1;
	KAppsItems[idxPrint].pcDesc = NULL;
	memcpy(KAppsMenu[0].scHeader, "TECNICO", 7);
	KAppsMenu[0].psMenuItem = KAppsItems;


	while (1)
	{
		os_beep_close();
		int select = 0;
		select = MenuDinamico(&KAppsMenu[0], select);

		if (select == -1)
		{
			IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
			return;
		}

		switch (select)
		{
		case 1:
			ConfigConexion(); // Conf. Conexion
			break;

		case 2:
			MenuParIniciales(); // Param. Inicial
			break;

		case 3:
			InitTrans(FALSE); //Inicializacion
			break;

		case 4:
			LogonTrans(FALSE); //Logon
			break;

		case 5:
			MenuCargaLlaves(); //Interfaz Llaves
			break;

		case 6:
			MenuParamEspecial();		//Param. Especial
			break;

		case 7:
			MenuBorrar(); //Borrar
			break;

		case 8:
			MenuConfigComercio(); //Cfg. Comercio
			break;

		case 9:
			configTarjetaVirtual(); //T. Virtual
			break;

		case 10:
			configKIN(); //Key Index DIALUP
			break;

		case 11:
			ReportComerciosVirtuales(); //Imp.Comer.Virt.
			break;

		case 12:
			//			cambioClaveBono(); //Bono obsequio
			break;

		case 13:
			DownloadFile(); //Descargar Archivos
			break;

		case 14:
			MenuEditCTLS(); //Editar CTLS
			break;

		case 15:
#ifdef FTPDL
			//        Test();
			MenuFtp();
#endif
			break;


		default:
			break;
		}
	}

	RefreshDispAfter(0);
	ResetTerm();
	return;
}

//*****************************************************************************
//  Function        : MenuConfigComercio
//  Description     : Despliega Menu para configuracion del comercio.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

void MenuConfigComercio(void)
{

	while (1)
	{
		int select = 0;

		select = MenuSelectVisa(&KConfigComercioMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			EditarPorcentajePropina(); // Propina
			break;

		case 2:
			editarNumCaja(); //Numero de caja
			break;

		case 3:
			break;

		case 4:
			break;

		case 5:
			SugerirCierre(); // sugerir
			break;

		case 6:
			Edit_Autocierre(); //autocierre
			break;

		case 7:
			break;

		case 8:
			break;

		case 9:
			habilitarEnvioPAN(); // habilitar envio de PAN
			break;

		case 10:
			habilitarEnvioTracks(); // habilitar envio de tracks
			break;

		case 11:
			break;

		case 12:
			InformePolar();// habilitar informe la polar
			break;

		case 13:
			habilitarEnvioSerial(); // habilitar envio de serial
			//AutoCierre();
			break;

		case 14:
			break;

		case 15:
			break;

		case 16:
			break;

		default:
			break;
		}
	}
}


static const struct MENU_ITEM KConfiConexItems[] =
{
		{ 1, "NII Recarga" },
		{ 2, "NII Pago Celular " },
		{ 3, "Tipo de Conexion" },
		{ 4, "Ethernet" },
		{ 5, "Conf. GPRS" },
		{ 6, "Impr. Params." },
		{ 7, "Ping"},
		{ -1, NULL }, };


void ConfigConexion(void)
{

	int idxPrint = 0;
	int idxMaster = 0;
	static struct MENU_ITEM_DOS KAppsItems[12];
	static struct MENU_DAT_DOS KAppsMenu[1];
	struct TABLA_CERO tabla_Cero; //MFBC/02/05/13
	GetTablaCero(&tabla_Cero);

	memset(&KAppsItems, 0x00, 12);

	while (TRUE)
	{

		if((KConfiConexItems[idxMaster].iID == 4 ) &&gAppDat.ConexionDIAL == TRUE )
			idxMaster++;

		if ((KConfiConexItems[idxMaster].iID == 5 ) &&  IsGRPS() == FALSE )
			idxMaster++;

		if (KConfiConexItems[idxMaster].pcDesc == NULL)
			break;
		KAppsItems[idxPrint].iID = idxPrint + 1;
		KAppsItems[idxPrint].pcDesc = NULL;
		KAppsItems[idxPrint].iDX = KConfiConexItems[idxMaster].iID;
		memset(KAppsItems[idxPrint].scDescDat, 0x20, sizeof(KAppsItems[idxPrint].scDescDat));
		memcpy(KAppsItems[idxPrint].scDescDat, KConfiConexItems[idxMaster].pcDesc,	strlen(KConfiConexItems[idxMaster].pcDesc));
		idxPrint++;
		idxMaster++;
	}

	KAppsItems[idxPrint].iID = -1;
	KAppsItems[idxPrint].pcDesc = NULL;
	memcpy(KAppsMenu[0].scHeader, "CONF. CONEXION", 14);
	KAppsMenu[0].psMenuItem = KAppsItems;


	while (1)
	{
		int select = 0;

		//	select = MenuSelectVisa(&KConexionMenu, select);
		select = MenuDinamico(&KAppsMenu[0], select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			EditNII();
			break;

		case 2:
			EditNII_Movil();
			break;

		case 3:
			MenuTipoConexion(); //Tipo de conexion
			break;

		case 4:
			MenuDevices(); // ethernet
			break;

		case 5:
			ConfigGPRS();
			//APM_MerchFunc(10); //Configuracion GPRS
			break;

		case 6:
			ImpresionParam(); // impresion de parametros
			break;


		case 7:
			//pendiente el ping
			Ping();
			break;

		default:
			break;
		}

		RefreshDispAfter(0);
		ResetTerm();
	}
}

void MenuDevices(void)
{
	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KConfiMenu, select);

		if (select == -1)
			return;

		switch (select)
		{

		case 1:
			MenuConfHost();
			break;

		case 2:
			TermIPFunc();
			break;

		case 3:
			MenuConfCaja();
			break;

		default:
			break;
		}
	}
}

//*****************************************************************************
//  Function        : MenuCargaLlaves
//  Description     : Despliega Menu para carga de llaves.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

void MenuCargaLlaves(void)
{

	while (1)
	{
		int select = 0;

		select = MenuSelectVisa(&KCargaLlavesMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			loadPinKeyAndCommKey();
			break;

		case 2:
			loadPinKey();
			break;

		case 3:
			loadCommKey();
			break;

		default:
			break;
		}
	}
}



static const struct MENU_ITEM KConfiTipConexItems[] =
{
		{ 1, "DialUP" },
		{ 2, "Ethernet" },
		{ 3, "GPRS" },
		{ -1, NULL }, };


void MenuTipoConexion(void)
{

	int idxPrint = 0;
	int idxMaster = 0;
	static struct MENU_ITEM_DOS KAppsItems[7];
	static struct MENU_DAT_DOS KAppsMenu[1];
	struct TABLA_CERO tabla_Cero; //MFBC/02/05/13
	GetTablaCero(&tabla_Cero);

	memset(&KAppsItems, 0x00, 7);

	while (TRUE)
	{
		if (KConfiTipConexItems[idxMaster].iID == 3 &&  IsGRPS() == FALSE )
			idxMaster++;


		if (KConfiTipConexItems[idxMaster].pcDesc == NULL)
			break;
		KAppsItems[idxPrint].iID = idxPrint + 1;
		KAppsItems[idxPrint].pcDesc = NULL;
		KAppsItems[idxPrint].iDX = KConfiTipConexItems[idxMaster].iID;
		memset(KAppsItems[idxPrint].scDescDat, 0x20, sizeof(KAppsItems[idxPrint].scDescDat));
		memcpy(KAppsItems[idxPrint].scDescDat, KConfiTipConexItems[idxMaster].pcDesc,	strlen(KConfiTipConexItems[idxMaster].pcDesc));
		idxPrint++;
		idxMaster++;
	}

	KAppsItems[idxPrint].iID = -1;
	KAppsItems[idxPrint].pcDesc = NULL;
	memcpy(KAppsMenu[0].scHeader, "TIPOS DE CONEXION", 17);
	KAppsMenu[0].psMenuItem = KAppsItems;



	while (1)
	{
		int select = 0;
		//	select = MenuSelectVisa(&KTipoConexionMenu, select);
		select = MenuDinamico(&KAppsMenu[0], select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			ConexionTipoDial();
			break;

		case 2:
			ConexionTipoLAN();
			break;

		case 3:
			ConexionTipoGPRS();
			break;

		default:
			break;

		}
		RefreshDispAfter(0);
		ResetTerm();
	}
}

void ConexionTipoDial(void)
{
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	TextColor("Conectar por DIAL ?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	displaySI_NO();

	if (APM_YesNo() == 2)
	{
		AcceptBeep();
		((struct APPDATA*) conf_AppData)->ConexionDIAL = TRUE;
		((struct APPDATA*) conf_AppData)->ConexionLAN = FALSE;
		((struct APPDATA*) conf_AppData)->ConexionGPRS = FALSE;

		SaveDataFile((struct APPDATA*) conf_AppData);
		EstadoOpciones("Conexion DIAL", TRUE);
	}
	FreeMW(conf_AppData);
	return;
}

void ConexionTipoLAN(void)
{
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	TextColor("Conectar por LAN?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	displaySI_NO();

	if (APM_YesNo() == 2)
	{
		AcceptBeep();
		((struct APPDATA*) conf_AppData)->ConexionDIAL = FALSE;
		((struct APPDATA*) conf_AppData)->ConexionLAN = TRUE;
		((struct APPDATA*) conf_AppData)->ConexionGPRS = FALSE;
		SaveDataFile((struct APPDATA*) conf_AppData);
		TextColor("Conexion LAN", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("Habilitada!!", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
	}
	FreeMW(conf_AppData);
	return;
}

void ConexionTipoGPRS(void)
{
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	TextColor("Conectar por GPRS?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);

	displaySI_NO();

	if (APM_YesNo() == 2)
	{

		((struct APPDATA*) conf_AppData)->ConexionDIAL = FALSE;
		((struct APPDATA*) conf_AppData)->ConexionLAN = FALSE;
		((struct APPDATA*) conf_AppData)->ConexionGPRS = TRUE;
		SaveDataFile((struct APPDATA*) conf_AppData);
		AcceptBeep();

		EstadoOpciones("Conexion GPRS", TRUE);

	}
	FreeMW(conf_AppData);
	return;
}

void MenuConfCaja(void)//Daniel Cajas TCP
{
	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KConfiCajaMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			ConfigIpCaja();
			break;

		case 2:
			ConfigPortCaja();
			break;

		default:
			break;

		}
	}
}

void MenuConfHost(void)
{
	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KConfiHostMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			ConfigIpHost();
			break;

		case 2:
			ConfigPortHost();
			break;

		case 3:
			configSSL();
			break;

		default:
			break;

		}
	}
}

void ConfigIpHost(void)
{
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	TextColor("Direccion IP", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	//ShowIp(((struct APPDATA*)conf_AppData)->IpHost , MW_LINE4, FALSE);
	ShowIp(gAppDat.IpHost, MW_LINE4, FALSE);

	if (!EditIp(((struct APPDATA*) conf_AppData)->IpHost, MW_LINE4))
	{
		FreeMW(conf_AppData);
		return;
	}
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
}

void ConfigPortHost(void)
{
	BYTE kbdbuf[8];
	BYTE Port[8];
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	TextColor("Numero de Puerto", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	sprintf(Port, "%05d", gAppDat.PortHost);
	LTrim(Port, 0x30);

	memcpy(&kbdbuf[1], Port, strlen(Port));
	kbdbuf[0] = strlen(Port);
	if (!APM_GetKbdSpectra(NUMERIC_INPUT | ECHO | MW_SMFONT | MW_LINE6,
			+ IMAX(5), kbdbuf))
	{
		FreeMW(conf_AppData);
		return;
	}
	((struct APPDATA*) conf_AppData)->PortHost = atoi(&kbdbuf[1]);

	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);

}

void ConfigIpCaja(void)//Daniel Cajas TCP
{
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	TextColor("Direccion IP", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	//ShowIp(((struct APPDATA*)conf_AppData)->IpHost , MW_LINE4, FALSE);
	ShowIp(gAppDat.IpCaja, MW_LINE4, FALSE);

	if (!EditIp(((struct APPDATA*) conf_AppData)->IpCaja, MW_LINE4))
	{
		FreeMW(conf_AppData);
		return;
	}
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
}

void ConfigPortCaja(void)//Daniel Cajas TCP
{
	BYTE kbdbuf[8];
	BYTE Port[8];
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	TextColor("Numero de Puerto", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_LEFT | MW_CLRDISP, 0);
	sprintf(Port, "%05d", gAppDat.PortCaja);
	LTrim(Port, 0x30);

	memcpy(&kbdbuf[1], Port, strlen(Port));
	kbdbuf[0] = strlen(Port);
	if (!APM_GetKbdSpectra(NUMERIC_INPUT | ECHO | MW_SMFONT | MW_LINE6,
			+ IMAX(5), kbdbuf))
	{
		FreeMW(conf_AppData);
		return;
	}
	((struct APPDATA*) conf_AppData)->PortCaja = atoi(&kbdbuf[1]);

	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
}

void EditNII(void)
{
	BYTE kbdbuf[6];
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	TextColor("NII Recarga", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_LEFT| MW_CLRDISP, 0);


	TextColor("Valor NII", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 0);

	split(&kbdbuf[1], gAppDat.NII, 2);
	kbdbuf[0] = 4;
	if (!APM_GetKbdSpectra(NUMERIC_INPUT | ECHO | MW_SMFONT | MW_LINE6, IMIN(4)
			+ IMAX(4), kbdbuf))
	{
		FreeMW(conf_AppData);
		return;
	}
	compress(((struct APPDATA*) conf_AppData)->NII, &kbdbuf[1], 2);
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	//printf("\f nuevo NII= %x%x",gAppDat.NII[0], gAppDat.NII[1]); WaitKey(3000, 0);
}


void EditNII_Movil(void)
{
	BYTE kbdbuf[6];
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));

	TextColor("NII Pago Celular", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_LEFT| MW_CLRDISP, 0);

	TextColor("Valor NII", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 0);

	split(&kbdbuf[1], gAppDat.NII_Moviles, 2);
	kbdbuf[0] = 4;
	if (!APM_GetKbdSpectra(NUMERIC_INPUT | ECHO | MW_SMFONT | MW_LINE6, IMIN(4)
			+ IMAX(4), kbdbuf))
	{
		FreeMW(conf_AppData);
		return;
	}
	compress(((struct APPDATA*) conf_AppData)->NII_Moviles, &kbdbuf[1], 2);
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	//printf("\f nuevo NII= %x%x",gAppDat.NII[0], gAppDat.NII[1]); WaitKey(3000, 0);
}

void Edit_Terminal(void)
{
	BYTE kbdbuf[17];
	BYTE termTemp[17];
	BOOLEAN reset;
	//WORD rec_cnt; // MFBC 11-10-2012
	//WORD posicion = 0;
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//	memset(gAppDat.TermNo, 0x00, sizeof(gAppDat.TermNo)); //MFBC/04/12/12
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(termTemp, 0x00, sizeof(termTemp));


	if (check_lote() == TRUE)
	{
		LongBeep();
		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
		FreeMW(conf_AppData);
		return;
	}

	while(TRUE)
	{
		memset(kbdbuf, 0x00, sizeof(kbdbuf));
		memset(termTemp, 0x00, sizeof(termTemp));

		DispClrBelowMW(MW_LINE2);

		TextColor("Terminal ID", MW_LINE3, COLOR_VISABLUE, MW_CENTER | MW_SMFONT , 0);

		//memcpy(&kbdbuf[1], gAppDat.TermNo, 8);
		//		memcpy(termTemp, gAppDat.TermNo, strlen(gAppDat.TermNo));
		memcpy(termTemp, gAppDat.TermNo, 8);
		//LTrim(termTemp, '0');
		memcpy(&kbdbuf[1], termTemp, strlen(termTemp));
		kbdbuf[0] = strlen(termTemp);

		if (!APM_GetKbdSpectra(ALPHA_INPUT | ECHO | MW_SMFONT | MW_LINE5, IMAX(8), kbdbuf))
		{
			FreeMW(conf_AppData);
			return;
		}


		if( kbdbuf[0] != 8)
			continue;

		memset(termTemp, 0x00, sizeof(termTemp));
		memcpy(termTemp, &kbdbuf[1], kbdbuf[0]);

		memset(kbdbuf, 0x00, sizeof(kbdbuf));

		DispClrBelowMW(MW_LINE5);
		TextColor("Confirmar Terminal", MW_LINE7, COLOR_VISABLUE, MW_CENTER | MW_SMFONT , 0);

		if (!APM_GetKbdSpectra(ALPHA_INPUT | ECHO | MW_SMFONT | MW_LINE5, IMIN(
				8) + IMAX(8), kbdbuf))
		{
			FreeMW(conf_AppData);
			return;
		}


		if(memcmp(termTemp, &kbdbuf[1], 8) != 0)
		{
			DispClrBelowMW(MW_LINE2);
			LongBeep();
			TextColor("REINGRESO TID", MW_LINE3, COLOR_RED, MW_CENTER | MW_SMFONT , 0 );
			TextColor("INCORECTO!",  MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT, 3);
			continue;
		}
		else
			break;

	}

	DispClrBelowMW(MW_LINE7);
	AcceptBeep();
	TextColor("TERMINAL ID ACEPTADO", MW_LINE7, COLOR_VISABLUE, MW_CENTER |MW_SMFONT, 3);
	//	memset(termTemp, 0x30, sizeof(termTemp)); //MFBC/26/11/12  no colocar los ceros antes del numero de la terminal y no visualizarlos, si se envian
	//	posicion = (8 - kbdbuf[0]);
	//	memcpy(&termTemp[posicion], &kbdbuf[1], kbdbuf[0]);
	//	memcpy(((struct APPDATA*) conf_AppData)->TermNo, termTemp, 8);


	// Jorge Numa 15/10/2013
	if( memcmp(((struct APPDATA*)conf_AppData)->TermNo, "\x00\x00\x00\x00\x00\x00\x00\x00", 8) == 0 )
	{
		reset = FALSE;
	}
	else
	{
		reset = TRUE;
	}

	if( memcmp(((struct APPDATA*)conf_AppData)->TermNo, &kbdbuf[1], 8) == 0 )
	{
		FreeMW(conf_AppData);
		return;
	}

	Short2Beep();
	TextColor("DEBE INICIALIZAR", MW_LINE4, COLOR_RED, MW_CLRDISP|MW_CENTER|MW_SMFONT, 0);
	TextColor("LA TERMINAL", MW_LINE5, COLOR_RED, MW_CENTER |MW_SMFONT, 3);

	memcpy(((struct APPDATA*)conf_AppData)->TermNo, &kbdbuf[1], 8);
	((struct APPDATA*) conf_AppData)->estadoInit = FALSE;// MFBC 11-10-2012
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);

	if (reset == TRUE) {
		ResetKey(MKEY_IDX);
	}

}


void TelEdit(BOOLEAN PrinOrSecTel)
{
	//DWORD rec_cnt = 0;
	BYTE kbdbuf[24 + 1];
	BYTE Telf[6 + 1];
	BYTE mostrarTel[12 + 1];
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*) conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(mostrarTel, 0x00, sizeof(mostrarTel));
	memset(Telf, 0xFF, sizeof(Telf));

	DispCtrlMW(MW_CLR_DISP);
	//	rec_cnt = APM_GetRecCount();
	//	if (rec_cnt != 0) //MFBC/07/02/13
	//	{
	//		LongBeep();
	//		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
	//		FreeMW(conf_AppData);
	//		return ;
	//	}

	if (check_lote() == TRUE)
	{
		LongBeep();
		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
		FreeMW(conf_AppData);
		return ;

	}

	if (PrinOrSecTel == TRUE)
	{
		TextColor("Tel. Principal", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT | MW_CLRDISP, 0);

		split(mostrarTel, gAppDat.TelPrincipal, 6);
		//printf("\f telSplit %s", mostrarTel); WaitKey(3000, 0);
		//LTrim(mostrarTel, 0x20);
		RTrim(mostrarTel, 'F');
		memcpy(&kbdbuf[1], mostrarTel, strlen(mostrarTel));
		kbdbuf[0] = strlen(mostrarTel);

		if (!APM_GetKbdSpectra(NUMERIC_INPUT | ECHO | MW_SMFONT | MW_LINE6,
				IMIN(7) + IMAX(12), kbdbuf))
		{
			FreeMW(conf_AppData);
			return;
		}
		if (kbdbuf[0] % 2 != 0)
		{
			memcpy(&kbdbuf[(kbdbuf[0] + 1)], "F", 1);
			kbdbuf[0]++;
		}

		compress(&Telf[0], &kbdbuf[1], kbdbuf[0] / 2);
		//memset(mostrarTel, 0x00, sizeof(mostrarTel));
		//split(mostrarTel, Telf, 6);

		((struct APPDATA*) conf_AppData)->estadoInit = FALSE; //MFBC/23/11/12
		memcpy(((struct APPDATA*) conf_AppData)->TelPrincipal, Telf, 6);
		SaveDataFile((struct APPDATA*) conf_AppData);
	}

	else
	{
		TextColor("Tel. Secundario", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT | MW_CLRDISP, 0);

		split(mostrarTel, gAppDat.TelSecundario, 6);
		//LTrim(mostrarTel, 0x20);
		RTrim(mostrarTel, 'F');
		memcpy(&kbdbuf[1], mostrarTel, strlen(mostrarTel));
		kbdbuf[0] = strlen(mostrarTel);

		if (!APM_GetKbdSpectra(NUMERIC_INPUT | ECHO | MW_SMFONT | MW_LINE6,
				IMIN(7) + IMAX(12), kbdbuf))
		{
			FreeMW(conf_AppData);
			return;
		}

		if (kbdbuf[0] % 2 != 0)
		{
			memcpy(&kbdbuf[(kbdbuf[0] + 1)], "F", 1);
			kbdbuf[0]++;
		}
		compress(&Telf[0], &kbdbuf[1], kbdbuf[0] / 2);

		((struct APPDATA*) conf_AppData)->estadoInit = FALSE; //MFBC/23/11/12
		memcpy(((struct APPDATA*) conf_AppData)->TelSecundario, Telf, 6);
		SaveDataFile((struct APPDATA*) conf_AppData);
	}
	FreeMW(conf_AppData);
}

void MenuParIniciales(void)
{

	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KParInitMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			Edit_Terminal();
			break;

		case 2:
			TelEdit(TRUE);
			break;

		case 3:
			TelEdit(FALSE);
			break;

		case 4:
			Prefijo();
			break;

		case 5:
			posFijo();
			break;

		case 6:
			TipoMarcado();
			break;

		case 7:
			TipoLlamada();
			break;

		default:
			break;

		}
	}
}

void ImpresionParam(void)
{
	BYTE auxiliar[20];
	BYTE Ip[12];
	BYTE dhcp;
	//BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12

	//GetAppdata((struct APPDATA*) conf_AppData);

	memset(auxiliar, 0x00, sizeof(auxiliar));
	memset(Ip, 0x00, sizeof(Ip));
	GetSysCfgMW(MW_SYSCFG_DHCP_ENABLE, &dhcp);

	GetSysCfgMW(MW_SYSCFG_IP, &Ip[0]);
	GetSysCfgMW(MW_SYSCFG_NETMASK, &Ip[4]);
	GetSysCfgMW(MW_SYSCFG_GATEWAY, &Ip[8]);

	//printf("\f  dhcp %d", dhcp); WaitKey(300,0);
	MsgBufSetup();
	pack_space(10);
	pack_mem(MW_LPT_FONT_SMALL, 3);
	pack_str("CONFIGURACION ETHERNET");
	pack_lf();
	pack_str("IP HOST : ");
	sprintf(auxiliar, "%d.%d.%d.%d", gAppDat.IpHost[0], gAppDat.IpHost[1], gAppDat.IpHost[2], gAppDat.IpHost[3]);
	pack_str(auxiliar);
	pack_lf();
	pack_str("PUERTO HOST : ");
	memset(auxiliar, 0x00, sizeof(auxiliar));
	sprintf(auxiliar, "%07d", gAppDat.PortHost);
	pack_str(auxiliar);
	pack_lf();
	pack_str("Utilizar DHCP : ");
	if (dhcp != 0)
		pack_str("SI");
	else
		pack_str("NO");
	pack_lf();
	pack_str("IP LOCAL : ");
	memset(auxiliar, 0x00, sizeof(auxiliar));
	sprintf(auxiliar, "%d.%d.%d.%d", Ip[0], Ip[1], Ip[2], Ip[3]);
	pack_str(auxiliar);
	pack_lf();
	pack_str("IP SUBMASK : ");
	memset(auxiliar, 0x00, sizeof(auxiliar));
	sprintf(auxiliar, "%d.%d.%d.%d", Ip[4], Ip[5], Ip[6], Ip[7]);
	pack_str(auxiliar);
	pack_lf();
	pack_str("IP DNS1 : ");
	pack_str("0.0.0.0");  //MFBC/22/04/13
	pack_lf();
	pack_str("IP DNS2 : ");
	pack_str("0.0.0.0");  //MFBC/22/04/13
	pack_lf();
	pack_str("IP GATEWAY :");
	memset(auxiliar, 0x00, sizeof(auxiliar));
	sprintf(auxiliar, "%d.%d.%d.%d", Ip[8], Ip[9], Ip[10], Ip[11]);
	pack_str(auxiliar);
	pack_lf();
	pack_str("Habilitar SSL : ");
	if(gConfigComercio.habSSL == TRUE)
		pack_str("SI"); //MFBC/22/04/13
	else
		pack_str("NO"); //MFBC/22/04/13
	pack_lfs(6); //MFBC/22/04/13
	//FreeMW(conf_AppData);
	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	MsgBufSetup();
	PackMsgBufLen();


}

//Daniel 01/11/12
void configTarjetaVirtual(void) //MFBC/22/04/13  cambiar toda la funcion
{
	BYTE kbdbuf[20];
	BYTE Anio;
	BYTE Mes;
	BYTE *ptrTarjVirtual;

	ptrTarjVirtual = (void *) MallocMW(sizeof(struct TARJETA_VIRTUAL));
	memcpy(ptrTarjVirtual, &gTarjVirtual, sizeof(struct TARJETA_VIRTUAL));

	memset(kbdbuf, 0x00, sizeof(kbdbuf));

	DispLineMW("TARJETA VIRTUAL", MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);

	kbdbuf[0] = strlen(gTarjVirtual.PAN);
	memcpy(&kbdbuf[1], gTarjVirtual.PAN, 16);
	//	TextColor("Digite el PAN:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
	TextColor("Tarjeta Virtual:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);

	if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE5, IMAX(16) , kbdbuf))
	{
		FreeMW(ptrTarjVirtual);
		return;
	}
	//memcpy(gTarjVirtual.PAN, &kbdbuf[1], kbdbuf[0]);

	if (kbdbuf[0] != 0)
	{
		memcpy(((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->PAN, &kbdbuf[1], kbdbuf[0]);
		((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->Flag_pan = TRUE;
	}
	else
	{
		memset(((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->PAN, 0x00, sizeof(((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->PAN));
		((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->Flag_pan = FALSE;
	}

	guardarTarjVirtual((struct TARJETA_VIRTUAL*) ptrTarjVirtual);


	while (TRUE)
	{
		DispClrBelowMW(MW_LINE2);
		memset(kbdbuf, 0x00, sizeof(kbdbuf));
		kbdbuf[0] = 4;
		memcpy(&kbdbuf[1], gTarjVirtual.fechaVenc, 4);
		TextColor("Fecha de vencimiento", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
		TextColor("(AAMM):", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);

		if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE6,  IMAX(4), kbdbuf))
		{
			FreeMW(ptrTarjVirtual);
			return;
		}
		//memcpy(gTarjVirtual.fechaVenc, &kbdbuf[1], kbdbuf[0]);

		if(kbdbuf[0] != 0)
		{

			compress(&Anio, &kbdbuf[1], 1);
			compress(&Mes, &kbdbuf[3], 1);

			if(!expT_virtual( Anio, Mes))
			{
				DispClrBelowMW(MW_LINE2);
				LongBeep();
				TextColor("Fecha Invalida", MW_LINE3, COLOR_RED, MW_SMFONT | MW_CENTER, 3);
			}
			else
			{
				memcpy(((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->fechaVenc, &kbdbuf[1], kbdbuf[0]); //MFBC/14/11/12
				((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->Flag_fecha = TRUE;
				break;
			}

		}
		else
		{
			memset(((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->fechaVenc, 0x00, sizeof(gTarjVirtual.fechaVenc) );
			((struct TARJETA_VIRTUAL*) ptrTarjVirtual)->Flag_fecha = FALSE;
			break;
		}

	}

	guardarTarjVirtual((struct TARJETA_VIRTUAL*) ptrTarjVirtual);
	FreeMW(ptrTarjVirtual);
}

void configKIN(void)
{
	int indice = 0;
	BYTE kbdbuf[20];
	BYTE bufTemp[20];
	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*)conf_AppData);

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(bufTemp, 0x00, sizeof(bufTemp));

	DispLineMW("Key Index DIALUP", MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);

	memcpy(bufTemp, gAppDat.KIN, 3);
	LTrim(bufTemp,'0');
	kbdbuf[0] = strlen(bufTemp);

	memcpy(&kbdbuf[1], bufTemp, strlen(bufTemp));

	TextColor("Nuevo Index", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);

	if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE5, IMIN(1) + IMAX(2), kbdbuf))
	{
		FreeMW(conf_AppData);
		return;
	}

	memset(bufTemp, 0x30, sizeof(bufTemp));
	indice = 3 - kbdbuf[0];
	memcpy(&bufTemp[indice], &kbdbuf[1], kbdbuf[0]);
	//sprintf(((struct APPDATA*) conf_AppData)->KIN, "%03s", bufTemp);
	memcpy(((struct APPDATA*) conf_AppData)->KIN, bufTemp, sizeof(gAppDat.KIN));
	SaveDataFile((struct APPDATA*) conf_AppData);
	//memcpy(gAppDat.KIN, &kbdbuf[1], kbdbuf[0]);
	//SaveDataFile(&gAppDat);
	FreeMW(conf_AppData);
}

void Edit_Autocierre(void) //MFBC/23/11/12
{
	BYTE kbdbuf[20];
	BYTE Dia[2 + 1];
	BYTE Hora[2 + 1];
	BYTE Minutos[2 + 1];
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(Dia, 0x00, sizeof(Dia));
	memset(Hora, 0x00, sizeof(Hora));
	memset(Minutos, 0x00, sizeof(Minutos));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	kbdbuf[0] = sizeof(gConfigComercio.EditCierre);
	memcpy(	&kbdbuf[1], gConfigComercio.EditCierre, sizeof(gConfigComercio.EditCierre));

	if(AutoCierre((struct CONFIG_COMERCIO*) conf_comer) != 2 )
	{
		memset(((struct CONFIG_COMERCIO*) conf_comer) ->EditCierre, 0x00, sizeof(((struct CONFIG_COMERCIO*) conf_comer) ->EditCierre));
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		FreeMW(conf_comer);
		return;
	}

	while (TRUE)
	{

		TextColor("DD/HH/Mm", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT | MW_CLRDISP, 0);

		if (!APM_GetKbdSpectra(NUMERIC_INPUT | ECHO | MW_SMFONT | MW_LINE6,
				IMIN(6) + IMAX(6), kbdbuf))
		{
			((struct CONFIG_COMERCIO*) conf_comer)->AutoCierre = FALSE;
			SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
			FreeMW(conf_comer);
			return;
		}

		memcpy(Dia, &kbdbuf[1], 2);
		memcpy(Hora, &kbdbuf[3], 2);
		memcpy(Minutos, &kbdbuf[5], 2);

		if (atoi(Dia) <= 0 || atoi(Dia) > 31 || atoi(Hora) > 24 || atoi(Minutos) > 60)
		{
			LongBeep();
			TextColor("Entrada  Invalida", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT | MW_CLRDISP, 3);
			continue;
		}

		break;
	}

	//printf(" \f estado auto %d", conf_comer.AutoCierre); WaitKey(3000, 0);
	memcpy(((struct CONFIG_COMERCIO*) conf_comer) ->EditCierre, &kbdbuf[1], 6);
	SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
	FreeMW(conf_comer);
	return;

}

//MFBC/23/11/12
BOOLEAN VerificaAutoCierre(void)
{
	WORD rec_cnt;
	BYTE Rtc[14];
	BYTE compRtc[6];
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	memset(Rtc, 0x00, sizeof(Rtc));
	memset(compRtc, 0x00, sizeof(compRtc));

	if (gConfigComercio.AutoCierre == FALSE ||
			(memcmp(gConfigComercio.EditCierre, "\x00\x00\x00\x00\x00\x00", sizeof(gConfigComercio.EditCierre) ) == 0) )
	{
		FreeMW(conf_comer);
		return FALSE;
	}
	RtcGetMW(Rtc);
	memcpy(compRtc, &Rtc[6], 6);

	if (atoi(compRtc) == atoi(gConfigComercio.EditCierre) || atoi(compRtc) > atoi(gConfigComercio.EditCierre))
	{
		Short1Beep();
		TextColor("AUTOCIERRE", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_BIGFONT | MW_CLRDISP, 3);
		rec_cnt = APM_GetRecCount();
		if (rec_cnt <= 0)
		{
			TextColor("Transacciones", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT | MW_CLRDISP, 0);
			TextColor("No encontradas", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 3);
			RefreshDispAfter(0);
			((struct CONFIG_COMERCIO*) conf_comer)->AutoCierre = FALSE;
			SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
			FreeMW(conf_comer);
			return FALSE;

		}
		else
		{
			Short2Beep();
			TextColor("Auto Cierre en", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT | MW_CLRDISP, 0);
			TextColor("Proceso...", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 3);
			((struct CONFIG_COMERCIO*) conf_comer)->AutoCierre = FALSE;
			SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
			if (SettleTransVisa(TRUE))//MFBC/10/12/12
			{
				APM_BatchClear(0);
			}
			RefreshDispAfter(0);
		}
		FreeMW(conf_comer);
		return TRUE;
	}
	FreeMW(conf_comer);
	return FALSE;
}

BOOLEAN MenuSSL(BYTE *certSslName) //Daniel SSL
{
	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KSSLMenu, select);

		if (select == -1)
			return FALSE;

		sprintf(certSslName, "PARAM%d", select - 1);
		break;
	}
	return TRUE;
}

void configSSL(void) //MFBC/19/03/13
{ ////Daniel SSL
	BYTE opcion = 0x00;
	BYTE certSslName[20];
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	memset(certSslName, 0x00, sizeof(certSslName));
	DispLineMW("    HABILITAR SSL    ", MW_LINE1, MW_CLRDISP | MW_REVERSE | MW_LEFT | MW_SMFONT);
	TextColor("Habilitar SSL ?", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
	displaySI_NO();

	if (gConfigComercio.habSSL == TRUE)
	{
		TextColor("ACTUAL: ", MW_LINE5, COLOR_VISABLUE, MW_SMFONT, 0);
		TextColor("SI", MW_LINE5 + 8, COLOR_GREEN, MW_SMFONT, 0);
	}
	else
	{
		TextColor("ACTUAL: ", MW_LINE5, COLOR_VISABLUE, MW_SMFONT, 0);
		TextColor("NO", MW_LINE5 + 8, COLOR_RED, MW_SMFONT, 0);
	}

	opcion = APM_YesNo();

	if (opcion == 2)
	{
		if (!MenuSSL(certSslName))
		{
			FreeMW(conf_comer);
			return;
		}

		if (!InjectCAKey(certSslName))
		{
			FreeMW(conf_comer);
			return;
		}
		memcpy(	((struct CONFIG_COMERCIO*) conf_comer)->certSslName, certSslName, 20);
		((struct CONFIG_COMERCIO*) conf_comer)->habSSL=TRUE;
		AcceptBeep();
		TextColor("Modo SSL", MW_LINE3, COLOR_VISABLUE,MW_CLRDISP|MW_SMFONT | MW_CENTER, 0);
		TextColor("Habilitado!!", MW_LINE4, COLOR_VISABLUE,MW_SMFONT | MW_CENTER, 3);
	}
	else if (opcion == 1)
		((struct CONFIG_COMERCIO*) conf_comer)->habSSL=FALSE;

	SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
	FreeMW(conf_comer);
}


void Prefijo(void) //MFBC/19/02/13
{
	BYTE kbdbuf[20 + 1];
	BYTE mostrarPrefijo[10 + 1];
	BYTE Prefijo[5 + 1];
	BYTE *p_PrefTem;
	//int lenStr = -1;
	memset(Prefijo, 0xFF, sizeof(Prefijo));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(mostrarPrefijo, 0x00, sizeof(mostrarPrefijo));

	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	split(mostrarPrefijo, gAppDat.Prefijo, 5);
	ConvB2Comma(mostrarPrefijo);
	p_PrefTem = RTrim(mostrarPrefijo, 'F');
	memcpy(&kbdbuf[1], p_PrefTem, strlen(p_PrefTem));
	kbdbuf[0] = strlen(p_PrefTem);
	TextColor("Numero de Prefijo", MW_LINE2, COLOR_VISABLUE, MW_CLRDISP | MW_SMFONT | MW_CENTER, 0);


	if (!APM_GetKbdSpectra(ALPHA_INPUT + ECHO + MW_SMFONT + MW_LINE6, IMAX(10), kbdbuf))
	{
		FreeMW(conf_AppData);
		return;
	}


	if (kbdbuf[0] == 0)
	{
		memset(((struct APPDATA*) conf_AppData)->Prefijo, 0xFF, sizeof(((struct APPDATA*) conf_AppData)->Prefijo));
		SaveDataFile((struct APPDATA*) conf_AppData);
		FreeMW(conf_AppData);
		return;
	}

	ConvCaracter(&kbdbuf[1]);


	if (kbdbuf[0] % 2 != 0)
	{
		memcpy(&kbdbuf[(kbdbuf[0] + 1)], "F", 1);
		kbdbuf[0]++;
	}

	compress(&Prefijo[0], &kbdbuf[1], kbdbuf[0] / 2);
	memcpy(((struct APPDATA*) conf_AppData)->Prefijo, Prefijo, 5);
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	return;

}

void TipoLlamada(void)
{
	int select = 0;

	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	select = MenuSelectVisa(&KTPCallMenu, select);

	if (select == -1)
		return;
	if (select == 1)
		((struct APPDATA*) conf_AppData)->ModoLlamada = TRUE;

	else if (select == 2)
		((struct APPDATA*) conf_AppData)->ModoLlamada = FALSE;

	AcceptBeep();
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	return;

}

void TipoMarcado(void)
{
	int select = 0;

	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	select = MenuSelectVisa(&KTPMarcadoMenu, select);

	if (select == -1)
		return;
	if (select == 1)
		((struct APPDATA*) conf_AppData)->TipoMarcado = TRUE;

	else if (select == 2)
		((struct APPDATA*) conf_AppData)->TipoMarcado = FALSE;

	AcceptBeep();
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	return;

}

//NM-25/04/13
void editarNumCaja(void)
{
	BYTE kbdbuff[9];
	BYTE numCajaLoc[5];
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));
	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));


	memset(kbdbuff, 0x00, sizeof(kbdbuff));
	memset(numCajaLoc, 0x00, sizeof(numCajaLoc));

	TextColor("Configurar Comercio", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);

	TextColor("Numero de Caja:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
	sprintf(numCajaLoc, "%04d", gConfigComercio.numCaja);
	TextColor(numCajaLoc, MW_LINE5, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("[0 a 9999]", MW_LINE7, COLOR_VISABLUE, MW_SMFONT, 0);
	displaySI_NO();

	if (APM_YesNo() == 2)
	{
		TextColor("Digite Num. Caja", MW_LINE3, COLOR_VISABLUE, MW_BIGFONT | MW_LEFT | MW_CLRDISP, 0);

		if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE6, IMIN(1) + IMAX(4), kbdbuff))
		{
			FreeMW(conf_comer);
			return;
		}

		((struct CONFIG_COMERCIO*) conf_comer)->numCaja = atoi(&kbdbuff[1]);
		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
		AcceptBeep();
		TextColor("Numero Caja Editado", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("Exitosamente!!", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);
	}

	FreeMW(conf_comer);
	return;
}


BOOLEAN editar_Recibo (void)
{

	BYTE kbdbuff[15 + 1];
	BYTE numRecibo[40];
	BYTE NumRecibo[3];
	DWORD recibo = 0;
	//int rec_cnt = 0;
	struct APPDATA *Global;
	Global= (void *) MallocMW(sizeof(struct APPDATA));
	int indice = 0;
	memset(kbdbuff, 0x00, sizeof(kbdbuff));
	memset(numRecibo, 0x00, sizeof(numRecibo));
	memset(NumRecibo, 0x00, sizeof(NumRecibo));

	//	rec_cnt = APM_GetRecCount();
	//	if (rec_cnt != 0) //MFBC/07/02/13
	//	{
	//		LongBeep();
	//		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
	//		return ;
	//	}

	if (check_lote() == TRUE)
	{
		LongBeep();
		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
		FreeMW(Global);
		return FALSE;

	}

	memcpy(Global,&gAppDat,sizeof(struct APPDATA));



	TextColor("Configurar Recibo", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP | MW_REVERSE, 0);

	TextColor("Numero de recibo:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);


	recibo = BCDtoDEC(Global->NumRoc, 3);

	sprintf(numRecibo, "%06d", recibo );
	TextColor(numRecibo, MW_LINE5, COLOR_VISABLUE, MW_SMFONT, 0);
	TextColor("[0 a 999999]", MW_LINE7, COLOR_VISABLUE, MW_SMFONT, 0);
	displaySI_NO();


	while(TRUE){
		switch (WaitKey(9000,0)){
		case MWKEY_ENTER:
			TextColor("Digite Num. recibo", MW_LINE3, COLOR_VISABLUE, MW_BIGFONT | MW_LEFT | MW_CLRDISP, 0);

			if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE6, IMIN(1) + IMAX(6), kbdbuff))
				return FALSE;
			memset(numRecibo, 0x30, sizeof(numRecibo));
			indice = (6 - kbdbuff[0]);
			memcpy(&numRecibo[indice], &kbdbuff[1], kbdbuff[0]);
			compress(numRecibo, numRecibo, 3);

			memcpy(Global->NumRoc, numRecibo, 3);

			if (memcmp(Global->NumRoc, "\x00\x00\x00", 3) == 0)
				bcdinc(Global->NumRoc, 3);
			//			printf("\f compress <%02x,%02x,%02x>",numRecibo[0],numRecibo[1],numRecibo[2]);
			//			APM_WaitKey(9000,0);
			SaveDataFile(Global);
			FreeMW(Global);

			AcceptBeep();
			TextColor("Num. recibo Editado", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor("Exitosamente!!", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 3);

			break;
		case MWKEY_CLR:
			return TRUE;
		case MWKEY_CANCL:
			return FALSE;
			break;
		default:
			continue;
			break;
		}
		break;
	}

	return TRUE;


}


//NM-22/04/13 Agregue esta Funcion
#ifdef FTPDL

void MenuFtp(void)
{

	//	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	//	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	if(passAdminComercio(MW_LINE1) != TRUE)
	{
		IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
		return;

	}


	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KMenuFtp, select);

		if (select == -1)
		{
			IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
			return;

		}

		switch (select)
		{
		case 1:	//Tipo de Conexion
			ConfigTimeFTP();
			//                MenuTipoConexion();
			break;
			/*
            case 2:	//Config. LAN
                TermIPFunc();
                break;

            case 3:	//Config. GPRS
                APM_MerchFunc(10);
                break;

            case 4:	//Config. DIAL
                PPPConnect();
                break;

            case 5:	//Descarga
                DownloadFtp();
                break;

            case 6:
                ConfigTimeFTP();
                break;
            case 7:
                ConsultaFTP();
                break;
            case 8:
                InicioFTP();
                break;

            default:
                break;
			 */

		}
	}

	IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
	return;

}
#endif




//*****************************************************************************
//  Function        : MenuEditCTLS
//  Description     : Despliega Menu para configuracion de CTLS
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//  By : Manuel Barbaran
//*****************************************************************************
void MenuEditCTLS (void)
{


	while (1)
	{
		int select = 0;
		select = MenuSelectVisa(&KCtlsMenu, select);

		if (select == -1)
			return;

		switch (select)
		{
		case 1:
			edit_Trans_limit();
			break;

		case 2:
			edit_CVM_limit();
			break;

			//		case 3:
			//			edit_Floor_limit();
			//			break;

		default:
			break;

		}
	}

}


//*****************************************************************************
//  Function        : edit_Trans_limit
//  Description     : modifica y guarda el trans limit
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//  By : Manuel Barbaran
//*****************************************************************************
void edit_Trans_limit (void)
{
	BYTE kbdbuff[40];
	BYTE montoTemp[20];
	BYTE monto[20];
	DDWORD monto_entero = 0, monto_entero_aux = 0;
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	struct CTLGEN ctl_gen;

	//	DWORD fileSize = 0;
	DWORD idFileGen = -1;
	BYTE fileName[10];

	memset(fileName, 0x00, sizeof(fileName));
	memset(kbdbuff, 0x00, sizeof(kbdbuff));
	memset(montoTemp, 0x00, sizeof(montoTemp));
	memset(monto, 0x00, sizeof(monto));

	if (check_lote() == TRUE)
	{
		LongBeep();
		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
		FreeMW(conf_AppData);
		return ;

	}

	memset(&ctl_gen, 0, sizeof(struct CTLGEN));
	memcpy(fileName, "CTLGEN", 6);
	idFileGen = fOpenMW(fileName);
	//fileSize = fLengthMW(idFileGen);

	fSeekMW(idFileGen, 0);
	fReadMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN));
	memcpy(montoTemp, ctl_gen.trans_limit, 12);

	if(atoi(montoTemp ) != 0)
		LTrim(montoTemp, '0');


	kbdbuff[0] = 4;
	memcpy(&kbdbuff[1], "   ", 3);

	kbdbuff[4] = '$';

	memcpy(monto, montoTemp, strlen(montoTemp) - 2); // para quitar decimales

	monto_entero = atoi(monto);
	monto_entero_aux = monto_entero;

	if(monto_entero != 0)
	{
		sprintf(&kbdbuff[5], "%d", monto_entero);
		kbdbuff[0] += strlen(monto);
	}
	else
	{
		sprintf(&kbdbuff[5], "%d", monto_entero);
		kbdbuff[0] += 1;
	}

	DispClrBelowMW(MW_LINE2);

	TextColor("Trans Limit", MW_LINE3, COLOR_VISABLUE, MW_BIGFONT | MW_LEFT , 0);

	if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT + ECHO + MW_LINE6, IMAX(9), kbdbuff))
	{
		fCloseMW(idFileGen);
		FreeMW(conf_AppData);
		return;
	}

	monto_entero = 0;
	monto_entero = atoi(&kbdbuff[1]);

	if(monto_entero != monto_entero_aux)
	{
		strcat(kbdbuff, "00");
		LSetStr(&kbdbuff[1], 12, '0');
		memcpy(ctl_gen.trans_limit, &kbdbuff[1], 12);
		fSeekMW(idFileGen, 0);
		fWriteMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN));
		((struct APPDATA*) conf_AppData)->estadoInit = FALSE;
		SaveDataFile((struct APPDATA*) conf_AppData);
		AcceptBeep();
		TextColor("Trans Limit ", MW_LINE4, COLOR_VISABLUE, MW_BIGFONT | MW_CENTER | MW_CLRDISP , 0);
		TextColor("Modificado", MW_LINE6, COLOR_VISABLUE, MW_BIGFONT | MW_CENTER  , 2);
		LongBeep();
		TextColor("INICIALICE!", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER| MW_CLRDISP  , 3);

	}
	else
		Short2Beep();

	fCloseMW(idFileGen);
	FreeMW(conf_AppData);
	return;
}

//*****************************************************************************
//  Function        : edit_CVM_limit
//  Description     : modifica y guarda el cvm limit
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//  By : Manuel Barbaran
//*****************************************************************************
void edit_CVM_limit (void)
{
	BYTE kbdbuff[40];
	BYTE montoTemp[25];
	BYTE monto[25];
	DDWORD monto_entero = 0;
	DDWORD monto_entero_aux = 0;
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	struct CTLGEN ctl_gen;


	//	DWORD fileSize = 0;
	DWORD idFileGen = -1;
	BYTE fileName[10];

	memset(fileName, 0x00, sizeof(fileName));
	memset(kbdbuff, 0x00, sizeof(kbdbuff));
	memset(montoTemp, 0x00, sizeof(montoTemp));
	memset(monto, 0x00, sizeof(monto));

	if (check_lote() == TRUE)
	{
		LongBeep();
		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
		FreeMW(conf_AppData);
		return ;

	}

	memset(&ctl_gen, 0, sizeof(struct CTLGEN));
	memcpy(fileName, "CTLGEN", 6);
	idFileGen = fOpenMW(fileName);
	//fileSize = fLengthMW(idFileGen);

	fSeekMW(idFileGen, 0);
	fReadMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN));
	memcpy(montoTemp, ctl_gen.cvm_limit, 12);

	if(atoi(montoTemp ) != 0)
		LTrim(montoTemp, '0');


	kbdbuff[0] = 4;
	memcpy(&kbdbuff[1], "   ", 3);

	kbdbuff[4] = '$';

	memcpy(monto, montoTemp, strlen(montoTemp) - 2); // para quitar decimales

	monto_entero = atoi(monto);
	monto_entero_aux = monto_entero;

	if(monto_entero != 0)
	{
		sprintf(&kbdbuff[5], "%d", monto_entero);
		kbdbuff[0] += strlen(monto) ;
	}
	else
	{
		sprintf(&kbdbuff[5], "%d", monto_entero);
		kbdbuff[0] += 1;
	}

	DispClrBelowMW(MW_LINE2);

	TextColor("CVM Limit", MW_LINE3, COLOR_VISABLUE, MW_BIGFONT | MW_LEFT , 0);

	if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT + ECHO + MW_LINE6, IMAX(9), kbdbuff))
	{
		fCloseMW(idFileGen);
		FreeMW(conf_AppData);
		return;
	}

	monto_entero = 0;
	monto_entero = atoi(&kbdbuff[1]);

	if(monto_entero != monto_entero_aux)
	{
		strcat(kbdbuff, "00");
		LSetStr(&kbdbuff[1], 12, '0');
		memcpy(ctl_gen.cvm_limit, &kbdbuff[1], 12);
		fSeekMW(idFileGen, 0);
		fWriteMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN));
		((struct APPDATA*) conf_AppData)->estadoInit = FALSE;
		SaveDataFile((struct APPDATA*) conf_AppData);
		AcceptBeep();
		TextColor("CVM Limit", MW_LINE4, COLOR_VISABLUE, MW_BIGFONT | MW_CENTER | MW_CLRDISP , 0);
		TextColor("Modificado", MW_LINE6, COLOR_VISABLUE, MW_BIGFONT | MW_CENTER  , 2);
		LongBeep();
		TextColor("INICIALICE!", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER| MW_CLRDISP  , 3);

	}
	else
		Short2Beep();


	fCloseMW(idFileGen);
	FreeMW(conf_AppData);
	return;
}


//*****************************************************************************
//  Function        : edit_Floor_limit
//  Description     : modifica y guarda el floor limit
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//  By : Manuel Barbaran
//*****************************************************************************
void edit_Floor_limit (void)
{
	BYTE kbdbuff[40];
	BYTE montoTemp[25];
	BYTE monto[25];
	DDWORD monto_entero = 0;
	DDWORD monto_entero_aux = 0;
	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	struct CTLGEN ctl_gen;

	//	DWORD fileSize = 0;
	DWORD idFileGen = -1;
	BYTE fileName[10];

	memset(fileName, 0x00, sizeof(fileName));
	memset(kbdbuff, 0x00, sizeof(kbdbuff));
	memset(montoTemp, 0x00, sizeof(montoTemp));
	memset(monto, 0x00, sizeof(monto));

	if (check_lote() == TRUE)
	{
		LongBeep();
		TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 3);
		FreeMW(conf_AppData);
		return ;

	}

	memset(&ctl_gen, 0, sizeof(struct CTLGEN));
	memcpy(fileName, "CTLGEN", 6);
	idFileGen = fOpenMW(fileName);
	//fileSize = fLengthMW(idFileGen);

	fSeekMW(idFileGen, 0);
	fReadMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN));
	memcpy(montoTemp, ctl_gen.floor_limit, 12);
	if(atoi(montoTemp ) != 0)
		LTrim(montoTemp, '0');


	kbdbuff[0] = 4;
	memcpy(&kbdbuff[1], "   ", 3);

	kbdbuff[4] = '$';

	memcpy(monto, montoTemp, strlen(montoTemp) - 2); // para quitar decimales

	monto_entero = atoi(monto);
	monto_entero_aux = monto_entero;

	if(monto_entero != 0)
	{
		sprintf(&kbdbuff[5], "%d", monto_entero);
		kbdbuff[0] += strlen(monto);
	}
	else
	{
		sprintf(&kbdbuff[5], "%d", monto_entero);
		kbdbuff[0] += 1;
	}

	DispClrBelowMW(MW_LINE2);

	TextColor("Floor Limit", MW_LINE3, COLOR_VISABLUE, MW_BIGFONT | MW_LEFT , 0);

	if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT + ECHO + MW_LINE6, IMAX(9), kbdbuff))
	{
		FreeMW(conf_AppData);
		fCloseMW(idFileGen);
		return;
	}

	monto_entero = 0;
	monto_entero = atoi(&kbdbuff[1]);


	if(monto_entero != monto_entero_aux)
	{
		strcat(kbdbuff, "00");
		LSetStr(&kbdbuff[1], 12, '0');
		memcpy(ctl_gen.floor_limit, &kbdbuff[1], 12);
		fSeekMW(idFileGen, 0);
		fWriteMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN));
		((struct APPDATA*) conf_AppData)->estadoInit = FALSE;
		SaveDataFile((struct APPDATA*) conf_AppData);
		AcceptBeep();
		TextColor("Floor Limit", MW_LINE4, COLOR_VISABLUE, MW_BIGFONT | MW_CENTER | MW_CLRDISP , 0);
		TextColor("Modificado", MW_LINE6, COLOR_VISABLUE, MW_BIGFONT | MW_CENTER , 2);
		LongBeep();
		TextColor("INICIALICE!", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER| MW_CLRDISP  , 3);
	}
	else
		Short2Beep();


	FreeMW(conf_AppData);
	fCloseMW(idFileGen);
	return;



}



void posFijo (void)
{

	BYTE kbdbuf[20 + 1];
	BYTE mostrarPosfijo[10 + 1];
	BYTE Posfijo[5 + 1];
	BYTE *p_PosfTem;
	memset(Posfijo, 0xFF, sizeof(Posfijo));
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(mostrarPosfijo, 0x00, sizeof(mostrarPosfijo));

	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	split(mostrarPosfijo, gAppDat.Posfijo, 5);
	ConvB2Comma(mostrarPosfijo);
	p_PosfTem = RTrim(mostrarPosfijo, 'F');

	memcpy(&kbdbuf[1], p_PosfTem, strlen(p_PosfTem));
	kbdbuf[0] = strlen(p_PosfTem);

	TextColor("Numero de PosFijo", MW_LINE2, COLOR_VISABLUE, MW_CLRDISP | MW_SMFONT | MW_CENTER, 0);

	if (!APM_GetKbdSpectra(ALPHA_INPUT  + ECHO +  MW_SMFONT + MW_LINE6, IMAX(10), kbdbuf))
	{
		FreeMW(conf_AppData);
		return;
	}

	if (kbdbuf[0] == 0)
	{
		memset(((struct APPDATA*) conf_AppData)->Posfijo, 0xFF, sizeof(((struct APPDATA*) conf_AppData)->Posfijo));
		SaveDataFile((struct APPDATA*) conf_AppData);
		FreeMW(conf_AppData);
		return;
	}

	ConvCaracter(&kbdbuf[1]);

	if (kbdbuf[0] % 2 != 0)
	{
		memcpy(&kbdbuf[(kbdbuf[0] + 1)], "F", 1);
		kbdbuf[0]++;
	}

	compress(&Posfijo[0], &kbdbuf[1], kbdbuf[0] / 2);
	memcpy(((struct APPDATA*) conf_AppData)->Posfijo, Posfijo, 5);
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	return;


}

/**********************************************
 * Descripcion: Menu para la configuracion de parametros especiales
 * **SR** 17/12/13
 ***********************************************/

void MenuParamEspecial (void){

	int select = 0;

	while(TRUE){
		os_beep_close();
		select = 0;
		select = MenuSelectVisa(&KMenuParamSpecial, select);

		if(select == -1)
			return;

		switch(select){

		case 1:
			editar_Recibo();
			break;
		case 2:
			VelModem();
			break;
		case 3:
			PariedadModem();
			break;
		case 4:
			VelCaja();
			break;
		default:
			continue;
		}
	}
	return;
}


