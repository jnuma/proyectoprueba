/*
 * cajas.h
 *
 *  Created on: 10/09/2012
 *      Author: Daniel J. Jacome Minorta
 */

#ifndef CAJAS_H_
#define CAJAS_H_

#include "sysutil.h"

#define 	CA_PIN			0
#define 	CA_COMM		1
#define 	CA_CANCEL	2
#define		CA_XD		3
#define		CA_XT		4

extern struct CAJAS gSendCajasAero;
extern struct CAJAS gSendCajasAdmin;

extern BOOLEAN gRecuperacionTrans;

BYTE calcularLRC(BYTE *mensaje, int lenMsg);
void cajasTest(void);
BOOLEAN procesarTramaCaja(BYTE *data, int LenData);
int unpackCajas(BOOLEAN aWaitAck);
BOOLEAN transaccionCaja(BYTE tipoTrans, BOOLEAN clearStruct, BOOLEAN WaitACK);
void packChequesPostFechadosCaja(BYTE numCheque, DDWORD valorCheque, BYTE *fechaExpCheque);
void enviarUltTxnCajas(void);

extern void NextProcCajas (void);
extern void ErrorTransCajas (WORD aType);
extern void PackInfoCajasInPOS (void);
extern BOOLEAN SendCajas (WORD aTaero);
extern BOOLEAN SendCajasBono (void);
extern void ErrorTransCajasBono (WORD aType);
extern void CompletDataVoidCaja (void);
extern void AddPinSerial (BYTE aTypetTrans);
extern void DeletPinSerial (BYTE aTypetTrans);
extern void ResetDataCajas (void);
extern BOOLEAN unpackCierreCajas(void);
extern void packTramaCajas(BYTE tipoTrans, BOOLEAN enviarACK);
extern void ErrorTransCajasAero (WORD aType);
extern BOOLEAN SendCajasAero (WORD aType);

extern void EMVErrorTransCajas (WORD aType);
extern void EMVSendCajas(void);

extern BOOLEAN LanActiveCajas (void);
extern BOOLEAN SendLanCajas (BYTE *aBuff, WORD aLen);
extern void ChequeCancelCajas (void);

/*-----  TRANSACTION TYPES CAJAS ---------------------------------*/
enum {
	PAGO_CON_TARJETA			,		//0
	PAGO_CON_TARJETA_2			,		//1
	PAGO_CON_TARJETA_PAN		,		//2
	PAGO_CON_TARJETA_PAN_2		, 		//3
	PAGO_CON_TARJETA_PAN_MULTICO	,		//4
	PAGO_CON_TARJETA_PAN_MULTICO_2	, 		//5
	PAGO_CON_TARJETA_SN			,		//6
	PAGO_CON_TARJETA_SN_2		,		//7
	PAGO_CON_TARJETA_PAN_SN		,		//8
	PAGO_CON_TARJETA_PAN_SN_2	,		//9
	ANULACION 					,		//10			//kt-091012
	ANULACION_2 				,		//11			//kt-091012
	CIERRE_INTEGRADO			, 		//14
	SUPERCUPO_fll				,		//15			// **SR**  09/09/13
	SUPERCUPO_fll_2				,		//16			// **SR**  09/09/13
	SEND_TRACKS					,		//17		// **SR**  09/09/13
	MAX_TXN_TYPE_CAJAS			,
};



struct TRANS_BITMAP_CAJAS {
	BYTE tipoTrans;
	BYTE transportHeader[10];
	BYTE presentHeader[7];
	BYTE bitMap[45];
};


struct CAJAS{
	BYTE authCode_1[6 + 1];			//  Campo 1
	BYTE BINTarjeta[12 + 1];		//  campo 30
	BYTE totalCompra_40[12 + 1];	//	Campo 40
	BYTE valorIva_41[12 + 1];		//  Campo 41
	BYTE numeroCaja_42[10 + 1];		//	Campo 42
	BYTE numeroRecibo_43[6 + 1];	//	Campo 43
	BYTE RRN_44[6 + 1];				//	Campo 44
	BYTE terminalID_45[8 + 1];		//	Campo 45
	BYTE dateTrans_46[6 + 1];		//	Campo 46
	BYTE timeTrans_47[4 + 1];		//	Campo 47
	BYTE respCode_48[2 + 1];		//	Campo 48
	BYTE franquisia_49[10 + 1];		//	Campo 49
	BYTE typeAcount_50[2 + 1];		//	Campo 50
	BYTE numCuotas_51[2 + 1];		//	Campo 51
	BYTE numTrans_53[10 + 1];		//  Campo 53
	BYTE ult4DigTarjeta_54[4 + 1];	//	Campo 54
	BYTE tipoDocumento_55[1 + 1];	//	Campo 55
	BYTE numDocumento_56[11 + 1];	//	Campo 56
	BYTE numTelefono_57[7 + 1];		//	Campo 57
	BYTE codBanco_58[2 + 1];		//	Campo 58
	BYTE numCuenta_59[13 + 1];		//  Campo 59
	BYTE cardBIN_75[6 + 1];			//	Campo 75
	BYTE expDateCard_76[4 + 1];		//	Campo 76
	BYTE codComercio_77[23 + 1];	//	Campo 77
	BYTE direcEstab_78[23 + 1];		//	Campo 78
	BYTE lable_79[2 + 1];			//  Campo 79
	BYTE valorBaseDev_80[12 + 1];	//	Campo 80
	BYTE propina_81[12 + 1];		//	Campo 81
	BYTE filler_82[12 + 1];			//	Campo 82
	BYTE idCajero_83[12 + 1];		//	Campo 83
	BYTE filler_84[12 + 1];			//	Campo 84		// Valor tasa Administrati
	BYTE filler_85[12 + 1];			//	Campo 85		// Valor IVA Tasa administrativa
	BYTE filler_86[12 + 1];			//	Campo 86		// valor base devolucion IVA tasa Administrativa
	BYTE filler_87[12 + 1];			//	Campo 87
	BYTE filler_88[12 + 1];			//	Campo 88
	BYTE filler_89[12 + 1];			//	Campo 89
	BYTE filler_90[12 + 1];			//	Campo 90
	BYTE filler_91[12 + 1];			//	Campo 91
	BYTE filler_92[12 + 1];			//	Campo 92
	BYTE filler_93[76 + 1];			//	Campo 93			// Track 1
	BYTE filler_94[37 + 1];			//	Campo 94			// track 2
	BYTE PAN_95[19 + 1];			//	Campo 95
	BYTE SN_96[19 + 1];				//	Campo 95
	BYTE codAgencia_98[2 + 1];		//	Campo 98
	BYTE filler_99[12 + 1];			//	Campo 99
};
extern struct CAJAS gCajas;
extern struct CAJAS gCajas2[2];		// Estructura usada para almacenar la ultima tx de cajas extraida de la memoria del POS
extern struct TRANS_BITMAP_CAJAS KTtransBitmapCajas[MAX_TXN_TYPE_CAJAS];			// Se deja editable **SR**26/08/18
extern void SendTracks (void);
extern BOOLEAN FindComerCajas (void);

#endif /* CAJAS_H_ */
