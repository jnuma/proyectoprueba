//-----------------------------------------------------------------------------
//  File          : record.c
//  Module        :
//  Description   : Include routines for handling record operation.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include "corevar.h"
#include "constant.h"
#include "message.h"
#include "tranutil.h"
#include "util.h"
#include "record.h"
#include "files.h"

//*****************************************************************************
//  Function        : MoveInput2Tx
//  Description     : Save data from input to TX_DATA.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : TX_DATA
//*****************************************************************************
void MoveInput2Tx(void)
{
    struct APPDATA *Global;
    Global= (void *) MallocMW(sizeof(struct APPDATA));
    memcpy(Global,&gAppDat,sizeof(struct APPDATA));

	if (INPUT.b_trans != VOID )
	{
		ReadRTC(&INPUT.s_dtg);
		memcpy(INPUT.sb_roc_no, Global->NumRoc, 3); // Jorge Numa 05-02-2013
	}

	ByteCopy((BYTE *) &TX_DATA, (BYTE *) &INPUT, TX_DATA.sb_proc_code
			- &TX_DATA.b_trans);
	ByteCopy((BYTE *) &TX_DATA.s_icc_data, (BYTE *) &INPUT.s_icc_data,
			sizeof(struct ICC_DATA));
	ByteCopy((BYTE *) &TX_DATA.s_trk1buf, (BYTE *) &INPUT.s_trk1buf,
			sizeof(struct MW_TRK1BUF)); //kt-121012
	ByteCopy((BYTE *) &TX_DATA.s_trk2buf, (BYTE *) &INPUT.s_trk2buf,
			sizeof(struct MW_TRK2BUF));

	ByteCopy((BYTE *) &TX_DATA.s_billpayment, (BYTE *) &INPUT.s_billpayment,
			117); // Jorge Numa 06-02-2013

	memcpy(TX_DATA.sb_pin, INPUT.sb_pin, sizeof(INPUT.sb_pin)); // online pin
	FreeMW(Global);
}
//*****************************************************************************
//  Function        : Input2RecordBuf
//  Description     : Save data from input to RECORD_BUF.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : RECORD_BUF;
//*****************************************************************************
void Input2RecordBuf(void)
{
	memset(&RECORD_BUF, 0x00, sizeof(struct TXN_RECORD));

	ByteCopy((BYTE *) &RECORD_BUF, (BYTE *) &INPUT, (DWORD)((DWORD)
			& RECORD_BUF.w_crc - (DWORD) & RECORD_BUF));

	memcpy(RECORD_BUF.sb_rrn, &RSP_DATA.sb_rrn, sizeof(RECORD_BUF.sb_rrn));
	memcpy(RECORD_BUF.sb_auth_code, &RSP_DATA.sb_auth_code,
			sizeof(RECORD_BUF.sb_auth_code));
	if (memcmp(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3) > 0)
		memcpy(RECORD_BUF.sb_trace_no, &TX_DATA.sb_trace_no, 3);

	if (memcmp(TX_DATA.sb_roc_no, INPUT.sb_roc_no, 3) > 0) // Jorge Numa 05-02-2013
		memcpy(RECORD_BUF.sb_roc_no, &TX_DATA.sb_roc_no, 3);

	RECORD_BUF.w_rspcode = RSP_DATA.w_rspcode;
	RECORD_BUF.b_PinVerified = RSP_DATA.b_PinVerified;
	RECORD_BUF.dd_org_amount = INPUT.dd_amount;
	RECORD_BUF.w_host_idx = INPUT.w_host_idx;
	RECORD_BUF.b_entry_mode = INPUT.b_entry_mode;

	if(INPUT.b_trans != VOID )
		memcpy(&RECORD_BUF.s_dtg, &RSP_DATA.s_dtg, sizeof(RECORD_BUF.s_dtg));

}
//*****************************************************************************
//  Function        : UpdateHostStatus
//  Description     : Update acquirer status;
//  Input           : aAction;       // host status flag
//  Return          : N/A
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
void UpdateHostStatus(BYTE aAction)
{
	struct BATSYS_HDR header;

	if (aAction == REV_PENDING)
	{
		if(INPUT.b_trans != VOID )
			ReadRTC(&RSP_DATA.s_dtg);

		Input2RecordBuf();
		header.b_status = STS_REC_REVERSAL;
		header.w_acq_id = INPUT.w_host_idx;

		memcpy(RECORD_BUF.sb_auth_code, TX_DATA.sb_auth_code, 6);
		memcpy(RECORD_BUF.sb_rrn, TX_DATA.sb_rrn, 12);

		memcpy(header.sb_trace_no, RECORD_BUF.sb_trace_no,
				sizeof(header.sb_trace_no));

		ByteCopy((BYTE *) &RECORD_BUF.s_icc_data, (BYTE *) &TX_DATA.s_icc_data,
				sizeof(struct ICC_DATA));

		APM_SaveBatRec(&RECORD_BUF, &header);
	}
	// APM_SetPending(INPUT.w_host_idx, aAction);
	SetPendingVISA(INPUT.w_host_idx, aAction); //kt-130912
	fCommitAllMW();
}
//*****************************************************************************
//  Function        : SaveRecord
//  Description     : Save record to Batch file
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SaveRecord(void)
{
	struct BATSYS_HDR header;
	BYTE msgBuf[21]; // Jorge Numa
	BYTE buf[16]; // Jorge Numa

	memset(buf, 0, sizeof(buf)); // Jorge Numa

	buf[1] = 0x30; // Jorge Numa

	buf[1] -= '0'; // Jorge Numa

	os_config_write(K_CF_Settled, &buf[1]); // Jorge Numa
	os_config_update(); // Jorge Numa

	memset(msgBuf, 0x00, sizeof(msgBuf));

	strcpy(msgBuf, "   REALICE CIERRE   ");

	os_config_write(K_CF_ReqSettleMsg, msgBuf); // Jorge Numa
	os_config_update();
	INPUT.w_rspcode = RSP_DATA.w_rspcode; //MFBC/26/04/13
	Input2RecordBuf();
	header.b_status = STS_REC_BATCH;
	header.w_acq_id = RECORD_BUF.w_host_idx;
	memcpy(header.sb_trace_no, RECORD_BUF.sb_trace_no,
			sizeof(header.sb_trace_no));
	memcpy(header.sb_roc_no, RECORD_BUF.sb_roc_no, sizeof(header.sb_roc_no));
	RECORD_BUF.w_crc = cal_crc((BYTE *) &RECORD_BUF, (BYTE *) &RECORD_BUF.w_crc
			- (BYTE *) &RECORD_BUF.b_trans);

	// Save Record & Clear Reversal
	APM_SaveBatRec(&RECORD_BUF, &header);

	UpdateHostStatus(NO_PENDING);
	fCommitAllMW(); // make sure all update in the same time
}
//*****************************************************************************
//  Function        : UpdateRecord
//  Description     : Update transaction record to recovery buffer.
//  Input           : aRecIdx;      // Record Index
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void UpdateRecord(int aRecIdx)
{
	struct BATSYS_HDR header;

	header.b_status = STS_REC_BATCH;
	header.w_acq_id = RECORD_BUF.w_host_idx;
	memcpy(header.sb_trace_no, RECORD_BUF.sb_trace_no,
			sizeof(header.sb_trace_no));
	memcpy(header.sb_roc_no, RECORD_BUF.sb_roc_no, sizeof(header.sb_roc_no));
	RECORD_BUF.w_crc = cal_crc((BYTE *) &RECORD_BUF, (BYTE *) &RECORD_BUF.w_crc
			- (BYTE *) &RECORD_BUF.b_trans);

	APM_UpdateBatRec(aRecIdx, &RECORD_BUF, &header);
	UpdateHostStatus(NO_PENDING);
	fCommitAllMW(); // make sure all update in the same time
}


//*****************************************************************************
//  Function        : UpdateRecordAero
//  Description     : Update transaction record to recovery buffer.
//  Input           : aRecIdx;      // Record Index
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void UpdateRecordAero(int aRecIdx)
{
	struct BATSYS_HDR header;

	header.b_status = STS_REC_BATCH;
	header.w_acq_id = RECORD_BUF.w_host_idx;
	memcpy(header.sb_trace_no, RECORD_BUF.sb_trace_no,
			sizeof(header.sb_trace_no));
	memcpy(header.sb_roc_no, RECORD_BUF.sb_roc_no, sizeof(header.sb_roc_no));
	RECORD_BUF.w_crc = cal_crc((BYTE *) &RECORD_BUF, (BYTE *) &RECORD_BUF.w_crc
			- (BYTE *) &RECORD_BUF.b_trans);

	memcpy(header.sb_Update_Aero_Status, "JMS", 3);

	APM_UpdateBatRec(aRecIdx, &RECORD_BUF, &header);
	fCommitAllMW(); // make sure all update in the same time
}




