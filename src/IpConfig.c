/*
 * IpConfig.c
 *
 *  Created on: 2/08/2012
 *      Author: MB
 */

#include "Sysutil.h"
#include "util.h"
#include "menu.h"
#include "VisaMenus.h"
#include "hardware.h"
#include "message.h"
#include "IpConfig.h"
#include "toggle.h"
#include "files.h"


//-----------------------------------------------------------------------------
//    Globals
//-----------------------------------------------------------------------------
//static BYTE bAccessDLL;
static struct TERM_DATA *psTermDataOffset = 0;
static int iTermDataFd = -1;

//-----------------------------------------------------------------------------
//    Constants
//-----------------------------------------------------------------------------
//static char KTermDataFile[] = { "TermData" };
// Default Terminal Data
static const struct TERM_DATA KDefaultTermData =
{ SETUP_MODE, // STIS mode
		12, // LCD contrast
		{ '0', '0', '0', '0', '0', '0', '1', '1', '7' }, // Term ID
		APM_COMM_SYNC, // Init Mode(COMM_SYNC = 0, COMM_TCPIP, COMM_ASYNC)
		{ 0x22, 0x64, 0x39, 0x12, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }, // Primary Phone
		0x30, 0x02, // Pri Conn time, redial
		{ 0x22, 0x64, 0x39, 0x12, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }, // Secondary Phone
		0x30, 0x02, // Sec Conn time, redial
		0x01, // Async Conn Delay
		DIAL_TONE, // Dial Mode (TONE=0, PULSE)
		{ 0xFF, 0xFF, 0xFF, 0xFF }, // PABX
		{ 0x00, 0x00, 0x01 },
		{ 0x00, 0x00, 0x01 }, // Trace & ROC Num.
		{ 0x0A, 0x01, 0x01, 0x4F },
		{ 0x27, 0x14 }, // InitIP & Port
		{ 0x00, 0x01 }, // Init NII
		0x00, // EMV Enable
		0x10, // Default Manager
		0x05, // Return to Base Timeout
		0x0000, // Training Status
		MAGIC, // Debug Status
		0x00, // Default Amount
		0x00, // Default Ext Pinpad Port
		0x00, // Default ECR Port
		{ '1', '2', '3', '4', '\0', '\0', '\0', '\0', '\0', '\0', '\0' }, // Default Pass Admin
		};

//#if (LAN_SUPPORT)
// TermIP Menu
static const struct MENU_ITEM KTermIPFuncItem[] =
{
{ 1, "IP Local" },
{ 2, "Habilitar DHCP" },
{ 3, "DNS" },
{ -1, NULL }, };

static const struct MENU_DAT KTermIPMenu =
{ "CONFIGURACION POS", KTermIPFuncItem, };


/*****************************************************************
 * Descripcion: Si es carga Full retorna los valores de ipde terminal a "0"
 * **SR** 01/10/13
 *****************************************************************/
void CleanTermianlIP (void){

	BYTE ip[12];

	memset(ip, 0x00, sizeof(ip));

	PutSysCfgMW(MW_SYSCFG_IP, &ip[0]);
	PutSysCfgMW(MW_SYSCFG_NETMASK, &ip[4]);
	PutSysCfgMW(MW_SYSCFG_GATEWAY, &ip[8]);
	UpdSysCfgMW();

	bs_lan_close();
	bs_lan_open();

	Delay10ms(5);

	return;
}

//*****************************************************************************
//  Function        : TermIPFunc
//  Description     : Config Terminal IP Address.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void TermIPFunc(void)
{
	int func_sel;
	struct MW_NIF_INFO netinfo;
	BYTE dhcp;
	//	int Seleccion = 0;
	BYTE ip[12];
	BYTE DNS[8];

	BOOLEAN reboot_required = FALSE;

	while (true)
	{
		func_sel = MenuSelectVisa(&KTermIPMenu, 0);
		if (func_sel == -1)
			return;


		GetSysCfgMW(MW_SYSCFG_DHCP_ENABLE, &dhcp);


		dhcp = (dhcp != 0) ? 1 : 0;
		if (dhcp)
		{
			NetInfoMW(MW_NIF_ETHERNET, &netinfo);
			memcpy(ip, (BYTE *) &netinfo.d_ip, 12);
			memcpy(DNS, (BYTE *) &netinfo.s_dns, 8);
		}
		else
		{
			GetSysCfgMW(MW_SYSCFG_IP, &ip[0]);
			GetSysCfgMW(MW_SYSCFG_NETMASK, &ip[4]);
			GetSysCfgMW(MW_SYSCFG_GATEWAY, &ip[8]);
			GetSysCfgMW(MW_SYSCFG_DNS1 , &DNS[0]);
			GetSysCfgMW(MW_SYSCFG_DNS2, &DNS[4]);
		}


		switch (func_sel)
		{
		case 1:
			if (ConfigTermIP(ip))
			{
				reboot_required = TRUE;
				PutSysCfgMW(MW_SYSCFG_IP, &ip[0]);
				PutSysCfgMW(MW_SYSCFG_NETMASK, &ip[4]);
				PutSysCfgMW(MW_SYSCFG_GATEWAY, &ip[8]);
				UpdSysCfgMW();
			}
			break;

		case 2:
			if (ToggleDHCP(&dhcp, ip))
			{
				reboot_required = TRUE;
				PutSysCfgMW(MW_SYSCFG_DHCP_ENABLE, &dhcp);
				PutSysCfgMW(MW_SYSCFG_IP, &ip[0]);
				PutSysCfgMW(MW_SYSCFG_NETMASK, &ip[4]);
				PutSysCfgMW(MW_SYSCFG_GATEWAY, &ip[8]);
				PutSysCfgMW(MW_SYSCFG_DNS1 , &DNS[0]);
				PutSysCfgMW(MW_SYSCFG_DNS2, &DNS[4]);
				UpdSysCfgMW();
			}
			break;

		case 3:
			if(configDNS(DNS))
			{
				reboot_required = TRUE;
				PutSysCfgMW(MW_SYSCFG_DNS1 , &DNS[0]);
				PutSysCfgMW(MW_SYSCFG_DNS2, &DNS[4]);
				UpdSysCfgMW();
			}
			break;
		}


		if (reboot_required)
		{
			TextColor("El POS", MW_LINE3, COLOR_GREEN, MW_CLRDISP|MW_CENTER|MW_SMFONT, 0);
			TextColor("Se Reiniciara", MW_LINE4, COLOR_GREEN, MW_CENTER|MW_SMFONT, 0);
			TextColor("Para Guardar Cambios", MW_LINE5, COLOR_GREEN, MW_CENTER|MW_SMFONT, 4);
			ResetMW();
		}
	}
}
//#endif //(LAN_SUPPORT)


//*****************************************************************************
//  Function        : ShowIp
//  Description     : Show ip addr.
//  Input           : aIp;        // pointer to 4 byte ip.
//                    aPos;       // line pos.
//                    aReverse;   // TRUE=> reverse on.
//  Return          : -1;         // Cancel;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ShowIp(unsigned char *aIp, int aPos, int aReverse)
{
	BYTE buf[128];

	DispPutCMW(K_PushCursor);
	DispGotoMW(aPos, MW_SPFONT);
	if (aReverse)
		DispCtrlMW(MW_REV_ON);
	os_disp_textc(COLOR_VISABLUE2);
	sprintf(buf, "%3d.%3d.%3d.%3d", aIp[0], aIp[1], aIp[2], aIp[3]);
	DispLineMW(buf, aPos, MW_SPFONT);
	DispPutCMW(K_PopCursor);
	if (aReverse)
		DispCtrlMW(MW_REV_OFF);
}

//*****************************************************************************
//  Function        : EditIp
//  Description     : Edit IP address.
//  Input           : aip;    // pointer to 4 bytes IP address buffer.
//                    aPos;   // position to display.
//  Return          : TRUE;
//                    FALSE;    // USER CANCEL
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN EditIp(BYTE *aIp, DWORD aPos)
{
	DWORD curr_field;
	DWORD cursor = 0;
	BYTE data[IP_LEN], buf[128];
	DWORD i;
	DWORD keyin;

	memcpy(data, aIp, IP_LEN);

	curr_field = IP_LEN;
	while (1)
	{
		if (curr_field < IP_LEN)
		{
			/* Show all data */
			ShowIp(data, aPos, FALSE);
			/* Show editing field */sprintf(buf, "%-3d", data[curr_field]);
			DispLineMW(buf, aPos+(curr_field*4)+EDITIP_COL, MW_REVERSE|MW_SPFONT);
		}
		else
			ShowIp(data, aPos, TRUE);

		keyin = WaitKey(KBD_TIMEOUT, 0);

		/* Valid numeric key */
		if ((keyin >= MWKEY_0) && (keyin <= MWKEY_9))
		{
			/* Start edit 1st field at all field selected */
			if (curr_field >= IP_LEN)
				curr_field = 0;
			/* Editing digit */
			if (cursor == 0)
			{
				data[curr_field] = (BYTE) (keyin - '0');
				cursor++;
			}
			else if (cursor < EDITIP_CURSOR_MAX)
			{
				i = data[curr_field] * 10 + (BYTE) (keyin - '0');
				if (i > 255)
					i = 255;
				data[curr_field] = (BYTE) i;
				cursor++;
			}
			/* Last digit, auto ENTER */
			if (cursor == EDITIP_CURSOR_MAX)
				keyin = MWKEY_ENTER;
		}

		/* Other keys */
		switch (keyin)
		{
		case MWKEY_ENTER:
			if (curr_field >= IP_LEN)
			{
				memcpy(aIp, data, IP_LEN);
				ShowIp(data, aPos, FALSE);
				return TRUE;
			}
			curr_field++;
			cursor = 0;
			break;
		case MWKEY_CLR:
			if (curr_field >= IP_LEN)
			{
				curr_field = 0;
			}
			else if ((cursor == 0) && (curr_field > 0))
				curr_field--;
			else if (curr_field < IP_LEN)
			{
				data[curr_field] = 0;
				cursor = 0;
			}
			break;
		case MWKEY_CANCL:
			if (curr_field < IP_LEN)
			{
				memcpy(data, aIp, IP_LEN);
				curr_field = IP_LEN;
				cursor = 0;
			}
			else
				return FALSE;
			break;
		}
	} // while
}

//*****************************************************************************
//  Function        : DispTermIP
//  Description     : Display terminal ip, netmask, gateway ip.
//  Input           : aIP;      // pointer to ip info.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispTermIP(BYTE *aIP)
{
	DispPutCMW(K_PushCursor);
	os_disp_textc(COLOR_VISABLUE);
	DispClrBelowMW(MW_LINE2);
	DispLineMW("IP Address", MW_LINE2, MW_SMFONT);
	ShowIp(aIP, MW_LINE3, FALSE);
	DispLineMW("Netmask", MW_LINE4, MW_SMFONT);
	ShowIp(&aIP[4], MW_LINE5, FALSE);
	DispLineMW("Gateway IP", MW_LINE6, MW_SMFONT);
	ShowIp(&aIP[8], MW_LINE7, FALSE);
	DispPutCMW(K_PopCursor);
}
//*****************************************************************************
//  Function        : ConfigTermIP
//  Description     : Setup ip, netmask, gateway ip
//  Input           : aIP;      // pointer to ip info.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ConfigTermIP(BYTE *aIP)
{
	DispPutCMW(K_PushCursor);
	os_disp_backc(COLOR_VISAYELLOW);
	DispLineMW(KIpHdr, MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);
	DispPutCMW(K_PopCursor);
	DispTermIP(aIP);

	if (!EditIp(aIP, MW_LINE3))
		return FALSE;

	if (!EditIp(&aIP[4], MW_LINE5))
		return FALSE;

	if (!EditIp(&aIP[8], MW_LINE7))
		return FALSE;

	DispPutCMW(K_PushCursor);
	os_disp_backc(COLOR_VISAYELLOW);
	DispLineMW(KEnterSave, MW_LINE9, MW_REVERSE|MW_CLREOL|MW_CENTER|MW_SPFONT);
	DispPutCMW(K_PopCursor);

	if (WaitKey(KBD_TIMEOUT, 0) != MWKEY_ENTER)
		return FALSE;

	return TRUE;
}
//*****************************************************************************
//  Function        : ToggleDHCP
//  Description     : Toggle DHCP setting
//  Input           : aDHCP;            // current DHCP setting
//                    aIP;              // current IP config
//  Return          : TRUE/FALSE;       // TRUE => state changed.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

BOOLEAN ToggleDHCP(BYTE *aDHCP, BYTE *aIP)
{
	BOOLEAN ret;
//	BYTE cur_val;
	int sel = -1 ;
	ret = FALSE;

	DispPutCMW(K_PushCursor);
	os_disp_backc(COLOR_VISAYELLOW);
	DispLineMW(KDHCPHdr, MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);
	DispPutCMW(K_PopCursor);
	DispTermIP(aIP);

	if(*aDHCP == 1)
	{
		TextColor("HABILITADO", MW_LINE8, COLOR_VISABLUE, MW_CENTER|MW_SMFONT|MW_CLREOL, 0 );
	}
	else
	{
		TextColor("DESHABILITADO", MW_LINE8, COLOR_RED, MW_CENTER|MW_SMFONT|MW_CLREOL, 0 );
	}

	displaySI_NO();
	sel = WaitKey(3000, 0);

	if( sel == MWKEY_ENTER)
	{
		if(*aDHCP != 1)
		{
			AcceptBeep();
			TextColor("DHCP HABILITADO", MW_LINE4, COLOR_VISABLUE, MW_CENTER|MW_SMFONT|MW_CLRDISP, 3 );
			ret = TRUE;
		}

		*aDHCP = TRUE;
	}
	else if( sel == MWKEY_CLR)
	{
		if(*aDHCP != 0)
		{
			LongBeep();
			TextColor("DHCP DESHABILITADO", MW_LINE4, COLOR_VISABLUE, MW_CENTER|MW_SMFONT|MW_CLRDISP, 3 );
			ret = TRUE;
		}

		*aDHCP = FALSE;
	}

//	cur_val = ToggleOption(NULL, KEnable, *aDHCP | TOGGLE_MODE_8LINE);
//
//	printf("\f curval<%02x>", cur_val);
//	printf("\n dhcp<%p>", aDHCP); WaitKey(3000, 0);
//
//	if ((cur_val != *aDHCP) && (cur_val != 0xFF))
//	{
//		*aDHCP = cur_val;
//		ret = TRUE;
//	}

	return ret;
}

//*****************************************************************************
//  Function        : UpdateTermID
//  Description     : Update terminal id value.
//  Input           : N/A
//  Return          : TRUE/FALSE
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdateTermID(void)
{
	struct TERM_DATA term_data;

	BYTE kbdbuf[MAX_INPUT_LEN + 1];

	DispClrBelowMW(MW_LINE3);
	ReadSFile(iTermDataFd, (DWORD) psTermDataOffset->sb_term_id, &kbdbuf[1], 9);
	kbdbuf[0] = 9;
	while (1)
	{
		//DispLineMW(KTermId, MW_LINE5, MW_BIGFONT);
		DispLineMW(KTermId, MW_LINE4, MW_SMFONT | MW_CENTER);

		//SetCol((MW_NOR_LINESIZE - (kbdbuf[0] + 1)) / 2);

		//if (!GetKbd(NUMERIC_INPUT+ECHO+MW_SMFONT+MW_LINE7+RIGHT_JUST, IMIN(9)+IMAX(9), kbdbuf))
		if (!APM_GetKbd(NUMERIC_INPUT+ECHO+MW_SMFONT+MW_LINE7, IMIN(9)+IMAX(9), kbdbuf))
			return FALSE;

		if (chk_digit_ok(&kbdbuf[1], 9))
		{
			ReadSFile(iTermDataFd, (DWORD) psTermDataOffset, &term_data, sizeof(struct TERM_DATA));
			memcpy(term_data.sb_term_id, &kbdbuf[1], 9);
			UpdTermData(&term_data);
			return TRUE;
		}

		DispLineMW(KChkSumErr, MW_LINE7, MW_CENTER|MW_BIGFONT);
		ErrorDelay();
	}
	return FALSE;
}

//*****************************************************************************
//  Function        : UpdTermData
//  Description     : Update Terminal Data File
//  Input           : aDat;         // pointer data buffer
//                    aCalCRC;      // 1 => ReCal CRC before update.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN UpdTermData(struct TERM_DATA *aDat)
{
	if (iTermDataFd < 0)
		return FALSE;

	aDat->w_crc = (WORD) cal_crc((BYTE *) aDat, (BYTE *) &aDat->w_crc - (BYTE *) aDat);
	WriteSFile(iTermDataFd, (DWORD) psTermDataOffset, aDat, sizeof(struct TERM_DATA));
	return TRUE;
}


BOOLEAN configDNS (BYTE *DNS)
{
	DispPutCMW(K_PushCursor);
	os_disp_backc(COLOR_VISAYELLOW);
	DispLineMW("DNS", MW_LINE1, MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_SMFONT);
	DispPutCMW(K_PopCursor);

	DispTermDNS(DNS);

	if (!EditIp(DNS, MW_LINE4))
		return FALSE;

	if (!EditIp(&DNS[4], MW_LINE7))
		return FALSE;

	DispPutCMW(K_PushCursor);
	os_disp_backc(COLOR_VISAYELLOW);
	DispLineMW(KEnterSave, MW_LINE9, MW_REVERSE|MW_CLREOL|MW_CENTER|MW_SPFONT);
	DispPutCMW(K_PopCursor);

	if (WaitKey(KBD_TIMEOUT, 0) != MWKEY_ENTER)
		return FALSE;

	return TRUE;
}


//*****************************************************************************
//  Function        : DispTermDNS
//  Description     : Muestra los DNS de la terminal
//  Input           : dns;      // pointer to ip info.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DispTermDNS(BYTE *dns)
{
	DispPutCMW(K_PushCursor);
	os_disp_textc(COLOR_VISABLUE);
	DispClrBelowMW(MW_LINE2);
	DispLineMW("DNS Primario", MW_LINE3, MW_SMFONT);
	ShowIp(dns, MW_LINE4, FALSE);
	DispLineMW("DNS Secundario", MW_LINE6, MW_SMFONT);
	ShowIp(&dns[4], MW_LINE7, FALSE);
	DispPutCMW(K_PopCursor);
}





