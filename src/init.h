/*
 * init.h
 *
 *  Created on: 28/07/2012
 *      Author: Jorge Numa
 */

#ifndef INIT_H_
#define INIT_H_

#include "common.h"

extern WORD gCnt;

extern BOOLEAN InitTrans(BOOLEAN automatico);
extern BYTE UnpackFld60( void );
extern void InitConfirm(void);
#endif /* INIT_H_ */
