//-----------------------------------------------------------------------------
//  File          : tmlpttest.c
//  Module        :
//  Description   : Thermal Printer function test.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  01 Apr  2009  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include "midware.h"
#include "sysutil.h"
#include "logo1.h"
#include "apm.h"
#include "lptutil.h"
#include "tmlpttest.h"
#include "hardware.h"
#include "corevar.h"

//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
// assume space between 2 bookmark is 33 lines * 12 dot per line
#define MAX_BOOKMARK_GAP     33 * 12
#define BOOKMARK_STEPS       20

// Printer Related
#define MAX_PRN_WIDTH        384
#define LINE_STEPS           2

#define MAX_ENG_FONT         3
#define MAX_DOT_MODE         2

// Test Print state
enum {
  PRINT_LOGO         = 0,
  PRINT_CONSTLINE       ,
  PRINT_ATTR            ,
  PRINT_SPFONT          ,
  PRINT_ERR             ,
  PRINT_END             ,
};

//-----------------------------------------------------------------------------
// Globals
//-----------------------------------------------------------------------------
static int gTmLptHandle = -1;

//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------
enum {
  PRN_BOLD,
  PRN_INVERT,
  PRN_UNDERLINE,
  PRN_DWIDTH,
  PRN_DHEIGHT,
  PRN_ATTR_MAX,
};
enum {
  FNT_ENG,
  FNT_TCHI,
  FNT_SCHI,
  FNT_FONT_MAX
};
#if 0
static const BYTE *KPrnMsg[FNT_FONT_MAX][PRN_ATTR_MAX] = {
  { "Bold", "Invert", "Underline", "DWidth", "DHeight" },
  { "����", "�ϥ�", "���u", "���e", "����" },
  { "����", "����", "����", "˫��", "˫��" },
};
static const BYTE KPrnCmd[PRN_ATTR_MAX] = { 'B', 'I', 'U', 'W', 'H'};
#endif

//*****************************************************************************
//  Function        : WaitPrnFinish
//  Description     : Wait Print finish or printer error.
//  Input           : N/A
//  Return          : current printer status.
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
/*static DWORD WaitPrnFinish(void)
{
  DWORD status;
  Delay10ms(50);
  status = StatMW(gTmLptHandle, MW_TMLPT_NORSTATUS, NULL);
  if (status & MW_TMLPT_FPRINTING) {
    do {
      SleepMW();
      status = StatMW(gTmLptHandle, MW_TMLPT_NORSTATUS, NULL);
      if ((status & 0xFF) != 0)    // Error
        break;
    } while ((status & MW_TMLPT_FFINISH) == 0);
  }
  return status;
}*/
//*****************************************************************************
//  Function        : LptClose
//  Description     : Close the printer.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
/*static void LptClose(void)
{
  WaitPrnFinish();
  CloseMW(gTmLptHandle);
  gTmLptHandle = -1;
}*/
//*****************************************************************************
//  Function        : LptOpen
//  Description     : Open and reset the printer.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
/*static DWORD LptOpen(void *aCfg)
{
  char filename[32];
  LptClose();
  strcpy(filename, DEV_LPT2);
  gTmLptHandle = OpenMW(filename, MW_WRONLY);
  if (gTmLptHandle >= 0) {
    StatMW(gTmLptHandle, MW_TMLPT_RESET, NULL);
    return TRUE;
  }
  return FALSE;
}*/
//*****************************************************************************
//  Function        : LptPutC
//  Description     : Send the char to printer.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
#if 0
static DWORD LptPutC(DWORD aChar)
{
  DWORD status;

  // Loop until fatal error or char send to printer.
  while (1) {
    status = StatMW(gTmLptHandle, MW_TMLPT_NORSTATUS, NULL);
    switch (status & 0xFF) {
      case MW_TMLPT_SOK:
        break;
      case MW_TMLPT_STEMPHIGH:
        Delay10ms(5*100); // Wait 5 Second for printer cool down;
        StatMW(gTmLptHandle, MW_TMLPT_ERR_RESTART, NULL);
        break;
      case MW_TMLPT_SVOLTLOW:
        Delay10ms(5*100); // Wait 5 Second for power stable
        status = StatMW(gTmLptHandle, MW_TMLPT_NORSTATUS, NULL);
        if (status & MW_TMLPT_SVOLTLOW)  // real voltage low
          return status;
        StatMW(gTmLptHandle, MW_TMLPT_ERR_RESTART, NULL);
        break;
      default :
        return status;
    }
    if ((status & 0xFF) == MW_TMLPT_SOK) {
      if (WriteMW(gTmLptHandle, &aChar, 1))
        break;
    }
  }
  return status;
}
#endif
//*****************************************************************************
//  Function        : LptPutN
//  Description     : Send the chars to printer.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
/*static DWORD LptPutN(const BYTE *aDat, DWORD aLen)
{
  DWORD status = StatMW(gTmLptHandle, MW_TMLPT_NORSTATUS, NULL);
  while (aLen--) {
    status = LptPutC(*aDat);
    if ((status & 0xFF) != MW_TMLPT_SOK)
      break;
    aDat++;
  }
  return status;
}*/
//*****************************************************************************
//  Function        : Adv2Bookmark
//  Description     : Align the paper to the next book mark.
//  Input           : aMaxStep; // Max Advance steps to find the bookmark
//  Return          : 0:Fail, 1:Paper Out, 2:Aligned
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
/*DWORD Adv2Bookmark(DWORD aMaxSteps)
{
  DWORD status, ret;
  BYTE  cmd[3];

  ret = 0; // assume fail
  status = WaitPrnFinish();
  if (status & 0xFF)
    return ret;

  do {
    cmd[0] = K_Esc;
    cmd[1] = 'J';
    cmd[2] = 0x08;
    status = LptPutN(cmd, 3);
    if (status&MW_TMLPT_FPAPEROUT) {  // bookmark or paper out
      ret = 1;
      cmd[2] = (BYTE) BOOKMARK_STEPS;
      status = LptPutN(cmd, 3);
      break;
    }
  } while (aMaxSteps--);

  WaitPrnFinish();

  // Double Check
  status = StatMW(gTmLptHandle, MW_TMLPT_NORSTATUS, NULL);
  if (!(status & MW_TMLPT_FPAPEROUT) && (ret == 1))
    ret = 2;
  return ret;
}*/
//*****************************************************************************
//  Function        : PrintLogo
//  Description     : Print logo.
//  Input           : aWidth;     // in number of Byte
//                    aHeight;    // in number of Byte
//                    *aLogoPtr;  // pointer to constant data
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
/*static DWORD PrintLogo(DWORD aWidth, DWORD aHeight, const BYTE *aLogoPtr)
{
  DWORD status, logobuf_size;
  BYTE cmd[4] = { 0x1B, 'K', 0x00, 0x00 };

  cmd[2] = (BYTE) (aWidth/8);
  cmd[3] = (BYTE) (aHeight>248?248:aHeight);

  status = LptPutN(cmd, 4);
  if ((status & 0xFF) != MW_TMLPT_SOK)
    return status;

  logobuf_size = aWidth/8 * cmd[3];

  status = LptPutN(aLogoPtr, logobuf_size);
  status = LptPutC('\n');
  return status;
}*/
//*****************************************************************************
//  Function        : PackAttr
//  Description     : Pack test print attribute text.
//  Input           : aBuf;   // dest buf
//                    aAttr;  // attribute type
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if 0
static BYTE *PackAttr(BYTE *aBuf, BYTE aAttr)
{
  DWORD i;
  T_APP_INFO app_info;
  BYTE tmpbuf[256];
  BYTE *ptr = aBuf;

  i = FNT_ENG;
  SprintfMW(tmpbuf, "\x1B%c\x31%s\x1B%c\x30", KPrnCmd[aAttr], KPrnMsg[i][aAttr], KPrnCmd[aAttr]);
  memcpy(ptr, tmpbuf, strlen(tmpbuf));
  ptr += strlen(tmpbuf);
  if (os_app_info(K_Font, &app_info)) {
    if (memcmp(app_info.s_app_name, "04FONTBIG5", 10) == 0)
      i = FNT_TCHI;
    else
      i = FNT_SCHI;
    SprintfMW(tmpbuf, "\x1B%c\x31%s\x1B%c\x30", KPrnCmd[aAttr], KPrnMsg[i][aAttr], KPrnCmd[aAttr]);
    memcpy(ptr, tmpbuf, strlen(tmpbuf));
    ptr += strlen(tmpbuf);
  }
  return ptr;
}
//*****************************************************************************
//  Function        : TestPrintAttr
//  Description     : Print char with different attribute.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************

static DWORD TestPrintAttr(void)
{
  const char *KFontName[3] = { "12x16", "8x16", "12x16T" };
  DWORD status;
  DWORD half_dot, font_type;
  BYTE  i, tmp[1024], *ptr, tmpbuf[256];


  for (half_dot=0; half_dot < MAX_DOT_MODE; half_dot++) {
    ptr = tmp;
    for (font_type=0; font_type < MAX_ENG_FONT; font_type++) {
      // Half Dot
      SprintfMW(tmpbuf, "\n\n\x1B%c%cHalf Dot Mode: %d  ", 'D', half_dot+'0', half_dot);
      memcpy(ptr, tmpbuf, strlen(tmpbuf));
      ptr += strlen(tmpbuf);

      // Font
      SprintfMW(tmpbuf, "\x1B%c%cFont: %s\n", 'F', font_type+'0',  KFontName[font_type]);
      memcpy(ptr, tmpbuf, strlen(tmpbuf));
      ptr += strlen(tmpbuf);
      if (font_type == 1) {   // 8x16
        //memset(ptr, 'H', 48);
        //ptr += 48;
        for (i='A'; i<='Z'; i++, ptr++)
          *ptr = i;
        for (i='a'; i<='z'; i++, ptr++)
          *ptr = i;
      }
      else {
        //memset(ptr, 'H', 32);
        //ptr += 32;
        for (i='A'; i<='Z'; i++, ptr++)
          *ptr = i;
        for (i='a'; i<='z'; i++, ptr++)
          *ptr = i;
      }
      *ptr++ = '\n';

      ptr = PackAttr(ptr, PRN_BOLD);
      ptr = PackAttr(ptr, PRN_INVERT);
      ptr = PackAttr(ptr, PRN_UNDERLINE);
      ptr = PackAttr(ptr, PRN_DWIDTH);
      ptr = PackAttr(ptr, PRN_DHEIGHT);
      ptr = PackAttr(ptr, PRN_DWIDTH);
      ptr = PackAttr(ptr, PRN_UNDERLINE);
      ptr = PackAttr(ptr, PRN_INVERT);
      ptr = PackAttr(ptr, PRN_BOLD);

      *ptr++ = '\n';
    }
    status = LptPutN(tmp, ptr-tmp);
    if ((status & 0xFF) != K_LptOk)
      return status;
  }
  return status;
}

//*****************************************************************************
//  Function        : TestPrintSpFont
//  Description     : Print char with Sp Font
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static DWORD TestPrintSpFont(void)
{
  BYTE  tmp[1024], *ptr, tmpbuf[256], app_name[25];
  T_APP_INFO app_info;
  DWORD i, status;

  if (!os_app_info(K_FontSp, &app_info))
    return K_LptOk;

  memset(app_name, 0, sizeof(app_name));
  memcpy(app_name, app_info.s_app_name, sizeof(app_info.s_app_name));
  for (i = 3; i <= 6; i++) {
    ptr = tmp;
    SprintfMW(tmpbuf, "\n\n\x1B%c0%s-Mode[%d]\n[", 'F', app_name, i);
    memcpy(ptr, tmpbuf, strlen(tmpbuf));
    ptr += strlen(tmpbuf);

    SprintfMW(tmpbuf, "\x1B%c%c\xF1\xF2\xF3\xF4\xF5\xF6\xF7\xF8\xF9\xFA", 'F', i);
    memcpy(ptr, tmpbuf, strlen(tmpbuf));
    ptr += strlen(tmpbuf);

    SprintfMW(tmpbuf, "\x1B%c0]\n", 'F');
    memcpy(ptr, tmpbuf, strlen(tmpbuf));
    ptr += strlen(tmpbuf);

    status = LptPutN(tmp, ptr-tmp);
    if ((status & 0xFF) != MW_TMLPT_SOK)
      break;
  }
  return status;
}
#endif
//*****************************************************************************
//  Function        : TMLptTest
//  Description     : Print a test page on thermal printer.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//****************************************************************************
void TMLptTest(void)
{
  DWORD status, state;
  BYTE tmp[32*3+1], tmpbuf[250];
  //BYTE tmpt[18];
 // struct DATETIME dtg;


  if (!LptOpen(NULL)) {
	  TextColor("ERROR DE IMPRESORA", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CLRDISP | MW_CENTER, 3);
    return;
  }
  TextColor("IMPRIMIENDO", MW_LINE4, COLOR_VISABLUE, MW_BIGFONT | MW_CLRDISP | MW_CENTER, 0);

  state = 0;
  while (1) {
    switch (state) {
      case PRINT_LOGO:
        status = PrintLogo(384, 48, KSpLogoPrn);
        if ((status & 0xFF) != MW_TMLPT_SOK) {
          state = PRINT_ERR;
          break;
        }
        state = PRINT_CONSTLINE;
        //state = PRINT_END;
        break;
      case PRINT_CONSTLINE:
        LptPutS("\n");
        LptPutN("VERSION:             ", 21);
//        LptPutN("SPXV01_C01", 10);
        LptPutN(VERCB, 10);
        LptPutS("\n");
       

        LptPutS("FECHA/HORA:   ");

      //  ReadRTC(&dtg);
      //ConvDateTime(tmpt, &dtg, 1);
     //  LptPutN(tmpt, 18);
        LptPutN(FECHA_COMPILACION, 13);
        LptPutN(HORA_COMPILACION, 5);
        LptPutS("\n");
        LptPutS("\n");

        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%c           CREDIBANCO\n", 'F','0');
        LptPutN(tmpbuf, 25);

        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%cABCDEFGHIJKLMNOPQRSTVWXYZ0123456\n", 'F','0');
        LptPutN(tmpbuf, 32);
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%c789!\"#$\\/()=?+*~{}^[],;:.-_\n", 'F','0');
        LptPutN(tmpbuf, 30);
        
        LptPutS("\n");
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%cABCDEFGHIJKLMNOPQRSTVWXYZ0123456\n", 'F','1');
        LptPutN(tmpbuf, 32);
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%c789!\"#$\\/()=?+*~{}^[],;:.-_\n", 'F','1');
        LptPutN(tmpbuf, 30);
        
        LptPutS("\n");
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%cABCDEFGHIJKLMNOPQRSTVWXYZ0123456\n", 'F','2');
        LptPutN(tmpbuf, 32);
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%c789!\"#$\\/()=?+*~{}^[],;:.-_\n", 'F','2');
        LptPutN(tmpbuf, 30);

         LptPutS("\n");
        
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%cABCDEFGHIJKLMNOPQRSTVWXYZ0123456\n", 'U','1');
        LptPutN(tmpbuf, 32);
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%c789!\"#$\\/()=?+*~{}^[],;:.-_\n", 'U','1');
        LptPutN(tmpbuf, 30);

        LptPutS("\n");
        
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%cABCDEFGHIJKLMNOPQRSTVWXYZ0123456\n", 'I','1');
        LptPutN(tmpbuf, 32);
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%c789!\"#$\\/()=?+*~{}^[],;:.-_\n", 'I','1');
        LptPutN(tmpbuf, 30);

         LptPutS("\n");
        
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%cABCDEFGHIJKLMNOPQRSTVWXYZ0123456\n", 'W','1');
        LptPutN(tmpbuf, 32);
        memset(tmpbuf,0x00,sizeof(tmpbuf));
        SprintfMW(tmpbuf, "\x1B%c%c789!\"#$\\/()=?+*~{}^[],;:.-_\n\x1B%c", 'W','1','@');
        LptPutN(tmpbuf, 30);

           
        
        if ((status & 0xFF) != MW_TMLPT_SOK) {
          state = PRINT_ERR;
          break;
        }
        
        
        state = PRINT_END;
        break;
      case PRINT_ERR:
        SprintfMW(tmp, "Lpt Err: %04X\x05", status);
        DispLineMW(tmp, MW_LINE4, MW_BIGFONT);
        //ShowStatus(tmp, STS_ERROR);
        StatMW(gTmLptHandle, MW_TMLPT_ERR_RESTART, NULL);
        APM_WaitKey(3000,0);
        break;
    }
    if ((state == PRINT_END) || (state == PRINT_ERR))
      break;
  }
  Adv2Bookmark(5*LINE_STEPS);


  LptClose();
  AcceptBeep();
  TextColor("TERMINADO!", MW_LINE4, COLOR_VISABLUE, MW_BIGFONT | MW_CLRDISP | MW_CENTER, 2);
  //ShowStatus("Completed!\x05", STS_OK);
}






 
  
