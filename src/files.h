/*
 * files.h
 *
 *  Created on: 30/07/2012
 *      Author: Jorge Numa
 */
#include "Corevar.h"


#ifndef FILES_H_
#define FILES_H_

#define KTabla0File  "Tabla0"
#define KTabla2File  "Tabla2"
#define KTabla3File  "Tabla3"
#define KTabla4File  "Tabla4"
#define KTabla5File  "Tabla5"
#define KTabla6File  "Tabla6"
#define KTabla12File "Tabla12"
#define KTabla0CFile "Tabla0C"
#define KTabla0DFile "Tabla0D"
#define KTabla0FFile "Tabla0F"
#define KTabla0EFile "Tabla0E"

#define KCTLGEN     "CTLGEN"
#define KCTLPPASS   "CTLPPASS"
#define KCTLPWAVE   "CTLPWAVE"

extern int gIdTabla0;
extern int gIdTabla2;
extern int gIdTabla3;
extern int gIdTabla4;
extern int gIdTabla5;
extern int gIdTabla6;
extern int gIdTabla12;
extern int gIdTabla0C;
extern int gIdTabla0D;
extern int gIdTabla0F;
extern int gIdTabla0E;

extern int gIdUsb;;
//================ TABLAS ===================

struct TABLA_CERO
{
	BYTE b_dll;
	BYTE b_ctrl_init; // "No aplica"
	BYTE b_vel_imp; // Velocidad Impresion
	BYTE b_iva;
	BYTE b_fecha_hora[6];
	BYTE b_opt_tel; // Opciones de telefono "No aplica"
	BYTE b_pass_super[2]; // Password supervisor
	BYTE b_tel_ayuda[12];
	BYTE b_opt1; // Ver opciones en la documentacion
	BYTE b_opt2; // Ver opciones en la documentacion
	BYTE b_opt3; // Ver opciones en la documentacion
	BYTE b_opt4; // Ver opciones en la documentacion
	BYTE b_dir_ciudad[46]; // Direccion y Ciudad del establecimiento
	BYTE b_nom_establ[23]; // Nombre del establecimiento
	BYTE b_simb_moneda;
	BYTE b_num_digitos; // Numero de digitos para monto transaccion
	BYTE b_num_decimales;
	BYTE b_lenguaje;
	BYTE b_num_cierre; // Numero de digitos para el cierre
	BYTE b_check_service; // No aplica
	BYTE b_iss_tbl_id; // check service issuer table id (No habilitado)
	BYTE b_acq_tbl_id; // check service aquirer table id (No habilitado)
	BYTE b_opt_local; // Opciones locales del terminal
	BYTE b_trans_default; // Transaccion por defecto (00h Compra, 01h Avance, 02h Consulta)
	BYTE b_tbl_emisor; // Selecci�n de tabla emisor para transacciones debito - "No aplica"
	BYTE b_tbl_adquir; // Selecci�n de tabla adquirente para transacciones debito - "No aplica"
	BYTE b_seg_espera; // Segundos de espera antes de hacer una transaccion - "No aplica"
	BYTE b_reservado;
	BYTE b_tipo_pinpad; // Tipo Pinpad - "No aplica"
	BYTE b_tipo_impres; // Tipo impresora - "No aplica"
	BYTE b_lim_cash_back; // Limite Cash Back - "No aplica"
	BYTE b_man_claves[2]; // Mantenimiento de claves - "No aplica"
	BYTE b_campos_reservados[46];

	WORD w_crc;
};

struct TABLA_CERO gTablaCero;

struct TABLA_DOS
{
	BYTE sb_pan_range_low[5];
	BYTE sb_pan_range_high[5];
	BYTE b_issuer_id;
	BYTE b_acquirer_id;
	BYTE b_pan_len;
	BYTE b_opt1; // Ver opciones en la documentacion
	BYTE b_opt2; // Ver opciones en la documentacion "No usado"
	BYTE b_opt3; // Ver opciones en la documentacion "No usado"
	BYTE b_opt4; // Ver opciones en la documentacion "No usado"
	BYTE b_id_tabla_iss; // ID tabla emisor "NO APLICA"
	BYTE b_id_tabla_acq; // ID tabla adquirente "NO APLICA"
	BYTE b_reservado[14];

	WORD w_crc;
};

struct TABLA_DOS gTablaDos;

struct TABLA_TRES
{
	BYTE b_emisor_id;
	BYTE b_nom_emisor[10];
	BYTE b_tel_ref[12];
	BYTE b_opt1; // Ver opciones en la documentacion
	BYTE b_opt2; // Ver opciones en la documentacion
	BYTE b_opt3; // Ver opciones en la documentacion
	BYTE b_opt4; // Ver opciones en la documentacion
	BYTE b_cuenta_def; // Cuenta por defecto
	BYTE b_reservado[2];
	BYTE b_limite_piso[2]; // No aplica
	BYTE b_no_implemt[21]; // No implementado

	WORD w_crc;
};

struct TABLA_TRES gTablaTres;

struct TABLA_CUATRO
{
	BYTE b_adquirente_id;
	BYTE b_nom_prog_adq[10];
	BYTE b_nom_adquirente[10];
	BYTE b_tel_pri[12];
	BYTE b_tiempo_con_1;
	BYTE b_num_intentos_1;
	BYTE b_tel_sec[12];
	BYTE b_tiempo_con_2;
	BYTE b_num_intentos_2;
	BYTE b_tel_cierre[12];
	BYTE b_timepo_con_3; // Tiempo de conexion para Cierre
	BYTE b_num_intentos_3;
	BYTE b_tel_cierre_2[12];
	BYTE b_tiempo_con_4;
	BYTE b_num_intentos_4;
	BYTE b_modo_modem;
	BYTE b_opt1; // Ver opciones en la documentacion
	BYTE b_opt2; // Ver opciones en la documentacion
	BYTE b_opt3; // Ver opciones en la documentacion
	BYTE b_opt4; // Ver opciones en la documentacion
	BYTE b_nii[2];
	BYTE b_cod_terminal[8];
	BYTE b_cod_estable[15]; // Codigo establecimiento
	BYTE b_timeout;
	BYTE b_num_lote[3];
	BYTE b_prox_lote[3];
	BYTE b_hora_cierre[4]; // No usado
	BYTE b_dia_cierre[2]; // No usado
	BYTE b_ewk1[8];
	BYTE b_idx_mk; // Indice MASTER KEY (ewk1)
	BYTE b_mac[8]; // MAC
	BYTE b_idx_mac; // Indice MAC
	BYTE b_id_visa1[23];
	BYTE b_modem_cierre; // No aplica
	BYTE b_num_intentos_cierre; // No usado
	BYTE b_logon_id_cierre[3]; // No usado
	BYTE b_protoc_cierre[11]; // No usado
	BYTE b_filler[25];
	BYTE b_idx_mk2; // Indice MASTER KEY (ewk2)
	BYTE b_ewk2[8];

	BYTE b_pending; // Este byte antes del crc
	WORD w_crc;
};

struct TABLA_CUATRO gTablaCuatro;

struct TABLA_CINCO
{
	BYTE b_sel_key;
	BYTE b_pos_ini;
	BYTE b_cant_datos;
	BYTE b_cod_ean[20];

	WORD w_crc;
};

struct TABLA_CINCO gTablaCinco;

struct TABLA_SEIS
{
	BYTE b_cod_prepago[4];
	BYTE b_descripcion[20];
	BYTE b_monto[18];
	BYTE b_tel[12];

	WORD w_crc;
};

struct TABLA_SEIS gTablaSeis;	//NM-29/04/13 Agregue esta linea


// ESTA TABLA NO SE USA - Esto advierte la documentaci�n
struct TABLA_SIETE
{
	BYTE b_data[100];

	WORD w_crc;
};

struct TABLA_12
{
	BYTE b_id; //kt-211112
	BYTE b_term_virtual[8];
	BYTE b_establ_virtual[9];
	BYTE b_nom_establ[15];

	WORD w_crc;
};

struct TABLA_12 gTablaDoce; //kt-211112

/*struct TABLA_0C {
 BYTE b_data[24];      // faltan los campos

 WORD w_crc;
 };

 struct TABLA_0D {
 BYTE b_data[75];      // faltan los campos

 WORD w_crc;
 };*/

struct TABLA_0C
{
	BYTE b_iss_id;
	BYTE b_icc_opt1;
	BYTE b_icc_opt2;
	BYTE b_icc_opt3;
	BYTE b_target; // Target Pocentage for biased random selection
	BYTE b_max_target; // Max Target Porcentage for biased random selection
	BYTE sb_Threshold[2]; // Threshold amount for biased random selection
	BYTE sb_tac_denial[5]; // Terminal action code denial
	BYTE sb_tac_online[5]; // Terminal action code online
	BYTE sb_tac_default[5]; // Terminal action code default
	BYTE b_trans_category_code; // Transaction category code default

	WORD w_crc;
};

struct TABLA_0D
{
	BYTE b_id;
	BYTE b_len_aid;
	BYTE sb_aid[16];
	BYTE b_icc_opt1;
	BYTE sb_aid_floor_limit[2];
	BYTE sb_aid_version1[2];
	BYTE sb_aid_version2[2];
	BYTE sb_aid_version3[2];
	BYTE sb_tdol[20];
	BYTE sb_ddol[20];
	BYTE sb_offl_approved_resp_code[2];
	BYTE sb_offl_declined_resp_code[2];
	BYTE sb_unable2go_online_appr[2]; // Unable to go online (Offline approved)
	BYTE sb_unable2go_online_decl[2]; // Unable to go online (Offline declined)

	//BYTE b_data[75];      // faltan los campos

	WORD w_crc;
};

struct TABLA_0F
{
	BYTE b_data[89]; // faltan los campos

	WORD w_crc;
};

/*struct TABLA_0E {
 BYTE b_data[276];      // faltan los campos

 WORD w_crc;
 };*/

typedef struct
{
	BYTE b_public_key_idx;
	BYTE b_key_len;
	BYTE sb_public_key[248];
	BYTE sb_public_key_rid[5];
	BYTE b_public_key_exp;
	BYTE sb_public_key_hash[20];

	//BYTE b_data[276];      // faltan los campos

	WORD w_crc;
} TABLA_0E;

struct TAGs_TX_DECLINADA		//kt-240413
{
	BYTE TAG_5F2A[5];     // TX CURRENCY CODE
	BYTE TAG_5F34[3];     // PAN SEQ No
	BYTE TAG_82[5];       // APP INTER PROFILE
	BYTE TAG_95[11];      // TVR
	BYTE TAG_9A[7];       // TXN DATE
	BYTE TAG_9C[3];       // TXN TYPE
	BYTE TAG_9F02[13];    // AMOUNT
	BYTE TAG_9F03[13];    // OTHER AMOUT
	BYTE TAG_9F10[17];    // IAD
	BYTE TAG_9F1A[5];     // TERM COUNTRY CODE
	BYTE TAG_9F26[15];    // APP CRYP
	BYTE TAG_9F27[3];     // CRYP INFO DATA
	BYTE TAG_9F34[7];     // CVM
	BYTE TAG_9F36[5];     // APP TXN COUNTER
	BYTE TAG_9F37[9];     // UNPREDICTABLE No
	BYTE TAG_5A[20];      // PAN (TRUNCATE)
	BYTE TAG_9F5B[8];     // ISSUER SCRIPT RESULT
	WORD TAG_9F5BLen;

}__attribute__((packed));

struct CTLS_FILE
{
	BYTE cs_paramc[5];
	BYTE cs_paramp[5];
	BYTE cs_paramw[5];
};

struct CTLGEN
{
    BYTE trans_limit[12];
    BYTE cvm_limit[12];
    BYTE floor_limit[12];
};

struct CTLGEN gCTLGEN;

#define KConfiComercioFile "CFCOMER"   //MFBC/08/11/12

struct CONFIG_COMERCIO
{
	WORD habEnvioPan; // Solo aplica para modo caja
	BYTE habEnvioSerial; // Habilita el envio del serial en las transacciones de modo caja
	BYTE habEnvioTracks;//Habilita la opcion envio de tracks
	BYTE habEnvioPAN;//Habilita la opcion envio de PAN
	BYTE SugerirCierre;//Habilita la opcion de sugerir cierre
	BYTE AutoCierre;//Habilita la opcion de auto cierre
	BYTE InformePolar;//Habilita la opcion de informe polar.
	BYTE flagCaja;//Habilita el POS para operar en modo caja
	BYTE modoCajaTCP;//Habilita la comunicacion TCP entre el POS y la caja	Daniel Cajas TCP
	BYTE EnviarHoraCaja;   // Enviar la hora a la caja   **SR**15/10/13
	BYTE habSSL;// Habilita SSL para la comunicacion TCP //Daniel SSL
	BYTE certSslName[20];//Nombre del certificado SSL	//Daniel SSL	
	BYTE EditCierre[6];//Opcion que guarda la hora del cierre //MFBC/21/11/12
	BYTE Referencia[16];//Opcion que guarda la refrencia //MFBC/20/09/13
	struct TAGs_TX_DECLINADA gTAG_Tx_declinada;		//kt-240413
	DWORD numCaja; //NORA
}__attribute__((packed));

struct CONFIG_COMERCIO gConfigComercio;




#define KTablaIvasFile "TablIVA"		//kt-311012

struct TABLA_IVAS //kt-311012
{
	WORD w_estadoBaseIva;
	BYTE b_ivas[3];
};

struct TABLA_IVAS gTablaIvas; //kt-311012

struct FILELIST
{
	DWORD	len;
	BYTE	filename[256];    /* maximum length created by Midware is 12 character only */
};

//-----------------------------------------------------------------------------
//      API
//-----------------------------------------------------------------------------

extern void ListFile(void);
#if 0
extern void CreateFile(void);
#endif
extern void DeleteFile(void);
extern void ReadFile(void);
#if 0
extern void WriteFile(void);
extern void ParamFile(void);
//extern void dummyMW(void);			// debug
//extern void dummyOS(void);			// debug
#endif

extern int CreateEmptyFile(const char *aFilename);
extern int OpenFile(const char *aFilename);
extern int CloseFile(int aIdFile);
extern BOOLEAN ReadSFile(DWORD aFileHandle, DWORD aOffset, void *aBuf, DWORD aLen);
extern BOOLEAN WriteSFile(DWORD aFileHandle, DWORD aOffset, const void *aBuf, DWORD aLen);
extern BOOLEAN WriteSFile2(DWORD aFileHandle, DWORD aOffset, void *aBuf, DWORD aLen);
extern BOOLEAN UpdTablaCero(int aFileHandle, struct TABLA_CERO *aDat);
extern BOOLEAN UpdTablaDos(int aFileHandle, DWORD aIdx, struct TABLA_DOS *aDat);
extern BOOLEAN UpdTablaTres(int aFileHandle, DWORD aIdx, struct TABLA_TRES *aDat);
extern BOOLEAN UpdTablaCuatro(int aFileHandle, DWORD aIdx, struct TABLA_CUATRO *aDat);
extern BOOLEAN UpdTablaCinco(int aFileHandle, DWORD aIdx, struct TABLA_CINCO *aDat);
extern BOOLEAN UpdTablaSeis(int aFileHandle, DWORD aIdx, struct TABLA_SEIS *aDat);
extern BOOLEAN UpdTabla12(int aFileHandle, DWORD aIdx, struct TABLA_12 *aDat);
extern BOOLEAN UpdTabla0C(int aFileHandle, DWORD aIdx, struct TABLA_0C *aDat);
extern BOOLEAN UpdTabla0D(int aFileHandle, DWORD aIdx, struct TABLA_0D *aDat);
extern BOOLEAN UpdTabla0F(int aFileHandle, DWORD aIdx, struct TABLA_0F *aDat);
extern BOOLEAN UpdTabla0E(int aFileHandle, DWORD aIdx, TABLA_0E *aDat);

extern BOOLEAN GetTablaCero(struct TABLA_CERO *aDat);
extern BOOLEAN GetTablaDos(DWORD aIdx, struct TABLA_DOS *aDat);
extern BOOLEAN GetTablaTres(DWORD aIdx, struct TABLA_TRES *aDat);
extern BOOLEAN GetTablaCuatro(DWORD aIdx, struct TABLA_CUATRO *aDat);
extern BOOLEAN GetTablaCinco(DWORD aIdx, struct TABLA_CINCO *aDat);
extern BOOLEAN GetTablaSeis(DWORD aIdx, struct TABLA_SEIS *aDat);	//NM-29/04/13 Agregue esta funcion

extern BOOLEAN GetTablaDoce(DWORD aIdx, struct TABLA_12 *aDat); //kt-211112

extern int GetTabla12Count(void); //kt-211112
extern int GetTabla2Count(void);
extern int GetTabla3Count(void);
extern int GetTabla4Count(void);
extern int GetTabla5Count(void);
extern int GetTabla6Count(void);	//NM-29/04/13 Agregue esta funcion.

extern void FilesVisaInit(void);
extern void OpenFilesInit(void);
extern void CloseFilesInit(void);
extern BOOLEAN GetTabla0C(DWORD aIdx, struct TABLA_0C *aDat);
extern BOOLEAN GetTabla0D(DWORD aIdx, struct TABLA_0D *aDat);
extern int GetTabla0CCount(void);
extern int GetTabla0DCount(void);


extern void SetFL63_BILL_TX(void);
extern void SetTokConsRecarga(void); //NM-29/04/13 Agregue esta funcion.

extern void VisaInitSet(BOOLEAN flag_init); //MFBC/02/05/13
extern BOOLEAN GetTabla0E(DWORD aIdx, TABLA_0E *aDat);
extern int GetTabla0ECount(void);

extern void SetPendingVISA(int aIdx, BYTE aVal);
extern BYTE GetPendingVISA(int aIdx);


extern BOOLEAN UpdTablaIvas(int aFileHandle, DWORD aIdx, struct TABLA_IVAS *aDat); //kt-311012
extern BOOLEAN GetTablaIvas(DWORD Id, DWORD aIdx, struct TABLA_IVAS *aDat); //kt-311012
extern int GetTablaIvasCount(void); //kt-311012
extern BOOLEAN ReadSConfigFile(struct CONFIG_COMERCIO *aConfig_data); //MFBC/09/11/12
extern void SaveConfigFile(struct CONFIG_COMERCIO *aConfig_data) ;//MFBC/08/11/12

extern BOOLEAN UpdConfigGPRS (struct CNGPRS *aData, DWORD aLen);

extern void DownloadFile(void);
extern BOOLEAN GetPARAMFile(BYTE *buff, BYTE *aFileName, BOOLEAN aShowError);
extern BOOLEAN IsThereFile(BYTE *aFileName, BOOLEAN aShowError);
extern BOOLEAN CSParamFile(BYTE *aChecksum, BYTE *aFileName, BOOLEAN aShowError);
extern void ContactlessInit( void );
extern void upackedCTLGEN(BYTE *buff);
#endif /* FILES_H_ */

