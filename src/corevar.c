//-----------------------------------------------------------------------------
//  File          : corevar.c
//  Module        :
//  Description   : Include Globals variables for EDC.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include "midware.h"
#include "hardware.h"
#include "constant.h"
#include "util.h"
#include "stdlib.h"
#include "corevar.h"
#include "record.h"
#include "files.h"
#include "cajas.h"
#include "coremain.h"

#ifdef FTPDL
#include "ftpFiles.h"
#endif // FTPDL

//-----------------------------------------------------------------------------
//      Global Data Structure
//-----------------------------------------------------------------------------
//WORD gRspCode;

struct GDS *gGDS;
struct APPDATA gAppDat;
struct FL63_BILL gfl63_bill_tx;
struct FL63_BILL gfl63_bill_rx;
struct BINES gBines[MAX_BINES];
struct TARJETA_VIRTUAL gTarjVirtual;
struct CONFIG_COMERCIO gConfigComercio;
struct BIN_TIPO gBin_Tipo; //MFBC/03/12/12
struct CNGPRS gConfigGPRS;   		// Estructura para la configuracion GPRS   **SR**  20/09/13

BYTE gSSLKeyIdx;//Daniel SSL
DWORD gSSL_KEY;

struct TXN_RECORD gOrg_rec;
DWORD gRecIdx;

BYTE gTrack2[20]; // Track2 Auxiliar usado en la transaccion de cheques y recaudo exito
//CHEQUES_POSTFECHADO
BYTE gField48[256]; // Campo 48 auxiliar usado en la transaccion de cheques
BYTE tipoTransCaja;
BYTE gEWKPIN[16];  //MFBC/10/12/12
BYTE gActualiza_Logon[10];  //MFBC/08/04/13
BOOLEAN g_sync_logon;
BOOLEAN g_Puntos_Cierre;
/*
BYTE gDir_ciudad[46];  //MFBC/10/12/12
BYTE gNom_establ[23];  //MFBC/10/12/12
 */
BOOLEAN flagAnul;		//kt-domingo
int contAero; 	//kt-090413

BYTE g_roc_no_aero[3];		//MFBC/14/03/13
BOOLEAN init_auto;		//MFBC/05/06/13
BOOLEAN logon_auto;		//MFBC/05/06/13
BYTE g_TransCodeAgencia[3];		//MFBC/15/08/13
BYTE g_aeroTransCode[3];		//MFBC/15/08/13
DDWORD g_count_text;
DDWORD g_count_ind;
BOOLEAN gCierreIntegrado;
BOOLEAN g_pagosHechos;

BYTE gPinBlock[8];

//----------------------------------------------------------------------
//  Application data file
//----------------------------------------------------------------------
int gAppDatHdl = -1;
static const char KAppDatFile[] =
{ "EDCFILE" };
static const char KBinesPINFile[] =
{ "BINES1" }; // Archivo que contiene los Bines para los cuales se pide PIN en el Avance
static const char KBinesPANFile[] =
{ "BINES2" }; // Archivo que contiene los Bines para los cuales se debe enviar el PAN (solo para cajas)
static const char KBinesCtasFile[] =
{ "BINES3" }; // Archivo que contiene los Bines Para los cuales se solicita numero de cuotas en el Avance
static const char KBinesTrackFile[] =
{ "BINES4" }; // Archivo que contiene los Bines Para los cuales se solicita numero de cuotas en el Avance
static const char KTarjVirtFile[] =
{ "TARJETA" }; // Archivo que contiene los datos de la tarjeta virtual
static const char KCajasFile[] =
{ "CAJAS" }; // Archivo qu contiene la ultima transaccion hecha con cajas

struct BINES *gBinesOffset = 0;
struct APPDATA *gAppdataOffset = 0; //MFBC/13/11/12
struct TARJETA_VIRTUAL *gTarjVirtualOffset = 0;
struct CAJAS *gCajasOffset = 0;


//*****************************************************************************
//  Function        : DataFileInit
//  Description     : Init. Data file.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DataFileInit(void)
{
	BYTE filename[32];
	struct APPDATA app_data;

	memset(filename, 0x00, sizeof(filename));
	strcpy(filename, KAppDatFile);
	gAppDatHdl = fOpenMW(filename);

	if (gAppDatHdl < 0)
	{
		//printf("\fVoy a crear");APM_WaitKey(3000, 0);
		//DispLineMW("Create: EDCFile", MW_MAX_LINE, MW_CLREOL|MW_SPFONT);
		gAppDatHdl = fCreateMW(filename, FALSE);
		memset(&app_data, 0x00, sizeof(struct APPDATA));
		app_data.ConexionLAN = TRUE;
		app_data.ModoLlamada = TRUE;
		app_data.TipoMarcado = TRUE;
		memset(app_data.Prefijo, 0xFF, sizeof(app_data.Prefijo));
		memset(app_data.Posfijo, 0xFF, sizeof(app_data.Posfijo));
		app_data.porcPropina = 10; //MFBC/14/11/12
		memcpy(app_data.IpHost, "\xAC\x13\xC8\x55", 4); //MFBC/03/10/13  IP host por defecto sugerida por credibanco 172.19.200.85
		//		app_data.PortHost = 2100; //MFBC/10/12/12  port host por defecto											// **SR** 27/09/13
		memcpy(app_data.NII, "\x00\x02", 2);
		memcpy(app_data.NII_Moviles, "\x00\x01", 2);
		memcpy(app_data.NumRoc, "\x00\x00\x01", 3);
		memcpy(app_data.NumTrans, "\x00\x00\x01", 3);
		app_data.tipoTransCaja = -1;	//Daniel Jacome 09/09/13

		fWriteMW(gAppDatHdl, &app_data, sizeof(struct APPDATA));
		fCommitAllMW();
	}
	else
	{
		//printf("\fVoy a leer");APM_WaitKey(3000, 0);
		fReadMW(gAppDatHdl, &app_data, sizeof(struct APPDATA));
	}

	//	if(gf_CleanIPHost == TRUE){				// En este punto se limpian los datos cuando es carga parcial **SR**  02/10/13
	//		//memset(app_data.IpHost, 0x00, sizeof(app_data.IpHost));
	//		//memcpy(app_data.IpHost, "\xAC\x13\xC8\x55", 4); //MFBC/03/10/13  IP host por defecto sugerida por credibanco 172.19.200.85
	//		memcpy(app_data.NII, "\x00\x02", 2);
	//		memcpy(app_data.NII_Moviles, "\x00\x01", 2);
	//		fWriteMW(gAppDatHdl, &app_data, sizeof(struct APPDATA));
	//		fCommitAllMW();
	//		gf_CleanIPHost = FALSE;
	//	}

	memcpy(&gAppDat, &app_data, sizeof(struct APPDATA));
	//	printf("\fRocDATA |%02x%02x%02x|", gAppDat.RocNo[0], gAppDat.RocNo[1], gAppDat.RocNo[2]); WaitKey(3000, 0);

	//printf("\fRocDATA %d - %d", gAppDat.RocNo);APM_WaitKey(3000, 0);
	fCloseMW(gAppDatHdl);
}

//*****************************************************************************
//  Function        : DataFileUpdate
//  Description     : Update Data File from memory.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DataFileUpdate(void)
{
	fCommitAllMW();
}
//*****************************************************************************
//  Function        : DataFileClose
//  Description     : Close local data file.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void DataFileClose(void)
{
	struct APPDATA app_dat;

	memcpy(&app_dat, &gAppDat, sizeof(struct APPDATA));
	fWriteMW(gAppDatHdl, &app_dat, sizeof(struct APPDATA));
	fCloseMW(gAppDatHdl);
	fCommitAllMW();
	gAppDatHdl = -1;
}


void cajasFileInit(void)
{
	BYTE filename[32];
	struct CAJAS st_cajas;
	int fd;

	memset(&st_cajas, 0, sizeof(struct CAJAS));
	strcpy(filename, KCajasFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: T.VIRTUAL FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		memset(&st_cajas, 0x00, sizeof(struct CAJAS)); //MFBC/22/04/13
		fWriteMW(fd, &st_cajas, sizeof(struct CAJAS));
		fCommitAllMW();
	}
	/*else
	{
		fReadMW(fd, &tVirtual, sizeof(struct TARJETA_VIRTUAL));
	}*/
	//memcpy(&gTarjVirtual, &tVirtual, sizeof(struct TARJETA_VIRTUAL));
	fCloseMW(fd);
}

/*****************************************
 * Descripcion: Inicial y lee el archivo de configuracion GPRS
 *  ** SR** 20/09/13
 ****************************************/

void InitFileGPRS (void){

	BYTE filename[32];
	int fd = -1;
	struct CNGPRS AuxCongiGPRS;

	memset(&AuxCongiGPRS, 0x00, sizeof(AuxCongiGPRS));
	memset(filename, 0x00, sizeof(filename));

	strcpy(filename, KConfigGPRS);
	fd = fOpenMW(filename);

	if(fd  < 0){
		fd = fCreateMW(filename, FALSE);
		fWriteMW(fd, &AuxCongiGPRS, sizeof(struct CNGPRS));
	}
	else
		fReadMW(fd, &AuxCongiGPRS, sizeof(struct CNGPRS));

	memcpy(&gConfigGPRS, &AuxCongiGPRS, sizeof(struct CNGPRS));

	fCommitAllMW();
	fCloseMW(fd);

	return;
}


void tarjetaVirtualInit(void)
{
	BYTE filename[32];
	struct TARJETA_VIRTUAL tVirtual;
	int fd;

	memset(&tVirtual, 0, sizeof(struct TARJETA_VIRTUAL));
	strcpy(filename, KTarjVirtFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: T.VIRTUAL FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		tVirtual.Flag_fecha = FALSE; //MFBC/22/04/13
		tVirtual.Flag_pan = FALSE; //MFBC/22/04/13
		memset(tVirtual.PAN, 0x00, sizeof(tVirtual.PAN)); //MFBC/22/04/13
		memset(tVirtual.fechaVenc, 0x00, sizeof(tVirtual.fechaVenc)); //MFBC/22/04/13
		fWriteMW(fd, &tVirtual, sizeof(struct TARJETA_VIRTUAL));
		fCommitAllMW();
	}
	else
	{
		fReadMW(fd, &tVirtual, sizeof(struct TARJETA_VIRTUAL));
	}
	memcpy(&gTarjVirtual, &tVirtual, sizeof(struct TARJETA_VIRTUAL));
	fCloseMW(fd);
}

void BinesPINFileInit(void)
{
	BYTE filename[32];
	struct BINES tabBines[MAX_BINES];
	int fd;

	strcpy(filename, KBinesPINFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: BINES FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		memset(&tabBines, 0, sizeof(tabBines));
		fWriteMW(fd, &tabBines, sizeof(tabBines));
		fCommitAllMW();
	}
	fCloseMW(fd);
}

void BinesPANFileInit(void)
{
	BYTE filename[32];
	struct BINES tabBines[MAX_BINES];
	int fd;

	strcpy(filename, KBinesPANFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: BINES FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		memset(&tabBines, 0, sizeof(tabBines));
		fWriteMW(fd, &tabBines, sizeof(tabBines));
		fCommitAllMW();
	}
	fCloseMW(fd);
}

void BinesCtasFileInit(void)
{
	BYTE filename[32];
	struct BINES tabBines[MAX_BINES];
	int fd;

	strcpy(filename, KBinesCtasFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: BINES FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		memset(&tabBines, 0, sizeof(tabBines));
		fWriteMW(fd, &tabBines, sizeof(tabBines));
		fCommitAllMW();
	}
	fCloseMW(fd);
}

void BinesTrackFileInit(void)
{
	BYTE filename[32];
	struct BINES tabBines[MAX_BINES];
	int fd;

	strcpy(filename, KBinesTrackFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: BINES FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		memset(&tabBines, 0, sizeof(tabBines));
		fWriteMW(fd, &tabBines, sizeof(tabBines));
		fCommitAllMW();
	}
	fCloseMW(fd);
}

/****************************************************************************/

void configComercioInit(void)
{
	BYTE filename[32];
	struct CONFIG_COMERCIO ConfigComercio;
	int fd;

	strcpy(filename, KConfiComercioFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: C.Comercio FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		memset(&ConfigComercio, 0, sizeof(ConfigComercio));
		ConfigComercio.habEnvioTracks = TRUE;
		memset(ConfigComercio.EditCierre, 0x30, sizeof(ConfigComercio.EditCierre)); //MFBC/23/11/12
		memset(ConfigComercio.Referencia, 0x00, sizeof(ConfigComercio.Referencia)); //MFBC/23/11/12
		fWriteMW(fd, &ConfigComercio, sizeof(ConfigComercio));
		fCommitAllMW();
	}
	else
	{
		fReadMW(fd, &ConfigComercio, sizeof(struct CONFIG_COMERCIO));
	}

	memcpy(&gConfigComercio, &ConfigComercio, sizeof(struct CONFIG_COMERCIO));
	fCloseMW(fd);
}

/****************************************************************************/
void TablaIvaFileInit(void) //kt-311012
{
	BYTE filename[32];
	struct TABLA_IVAS gTablaIva;
	int fd;

	strcpy(filename, KTablaIvasFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		//DispLineMW("Create: TABLA IVA. FILE", MW_MAX_LINE, MW_CENTER|MW_CLREOL|MW_SMFONT);
		fd = fCreateMW(filename, FALSE);
		memset(&gTablaIva, 0, sizeof(gTablaIva));
		fWriteMW(fd, &gTablaIva, sizeof(gTablaIva));
		fCommitAllMW();
	}
	fCloseMW(fd);
}

//*****************************************************************************
//  Function        : CorrectHost
//  Description     : Check Whether Support This host.
//  Input           : aHostType;        // Host Type
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN CorrectHost(BYTE aHostType)
{
	//return (aHostType == HANDLE_HOST_TYPE);
	return TRUE;
}
//*****************************************************************************
//  Function        : GenAuthCode
//  Description     : Auto generate Authorization code.
//  Input           : aAuthCode;        // pointer to 6 byte buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void GenAuthCode(BYTE *aAuthCode)
{
	//BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	//GetAppdata((struct APPDATA*) conf_AppData);
	bcdinc(gAppDat.auto_auth_code, 3);
	split(aAuthCode, gAppDat.auto_auth_code, 3);
	//FreeMW(conf_AppData);   //MFBC/14/11/12

}
//*****************************************************************************
//  Function        : SetDefault
//  Description     : Restore to Default Setup.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SetDefault(void)
{
	memset(&gAppDat, 0, sizeof(struct APPDATA));
}
//*****************************************************************************
//  Function        : MsgBufSetup
//  Description     : Initialize pack utils for msgbuf.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void MsgBufSetup(void)
{
	set_pptr(MSG_BUF.sb_content, MSG_BUF_LEN);
}
//*****************************************************************************
//  Function        : PackMsgBufLen
//  Description     : Update the msgbuf's len;
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackMsgBufLen(void)
{
	MSG_BUF.d_len = get_distance();
}
//*****************************************************************************
//  Function        : TxBufSetup
//  Description     : Initialize pack utils for txbuf.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void TxBufSetup(BOOLEAN aAdd2ByteLen)
{
	set_pptr(TX_BUF.sbContent, MSG_BUF_LEN);
	// pack additonal 2 byte msg len for TCP/IP connection
	if (aAdd2ByteLen)
		inc_pptr(2);
}
//*****************************************************************************
//  Function        : PackTxBufLen
//  Description     : Update the TxBuf's len;
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackTxBufLen(BOOLEAN aAdd2ByteLen)
{
	DWORD len;

	TX_BUF.wLen = get_distance();
	if (aAdd2ByteLen)
	{
		len = TX_BUF.wLen - 2;
		TX_BUF.sbContent[0] = (BYTE) ((len >> 8) & 0xFF);
		TX_BUF.sbContent[1] = (BYTE) (len & 0xFF);
	}
}
//*****************************************************************************
//  Function        : RxBufSetup
//  Description     : Initialize pack utils for txbuf.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void RxBufSetup(BOOLEAN aAdd2ByteLen)
{
	set_pptr(RX_BUF.sbContent, MSG_BUF_LEN);
	// pack additonal 2 byte msg len for TCP/IP connection
	if (aAdd2ByteLen)
		inc_pptr(2);
}
//*****************************************************************************
//  Function        : PackRxBufLen
//  Description     : Update the TxBuf's len;
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PackRxBufLen(BOOLEAN aAdd2ByteLen)
{
	DWORD len;

	RX_BUF.wLen = get_distance();
	if (aAdd2ByteLen)
	{
		len = RX_BUF.wLen - 2;
		RX_BUF.sbContent[0] = (BYTE) ((len >> 8) & 0xFF);
		RX_BUF.sbContent[1] = (BYTE) (len & 0xFF);
	}
}
//*****************************************************************************
//  Function        : ValidRecordCRC
//  Description     : Valid the record's crc.
//  Input           : aIdx;      // record index
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ValidRecordCRC(WORD aIdx)
{
	return (RECORD_BUF.w_crc == cal_crc((BYTE *) &RECORD_BUF, (BYTE *) &RECORD_BUF.w_crc - (BYTE *) &RECORD_BUF.b_trans));
}
//*****************************************************************************
//  Function        : RefreshDispAfter
//  Description     : Set Display Timer & display changed flag.
//  Input           : aSec;     // timer in sec.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void RefreshDispAfter(DWORD aSec)
{
	gGDS->b_disp_chgd = TRUE;
	aSec *= 200; // convert unit of 10ms
	TimerSetMW(gTimerHdl[TIMER_DISP], aSec);
}
//*****************************************************************************
//  Function        : FreeGDS
//  Description     : Free GDS buffer.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void FreeGDS(void)
{
	if (gGDS != NULL)
		FreeMW(gGDS);
}
//*****************************************************************************
//  Function        : MallocGDS
//  Description     : Malloc all buffer required.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN MallocGDS(void)
{
	gGDS = (struct GDS *) MallocMW(sizeof(struct GDS));
	MemFatalErr(gGDS); // Check Memory
	memset(gGDS, 0x00, sizeof(struct GDS));
	gGDS->s_EMVIn.bMsg = gGDS->sb_IOBuf;
	gGDS->s_EMVOut.bMsg = gGDS->sb_IOBuf;
	gGDS->s_CTLIn.pbMsg = gGDS->sb_IOBuf;
	gGDS->s_CTLOut.pbMsg = gGDS->sb_IOBuf;
	return TRUE;
}
//*****************************************************************************
//  Function        : TrainingModeON
//  Description     : Check training mode status;
//  Input           : N/A
//  Return          : TRUE/FALSE;     // TRUE => On.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN TrainingModeON(void)
{
	return (STIS_TERM_DATA.w_training_status == MAGIC);
}
//*****************************************************************************
//  Function        : IncTraceNo
//  Description     : Increment the system trace no & save to input & tx_data.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void IncAPMTraceNo(void)
{
	struct APPDATA *Global;
	Global= (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(Global,&gAppDat,sizeof(struct APPDATA));

	//memcpy(INPUT.sb_roc_no, INPUT.sb_trace_no, 3);
	bcdinc(Global->NumTrans, 3);
	if (memcmp(Global->NumTrans, "\x00\x00\x00", 3) == 0)
		bcdinc(Global->NumTrans, 3);
	APM_SetTrace(Global->NumTrans);
	SaveDataFile(Global);
	FreeMW(Global);


	//	bcdinc(STIS_TERM_DATA.sb_trace_no, 3);
	//	if (memcmp(STIS_TERM_DATA.sb_trace_no, "\x00\x00\x00", 3) == 0)
	//		bcdinc(STIS_TERM_DATA.sb_trace_no, 3);
	//	APM_SetTrace(STIS_TERM_DATA.sb_trace_no);
}

//*****************************************************************************
//  Function        : SaveDataFile
//  Description     : Save APPDATA.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SaveDataFile(struct APPDATA *aApp_data) //MFBC/13/11/12
{
	int fd;
	char filename[32];

	strcpy(filename, KAppDatFile);

	fd = fOpenMW(filename);

	if (fd < 0)
	{
		DispLineMW("APPDATA File ERROR!", MW_MAX_LINE, MW_CLRDISP|MW_REVERSE|MW_CLREOL|MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		os_beep_close();
		return;
	}
	//printf("\fFlag3 = %d", gAppDat.enableCardExito);APM_WaitKey(3000, 0);
	WriteSFile(fd, (DWORD) &gAppdataOffset, aApp_data, sizeof(struct APPDATA));
	fCommitAllMW();
	memcpy(&gAppDat, aApp_data, sizeof(struct APPDATA));//Daniel 16/11/12	
	fCloseMW(fd);
}

void SaveBinIntoFile(BYTE *BIN, BYTE opcion, BYTE offset)
{
	int fd;
	char filename[32];

	switch (opcion)
	{
	case 1:
		strcpy(filename, KBinesPINFile);
		break;

	case 2:
		strcpy(filename, KBinesPANFile);
		break;

	case 3:
		strcpy(filename, KBinesCtasFile);
		break;

	case 4:
		strcpy(filename, KBinesTrackFile);
		break;
	}

	fd = fOpenMW(filename);

	if (fd < 0)
	{
		DispLineMW("APPDATA File ERROR!", MW_MAX_LINE, MW_CLRDISP|MW_REVERSE|MW_CLREOL|MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		os_beep_close();
		return;
	}
	WriteSFile2(fd, (DWORD) &gBinesOffset[offset], BIN, 6);
	fCloseMW(fd);
}

/*************************************************************/
//guardarBIN: Guarda un BIN de tarjeta digitado en su archivo
//opcion: 1 - Bines para PIN
//		  2 - Bines para PAN
//        3 - Bines para Ctas
/*************************************************************/
void guardarBIN(BYTE opcion) //MFBC/17/04/13
{
	BYTE cont = 0, i;
	BYTE label[21 + 1];
	BYTE kbdbuff[10];
	BYTE BIN[6 + 1];
	BOOLEAN flagRepetido;
	struct BINES binesTemp[6];
	leerBINES(opcion);

	struct CONFIG_COMERCIO *conf_comer = NULL;

	memset(binesTemp, 0x00, sizeof(struct BINES));
	memset(BIN, 0x00, sizeof(BIN));

	while (cont < MAX_BINES)
	{
		memset(BIN, 0x00, sizeof(BIN));
		flagRepetido = FALSE;
		DispLineMW("INGRESAR BINES", MW_LINE1, MW_CLRDISP|MW_CENTER|MW_REVERSE|MW_CLREOL|MW_SMFONT);
		sprintf(label, "BIN Numero %d de %d:", cont + 1, MAX_BINES);
		DispLineMW(label, MW_LINE3, MW_LEFT|MW_SMFONT);

		memset(kbdbuff, 0x00, sizeof(kbdbuff));
		memcpy(&kbdbuff[1], gBines[cont].BIN, strlen(gBines[cont].BIN));

		kbdbuff[0] = strlen(gBines[cont].BIN);
		if (!APM_GetKbdSpectra(NUMERIC_INPUT + ECHO + MW_SMFONT + MW_LINE5, /*IMIN(6) +*/IMAX(6), kbdbuff))
			return;

		if (kbdbuff[0] > 0)
		{
			memcpy(BIN, &kbdbuff[1], kbdbuff[0]);
		}
		else
		{
			memset(BIN, 0x00, sizeof(BIN));
		}

		if( atoi(&kbdbuff[1]) == 0 && kbdbuff[0] > 0)
		{
			LongBeep();
			continue;
		}


		if (memcmp(BIN, "\x00\x00\x00\x00\x00\x00", 6) != 0 )
		{
			for(i=0; i< MAX_BINES; i++)
			{
				if (memcmp(binesTemp[i].BIN, BIN, 6) == 0 )
				{
					LongBeep();
					TextColor("Bin Repetido", MW_LINE4, COLOR_RED, MW_CENTER | MW_CLRDISP, 3);
					flagRepetido = TRUE;
					os_beep_close();
					break;
				}

			}
		}

		if(flagRepetido == TRUE)
			continue;

		memcpy(binesTemp[cont].BIN, BIN, sizeof(BIN));

		SaveBinIntoFile(BIN, opcion, cont);
		cont++;
	}


	for(i=0; i< MAX_BINES; i++)
	{
		if (memcmp(binesTemp[i].BIN,  "\x00\x00\x00\x00\x00\x00", 6) != 0 )
			break;
	}

	if(i == MAX_BINES  )
	{

		conf_comer = (struct CONFIG_COMERCIO*) MallocMW(sizeof(struct CONFIG_COMERCIO));
		memcpy(&conf_comer->habEnvioPan, &gConfigComercio.habEnvioPan, sizeof(struct CONFIG_COMERCIO));

		switch(opcion)
		{
		//		case 1:
		//			((struct CONFIG_COMERCIO*) conf_comer)->habPinAvanc = FALSE;
		//			break;
		case 2:
			((struct CONFIG_COMERCIO*) conf_comer)->habEnvioPan = 0;
			break;
			//		case 3:
			//			((struct CONFIG_COMERCIO*) conf_comer)->habCuotasAvanc = FALSE;
			//			break;
		case 4:
			((struct CONFIG_COMERCIO*) conf_comer)->habEnvioTracks = FALSE;
			break;

		}

		SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);

	}
	FreeMW(conf_comer);

}

/*******************************************************************/
//leerBINES: Lee todos los BINES de tarjeta del archivo seleccionado
//opcion: 1 - Bines para PIN
//		  2 - Bines para PAN
//        3 - Bines para Ctas
/*******************************************************************/
void leerBINES(BYTE opcion)
{

	int fd;
	char filename[32];
	struct BINES tabBines[MAX_BINES];
	//BYTE i;

	memset(&tabBines, 0x00, sizeof(tabBines));

	switch (opcion)
	{
	case 1:
		strcpy(filename, KBinesPINFile);
		break;

	case 2:
		strcpy(filename, KBinesPANFile);
		break;

	case 3:
		strcpy(filename, KBinesCtasFile);
		break;

	case 4:
		strcpy(filename, KBinesTrackFile);
		break;

	}

	fd = fOpenMW(filename);
	fReadMW(fd, &tabBines, sizeof(tabBines));
	memcpy(&gBines, &tabBines, sizeof(tabBines));
	fCloseMW(fd);
	/*for(i=0; i<MAX_BINES; i++){
	 if( strlen(gBines[i].BIN) > 0 ){
	 printf("\fBIN-%d = %s", i, gBines[i].BIN);
	 APM_WaitKey(3000, 0);
	 }
	 }*/
}

/*******************************************************************/
//buscarBines:Busca el BIN de una tarjeta dentro del archivo de BINES
//            seleccionado.
//opcion: 1 - Bines para PIN
//		  2 - Bines para PAN
//        3 - Bines para Ctas
/*******************************************************************/
BOOLEAN buscarBines(BYTE aOpcion, BYTE *aBin)
{
	BYTE i;
	BYTE binCard[6 + 1];

	memset(binCard, 0x00, sizeof(binCard));
	memcpy(binCard, aBin, 6);

	leerBINES(aOpcion);
	for (i = 0; i < MAX_BINES; i++)
	{
		if (memcmp(binCard, gBines[i].BIN, strlen(binCard)) == 0)
		{
			return TRUE;
		}
	}
	return FALSE;
}

/*************************************************************/
//borrarBINES: Borra todos los BINES almacenados en un archivo
//opcion: 1 - Bines para PIN
//		  2 - Bines para PAN
//        3 - Bines para Ctas
/*************************************************************/
void borrarBINES(BYTE opcion)
{
	BYTE filename[16];
	int fd;
	struct BINES tab_bines[MAX_BINES];

	memset(tab_bines, 0x00, sizeof(tab_bines));
	memset(filename, 0x00, sizeof(filename));

	switch (opcion)
	{
	case 1:
		strcpy(filename, KBinesPINFile);
		break;

	case 2:
		strcpy(filename, KBinesPANFile);
		break;

	case 3:
		strcpy(filename, KBinesCtasFile);
		break;
	}

	fd = fOpenMW(filename);

	if (fd < 0)
	{
		DispLineMW("APPDATA File ERROR!", MW_MAX_LINE, MW_CLRDISP|MW_REVERSE|MW_CLREOL|MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		os_beep_close();
		return;
	}
	fWriteMW(fd, &tab_bines, sizeof(tab_bines));
	fCommitAllMW();
	fCloseMW(fd);
}

void guardarTarjVirtual(struct TARJETA_VIRTUAL *aTarjVirtual)
{
	int fd;
	char filename[32];

	memset(filename, 0x00, sizeof(filename));
	strcpy(filename, KTarjVirtFile);

	fd = fOpenMW(filename);

	if (fd < 0)
	{
		DispLineMW("T. Virtual File ERROR!", MW_MAX_LINE, MW_CLRDISP|MW_REVERSE|MW_CLREOL|MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		os_beep_close();
		return;
	}
	//printf("\fFlag3 = %d", gAppDat.enableCardExito);APM_WaitKey(3000, 0);
	WriteSFile(fd, (DWORD) &gTarjVirtualOffset, aTarjVirtual, sizeof(struct TARJETA_VIRTUAL));
	fCommitAllMW();
	memcpy(&gTarjVirtual, aTarjVirtual, sizeof(struct TARJETA_VIRTUAL));
	fCloseMW(fd);
}

//*****************************************************************************
//  Function        : GetAppdata
//  Description     : Leer archivo Appdata
//  Input           : aIdx;         // Table Index N/A Cero
//                    aDat;         // pointer data buffer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetAppdata(struct APPDATA *aAppdata) //MFBC/13/11/12
{
	int fd;
	char filename[32];

	CheckPointerAddr(&aAppdata);
	strcpy(filename, KAppDatFile);
	fd = fOpenMW(filename);
	if (fd < 0)
	{
		DispLineMW("APPDATA Open ERROR!", MW_MAX_LINE, MW_CLRDISP|MW_REVERSE|MW_CLREOL|MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		os_beep_close();
		//FreeMW(conf_comer);
		return FALSE;
	}
	if (!ReadSFile(fd, (DWORD) &gAppdataOffset, aAppdata, sizeof(struct APPDATA)))
	{
		fCloseMW(fd);
		//	FreeMW(conf_comer);
		return FALSE;
	}

	memcpy(&gAppDat.commKey[0], &aAppdata->commKey[0], 16);
	memcpy(&gAppDat.auto_auth_code[0], &aAppdata->auto_auth_code[0], 3);
	memcpy(&gAppDat.IpHost[0], &aAppdata->IpHost[0], 5);
	memcpy(&gAppDat.NII[0], &aAppdata->NII[0], 2);
	memcpy(&gAppDat.TermNo[0], &aAppdata->TermNo[0], 10);
	memcpy(&gAppDat.TelPrincipal[0], &aAppdata->TelPrincipal[0], 6);
	memcpy(&gAppDat.TelSecundario[0], &aAppdata->TelSecundario[0], 6);
	gAppDat.reversal_count = aAppdata->reversal_count;
	gAppDat.porcPropina = aAppdata->porcPropina;
	gAppDat.ConexionDIAL = aAppdata->ConexionDIAL;
	gAppDat.ConexionLAN = aAppdata->ConexionLAN;
	gAppDat.ConexionGPRS = aAppdata->ConexionGPRS;
	gAppDat.PortHost = aAppdata->PortHost;
	gAppDat.estadoInit = aAppdata->estadoInit;
	gAppDat.loadKeyPin = aAppdata->loadKeyPin;
	gAppDat.loadKeyCom = aAppdata->loadKeyCom;

	fCloseMW(fd);

	return TRUE;
}

void SaveFileCajas(BYTE offset)
{
	int fd;
	char filename[32];
	struct CAJAS stCajas;

	memset(&stCajas, 0x00, sizeof(struct CAJAS));
	memcpy(&stCajas, &gCajas, sizeof(struct CAJAS));

	strcpy(filename, KCajasFile);
	fd = fOpenMW(filename);

	if (fd < 0)
	{
		DispLineMW("CAJAS File ERROR!", MW_MAX_LINE, MW_CLRDISP|MW_REVERSE|MW_CLREOL|MW_SPFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		os_beep_close();
		return;
	}
	WriteSFile2(fd, (DWORD) &gCajasOffset[offset], &stCajas, sizeof(struct CAJAS));
	fCloseMW(fd);
}



void readFileCajas(void)
{
	int fd;
	char filename[32];
	struct CAJAS stCajas[2];
	//BYTE i;

	memset(&stCajas, 0x00, sizeof(stCajas));
	strcpy(filename, KCajasFile);

	fd = fOpenMW(filename);
	fReadMW(fd, &stCajas, sizeof(stCajas));
	memcpy(&gCajas2, &stCajas, sizeof(stCajas));
	fCloseMW(fd);
}
