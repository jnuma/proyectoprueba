//-----------------------------------------------------------------------------
//  File          : settle.c
//  Module        :
//  Description   : Include settlement releated routines.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  10 May  2006  Lewis       Initial Version for new CONTROL
//-----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "util.h"
#include "sysutil.h"
#include "message.h"
#include "constant.h"
#include "corevar.h"
#include "chkoptn.h"
#include "input.h"
#include "print.h"
#include "tranutil.h"
#include "hostmsg.h"
#include "record.h"
#include "reversal.h"
#include "offline.h"
#include "emvtrans.h"
#include "settle.h"
#include "files.h"
#include "menu.h"
#include "rs232.h"
#include "cajas.h"

int MenuCierre(void);
BOOLEAN batchCajas(void);

static const struct MENU_ITEM KMenuCierre[] =
{
		{ 1, "MANUAL" },
		{ 2, "INTEGRADO" },
		{ -1, NULL }, };

static const struct MENU_DAT KCierre =
{ "CIERRE", KMenuCierre, };


//*****************************************************************************
//  Function        : FormSettleSlip
//  Description     : Pack the settlement slip.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
#if (NO_USED)
static void FormSettleSlip(void)
{
	static const BYTE KBusinessDate[] =
	{ "BUSINESS DATE - " };
	static const BYTE KSoc[] =
	{ "****** SUMMARY OF CHARGE ******" };
	static const BYTE KCloseBatch[] =
	{ "**** CLOSE BATCH CONFIRMED ****" };
	static const BYTE KNII[] =
	{ "NII " };
	BYTE tmp[31];
	struct DATETIME dtg;

	if (gGDS->s_bs_date.b_mm == 0 && gGDS->s_bs_date.b_dd == 0)
	{
		ReadRTC(&dtg);
		gGDS->s_bs_date.b_mm = dtg.b_month;
		gGDS->s_bs_date.b_dd = dtg.b_day;
	}

	MsgBufSetup();
	PackLogo();
	pack_lf();
	PackRcptHeader();
	pack_lf();
	pack_space(2);
	pack_mem("\x1BW1SETTLEMENT\x1BW0", 16);
	pack_lfs(2);

	pack_mem(STIS_ACQ_TBL(0).sb_name, 10);
	pack_space(MAX_CHAR_LINE_NORMAL - 18);
	pack_mem((BYTE *) KNII, 4);
	split_data(STIS_ACQ_TBL(0).sb_nii, 2);
	pack_lf();

	pack_mem((BYTE *) KBusinessDate, 16);
	pack_mem((BYTE *) GetMonthText((BYTE) bcd2bin(gGDS->s_bs_date.b_mm)), 3);
	pack_byte(' ');
	split_data(&gGDS->s_bs_date.b_dd, 1);
	pack_lf();

	ReadRTC(&dtg);
	ConvDateTime(tmp, &dtg, 1);
	pack_mem(tmp, 18);
	pack_lfs(2);

	pack_mem((BYTE *) KSoc, 31);
	pack_lf();
	pack_mem((BYTE *) KCloseBatch, 31);
	pack_lf();
	pack_mem(&RSP_DATA.text[1], 20);
	pack_lf();
	pack_mem(&RSP_DATA.text[21], 20);
	pack_lfs(2);
	PackTotals(&TERM_TOT);
	PackFF();
	PackIssuerSum(0);
	pack_lf();
	PackFF();

	PackMsgBufLen();
}
#endif

//*****************************************************************************
//  Function        : UploadTransOK
//  Description     : Upload transaction to host.
//  Input           : N/A
//  Return          : TRUE;  // upload success.
//                    FALSE;
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
static BOOLEAN UploadTransOK(void)
{
	DWORD i = 0;
	int rec_cnt;

	rec_cnt = APM_GetRecCount(); //Jorge Numa 14-07-13


	while (TRUE)
	{
		if (!APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF)))
			break;
		i++;

		if (RECORD_BUF.w_host_idx == INPUT.w_host_idx)
		{
			/*printf("\nDEBUG 1");
			 APM_WaitKey( 300, 0 );*/
			/*
			 if (!ValidRecordCRC((WORD)i)) {         // check record's crc
			 printf( "\nDEBUG 2" );
			 APM_WaitKey( 300, 0 );
			 SetRspCode('D'*256+'E');
			 PackRspText();
			 return(FALSE);
			 }
			 */
			ByteCopy((BYTE *) &TX_DATA, (BYTE *) &RECORD_BUF,
					(BYTE *) TX_DATA.sb_pin - (BYTE *) &TX_DATA);
			memcpy(TX_DATA.sb_org_trace_no, TX_DATA.sb_trace_no, 3);

			// Jorge Numa 21/10/2013 - Se comentan estas líneas porque en la versión de verifone 10_32 está así
			//            if (RECORD_BUF.b_trans_status & VOIDED)
			//				TX_DATA.dd_amount = 0;

			//	memcpy(&gOrg_rec, &RECORD_BUF, sizeof(RECORD_BUF)); //MFBC/02/04/13
			TX_DATA.b_org_trans = RECORD_BUF.b_trans;
			PackProcCode(TX_DATA.b_org_trans, RECORD_BUF.b_acc_ind);

			if (i == rec_cnt) {
				TX_DATA.sb_proc_code[2] = 0x00;
			}
			else{
				TX_DATA.sb_proc_code[2] = 0x01; /* more message to come */
			}
			TX_DATA.b_trans = TRANS_UPLOAD;
			IncTraceNo();
			memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3); // JANM 30-1-2013
			PackHostMsg();
			UpdateHostStatus(SETTLE_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();
				//SaveTraceNo();
				if (RSP_DATA.b_response == TRANS_ACP)
					UpdateHostStatus(NO_PENDING);
			}
			if ((RSP_DATA.b_response == TRANS_ACP) || (RSP_DATA.b_response
					== MORE_RSP))
				Short1Beep();
			else
				return FALSE;
		}
	}
	return TRUE;
}
//*****************************************************************************
//  Function        : ValidDate
//  Description     : Validate the date value.
//  Input           : aDate;    // pointer to date_t buffer.
//  Return          : TRUE;     // valid input.
//                    FALSE;
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
/*
 static BOOLEAN ValidDate(struct BS_DATE *aDate)
 {
 static const BYTE days_limit [13] = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
 DWORD var_x;

 var_x = (BYTE)bcd2bin(aDate->b_mm);
 if ((var_x == 0) || (var_x > 12))
 return FALSE;
 if (bcd2bin(aDate->b_dd) > days_limit[var_x])
 return FALSE;
 return TRUE;
 }
 */
//*****************************************************************************
//  Function        : GetBusinessDate
//  Description     : Prompt user to enter bussiness date.
//  Input           : N/A
//  Return          : TRUE;  // valid INPUT.
//                    FALSE; // user cancel
//  Note            : result store in globals gBusDate
//  Globals         : gBusDate;
//*****************************************************************************
/*
 static BOOLEAN GetBusinessDate(void)
 {
 static const BYTE Kddmm[]            = {"DDMM"};
 struct DATETIME dtg;
 BYTE kbdbuf[31];

 while (1) {
 kbdbuf[0] = 0;
 DispHeader(NULL);
 Disp2x16Msg(GetConstMsg(EDC_SE_BUS_DATE), MW_LINE3, MW_BIGFONT);
 if (DDMMBusinessDate())
 DispLineMW(Kddmm, MW_LINE5+12, MW_BIGFONT);
 if (!APM_GetKbd(NUMERIC_INPUT+NULL_ENB+ECHO+MW_LINE7+MW_BIGFONT+RIGHT_JUST, IMIN(4)+IMAX(4), kbdbuf))
 return FALSE;

 if (kbdbuf[0] == 0) {
 ReadRTC(&dtg);
 gGDS->s_bs_date.b_mm = dtg.b_month;
 gGDS->s_bs_date.b_dd = dtg.b_day;
 } else if (DDMMBusinessDate()) {
 compress(&kbdbuf[1],&kbdbuf[1], 2);
 gGDS->s_bs_date.b_dd = kbdbuf[1];
 gGDS->s_bs_date.b_mm = kbdbuf[2];
 } else
 CompressInputData(&gGDS->s_bs_date, kbdbuf, 2);

 if (ValidDate(&gGDS->s_bs_date)) {
 return TRUE;
 }
 LongBeep();
 }
 }
 */
//*****************************************************************************
//  Function        : GetSettleAmount
//  Description     : Prompt user to enter settlement amount.
//  Input           : N/A
//  Return          : TRUE;  // valid INPUT.
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals         : INPUT.dd_amount;
//*****************************************************************************
//static BOOLEAN GetSettleAmount(void)
//{
//	static const DWORD KDecimalPos[4] =
//	{ DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3 };
//	BYTE kbdbuf[31];
//
//	if (!APM_GetKbd(AMOUNT_INPUT + NULL_ENB + MW_LINE7
//			+ KDecimalPos[STIS_TERM_CFG.b_decimal_pos], IMIN(1) + IMAX(
//					STIS_TERM_CFG.b_settle_amount_len), kbdbuf))
//		return FALSE;
//	if (STIS_TERM_CFG.b_decimal_pos < 2)
//	{ // 8583 expect 2 decimal pos.
//		memset(&kbdbuf[kbdbuf[0] + 1], '0', 2);
//		kbdbuf[0] += (2 - STIS_TERM_CFG.b_decimal_pos);
//	}
//	INPUT.dd_amount = decbin8b(&kbdbuf[1], kbdbuf[0]);
//	return TRUE;
//}
//*****************************************************************************
//  Function        : PackBatchText
//  Description     : Pack settlement batch # in rspdata.text.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals         : rsp_data;
//*****************************************************************************
static void PackBatchText(void)
{
	RSP_DATA.text[0] = 40;
	memset(&RSP_DATA.text[1 + 5], ' ', 15);
	memcpy(&RSP_DATA.text[1], "BATCH", 5);
	split(&RSP_DATA.text[7], STIS_ACQ_TBL(0).sb_curr_batch_no, 3);
}
//*****************************************************************************
//  Function        : PrintSettle
//  Description     : Print Settlement slip.
//  Input           : aWorldHost
//  Return          : N/A
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
#if (NO_USED)
static void CloseBatch(void)
{
	memcpy(P_DATA.sb_roc_no, TX_DATA.sb_roc_no, 3);
	FormSettleSlip(); /* print it */
	memcpy(STIS_ACQ_TBL(0).sb_curr_batch_no, STIS_ACQ_TBL(0).sb_next_batch_no,
			3);
	bcdinc(STIS_ACQ_TBL(0).sb_next_batch_no, 3);
	APM_SetBatchNo(INPUT.w_host_idx, STIS_ACQ_TBL(0).sb_curr_batch_no);
	APM_BatchClear(INPUT.w_host_idx);
	fCommitAllMW();
}
#endif

void CloseBatchVisa(void)
{
	memcpy(P_DATA.sb_roc_no, TX_DATA.sb_roc_no, 3);
	//FormSettleSlip(); /* print it */
	memcpy(gTablaCuatro.b_num_lote, gTablaCuatro.b_prox_lote, 3);
	bcdinc(gTablaCuatro.b_prox_lote, 3);
	APM_SetBatchNo(INPUT.w_host_idx, gTablaCuatro.b_num_lote);
	APM_BatchClear(INPUT.w_host_idx);
	fCommitAllMW();
}

//*****************************************************************************
//  Function        : AllTotalZero
//  Description     : Return TRUE when all totals are zero.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BOOLEAN AllTotalZero(void)
{
	return ((TERM_TOT.s_sale_tot.dd_amount == 0)
			&& (TERM_TOT.s_refund_tot.dd_amount == 0)
			&& (TERM_TOT.s_void_sale.dd_amount == 0)
			&& (TERM_TOT.s_void_refund.dd_amount == 0)
			&& (TERM_TOT.s_puntos_tot.dd_amount == 0)
			&& (TERM_TOT.s_bill_tot.dd_amount == 0));
}
//*****************************************************************************
//  Function        : ComfirmSetlTot
//  Description     : Confirm/Enter Settlement total & Bussiness Date.
//  Input           : N/A
//  Return          : TRUE/FALSE;       // TRUE => confirmed.
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
BOOLEAN ConfirmSetlTot(BOOLEAN Autocierre) //kt-290413
{
	int count_Trans = 0, i = 0;
	BYTE auxCant[18];
	int cierreIntegrado = -1;
	//BYTE kbdbuf[9];
	//BYTE PassAdmin[4];
	BYTE flagSett[16]; // Jorge Numa

	memset(flagSett, 0, sizeof(flagSett)); // Jorge Numa

	flagSett[1] = 0x31; // Jorge Numa
	flagSett[1] -= '0'; // Jorge Numa


	//APM_GetAcqTbl(INPUT.w_host_idx, &STIS_ACQ_TBL(0));
	INPUT.w_host_idx = 0;

	INPUT.b_trans = SETTLEMENT;

	CalTotals(FALSE);
	if (AllTotalZero())
	{
		DispLineMW(GetConstMsg(KTransHeader[SETTLEMENT]), MW_LINE1, MW_REVERSE
				| MW_CENTER | MW_BIGFONT | MW_CLRDISP); //kt-190413
		DispErrorMsg(GetConstMsg(EDC_SE_NO_BATCH));
		RSP_DATA.w_rspcode = 'N' * 256 + 'T'; // For ECR response
		return FALSE;
	}


	DispHeaderTrans();

	if(Autocierre == FALSE)
	{
		if(TransPassAdmin(MW_LINE2) == FALSE)
			return FALSE;
	}

	if( Cajas() ){
		cierreIntegrado = MenuCierre();

		if(cierreIntegrado == TRUE){
			if( batchCajas() == FALSE)
				return FALSE;
			else{
				ReportSelttCajas();
			}
		}
		else if(cierreIntegrado == -1)
			return FALSE;
	}


	count_Trans = APM_GetRecCount();

	for(i = 0; i< count_Trans; i ++)
	{
		APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF));
		break;
	}

	if (i == count_Trans)
	{
		PackReportComercio(FALSE, FALSE); //kt-110413
		MsgBufSetup();
		PackMsgBufLen();
		CloseBatchVisa(); // 19-09-12 Jorge Numa ++
		os_config_write(K_CF_Settled, &flagSett[1]); // Jorge Numa
		os_config_update(); // Jorge Numa
		fCommitAllMW();
		RefreshDispAfter(0);
		return FALSE;
	}


	// No Confirmation for ECR Settlement
	if (gGDS->i_ecr_len > 0)
		return TRUE;


	if(Autocierre == FALSE)
	{
		memset(auxCant, 0x00, sizeof(auxCant));
		sprintf(auxCant, "%03d", TERM_TOT.s_sale_tot.w_count + TERM_TOT.s_bill_tot.w_count);

		DispLineMW(GetConstMsg(EDC_SE_SALE_TOTAL), MW_LINE1, MW_CLRDISP
				| MW_BIGFONT | MW_REVERSE); //kt-301012
		TextColor(auxCant, MW_LINE3, COLOR_BLACK, MW_BIGFONT, 0);

		DispAmnt(TERM_TOT.s_sale_tot.dd_amount
				+ TERM_TOT.s_bill_tot.dd_amount // se quito la sumatoria de propina eso ya viene sumado en el monto total
				, MW_LINE3, MW_BIGFONT); // 18-09-12 Jorge Numa ++

		if (PromptYesNo() != 2)
			return FALSE;

		DispLineMW(GetConstMsg(EDC_SE_REFUND_TOTAL), MW_LINE1, MW_CLRDISP
				| MW_BIGFONT | MW_REVERSE); //kt-301012

		memset(auxCant, 0x00, sizeof(auxCant));
		sprintf(auxCant, "%03d", TERM_TOT.s_void_sale.w_count + TERM_TOT.s_bill_void_tot.w_count);
		DispAmnt(TERM_TOT.s_void_sale.dd_amount + TERM_TOT.s_bill_void_tot.dd_amount, MW_LINE3, MW_BIGFONT); //LFGD 03/13/2013
		TextColor(auxCant, MW_LINE3, COLOR_BLACK, MW_BIGFONT, 0);

		if (PromptYesNo() != 2)
			return FALSE;
	}
	/*
	 if (!GetBusinessDate())
	 return FALSE;
	 */

	return TRUE;
}
//*****************************************************************************
//  Function        : SettleTrans
//  Description     : Do the settlement transaction.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************

#if (NO_USED)
BOOLEAN SettleTrans(void)
{
	DWORD upload_count;
	BOOLEAN connected, settle_all, settle_done = FALSE;
	WORD max_acq = APM_GetAcqCount();

	settle_all = (INPUT.w_host_idx == (WORD) -2);

	if (settle_all)
		INPUT.w_host_idx = 0;
	else
	{
		if (!ConfirmSetlTot())
			return FALSE;
	}

	//ProtectPutL(&gPRAM->reprint_buf.len, 0);
	for (; INPUT.w_host_idx < max_acq; INPUT.w_host_idx++) // Original

	{
		if (!APM_GetAcqTbl(INPUT.w_host_idx, &STIS_ACQ_TBL(0)))
			continue;
		if (STIS_ACQ_TBL(0).b_status == NOT_LOADED)
			continue;
		if (!CorrectHost(GetHostType(0)))
			continue;

		INPUT.b_trans = SETTLEMENT;
		if (settle_all)
		{
			DispHeader(STIS_ACQ_TBL(0).sb_name); // Original
			Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
			CalTotals(FALSE);
			if (AllTotalZero())
			{
				Disp2x16Msg(GetConstMsg(EDC_SE_NO_BATCH), MW_LINE5, MW_BIGFONT | MW_CLRDISP); //kt-190413
				Delay1Sec(3, 1);
				continue;
			}
		}
		else
			DispHeader(NULL);

		upload_count = 0;
		PackCommTest(INPUT.w_host_idx, TRUE);

		settle_done = FALSE;
		connected = FALSE;
		ClearResponse();
		if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
		{
			if (ReversalOK())
				if (OfflineSent(TRUE))
					connected = TRUE;
		}
		while (connected)
		{
			TX_DATA.b_trans = SETTLEMENT;
			if (upload_count == 0)
				PackProcCode(SETTLEMENT, 0x00);
			else
				memcpy(TX_DATA.sb_proc_code, KSetlPCode2, 3);

			memcpy(TX_DATA.sb_roc_no, INPUT.sb_roc_no, 3);
			IncTraceNo();
			MoveInput2Tx();
			PackHostMsg();
			UpdateHostStatus(SETTLE_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					!= COMM_OK)
				break;
			RSP_DATA.b_response = CheckHostRsp();
			//SaveTraceNo();
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				UpdateHostStatus(NO_PENDING);
				if (RSP_DATA.text[0] == 0)
				{
					PackBatchText();
					memcpy(&RSP_DATA.text[21], "CLOSED          ", 16);
				}
				CloseBatch();
			}
			else if (RSP_DATA.b_response == TRANS_REJ)
			{
				if (RSP_DATA.w_rspcode == '9' * 256 + '5')
				{
					upload_count += 1;
					if (upload_count == 4)
					{
						UpdateHostStatus(NO_PENDING);
						RSP_DATA.w_rspcode = '7' * 256 + '7';
					}
					else
					{
						Short1Beep();
						if (UploadTransOK())
							continue;
					}
				}
				else if (RSP_DATA.w_rspcode == '7' * 256 + '7')
				{
					UpdateHostStatus(NO_PENDING);
					if (RSP_DATA.text[0] == 0)
					{
						PackRspText();
						PackBatchText();
					}
					CloseBatch();
				}
			}
			break;
		} // End While

		TransEnd(FALSE);

		if (!settle_all)
		{
			RefreshDispAfter((RSP_DATA.b_response != TRANS_ACP) ? 90 : 30);
			settle_done = TRUE;
			break;
		}
		else
			Delay1Sec(3, 0);
	}

	return (settle_done == TRUE);
}
#endif

//*****************************************************************************
//  Function        : SettleTrans
//  Description     : Do the settlement transaction.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals         : N/A
//*****************************************************************************
BOOLEAN SettleTransVisa(BOOLEAN Autocierre)
{
	DWORD upload_count;
	WORD rec_cnt = 0; //kt-080413
	g_Puntos_Cierre = FALSE;
	//	BYTE kbdbuf[9];//kt-290413
	//	BYTE PassAdmin[4];
	BOOLEAN connected, settle_all, settle_done = FALSE;
	//WORD max_acq = APM_GetAcqCount();
	struct TABLA_CERO tabla0; // 18-09-12 Jorge Numa ++
	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO)); // 18-09-12 Jorge Numa ++
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12)); // kt-291112	

	WORD max_acq = GetTabla4Count(); // 18-09-12 Jorge Numa ++
	WORD max_issuer = GetTabla3Count(); //kt-080413

	INPUT.b_trans = SETTLEMENT;

	//	DispHeaderTrans();
	//
	//	if(TransPassAdmin(MW_LINE2) == FALSE)
	//		return FALSE;


	//int idTbl4_tmp = 0; // Cuidado con esta variable!!!
	BYTE flagSett[16]; // Jorge Numa

	memset(flagSett, 0, sizeof(flagSett)); // Jorge Numa

	flagSett[1] = 0x31; // Jorge Numa
	flagSett[1] -= '0'; // Jorge Numa

	GetTablaCero(&tabla0); // 18-09-12 Jorge Numa ++

	settle_all = (INPUT.w_host_idx == (WORD) - 2);

	if (settle_all)
		INPUT.w_host_idx = 0;
	else
	{
		if (!ConfirmSetlTot(Autocierre))
		{
			FreeMW(p_tabla4); //kt-291112
			FreeMW(p_tabla12); //kt-291112
			return FALSE;
		}
	}

	gIdTabla4 = OpenFile(KTabla4File); // 18-09-12 Jorge Numa ++
	gIdTabla12 = OpenFile(KTabla12File); //kt-291112

	//idTbl4_tmp = gIdTabla4;

	GetTablaDoce(INPUT.w_host_idx, (struct TABLA_12*) p_tabla12); //kt-291112

	memset(INPUT.sb_comercio_v, 0x20, sizeof(INPUT.sb_comercio_v));
	memset(INPUT.sb_nom_comer_v, 0x20, sizeof(INPUT.sb_nom_comer_v));
	memset(INPUT.sb_terminal_id, 0x20, sizeof(INPUT.sb_terminal_id));
	//ProtectPutL(&gPRAM->reprint_buf.len, 0);
	for (; INPUT.w_host_idx < max_acq; INPUT.w_host_idx++)
	{
		//if (!APM_GetAcqTbl(INPUT.w_host_idx, &STIS_ACQ_TBL(0)))
		if (!GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4))
			continue;

		memcpy(INPUT.sb_comercio_v, gTablaDoce.b_establ_virtual,9);
		memcpy(INPUT.sb_nom_comer_v, gTablaDoce.b_nom_establ, 15);
		memcpy(INPUT.sb_term_comer_v, gTablaDoce.b_term_virtual, 8);
		//		memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-101212
		memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8); //kt-101212


		INPUT.b_trans = SETTLEMENT;
		if (settle_all)
		{
			//DispHeader(STIS_ACQ_TBL(0).sb_name);    // Original
			DispHeader(((struct TABLA_CUATRO*) p_tabla4)->b_nom_adquirente); // 18-09-12 Jorge Numa ++
			Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
			CalTotals(FALSE);
			if (AllTotalZero())
			{
				DispClrBelowMW(MW_LINE2);//MFBC/15/04/13
				Disp2x16Msg(GetConstMsg(EDC_SE_NO_BATCH), MW_LINE5, MW_BIGFONT
						| MW_CLRDISP); //kt-190413
				Delay1Sec(3, 1);
				continue;
			}
		}
		else
			DispHeaderTrans();

		upload_count = 0;
		//PackComm(INPUT.w_host_idx, TRUE);         // Original

		PackCommTest(INPUT.w_host_idx, false); // 18-09-12 Jorge Numa ++

		settle_done = FALSE;
		connected = FALSE;
		ClearResponse();
		CloseFile(gIdTabla4);
		FreeMW(p_tabla4); // 18-09-12 Jorge Numa ++

		if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
		{
			if (ReversalOK())
				//if (OfflineSent(TRUE))
				connected = TRUE;
		}
		while (connected)
		{
			TX_DATA.b_trans = SETTLEMENT;
			if (upload_count == 0)
				PackProcCode(SETTLEMENT, 0x00);
			else
				memcpy(TX_DATA.sb_proc_code, KSetlPCode2, 3);
			memcpy(TX_DATA.sb_roc_no, INPUT.sb_roc_no, 3);

			IncTraceNo();
			MoveInput2Tx();
			PackHostMsg();
			UpdateHostStatus(SETTLE_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					!= COMM_OK)
				break;

			RSP_DATA.b_response = CheckHostRsp();

			//SaveTraceNo();
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				UpdateHostStatus(NO_PENDING);

				if (RSP_DATA.text[0] == 0)
				{
					//PackBatchText();
					//memcpy(&RSP_DATA.text[21], "CERRADO         ", 16);
					AcceptBeep();
					RSP_DATA.text[0] = 18;
					memcpy(&RSP_DATA.text[1], "  CIERRE EXITOSO  ", 18); //MFBC/20/05/13
					DispRspText2(FALSE);
					APM_WaitKey(100, 0);

					// Se incrementa el Lote
					p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
					gIdTabla4 = OpenFile(KTabla4File);
					bcdinc(gTablaCuatro.b_num_lote, 3);
					memcpy((struct TABLA_CUATRO*) p_tabla4, &gTablaCuatro, sizeof(struct TABLA_CUATRO));
					UpdTablaCuatro(gIdTabla4, INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);

					FreeMW(p_tabla4);
					CloseFile(gIdTabla4);

				}

				if (((struct TABLA_12*) p_tabla12)->b_id <= 0)
				{
					gIdTabla3 = OpenFile(KTabla3File);
					rec_cnt = APM_GetRecCount();
					PackVoucherSettleTotal(rec_cnt, max_issuer, TRUE, FALSE); //kt-110413
					CloseFile(gIdTabla3);
				}
				else
				{
					PackCierreMulticomercio(); //kt-180413
				}


				CloseBatchVisa(); // 19-09-12 Jorge Numa ++

				os_config_write(K_CF_Settled, &flagSett[1]); // Jorge Numa
				os_config_update(); // Jorge Numa
			}
			else if (RSP_DATA.b_response == TRANS_REJ)
			{

				if (RSP_DATA.w_rspcode == '9' * 256 + '5')
				{
					upload_count += 1;
					if (upload_count == 4)
					{
						UpdateHostStatus(NO_PENDING);
						RSP_DATA.w_rspcode = '7' * 256 + '7';
					}
					else
					{
						Short1Beep();
						if (UploadTransOK())
							continue;
					}
				}
				else if (RSP_DATA.w_rspcode == '7' * 256 + '7')
				{
					UpdateHostStatus(NO_PENDING);
					if (RSP_DATA.text[0] == 0)
					{
						PackRspText();
						PackBatchText();
					}
					CloseBatchVisa(); // 19-09-12 Jorge Numa ++
				}
			}
			break;
		} // End While

		TransEnd(FALSE);

		if (!settle_all)
		{
			RefreshDispAfter((RSP_DATA.b_response != TRANS_ACP) ? 90 : 30);
			settle_done = TRUE;
			break;
		}
		else
			Delay1Sec(3, 0);
	}

	FreeMW(p_tabla12); //kt-291112

	//DispCtrlMW(K_SelSpFont);

	//gIdTabla4 = CloseFile(idTbl4_tmp);
	gIdTabla12 = CloseFile(gIdTabla12);

	ResetTerm();

	return (settle_done == TRUE);
}




/***************************************************************
 * Funcion MenuCierre:Permite seleccionar si el cierre se hace integrado a cajas o no
 * return: TRUE-> Cierre con integracion a cajas; FALSE: Cierre Normal
 ***************************************************************/
int MenuCierre(void)
{
	while (1)
	{
		int select = 0;

		select = MenuSelectVisa(&KCierre, select);

		if (select == -1)
			return -1;

		switch (select)
		{
		case 1:	//Manual
			return FALSE;

		case 2://Integrado a cajas
			return TRUE;

		default:
			break;

		}
	}
}


BOOLEAN batchCajas(void){
	int count_Trans;
	int i;
	struct DATETIME dtg;
	BYTE *p_tabla3;
	BYTE bufferAux[30];
	BYTE panAscii[20 + 1];
	DWORD ret;
	BYTE retry = 0;
	BYTE linea[20];
	BYTE NACK[2] = {0x15, 0x00};
	BYTE ACK[2] = {0x06, 0x00};
	//BYTE emisor[20];

	p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));
	memset(bufferAux, 0x00, sizeof(bufferAux));
	memset(panAscii, 0x00, sizeof(panAscii));
	//memset(emisor, 0x00, sizeof(emisor));

	while(TRUE){
		gCierreIntegrado = TRUE;
		if( serialReciveCajas(3000) <= 0 ){
			gCierreIntegrado = FALSE;
			FreeMW(p_tabla3);
			return FALSE;
		}
		gCierreIntegrado = FALSE;
		if( !unpackCierreCajas() ){
			serialSend2(NACK, 1);
			TextColor("Desea", MW_LINE4, COLOR_BLACK, MW_CLRDISP | MW_CENTER| MW_SMFONT | MW_REVERSE, 0);
			TextColor("enviar denuevo ?", MW_LINE5, COLOR_BLACK, MW_CENTER| MW_SMFONT | MW_REVERSE, 0);
			displaySI_NO();
			if( WaitKey(3000, 0) == MWKEY_ENTER )
				continue;
			else{
				FreeMW(p_tabla3);
				return FALSE;
			}
		}
		else{
			//memset(ACK, 0x00, sizeof(ACK));
			//ACK[0] = 0x06;
			serialSend2(ACK, 1);
			break;
		}
	}
	//printf("\fProceso Correcto!!!");APM_WaitKey(3000, 0);
	gIdTabla3 = OpenFile(KTabla3File);
	count_Trans = APM_GetRecCount();
	for(i = 0; i< count_Trans; i ++)
	{
		if(retry >= 3){
			FreeMW(p_tabla3);
			CloseFile(gIdTabla3);
			return FALSE;
		}

		APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF));
		memset(&gCajas, 0x00, sizeof(struct CAJAS));

		memcpy(gCajas.authCode_1, RECORD_BUF.sb_auth_code, 6);
		sprintf(gCajas.totalCompra_40, "%012d", (RECORD_BUF.dd_amount/100));
		sprintf(gCajas.valorIva_41, "%012d", (RECORD_BUF.dd_iva/100));
		sprintf(gCajas.valorBaseDev_80, "%012d", (RECORD_BUF.dd_base/100));
		split(gCajas.numeroRecibo_43, RECORD_BUF.sb_roc_no, 3);
		memcpy(gCajas.RRN_44, &RECORD_BUF.sb_rrn[6], 6);
		memcpy(gCajas.terminalID_45, RECORD_BUF.sb_terminal_id, 8);
		//armo el campo 46 de cajas
		ReadRTC(&dtg);
		gCajas.dateTrans_46[0] = ((dtg.b_year & 0xF0) >> 4) + 0x30;
		gCajas.dateTrans_46[1] = (dtg.b_year & 0x0F) + 0x30;
		split(&gCajas.dateTrans_46[2], &RECORD_BUF.s_dtg.b_month, 2);
		/////
		split(gCajas.timeTrans_47, &RECORD_BUF.s_dtg.b_hour, 2);
		gCajas.respCode_48[0] = RECORD_BUF.w_rspcode>>8;
		gCajas.respCode_48[1] = RECORD_BUF.w_rspcode&0xFF;
		if( memcmp(gCajas.respCode_48, "AP", 2) == 0 || memcmp(gCajas.respCode_48, "TA", 2) == 0 )
			memcpy(gCajas.respCode_48, "00", 2);
		//armo el campo 49 de cajas (franquisia)
		GetTablaTres(RECORD_BUF.w_issuer_idx, (struct TABLA_TRES*) p_tabla3 );
		memset(gCajas.franquisia_49, 0x20, 10);
		memcpy(gCajas.franquisia_49, gTablaTres.b_nom_emisor, 10);
		//printf("\f<%s>", gTablaTres.b_nom_emisor);APM_WaitKey(3000, 0);
		////////
		switch (RECORD_BUF.b_tipo_cuenta)
		{
		case 0x10:
			memcpy(gCajas.typeAcount_50, "AH", 2);
			break;
		case 0x20:
			memcpy(gCajas.typeAcount_50, "CO", 2);
			break;
		case 0x30:
			memcpy(gCajas.typeAcount_50, "CR", 2);
			break;
		case 0x40:
			memcpy(gCajas.typeAcount_50, "CR", 2);
			break;
		}
		memcpy(gCajas.numCuotas_51, RECORD_BUF.sb_cuota, 2);
		memcpy(gCajas.ult4DigTarjeta_54, RECORD_BUF.sb_cuota, 2);
		//armo el campo 54 de cajas (ultimos 4 de la tarjeta)
		split(bufferAux, RECORD_BUF.sb_pan, 10);
		ret = (BYTE) fndb(bufferAux, 0x46, 20);
		memcpy(panAscii, bufferAux, ret);
		ret = strlen(panAscii);
		memcpy(gCajas.ult4DigTarjeta_54, &panAscii[ret - 4], 4);
		////////////////////
		memcpy(gCajas.cardBIN_75, panAscii, 6);
		split(gCajas.expDateCard_76, RECORD_BUF.sb_exp_date, 2);
		memset(gCajas.codComercio_77, 0x20, 23);
		memcpy(gCajas.codComercio_77, RECORD_BUF.sb_comercio_v, 11);
		memcpy(gCajas.direcEstab_78, gTablaCero.b_dir_ciudad, 23);
		memcpy(gCajas.lable_79, "00", 2);
		memset(gCajas.filler_91, 0x30, 12);
		if( i < (count_Trans-1) ){
			gCajas.filler_91[11] = 0x31;
		}
		packTramaCajas(CIERRE_INTEGRADO, TRUE);
		if( serialSendAndReciveCajas(1000) <= 0){
			FreeMW(p_tabla3);
			CloseFile(gIdTabla3);
			return FALSE;
		}
		if( RX_BUF.sbContent[0] != 0x06 ){
			i--;
			retry++;
			continue;
		}
		memset(linea, 0x00, sizeof(linea));
		sprintf(linea, "%d de %d", i+1, count_Trans);
		TextColor(linea, MW_LINE3, COLOR_VISABLUE, MW_CENTER| MW_SMFONT, 1);
	}
	FreeMW(p_tabla3);
	CloseFile(gIdTabla3);
	return TRUE;
}
