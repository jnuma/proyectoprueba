//-----------------------------------------------------------------------------
//  File          : corevar.h
//  Module        :
//  Description   : Declrartion & Defination for corevar.c
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _COREVAR_H_
#define _COREVAR_H_
#include "emv2dll.h"
#include "ecrdll.h"
#include "apm.h"
#include "hardware.h"
#include "sysutil.h"
#include "emvcl2dll.h"
//-----------------------------------------------------------------------------
//      Defines
//-----------------------------------------------------------------------------
#define CTL_ENABLE	TRUE	// Contactless
#define BIN_ACP  0  //MFBC/04/12/12
#define BIN_FAIL  1
#define BIN_NO_FILE  2
#define NO_USED		0					//compilacion condicional 030513


//DEFINICIONES PARA VOUCHER Y PANTALLA  // MFBC/23/05/13
#define VERCB								"SPUV01_I03"		  //   version para impresion de voucher y pantalla
#define FECHA_COMPILACION		"MAY 29, 2014 "     // 13 caracteres fijos
#define HORA_COMPILACION		"16:00"                   // 5 caracteres fijos
#define AMB_PRU   1  //variar para produccion a cero

// Definicion para envio de informacion en  DCC
//#ifdef FULL_PRODUCCION ||PARCIAL_PRODUCCION
//#define AMB_PRU   0
//#else
//#define AMB_PRU   1
//#endif

////////////////////////

#define HANDLE_HOST_TYPE  0x0B      // Amex Host
#define COMMON_DATA                    \
		BYTE            b_trans;           \
		BYTE            b_trans_status;    \
		BYTE            sb_trace_no[3];    \
		BYTE            b_acc_ind;         \
		BYTE            sb_roc_no[3];      \
		BYTE            sb_pan[10];        \
		BYTE            sb_exp_date[2];    \
		BYTE            b_posCondCode;    \
		DDWORD          dd_amount;         \
		DDWORD          dd_amount_saldo_bono;         \
		DDWORD          dd_amount_dividido;\
		DDWORD          dd_tip_dividido;\
		DDWORD          dd_tip;            \
		DDWORD          dd_org_amount;     \
		DDWORD          dd_IAC;    \
		DDWORD          dd_net_mount;   \
		BYTE            sb_product[4];     \
		BYTE            b_entry_mode;      \
		BYTE            sb_holder_name[26];\
		BYTE            sb_ecr_ref[16];    \
		BYTE            sb_auth_code[6];   \
		WORD            w_rspcode;         \
		BYTE            b_PinVerified;     \
		BYTE            sb_rrn[12];        \
		WORD            w_host_idx;        \
		WORD            w_issuer_idx;      \
		BYTE            b_tipo_cuenta;     \
		DDWORD          dd_base;           \
		DDWORD          dd_iva;            \
		DDWORD          dd_base_tasa_aero; \
		DDWORD          dd_iva_tasa_aero;  \
		DDWORD          dd_IAC_tasa_aero; \
		DDWORD          dd_net_tasa_aero; \
		DDWORD          dd_donacion;       \
		DDWORD          dd_valor_efectivo; \
		DDWORD          dd_tasa_aeropor;   \
		DDWORD          dd_tasa_admin;     \
		DDWORD          dd_Puntos;     \
		DDWORD          dd_Punts_Acum_sale;     \
		DDWORD          dd_Total_puntos;     \
		BYTE            sb_cuota[2];       \
		BYTE            sb_ced_cajero[13]; \
		BYTE            sb_cedula[13];     \
		BYTE            sb_num_ref[16];    \
		WORD            w_card_idx;        \
		WORD            w_num_clientes;    \
		WORD            w_lenField63;    \
		WORD            w_TipoTransAero;    \
		BYTE 			b_CodAerolinea[40];\
		BYTE 			b_CodAgencia[40];  \
		BYTE 			b_EmpDona[15];     \
		BYTE            sb_trace_caja[6];  \
		BYTE            sb_comercio_v[21]; \
		BYTE            sb_nom_comer_v[21];\
		BYTE            sb_term_comer_v[8];\
		BYTE            sb_terminal_id[9]; \
		BYTE            b_trans_void;      \
		BYTE            sb_field63[120];      \
		BYTE 			sb_descrip_bin[16];\
		BYTE 			b_natur_bolsillo[2];  \
		BYTE 			b_costoTrans[6 + 1];  \
		BYTE 			b_terminal;  		\
		BOOLEAN           b_flag_monto; \
		BOOLEAN           b_flag_tasa_adm; \
		BOOLEAN           b_flag_tasa_aero; \
		BOOLEAN           b_transAcep; \
		BOOLEAN           b_print_baseDev; \
		BOOLEAN           b_flag_Consulta_Costo; \
		BOOLEAN           b_TransForCaja;\
		BYTE 					sb_codEmpresa_Recarg[4]; \
		BYTE 					sb_numCelular_Recarg[16]; \
		BYTE 					sb_operador_Recarg [20]; \
		BYTE 					sb_cel_suport_Recarg [12]; \
        BYTE           b_bono_pago; \
        BYTE           sb_aero_trans_code[3]; \
        BYTE  			sb_idCajero_83[12 + 1];\
        BYTE 			sb_numTrans_53[10 + 1];	\
        BYTE			sb_numeroCaja_42[10 + 1];\
        BYTE		   sb_comprobante[15];\
        BYTE           b_tipo_trans_movil[2 + 1];			\
        BYTE           b_marca_Uno_movil;			\
        BYTE           b_marca_tres_movil;			\
        DDWORD      dd_cod_preregistro;         \
        DDWORD      dd_descuento;         \
		struct DATETIME s_dtg_Aero;        \
		struct DATETIME s_dtg_init;        \
		struct DATETIME s_dtg;             \
		struct ICC_DATA s_icc_data;        \
		struct FL63_BILL s_billpayment;	   \

//-----------------------------------------------------------------------------
//      Transaction ENTRY mode
//-----------------------------------------------------------------------------
#define MANUAL            'M'
#define SWIPE             'S'
#define ICC               'I'
#define MANUAL_4DBC       'm'  // manual with 4DBC
#define SWIPE_4DBC        's'  // swipe with 4DBC
#define FALLBACK          'F'  // Swipe during ICC not available
#define FALLBACK_4DBC     'f'  // Swipe during ICC with 4DBC
#define CONTACTLESS   	  'T'  // contactless
#define SIN_TARJETA   	  'N'  // pago sin tarjeta
//-----------------------------------------------------------------------------
//      Common element for struct
//-----------------------------------------------------------------------------
extern BYTE g_roc_no_aero[3]; //MFBC/14/03/13
extern BYTE g_Auth_code_aero[6]; //MFBC/14/03/13
extern BYTE g_rnn_aero[12]; //MFBC/14/03/13
extern BYTE g_aeroTransCode[3];		//MFBC/15/08/13
extern BYTE g_TransCodeAgencia[3];		//MFBC/15/08/13
extern BYTE g_TransCodeAerolinea[3];		//MFBC/15/08/13
extern BOOLEAN init_auto;		//MFBC/05/06/13
extern BOOLEAN logon_auto;		//MFBC/05/06/13
extern DDWORD g_tasaAero;
extern BOOLEAN g_flag_AeroPrint; //MFBC/14/03/13
extern DDWORD g_count_text;
extern DDWORD g_count_ind;
extern BOOLEAN g_pagosHechos;

extern BYTE gPinBlock[8];

struct ICC_DATA
{
	BYTE sb_label[16];
	BYTE b_aid_len;
	BYTE b_tag_95_len; //kt-111012
	BYTE b_tag_9b_len; //kt-111012
	BYTE b_tag_9f26_len; //kt-111012
	BYTE sb_aid[16];
	BYTE sb_tag_95[5];
	BYTE sb_tag_9b[2];
	BYTE sb_tag_9f26[8];
	BYTE sb_tag_9f12[21];
	BYTE b_tag_9f12_len;
	BYTE b_tag_50_len;
	WORD w_misc_len;
//	BYTE sb_misc_content[280];
    BYTE sb_misc_content[512];  // Jorge Numa 09/09/2013
	WORD w_trk1_len; // John - 26-09-12 Jorge Numa ++
	BYTE sb_trk1_content[280]; // John - 26-09-12 Jorge Numa ++
};

//////////////////// Tabla de campo 63 pago de factura////////////////////MB
struct FL63_BILL
{
	BYTE b_Cod_Facturadora[4]; // Empresa Facturadora/ vendedor.
	BYTE b_Num_Factura[48]; // numero de factura.
	BYTE b_Nombre_Facturadora[20]; // Nombre de empresa facturadora.
	BYTE b_IAC_NIT[13]; // Identificador de Empresa Facturadora � IAC � NIT.
	BYTE bCod_EUEF[15]; //  C�d de Establecimiento �nico de Empresa Facturadora.
	BYTE b_Valor_Factura[8]; // valor de factura.
	BYTE b_Vencimiento_Factura[8]; //fecha de vencimiento de la factura.
	BYTE b_Id_PagoFactura; //Indicativo de pago de la factura Y/N
};

struct TXN_RECORD
{ // Transaction record
	COMMON_DATA
	BYTE sb_proc_code[3];
	struct MW_TRK1BUF s_trk1buf;
	struct MW_TRK2BUF s_trk2buf;
	BYTE sb_pin[8];
	WORD w_crc;
};

struct ACQUIRER_REV
{ // Reversal data
	COMMON_DATA
};

struct PRN_DATA
{ // Print data
	COMMON_DATA
	BYTE b_type;
	DDWORD dd_base_amount;
};

struct TOTAL
{ // Total
	WORD w_count;
	DDWORD dd_amount;
};

struct TOTAL_STRUCT
{ // Transaction totals
	DWORD d_adjust_count;
	struct TOTAL s_sale_tot;
	struct TOTAL s_base_tot;
	struct TOTAL s_iva_tot;
	struct TOTAL s_tips_tot;
	struct TOTAL s_donacion_tot;
	struct TOTAL s_void_iva_tot;
	struct TOTAL s_void_tips_tot;
	struct TOTAL s_void_donacion_tot;
	struct TOTAL s_void_base_tot;
	struct TOTAL s_refund_tot;
	struct TOTAL s_puntos_tot;
	struct TOTAL s_void_sale;
	struct TOTAL s_void_refund;
	struct TOTAL s_iac_tot;
	struct TOTAL s_bill_tot;
	struct TOTAL s_bill_void_tot;
	struct TOTAL s_cr_tot;
	struct TOTAL s_tAdm_tot; //LFGD 03/15/2013
	struct TOTAL s_tasa_ad_tot;
	struct TOTAL s_cash_back;
};

struct STIS_DATA
{ // STIS data
	struct TERM_CFG s_term_cfg;
	struct DESC_TBL s_desc_tbl[16]; // !TODO
	struct ACQUIRER_TBL s_acquirer_tbl[2];
	struct ISSUER_TBL s_issuer_tbl[2];
	struct CARD_TBL s_card_tbl[2];
};

struct BS_DATE
{ // Business date
	BYTE b_mm;
	BYTE b_dd;
};

//-----------------------------------------------------------------------------
//      Global Data Structure
//-----------------------------------------------------------------------------
//#define MSG_BUF_LEN        3*1024
//extern WORD gRspCode;

#define MSG_BUF_LEN        16*1024
struct MSG_BUF
{
	DWORD d_len;
	BYTE sb_content[MSG_BUF_LEN];
};

#define MAX_TAG_LIST   150
struct GDS
{
	// Add Global Variables here
	struct STIS_DATA s_stis_data;
	struct TERM_DATA s_term_data;
	//struct SELECTMENU   select_menu[NBR_ACQUIRER+1];
	struct TXN_RECORD s_record_buf;
	struct ACQUIRER_REV s_acq_rev;
	struct PRN_DATA s_p_data;
	struct TOTAL_STRUCT s_term_tot;
	struct MSG_BUF s_MsgBuf;
	struct COMMBUF s_TxBuf;
	struct COMMBUF s_RxMsg;
	BYTE sb_IOBuf[1500];
	sIO_t s_EMVIn;
	sIO_t s_EMVOut;
	CL_IO s_CTLIn;
	CL_IO s_CTLOut;
	TLIST s_TagList[MAX_TAG_LIST];
	BOOLEAN b_disp_chgd;
	struct BS_DATE s_bs_date;
	struct ECRDATA s_ecr_data;
	int i_ecr_len;
};
extern struct GDS *gGDS;

#define STIS_TERM_CFG     gGDS->s_stis_data.s_term_cfg
#define STIS_ACQ_TBL(x)   gGDS->s_stis_data.s_acquirer_tbl[x]
#define STIS_ISS_TBL(x)   gGDS->s_stis_data.s_issuer_tbl[x]
#define STIS_CARD_TBL(x)  gGDS->s_stis_data.s_card_tbl[x]
#define STIS_TERM_DATA    gGDS->s_term_data

#define RECORD_BUF        gGDS->s_record_buf
#define P_DATA            gGDS->s_p_data
#define TERM_TOT          gGDS->s_term_tot
#define RX_BUF            gGDS->s_RxMsg
#define TX_BUF            gGDS->s_TxBuf
#define MSG_BUF           gGDS->s_MsgBuf

//-----------------------------------------------------------------------------
//      Protected RAM area structure
//-----------------------------------------------------------------------------
struct APPDATA
{
	BYTE commKey[16];
	BYTE auto_auth_code[3];
	WORD reversal_count;
	WORD porcPropina; //guarda el porcentaje de la propina
	BYTE KIN[3]; //Identificador de la llave de comunicaciones //Daniel 01/11/12
	//variables  de conexion
	BYTE ConexionDIAL;
	BYTE ConexionLAN;
	BYTE ConexionGPRS;
	BYTE IpHost[5 + 1];
	BYTE IpCaja[5 + 1]; //Daniel Cajas TCP
	BYTE NII[2 + 1];
	BYTE NII_Moviles[2 + 1];
	BYTE TermNo[10 + 1];
	BYTE TelPrincipal[6 + 1];
	BYTE TelSecundario[6 + 1];
	BYTE Prefijo[6];
	BYTE Posfijo[6];
	DWORD PortHost;
	DWORD PortCaja; //Daniel Cajas TCP
	BYTE estadoInit; //manuel-081012
	//BYTE loadKeys;
	BYTE loadKeyPin; //MFBC/14/11/12
	BYTE loadKeyCom; //MFBC/14/11/12
	BYTE ModoLlamada; //MFBC/20/02/13
	BYTE TipoMarcado; //MFBC/20/02/13
	DWORD transIdx; //MFBC/20/02/13
	BYTE RocNo[3];
	int tipoTransCaja;//Daniel Jacome 09/09/13
	BOOLEAN VelModem;		// **SR** 13/11/13
	BOOLEAN ParidadCaja;	// **SR** 13/11/13
	BOOLEAN VelCaja;	// **SR** 13/11/13
	BYTE NumTrans[3];
	BYTE NumRoc[3];
};
extern struct APPDATA gAppDat;

// Estructura creada para enviar la informacion del token QD para la transaccion DCC
struct TokenDCC
{
	BYTE sb_NombreToken[2];
	BYTE sb_IndicaTrans[2];
	BYTE sb_NumeroFolio[10];
	BYTE sb_Cuotas[2];
	BYTE sb_diasHopedaje[2];
	BYTE sb_FillerUno[2];
	BYTE sa_Moneda[3];
	BYTE b_CantidadDecimales;
	BYTE sb_MontoDCC[6];
	BYTE sb_TasaCambio[6];
	BYTE sb_MontoTrasDCCAuth[6];
	BYTE sb_TasaCambioAuth[5];
	BYTE sb_FillerDos[2];
}__attribute__((packed));


//Estructura del campo 63 para la transaccion carga de bono


#define MAX_BINES	6

struct BINES
{
	BYTE BIN[6 + 1];
};

struct TARJETA_VIRTUAL
{
	BYTE PAN[16];
	BYTE fechaVenc[4];
	BOOLEAN Flag_fecha; //MFBC/22/04/13
	BOOLEAN Flag_pan; //MFBC/22/04/13
};

struct BINES_BONO
{
	BYTE binMenor[10 + 1];
	BYTE binMayor[10 + 1];
};

struct BIN_TIPO //MFBC/30/11/12
{
	BYTE bines[8 + 1];
	BYTE codTrans[6 + 1];
	BYTE costoAprobado[6 + 1];
	BYTE costoNegado[15 + 1];

};

struct BIN_BANCO //LFGD 03/21/2013
{
	BYTE bines[6 + 1];
	BYTE issuer[10 + 1];
	BYTE banco[10 + 1];
	BYTE idbanco[4 + 1];

};

//NM-29/04/13 Cree esta struct para Token Consulta Recargas

/************************************************************
 *Struct GPRS			**SR** 20/09/13
 *************************************************************/
#define KConfigGPRS   "CNFGPRS"

struct CNGPRS{

	BYTE sb_Apn[32];
	BYTE sb_Pass[32];
	BYTE sb_User[32];
};


extern struct BIN_TIPO gBin_Tipo; //MFBC/01/12/12
extern struct BINES gBines[MAX_BINES];
extern struct FL63_BILL gfl63_bill_tx;
extern struct FL63_BILL gfl63_bill_rx;

struct CNGPRS gConfigGPRS;			// **SR** 20/09/13

extern struct CAJAS gCajas;
extern struct TARJETA_VIRTUAL gTarjVirtual;

extern struct TXN_RECORD gOrg_rec;
extern DWORD gRecIdx;

extern BYTE gTrack2[20];
extern BYTE gField48[256];
extern BYTE tipoTransCaja;
extern BYTE gSSLKeyIdx;//Daniel SSL
extern DWORD gSSL_KEY;//Daniel SSL
extern BYTE gEWKPIN[16]; //MFBC/10/12/12
extern BYTE gActualiza_Logon[10]; //MFBC/08/04/13
extern BOOLEAN g_sync_logon; //MFBC/22/05/13
extern BOOLEAN g_Puntos_Cierre;
/*
 extern BYTE gDir_ciudad[46];  //MFBC/10/12/12
 extern BYTE gNom_establ[23];  //MFBC/10/12/12
 */
extern BOOLEAN flagAnul; //kt-domingo
extern BOOLEAN gCierreIntegrado;


//-----------------------------------------------------------------------------
//      Functions Handle the data
//-----------------------------------------------------------------------------
extern void DataFileInit(void);
extern void DataFileUpdate(void);
extern void DataFileClose(void);
extern void BinesPINFileInit(void);
extern void BinesPANFileInit(void);
extern void BinesCtasFileInit(void);
extern void BinesTrackFileInit(void);  // **SR** 13/11/13
extern void PagoDividFileInit(void);
extern void TablaIvaFileInit(void); //kt-311012
extern BOOLEAN CorrectHost(BYTE aHostType);
extern void GenAuthCode(BYTE *aAuthCode);
extern void SetDefault(void);

extern void MsgBufSetup(void);
extern void PackMsgBufLen(void);
extern void TxBufSetup(BOOLEAN aAdd2ByteLen);
extern void PackTxBufLen(BOOLEAN aAdd2ByteLen);
extern void RxBufSetup(BOOLEAN aAdd2ByteLen);
extern void PackRxBufLen(BOOLEAN aAdd2ByteLen);

extern int SearchRecord(BOOLEAN aIncVoidTxn);
extern int SearchLastTrans(BOOLEAN aLastTrans);
extern BOOLEAN ValidRecordCRC(WORD aIdx);
extern void RefreshDispAfter(DWORD aSec);

extern void FreeGDS(void);
extern BOOLEAN MallocGDS(void);
extern BOOLEAN TrainingModeON(void);
extern void IncAPMTraceNo(void);
extern void SaveDataFile(struct APPDATA *aApp_data);
extern void leerBINES(BYTE opcion);
extern void guardarBIN(BYTE opcion);
extern void borrarBINES(BYTE opcion);
extern BOOLEAN buscarBines(BYTE aOpcion, BYTE *aBin);
extern void tarjetaVirtualInit(void);
extern void guardarTarjVirtual(struct TARJETA_VIRTUAL *aTarjVirtual);
extern void configComercioInit(void);
extern BOOLEAN GetAppdata(struct APPDATA *aAppdata); //MFBC/13/11/12
extern void SaveFileCajas(BYTE offset);
extern void readFileCajas(void);
extern void cajasFileInit(void);
extern void InitFileGPRS (void);

#endif // _COREVAR_H_
