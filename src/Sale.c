//-----------------------------------------------------------------------------
//  File          : sale.c
//  Module        :
//  Description   : Include routines for sale transaction.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  10 May  2006  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "sysutil.h"
#include "message.h"
#include "constant.h"
#include "corevar.h"
#include "chkoptn.h"
#include "input.h"
#include "print.h"
#include "record.h"
#include "tranutil.h"
#include "hostmsg.h"
#include "reversal.h"
#include "offline.h"
#include "emvtrans.h"
#include "keytrans.h"
#include "key2dll.h"
#include "sale.h"
#include "menu.h"
#include "files.h"
#include "stdlib.h"
#include "rs232.h"
#include "cajas.h"
#include "visamenus.h"


DDWORD gValorTotalCaja = 0;

static const struct MENU_ITEM KCuentasItems[] =
{
		{ 1, "AHORROS           " },
		{ 2, "CORRIENTE         " },
		{ 3, "CREDITO           " },

		{ -1, NULL }, };

static const struct MENU_ITEM KCuentasItems2[] =
{
		{ 1, "AHORROS           " },
		{ 2, "CORRIENTE         " },
		{ 3, "CREDITO           " },
		{ 4, "CREDITO ROTATIVO  " }, //kt-110912

		{ -1, NULL }, };

static const struct MENU_DAT KCuentasMenu =
{ "Tipo Cuenta", KCuentasItems, };

static const struct MENU_DAT KCuentasMenu2 =
{ "Tipo Cuenta", KCuentasItems2, }; //kt-110912

static const struct MENU_ITEM KDocumentosItems[] =
{
		{ 1, "CEDULA CIUDADANIA" },
		{ 2, "NIT               " },
		{ 3, "CEDULA EXTRANJERIA" },
		{ -1, NULL }, };

static const struct MENU_DAT KTipoDocsMenu =
{ "TIPO DE DOCUMENTO", KDocumentosItems, };



/*****************************************************************************/
//                            VARIABLES GLOBALES
/*****************************************************************************/
DDWORD TotalIvas = 0;
DDWORD TotalBase = 0;
DDWORD TotalMonto = 0;

//*****************************************************************************
//  Function        : OffHookState
//  Description     : Reture TRUE if Phone is in Off Hook State.
//                    (Off Hook or Speaker Phone On)
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BOOLEAN OffHookState(void)
{
	DWORD status;
	int handle;

	handle = APM_GetHwHandle(APM_SDEV_MDM);

	if (handle < 0)
	{
		return FALSE;
	}

	if (IOCtlMW(handle, IO_MDM_HSET_STS, 0) == 1)
	{
		return TRUE;
	}

	status = IOCtlMW(handle, IO_MDM_GET_STATUS, 0);
	if ((status == MWMDM_VoiceHs) || (status == MWMDM_VoiceFrSpPh)
			|| (status == MWMDM_VoiceSpPh) || (status == MWMDM_VoiceSpPhFrHs))
	{
		return TRUE;
	}
	return FALSE;
}
#if (R700)
//*****************************************************************************
//  Function        : SetSpeakerGain
//  Description     : 
//  Input           : N/A
//  Return          : N/A
//  Note            : Gain may change after sending AT Cmd.
//  Globals Changed : N/A
//*****************************************************************************
static void SetSpeakerGain(int aHandle, DWORD aGain)
{
	DWORD status = IOCtlMW(aHandle, IO_MDM_GET_STATUS, 0);

	if ((status == K_VoiceHs) || (status == K_VoiceFrSpPh))
	{
		IOCtlMW(aHandle, IO_MDM_SPR_GAIN, aGain);
	}
	else if ((status == K_VoiceSpPh) || (status == K_VoiceSpPhFrHs))
	{
		IOCtlMW(aHandle, IO_MDM_SPR_GAIN, aGain);
	}
}
#endif //(R700)
//*****************************************************************************
//  Function        : MakeVoiceCall
//  Description     : Make Voice Call
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static void MakeVoiceCall(BYTE *aPhoneNum)
{
	int handle;
	// int i;  // For R700-P

	handle = APM_GetHwHandle(APM_SDEV_MDM);
	if (handle >= 0)
	{
#if (R700)
		SetSpeakerGain(handle, 9*15);
#endif

		IOCtlMW(handle, IO_MDM_PHONE_DIAL, aPhoneNum); // For R700

#if (R700)
		// For R700-P
		//for (i = 0; i < strlen(aPhoneNum); i++) {
		//  IOCtlMW(handle, IO_MDM_DIALCHAR, aPhoneNum[i]);
		//}
		SetSpeakerGain(handle, 9*15);
#endif
	}
}
void ReferralCall(void)
{
	int handle;

	handle = APM_GetHwHandle(APM_SDEV_MDM);
	if (handle >= 0)
	{
		IOCtlMW(handle, IO_MDM_SET_OPR_MODE, 21); // set voice mode
		DispClrBelowMW(MW_LINE3);
		DispLineMW("PLS LIFT HANDSET", MW_LINE5, MW_CENTER | MW_BIGFONT);
		while (1)
		{
			SleepMW();
			if (OffHookState())
			{ // Handset in use
				DispClrBelowMW(MW_LINE3);
				DispLineMW("Dialing...      ", MW_LINE5, MW_CENTER | MW_BIGFONT);
				break;
			}
			if (APM_GetKeyin() == MWKEY_CANCL || APM_GetKeyin() == -1)
				return;
		}
		DispClrBelowMW(MW_LINE3);
		MakeVoiceCall(STIS_ISS_TBL(0).sb_ref_tel_no);
		while (os_kbd_getkey() == 0)
			os_sleep();
		return;
	}
	DispLineMW("Phone Error!", MW_LINE5, MW_CENTER | MW_BIGFONT);
	ErrorDelay();
}
//*****************************************************************************
//  Function        : ReferralProcess
//  Description     : Process referral response.
//  Input           : N/A
//  return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ReferralProcess(void)
{
	BYTE tmpbuf[MW_MAX_LINESIZE + 1];

	PackRspText();
	memcpy(tmpbuf, &RSP_DATA.text[1], 16);
	tmpbuf[16] = 0;
	DispLineMW(tmpbuf, MW_LINE1, MW_CENTER | MW_BIGFONT);
	LongBeep();

	APM_ResetComm();
	RSP_DATA.w_rspcode = 'C' * 256 + 'N';
	RSP_DATA.text[0] = 0;

	/*	if (ReferralDial())
	 {
	 ReferralCall();
	 }*/

	if (GetAuthCode())
	{
		if (INPUT.b_trans >= SALE_SWIPE)
		{
			INPUT.b_trans = SALE_COMPLETE;
			INPUT.b_trans_status = OFFLINE;
		}

		DispHeader(STIS_ISS_TBL(0).sb_card_name);
		RSP_DATA.b_response = TRANS_ACP;
		PackDTGAA();
		PackInputP();
		IncTraceNo();
		if (INPUT.b_trans >= SALE_SWIPE)
			SaveRecord();
	}
	else
		DispHeader(STIS_ISS_TBL(0).sb_card_name);
	TransEnd((BOOLEAN) (RSP_DATA.b_response == TRANS_ACP));
}
//*****************************************************************************
//  Function        : SaleTrans
//  Description     : Do Online Sale transaction.
//  Input           : aInputMode;       // Transaction start mode
//  return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if(NO_USED)
DWORD SaleTrans(DWORD aInputMode)
{
	INPUT.b_trans = SALE_SWIPE;

	DispHeader(NULL);

	if (!TransAllowed(aInputMode))
	{
		RefreshDispAfter(0);
		return FALSE;
	}

	if (aInputMode == MANUAL)
		INPUT.b_trans = SALE_MANUAL;

	if (UnderFloorLimit(INPUT.dd_amount) && (!INPUT.b_card_expired)
			&& (aInputMode != FALLBACK))
	{
		INPUT.b_trans = SALE_UNDER_LMT;
		SetRspCode('0' * 256 + '0');
		RSP_DATA.b_response = TRANS_ACP;
		GenAuthCode(RSP_DATA.sb_auth_code);
		if (BlockAuthCode())
			memset(RSP_DATA.sb_auth_code, ' ', 6);
		PackDTGAA();
		IncTraceNo();
		SaveRecord();
		PackInputP();
		TransEnd(TRUE);
		return TRUE;
	}

	ClearResponse();
	//PackComm(INPUT.w_host_idx, FALSE);
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			//OfflineSent(0);
			IncTraceNo();
			MoveInput2Tx();
			PackProcCode(TX_DATA.b_trans, 0xFF);
			PackHostMsg();
			UpdateHostStatus(REV_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();
				if (RSP_DATA.b_response <= TRANS_REJ)
					UpdateHostStatus(NO_PENDING);
			}
			//SaveTraceNo();
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				if (RSP_DATA.w_rspcode == '0' * 256 + '0')
					RSP_DATA.w_rspcode = 'A' * 256 + 'P';
				SaveRecord();
				IncRocNo(); // Jorge Numa 05-02-2013
				PackInputP();
			}
			else if (RSP_DATA.b_response == TRANS_REJ)
			{
				if (VoiceReferral(RSP_DATA.w_rspcode))
				{
					ReferralProcess();
					return FALSE;
				}
			}
		}
	}
	TransEnd((BOOLEAN) (RSP_DATA.b_response == TRANS_ACP));
	return TRUE;
}
#endif

BOOLEAN SelCuenta(void)
{
	DWORD sel;

	INPUT.b_tipo_cuenta = 0x00;

	while (TRUE)
	{
		sel = 0;

		if (MostrarCreRotativo()) //kt-110912
			sel = MenuSelectVisa(&KCuentasMenu2, sel);
		else
			sel = MenuSelectVisa(&KCuentasMenu, sel);

		if (sel == -1)
			return FALSE;

		switch (sel)
		{
		case 1:
			INPUT.b_tipo_cuenta = 0x10;
			memcpy(gCajas.typeAcount_50, "AH", 2);
			return TRUE;
		case 2:
			INPUT.b_tipo_cuenta = 0x20;
			memcpy(gCajas.typeAcount_50, "CO", 2);
			return TRUE;
		case 3:
			memcpy(gCajas.typeAcount_50, "CR", 2);
			INPUT.b_tipo_cuenta = 0x30;
			return TRUE;
		case 4:
			memcpy(gCajas.typeAcount_50, "CR", 2);
			INPUT.b_tipo_cuenta = 0x40;
			return TRUE;
		}
	}
	return TRUE;
}

//**********************************************************************************
//  Function        : VirtualMerchantVisa
//  Description     : si viene tabla 12 en la inicializacion debe escojer comercio
//  Input           : N/A
//  Note            : N/A
//  Globals Changed : N/A
//**********************************************************************************
BOOLEAN VirtualMerchantVisa(void) //kt-231112
{
	//	BYTE kbdbuf[13];
	WORD MaxCount12 = 0;
	//	WORD ContComercio;
	//	int keyin;
	//	DDWORD idComercio = 0;
	BYTE *p_tabla4;


	//	DispClrBelowMW(MW_LINE2);
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));

	MaxCount12 = GetTabla12Count();
	gIdTabla12 = OpenFile(KTabla12File);
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	INPUT.b_terminal = 0; //kt-180413

	if (((struct TABLA_12*) p_tabla12)->b_id <= 0)
	{
		CloseFile(gIdTabla12);
		FreeMW(p_tabla12);
		return TRUE;
	}

	p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
	gIdTabla4 = OpenFile(KTabla4File);
	GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);

	//	while (TRUE)
	//	{
	//		DispClrBelowMW(MW_LINE2);			// **SR**  25/09/13
	memset(INPUT.sb_comercio_v, 0x20, sizeof(INPUT.sb_comercio_v));
	memset(INPUT.sb_nom_comer_v, 0x20, sizeof(INPUT.sb_nom_comer_v));
	memset(INPUT.sb_terminal_id, 0x20, sizeof(INPUT.sb_terminal_id));
	memset(INPUT.sb_term_comer_v, 0x20, sizeof(INPUT.sb_term_comer_v));
	//		memset(kbdbuf, 0x30, sizeof(kbdbuf));
	//		kbdbuf[0] = 1;

	//		DispLineMW("ELIJA COMERCIO", MW_LINE1,
	//				MW_SMFONT | MW_CENTER | MW_REVERSE | MW_CLRDISP);

	//		TextColor("Codigo unico:", MW_LINE4, COLOR_VISABLUE,
	//				MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);

	//		DWORD aux = APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT  + MW_LINE7,IMAX(9), kbdbuf);
	//		printf("\f aux <%d>",aux);APM_WaitKey(9000,0);
	//		printf("\f len <%d>",kbdbuf[0]);APM_WaitKey(9000,0);
	//		DebugPoint(&kbdbuf[1]);

	//		if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT  + MW_LINE7,
	//				IMAX(9), kbdbuf)) //kt-140912
	//		{
	//			FreeMW(p_tabla4);
	//			FreeMW(p_tabla12);
	//			CloseFile(gIdTabla4);
	//			CloseFile(gIdTabla12);
	//			return FALSE;
	//		}

	//		printf("\fkbdbuf: |%02x %02x %02x|", kbdbuf[0], kbdbuf[1], kbdbuf[2]);
	//		APM_WaitKey(9000,0);

	//		if ((kbdbuf[0] == 1 && kbdbuf[1] == 0x30)
	//				|| memcmp(&gTablaCuatro.b_cod_estable[4], &kbdbuf[1], kbdbuf[0]) == 0)
	//		{
	//
	//			DebugPoint("if");
	INPUT.b_terminal = 1; //kt-180413
	//			memcpy(INPUT.sb_comercio_v, &((struct TABLA_CUATRO*) p_tabla4)->b_cod_estable[4], 11);
	//	memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11);
	memcpy(INPUT.sb_comercio_v, gTablaDoce.b_establ_virtual,9);
	memcpy(INPUT.sb_nom_comer_v, gTablaDoce.b_nom_establ, 15);
	memcpy(INPUT.sb_term_comer_v, gTablaDoce.b_term_virtual, 8);
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);
	//		}
	//		else
	//		{
	//			DebugPoint("else");
	//			for (ContComercio = 0; ContComercio < MaxCount12; ContComercio++)		// Se le quita el <= y solo se deja < para que no busque en un registro que no existe  **SR** 18/12/13
	//			{
	//				if (!GetTablaDoce(ContComercio, (struct TABLA_12*) p_tabla12))
	//					continue;
	//
	//				idComercio = decbin8b(&kbdbuf[1], kbdbuf[0]);
	//
	//				/*	printf("\festavirtu: |%d| |%d|", ContComercio, memcmp(((struct TABLA_12*) p_tabla12)->b_establ_virtual, &kbdbuf[1], kbdbuf[0]));
	//				 APM_WaitKey(9000,0);
	//				 printf("\nnom_establ:     |%d|", (((struct TABLA_12*) p_tabla12)->b_id == idComercio));
	//				 APM_WaitKey(9000,0);*/
	//
	//				if (memcmp(((struct TABLA_12*) p_tabla12)->b_establ_virtual,&kbdbuf[1], kbdbuf[0]) == 0
	//						|| (((struct TABLA_12*) p_tabla12)->b_id == idComercio))
	//					break;
	//			}
	//
	//			if (ContComercio >= MaxCount12)		// se modifica la validacion de > a >= **SR** 18/12/13
	//			{
	//				DispLineMW("AVISO", MW_LINE1,
	//						MW_SMFONT | MW_CENTER | MW_REVERSE | MW_CLRDISP);
	//				TextColor("Comercio no valido", MW_LINE5, COLOR_RED,
	//						MW_SMFONT | MW_LEFT, 3);
	//				continue;
	//			}
	//
	//			INPUT.b_terminal = 2; //kt-180413
	//			memcpy(INPUT.sb_comercio_v,
	//					((struct TABLA_12*) p_tabla12)->b_establ_virtual, 9);
	//			memcpy(INPUT.sb_nom_comer_v,
	//					((struct TABLA_12*) p_tabla12)->b_nom_establ,
	//					sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ));
	//			memcpy(INPUT.sb_terminal_id,
	//					((struct TABLA_12*) p_tabla12)->b_term_virtual, 8);
	//		}

	//		while (TRUE)
	//		{
	//
	//			DispLineMW("Establecimiento", MW_LINE1,
	//					MW_SMFONT | MW_CENTER | MW_REVERSE | MW_CLRDISP);
	//			TextColor(INPUT.sb_nom_comer_v, MW_LINE4, COLOR_VISABLUE,
	//					MW_SMFONT | MW_LEFT, 0);
	//			TextColor(INPUT.sb_comercio_v, MW_LINE6, COLOR_VISABLUE,
	//					MW_SMFONT | MW_CENTER, 0);
	//			displaySI_NO_2();
	//
	//			keyin = APM_YesNo();
	//			if (keyin == 2)
	//			{
	//				FreeMW(p_tabla4);
	//				FreeMW(p_tabla12);
	//				CloseFile(gIdTabla4);
	//				CloseFile(gIdTabla12);
	//				return TRUE;
	//			}
	//			else if(keyin == -1 ||keyin == 0 )
	//			{
	//				FreeMW(p_tabla4);
	//				FreeMW(p_tabla12);
	//				CloseFile(gIdTabla4);
	//				CloseFile(gIdTabla12);
	//				return FALSE;
	//			}
	//
	//		}
	//	}

	FreeMW(p_tabla4);
	FreeMW(p_tabla12);
	CloseFile(gIdTabla4);
	CloseFile(gIdTabla12);
	return TRUE;
}

//*****************************************************************************
//  Function        : CompraTrans
//  Description     :
//  Input           :
//  Return          :
//  Note            : Transaccion principal
//  Globals Changed : N/A
//*****************************************************************************
DWORD CompraTrans(DWORD aInputMode, BYTE aTrans)
{
	//int retPin;
	BYTE nomIss[11];
	BYTE kbdbuf[31];
	DWORD ret = 0;
	//	DWORD keyin = 0;
	BYTE panBCD[10];
	//WORD idx_donacion;
	//BYTE tempDonacion[3];
	BYTE panAscii[20 + 1];
	INPUT.b_transAcep = FALSE; //MFBC/25/04/13
	RSP_DATA.b_response = TRANS_FAIL;
	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(panAscii, 0x00, sizeof(panAscii));
	memset(INPUT.sb_num_ref, 0x30, sizeof(INPUT.sb_num_ref)); //MFBC/29/04/13
	INPUT.b_trans = aTrans;

	ClearResponse();
	PackCommTest(INPUT.w_host_idx, false); //MFBC/05/03/13
	APM_PreConnect(); //MFBC/05/03/13

	INPUT.dd_tasa_aeropor = 0;
	INPUT.dd_donacion = 0;
	INPUT.dd_base_tasa_aero = 0;
	INPUT.dd_iva_tasa_aero = 0;


	if (Cajas()) { // La terminal esta configurada en modo caja
		if (!procesoCompraCaja1(panAscii)){
			APM_ResetComm(); //MFBC/05/03/13
			return -1;
		}

		if (!procesoCompraCaja2(panAscii)){			// **SR** 16/09/13 Despues de recivir la trama de la Caja realiza todas la validaciones correspondientes
			ErrorTransCajas(CA_CANCEL);
			APM_ResetComm();
			return -1;
		}
	}
	NextProcCajas();

	//	memset(tempDonacion, 0x00, sizeof(tempDonacion));

	/***  En este punto inician las validaciones de la compra ***/
	//Pide seleccion de cuenta
	if (SeleccionCuenta())
	{
		if (!SelCuenta())
		{
			ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
			APM_ResetComm(); //MFBC/05/03/13
			return -1;
		}
	}

	memset(nomIss, 0x00, sizeof(nomIss));
	memcpy(nomIss, gTablaTres.b_nom_emisor, 10);

	DispLineMW(nomIss, MW_LINE1, MW_CLRDISP | MW_REVERSE | MW_LEFT | MW_SMFONT);

	//	if (INPUT.b_trans == RETIRO_MULTIBOL)		//kt-160413
	//	{
	//		DispLineMW("    RETIRO", MW_LINE1, MW_REVERSE | MW_RIGHT | MW_SMFONT);
	//	}
	//	else
	//	{
	DispLineMW("    COMPRA", MW_LINE1, MW_REVERSE | MW_RIGHT | MW_SMFONT);
	//	}

	if (!Cajas()){
		//Captura monto
		if (!GetAmnt(TRUE))
		{
			APM_ResetComm(); //MFBC/22/05/13
			return -1;
		}

	}

	//Ingresar cedula cliente
	if (!GetCedulaCliente()) //MFBC/18/04/13
	{
		ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
		APM_ResetComm();
		return -1;
	}

	if (!Cajas())
	{
		if (iva_Comportamiento(INPUT.dd_amount) == FALSE)
		{
			APM_ResetComm(); //MFBC/22/05/13
			return -1;
		}
	}

	//PROPINA
	INPUT.dd_amount += INPUT.dd_tip; //MFBC/22/05/13




	// NUMERO DE REFERENCIA
	if (!GetReferencia()) //MFBC/18/04/13
	{
		ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
		APM_ResetComm();
		return -1;
	}

	// DONACION


	if (GetDonacion() == FALSE) //MFBC/20/05/13 se valida donacion
	{
		ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
		APM_ResetComm();
		return -1;
	}


	//Ingresar cedula cajero
	if (!GetCedulaCajero()) //MFBC/18/04/13
	{
		ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
		APM_ResetComm();
		return -1;
	}

	INPUT.dd_amount += INPUT.dd_valor_efectivo;

	if (INPUT.b_tipo_cuenta != 0x30 || INPUT.b_tipo_cuenta != 0x40)
	{
		if(confirmarMonto (INPUT.dd_amount) == FALSE) //MFBC/12/09/13
		{
			ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
			APM_ResetComm(); //MFBC/05/03/13
			return -1;
		}
	}


	//Captura fecha de exp.
	if (!GetExpDateVISA())		// Lee primero la fecha de expiracion antes de validar Cajas **SR** 17/08/13
	{
		ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
		APM_ResetComm(); //MFBC/05/03/13
		return -1;
	}

	//	INPUT.dd_amount -= INPUT.dd_donacion; //MFBC/04/03/13

	//Pide PIN
	if (INPUT.b_tipo_cuenta == 0x10 || INPUT.b_tipo_cuenta == 0x20
			|| !SeleccionCuenta())
	{
		if (PinDebito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				APM_ResetComm(); //MFBC/05/03/13
				return -1;
			}
		}
	}
	else
	{
		if (PinCredito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				APM_ResetComm(); //MFBC/05/03/13
				return -1;
			}
		}
	}



	// ULTIMOS 4 DIGITOS
	if ((INPUT.b_tipo_cuenta != 0x10 && INPUT.b_tipo_cuenta != 0x20)
			&& GetUlt4Digitos())
	{
		while (TRUE)
		{
			ret = Ult4Digitos();
			if (ret == -1)
			{
				ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				APM_ResetComm(); //MFBC/05/03/13
				return -1;
			}
			else if (ret == TRUE)
				break;
		}
	}


	// Cuotas - T Credito
	if ((INPUT.b_tipo_cuenta == 0x30 || INPUT.b_tipo_cuenta == 0x40)
			&& RequiereCuotas()) // Credito		//kt-domingo
	{
		if (!GetCuotas())
		{
			APM_ResetComm(); //MFBC/05/03/13
			ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
			return -1;
		}
	}

	/***  En este punto terminan las validaciones de la compra ***/

	if (!ConsultaCosto())
	{
		APM_ResetComm(); //MFBC/05/03/13
		ErrorTransCajas(CA_COMM);		// **SR**14/08/13
		return -1;
	}
	INPUT.b_trans = aTrans;

	/*Se elima de este punto el UnderFlorLimit y sus validaciones **SR** 19/08/13 */

	/*	ClearResponse();
	 //PackComm(INPUT.w_host_idx, FALSE);
	 PackCommTest(INPUT.w_host_idx, false);*/
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			//OfflineSent(0);
			IncTraceNo();
			MoveInput2Tx();
			PackProcCode(TX_DATA.b_trans, 0xFF);
			PackHostMsg();
			UpdateHostStatus(REV_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();

				if (RSP_DATA.w_rspcode == '5' * 256 + '5')  //MFBC/09/12/13
				{
					RSP_DATA.b_response = NuevoPIN();
				}


				if (RSP_DATA.b_response == TRANS_REJ)
				{
					UpdateHostStatus(NO_PENDING);
				}
			}
			else
			{
				ErrorTransCajas(CA_COMM);		// **SR**14/08/13
			}

			//SaveTraceNo();
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				if (RSP_DATA.w_rspcode == '0' * 256 + '0')
					RSP_DATA.w_rspcode = 'A' * 256 + 'P';
				INPUT.b_transAcep = TRUE; //MFBC/25/04/13
				SaveRecord();
				IncRocNo(); // Jorge Numa 05-02-2013
				PackInputP();
				SendCajas(1);		// **SR**/19/08/13

			}
			else if (RSP_DATA.b_response == TRANS_REJ)
			{
				IncRocNo(); // Jorge Numa 04-06-13
				SendCajas(1);			// **SR**/19/08/13
				/*Se elimina el ReferralProces de este punto **SR**19/08/13  */
				if (INPUT.b_flag_Consulta_Costo) //MFBC/23/04/13
				{
					PackInputP();
					TransEnd(TRUE);
					return -1;
				}

				TransEnd(FALSE); //MFBC/07/05/13
				return -1;
			}
		}
		else
			ErrorTransCajas(CA_COMM);			//  **SR** 09/09/13
	}
	else
		ErrorTransCajas(CA_COMM);		// **SR**14/08/13  // Si no responde la Caja una transaccion aprobada igualmente pasa bien

	TransEnd((BOOLEAN) (RSP_DATA.b_response == TRANS_ACP));
	return TRUE;
}

void armarTrack2(BYTE *binCheque, BYTE *numCuenta, BYTE tipoDoc, BYTE *numDoc,
		BYTE *telefono)
{
	BYTE track2[40 + 1];
	BYTE indice = 0;

	memset(track2, 0x00, sizeof(track2));
	memcpy(track2 + indice, "37", 2);
	indice += 2;
	memcpy(track2 + indice, binCheque, strlen(binCheque));
	indice += strlen(binCheque);
	memcpy(track2 + indice, numCuenta, strlen(numCuenta));
	indice += strlen(numCuenta);
	track2[indice++] = tipoDoc;
	memcpy(track2 + indice, numDoc, strlen(numDoc));
	indice += strlen(numDoc);
	memcpy(track2 + indice, telefono, strlen(telefono));
	//printf("\fLen: %d", strlen(track2));APM_WaitKey(9000, 0);
	//printf("\f|%s|", track2);APM_WaitKey(9000, 0);
	compress(gTrack2, track2, 20);
}

BOOLEAN procesoCompraCaja1(BYTE *aPanAscii)
{
	BYTE bufferAux[30];
	BYTE panAscii[20 + 1];
	BYTE SetNull[23 + 1];
	int len;

	memset(SetNull, 0x00, sizeof(SetNull));
	memset(bufferAux, 0x00, sizeof(bufferAux));
	memset(panAscii, 0x00, sizeof(panAscii));

	split(bufferAux, INPUT.sb_pan, 10);
	len = (BYTE) fndb(bufferAux, 0x46, 20);
	memcpy(panAscii, bufferAux, len);
	memcpy(aPanAscii, panAscii, len);

	if (gConfigComercio.habEnvioPan == 1){

		if (buscarBines(2, panAscii))
			tipoTransCaja = PAGO_CON_TARJETA_PAN;
		else
			tipoTransCaja = PAGO_CON_TARJETA;
	}
	else if (gConfigComercio.habEnvioPan == 2) // Solo se envian los 6 primeros y se completa con (*)
		tipoTransCaja = PAGO_CON_TARJETA_PAN;
	else if (gConfigComercio.habEnvioPan == 3)
		tipoTransCaja = PAGO_CON_TARJETA_PAN_MULTICO;
	else
		tipoTransCaja = PAGO_CON_TARJETA;

	if (gConfigComercio.habEnvioSerial == 1 && gConfigComercio.habEnvioPan != 3)			// se cambia a 1 **SR**26/08/13
	{
		if (tipoTransCaja != PAGO_CON_TARJETA_PAN)
			tipoTransCaja = PAGO_CON_TARJETA_SN;
		else
			tipoTransCaja = PAGO_CON_TARJETA_PAN_SN;
	}

	if (!transaccionCaja(tipoTransCaja, TRUE, FALSE))
		return FALSE;

	if(memcmp(gCajas.codComercio_77,"                       ", 23) == 0 ||
			memcmp(gCajas.codComercio_77,SetNull, 23) == 0){
		if(!VirtualMerchantVisa())		// **SR** 18/12/13
			return FALSE;
	}
	else{
		if(!FindComerCajas())
			return FALSE;
	}

	NextProcCajas();		// Siguiente paso para cajas **SR**
	if (atoi(gCajas.totalCompra_40) <= 0)
	{
		LongBeep();
		TextColor("Monto Invalido!!", MW_LINE4, COLOR_RED,
				MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
		TextColor("Monto = $0,00", MW_LINE5, COLOR_VISABLUE,
				MW_SMFONT | MW_CENTER, 3);
		ErrorTransCajas(CA_CANCEL);
		return FALSE;
	}
	return TRUE;
}

BOOLEAN procesoCompraCaja2(BYTE *aPanAscii)
{
	DDWORD Propi_CashBack = 0;
	DDWORD MaxIva;
	DDWORD ValorAuxiliar;
	BYTE porcentText[5]; //kt-101212
	DDWORD valorPropina = 0;
	BYTE DispTip[3 + 1];
	int len;

	memset(porcentText, 0x00, sizeof(porcentText));

	INPUT.dd_amount = atoi(gCajas.totalCompra_40) * 100;
	INPUT.dd_iva = atoi(gCajas.valorIva_41) * 100;
	INPUT.dd_base = atoi(gCajas.valorBaseDev_80) * 100;
	Propi_CashBack = atoi(gCajas.propina_81) * 100;
	INPUT.dd_IAC = atoi(gCajas.filler_82) * 100; //MFBC/08/04/13
	INPUT.dd_donacion = 0;

	INPUT.dd_net_mount = INPUT.dd_amount - INPUT.dd_iva - INPUT.dd_IAC ;

	if(INPUT.dd_base != 0)
		INPUT.b_print_baseDev = TRUE;			// para impresion de la base en el Voucher **SR** 12/09/13

	if (Propi_CashBack > 0){
		if (Propina()){
			INPUT.dd_tip = Propi_CashBack;
			INPUT.dd_amount += INPUT.dd_tip;
		}
		else if (GetCashback() > 0){
			INPUT.dd_valor_efectivo = Propi_CashBack;
		}
		else{
			LongBeep();
			TextColor("Propina y Cash Back", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor("No Habilitados", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER, 3);
			return FALSE;
		}
	}

	/*************************VALIDACION PARA LA PROPINA MAYOR AL MONTO INGRESADO***********************/		// **SR** 16/09/13
	valorPropina = ((INPUT.dd_amount * gAppDat.porcPropina) / 100);

	if (INPUT.dd_tip > valorPropina)
	{
		Short2Beep();
		sprintf(DispTip, "%02d%s", gAppDat.porcPropina, "%");
		DispClrBelowMW(MW_LINE2);


		TextColor("La Propina tiene", MW_LINE3, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);
		TextColor("Que ser menor a", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);

		TextColor(DispTip, MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 3);
		return FALSE;
	}
	/*************************VALIDACION PARA LA PROPINA MAYOR AL MONTO INGRESADO***********************/		// **SR** 16/09/13


	/*************************VALIDACION PARA IVA MAYOR AL MONTO INGRESADO*********************************/
	MaxIva = (INPUT.dd_amount * bcd2bin(gTablaCero.b_iva) ) / (bcd2bin(gTablaCero.b_iva) + 100); // ** SR** 12/09/13
	ValorAuxiliar = MaxIva;
	ValorAuxiliar /= 100;
	ValorAuxiliar *= 100;
	if(MaxIva - ValorAuxiliar > 50 )
		ValorAuxiliar+= 100;

	if (INPUT.dd_iva > ValorAuxiliar && bcd2bin(gTablaCero.b_iva) != 0)		// ** SR** 12/09/13
	{
		DispClrBelowMW(MW_LINE2);
		LongBeep();
		sprintf(porcentText, "%c %d", '%', bcd2bin(gTablaCero.b_iva));
		TextColor("IVA no debe ser", MW_LINE3, COLOR_RED, MW_CENTER | MW_SMFONT, 0);
		TextColor("mayor al", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT, 0);
		TextColor(porcentText, MW_LINE5, COLOR_RED, MW_CENTER | MW_SMFONT, 3);
		return FALSE;
	}
	/*************************VALIDACION PARA IVA MAYOR AL MONTO INGRESADO**********************************/

	/*************************VALIDACION PARA BASE DEV MAYOR AL MONTO INGRESADO**********************************/
	if (BaseIva())
	{
		if (INPUT.dd_base > INPUT.dd_amount)
		{
			DispClrBelowMW(MW_LINE2);
			TextColor("La base", MW_LINE3, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
			TextColor("de devolucion", MW_LINE4, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
			TextColor("debe ser menor o", MW_LINE5, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
			TextColor("igual al valor neto", MW_LINE6, COLOR_RED,MW_CENTER | MW_SMFONT, 3);
			ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
			return FALSE;
		}
	}
	/*************************VALIDACION PARA BASE DEV MAYOR AL MONTO INGRESADO**********************************/


	len = strlen(aPanAscii);
	memcpy(gCajas.ult4DigTarjeta_54, &aPanAscii[len - 4], 4);

	return TRUE;
}


BOOLEAN CalcularIvaBase(WORD max_ivas)
{
	static const DWORD KDecimalPos[4] =
	{ DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3 };

	BYTE *t_ivas = (void *) MallocMW(sizeof(struct TABLA_IVAS));
	DWORD fd, i;
	DDWORD PorcenIva;
	BYTE kbdbuf[20];
	BYTE PorcenHeader[22];

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(PorcenHeader, 0x00, sizeof(PorcenHeader));

	fd = OpenFile(KTablaIvasFile);

	DispLineMW("MULTIIVA", MW_LINE1,
			MW_SMFONT | MW_CENTER | MW_CLRDISP | MW_REVERSE);

	while (true)
	{
		while (true)
		{
			PorcenIva = 0;

			DispLineMW("MULTIIVA", MW_LINE1,
					MW_SMFONT | MW_CENTER | MW_CLRDISP | MW_REVERSE);

			TextColor("Ingrese IVA", MW_LINE3, COLOR_VISABLUE,
					MW_SMFONT | MW_LEFT, 0);
			kbdbuf[0] = 4;
			memcpy(&kbdbuf[1], "   ", 3);
			kbdbuf[4] = '%';

			if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
					+ MW_LINE5, IMAX(2), kbdbuf))
			{
				CloseFile(fd);
				FreeMW(t_ivas);
				return false;
			}

			if (kbdbuf[1] == 0)
				break;

			for (i = 1; i <= max_ivas; i++)
			{
				if (!GetTablaIvas(fd, i, (struct TABLA_IVAS*) t_ivas))
					continue;

				/*printf("\fIvas   |%02x%02x|", ((struct TABLA_IVAS *) t_ivas)->b_ivas[0], ((struct TABLA_IVAS *) t_ivas)->b_ivas[1]);
				 printf("\nkbdbuf |%02x%02x|", kbdbuf[1], kbdbuf[2]);
				 APM_WaitKey(9000,0);*/

				if (memcmp(((struct TABLA_IVAS *) t_ivas)->b_ivas, &kbdbuf[1],
						2) == 0)
					break;
			}

			if (i > max_ivas)
			{
				DispClrBelowMW(MW_LINE2);
				TextColor("Ingrese IVA ", MW_LINE4, COLOR_RED,
						MW_SMFONT | MW_CENTER, 0);
				TextColor("configurado", MW_LINE5, COLOR_RED,
						MW_SMFONT | MW_CENTER, 0);
				TextColor("en el menu Admin.", MW_LINE6, COLOR_RED,
						MW_SMFONT | MW_CENTER, 3);
				/*				DispPutCMW(K_PushCursor);
				 os_disp_textc(COLOR_RED);
				 DispLineMW("Ingrese IVA ", MW_LINE4, MW_SMFONT | MW_CENTER);
				 DispLineMW("configurado", MW_LINE5, MW_SMFONT | MW_CENTER);
				 DispLineMW("en el menu Admin.", MW_LINE6, MW_SMFONT | MW_CENTER);
				 DispPutCMW(K_PopCursor);
				 Delay10ms(300);
				 APM_WaitKey(300, 0);*/
				//LongBeep();
			}
			else
				break;
		}

		if (kbdbuf[1] != 0)
		{
			PorcenIva = decbin8b(&kbdbuf[1], kbdbuf[0]);

			while (true)
			{
				memset(kbdbuf, 0x00, sizeof(kbdbuf));
				sprintf(PorcenHeader, "PORCENTAJE IVA: %c%d ", '%', PorcenIva);
				DispLineMW(PorcenHeader, MW_LINE1,
						MW_SMFONT | MW_LEFT | MW_CLRDISP | MW_REVERSE);

				TextColor("Ingrese valor IVA", MW_LINE3, COLOR_VISABLUE,
						MW_SMFONT | MW_LEFT, 0);
				/*DispPutCMW(K_PushCursor);
				 os_disp_textc(COLOR_VISABLUE);
				 DispLineMW("Ingrese valor IVA", MW_LINE3, MW_SMFONT | MW_LEFT);
				 DispPutCMW(K_PopCursor);*/

				kbdbuf[0] = 4;
				memcpy(&kbdbuf[1], "   ", 3);
				kbdbuf[4] = '$';

				if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
						+ MW_LINE5 + KDecimalPos[bcd2bin(
								gTablaCero.b_num_decimales)], IMIN(1) + IMAX(bcd2bin(
										gTablaCero.b_num_digitos)), kbdbuf))
				{
					CloseFile(fd);
					FreeMW(t_ivas);
					return false;
				}

				INPUT.dd_iva = decbin8b(&kbdbuf[1], kbdbuf[0]);

				if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
				{
					INPUT.dd_iva *= 100;
				}

				INPUT.dd_base = (INPUT.dd_iva * 100) / PorcenIva;

				if (INPUT.dd_base > INPUT.dd_amount)
				{
					DispClrBelowMW(MW_LINE2);
					TextColor("La base de devolucion", MW_LINE3, COLOR_RED,
							MW_SMFONT | MW_RIGHT, 0);
					TextColor("  debe ser menor o   ", MW_LINE4, COLOR_RED,
							MW_SMFONT | MW_RIGHT, 0);
					TextColor(" igual al valor neto ", MW_LINE5, COLOR_RED,
							MW_SMFONT | MW_RIGHT, 3);
				}
				else
					break;
			}

			TotalIvas += INPUT.dd_iva;
			TotalBase += INPUT.dd_base;
		}
		else
			break;

		if (TotalBase + TotalIvas == TotalMonto)
			break;
	}

	CloseFile(fd);
	//CloseFile(fdpago);
	FreeMW(t_ivas);
	//FreeMW(p_div);
	return TRUE;

}
//*****************************************************************************
//  Function        : ConsultaCosto
//  Input           : N/A
//  Return          : TRUE/FALSE
//  Note            : By Manuel Barbaran.
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ConsultaCosto(void)
{
	BYTE screenAmount[8 + 1];
	DWORD read_Bin = 0;
	int seleccion = 0;
	memset(screenAmount, 0x00, sizeof(screenAmount));
	memset(&gBin_Tipo, 0x00, sizeof(struct BIN_TIPO));
	memset(INPUT.b_costoTrans, 0x00, sizeof(INPUT.b_costoTrans));

	struct TABLA_CERO tabla_cero;
	GetTablaCero(&tabla_cero);
	INPUT.b_flag_Consulta_Costo = FALSE; //MFBC/23/04/13

	read_Bin = LeerBinTipo();

	if (read_Bin == BIN_ACP)
	{
		INPUT.b_flag_Consulta_Costo = TRUE; //MFBC/02/05/13
		memcpy(INPUT.b_costoTrans, gBin_Tipo.costoAprobado, 6); //MFBC/02/05/13

		while(TRUE)
		{
			TextColor("Desea conocer", MW_LINE4, COLOR_VISABLUE,
					MW_CENTER | MW_CLRDISP | MW_SMFONT, 0);
			TextColor("el costo?", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT,
					0);
			displaySI_NO_2();
			seleccion = APM_YesNo() ;
			if (seleccion == 2)
			{
				while(TRUE)
				{
					seleccion = 0;

					TextColor("Costo de Transaccion", MW_LINE3, COLOR_VISABLUE,
							MW_CENTER | MW_CLRDISP | MW_SMFONT, 0);
					memcpy(&screenAmount[0], &gTablaCero.b_simb_moneda, 1);
					memcpy(&screenAmount[1], gBin_Tipo.costoAprobado, 6);
					TextColor(screenAmount, MW_LINE5, COLOR_VISABLUE,
							MW_LEFT | MW_SMFONT, 0);

					TextColor("Desea Continuar?", MW_LINE7, COLOR_VISABLUE,
							MW_CENTER | MW_SMFONT, 0);
					displaySI_NO_2();
					seleccion = APM_YesNo();

					if (seleccion == 2)
						return TRUE;
					else if (seleccion == 0  || seleccion == -1)
						return FALSE;
				}

			}
			else if(seleccion == -1)
			{
				return FALSE;
			}
			else if(seleccion == 0)
			{
				return TRUE;
			}

		}

	}
	else if (read_Bin == BIN_FAIL)
	{
		INPUT.b_flag_Consulta_Costo = FALSE; //MFBC/23/04/13
		LongBeep();
		TextColor("OPERACION NO", MW_LINE4, COLOR_VISABLUE,
				MW_CENTER | MW_SMFONT | MW_CLRDISP, 0); //MFBC/17/04/13
		TextColor("PERMITIDA", MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT,
				3);
		return FALSE;
	}

	//	TextColor("No BIN TIPO", MW_LINE4, COLOR_VISABLUE, MW_CENTER |MW_CLRDISP|MW_SMFONT , 3);
	return TRUE;
}

//*****************************************************************************
//  Function        : ConsultaCostoTrans
//  Input           : N/A
//  Return          : TRUE/FALSE
//  Note            : By Manuel Barbaran.
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ConsultaCostoTrans(void)
{

	INPUT.b_trans = CONSULTA_COSTO;
	memset(INPUT.b_costoTrans, 0x00, sizeof(INPUT.b_costoTrans)); //MFBC/25/04/13
	BYTE b_costos[12 + 1];
	memset(b_costos, 0x00, sizeof(b_costos));
	ClearResponse();
	PackCommTest(INPUT.w_host_idx, false);
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			//OfflineSent(0);
			IncTraceNo();
			MoveInput2Tx();
			//PackProcCode(TX_DATA.b_trans, 0xFF);
			PackHostMsg();

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();
			}
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				if (RSP_DATA.w_rspcode == '0' * 256 + '0')
				{
					RSP_DATA.w_rspcode = 'A' * 256 + 'P';
					sprintf(b_costos, "%12d", RSP_DATA.dd_amount);
					memcpy(gBin_Tipo.costoAprobado, b_costos, 6);
					memcpy(INPUT.b_costoTrans, gBin_Tipo.costoAprobado, 6); //MFBC/25/04/13
					memcpy(gBin_Tipo.costoNegado, &b_costos[6], 6);
					TransEnd((BOOLEAN) (RSP_DATA.b_response == TRANS_ACP));
					return TRUE;
				}
				else if (RSP_DATA.w_rspcode == 'N' * 256 + '1') //MFBC/23/04/13
				{
					TextColor("No se obtuvo", MW_LINE4, COLOR_RED,
							MW_CENTER | MW_CLRDISP | MW_SMFONT, 0);
					TextColor("Respuesta", MW_LINE5, COLOR_RED,
							MW_CENTER | MW_SMFONT, 3);
				}
			}
		}
	}
	TransEnd((BOOLEAN) (RSP_DATA.b_response == TRANS_ACP));
	return FALSE;
}

void digito10(BYTE *auxPan)
{
	DWORD chk = 0;
	BYTE dig10[2];

	chk = chk_digit(auxPan, 15);

	memset(dig10, 0x00, sizeof(dig10));
	sprintf(dig10, "%c", chk);
	strcat(auxPan, dig10);

}

DWORD superCupoFalabella (void)
{
	BYTE nomIss[11];
	BYTE kbdbuf[31];
	DWORD ret = 0;
	//	DWORD keyin = 0;
	BYTE panBCD[10];
	BYTE panAscii[20 + 1];
	INPUT.b_transAcep = FALSE;

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(panAscii, 0x00, sizeof(panAscii));
	INPUT.b_trans = SALE_SWIPE;

	ClearResponse();
	PackCommTest(INPUT.w_host_idx, FALSE); //MFBC/05/03/13
	APM_PreConnect(); //MFBC/05/03/13

	INPUT.dd_tasa_aeropor = 0;
	INPUT.dd_donacion = 0;
	INPUT.dd_base_tasa_aero = 0;
	INPUT.dd_iva_tasa_aero = 0;

	if (!GetCard(0, FALSE, SUPERCUPO, 1))
	{
		APM_ResetComm(); //MFBC/05/03/13
		return -1;
	}

	INPUT.b_trans = SUPERCUPO;

	if(memcmp(gTablaTres.b_nom_emisor, "CMR", 3) != 0)
	{
		TextColor("Tarjeta no permitida", MW_LINE5, COLOR_RED,MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		APM_ResetComm(); //MFBC/05/03/13
		return -1;
	}

	//Captura fecha de exp.
	if (!GetExpDateVISA())
	{
		APM_ResetComm(); //MFBC/05/03/13
		return -1;
	}

	if(Cajas()){
		tipoTransCaja = SUPERCUPO_fll;
		if (!procesoCompraCaja1(panAscii))
		{
			APM_ResetComm(); //MFBC/05/03/13
			return -1;
		}
	}
	NextProcCajas();			// **SR** 09/09/13


	//Pide seleccion de cuenta
	if (SeleccionCuenta())
	{
		if (!SelCuenta())
		{
			APM_ResetComm(); //MFBC/05/03/13
			ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
			return -1;
		}
	}

	if ((GetCashback() > 0) //MIRA si tiene cashback
			&& INPUT.b_tipo_cuenta == 0x30) //kt-miercoles
	{
		Short2Beep();
		TextColor("Tarjeta no permitida", MW_LINE5, COLOR_RED,MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		APM_ResetComm(); //MFBC/05/03/13
		ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
		return -1;
	}


	memset(nomIss, 0x00, sizeof(nomIss));
	memcpy(nomIss, gTablaTres.b_nom_emisor, 10);

	DispLineMW(nomIss, MW_LINE1, MW_CLRDISP  | MW_LEFT | MW_SMFONT);


	DispLineMW("VENTA", MW_LINE1,  MW_RIGHT | MW_SMFONT);

	//Captura monto


	if(!Cajas()){
		if (!GetAmnt(TRUE))
		{
			APM_ResetComm(); //MFBC/22/05/13
			return -1;
		}

	}
	else{

		if (!procesoCompraCaja2(panAscii))
			return -1;

		if (gTablaCero.b_dir_ciudad[43] == '1' || gTablaCero.b_dir_ciudad[43] == '3')
		{
			if (BaseIva())
			{
				if (INPUT.dd_base > INPUT.dd_amount)
				{
					DispClrBelowMW(MW_LINE2);
					TextColor("La base", MW_LINE3, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
					TextColor("de devolucion", MW_LINE4, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
					TextColor("debe ser menor o", MW_LINE5, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
					TextColor("igual al valor neto", MW_LINE6, COLOR_RED,MW_CENTER | MW_SMFONT, 3);
					ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
					APM_ResetComm(); //MFBC/22/05/13
					return -1;
				}
			}
		}

	}

	//Ingresar cedula cliente
	if (!GetCedulaCliente()) //MFBC/18/04/13
	{
		ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
		APM_ResetComm();
		return -1;
	}


	if(!Cajas())
	{
		if (iva_Comportamiento(INPUT.dd_amount) == FALSE)
		{
			APM_ResetComm(); //MFBC/22/05/13
			return -1;
		}
	}


	// NUMERO DE REFERENCIA
	if (!GetReferencia()) //MFBC/18/04/13
	{
		ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
		APM_ResetComm();
		return -1;
	}

	//Ingresar cedula cajero
	if (!GetCedulaCajero()) //MFBC/18/04/13
	{
		ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
		APM_ResetComm();
		return -1;
	}


	//PROPINA
	INPUT.dd_amount += INPUT.dd_tip; //MFBC/22/05/13



	if (INPUT.b_tipo_cuenta != 0x30 || INPUT.b_tipo_cuenta != 0x40)
	{
		if(confirmarMonto (INPUT.dd_amount) == FALSE) //MFBC/12/09/13
		{
			ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
			APM_ResetComm(); //MFBC/05/03/13
			return -1;
		}

	}


	//Pide PIN
	if (INPUT.b_tipo_cuenta == 0x10 || INPUT.b_tipo_cuenta == 0x20
			|| !SeleccionCuenta())
	{
		if (PinDebito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
				APM_ResetComm(); //MFBC/05/03/13
				return -1;
			}
		}
	}
	else
	{
		if (PinCredito())
		{
			if (!getPinBlock(panBCD, TRUE))
			{
				ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
				APM_ResetComm(); //MFBC/05/03/13
				return -1;
			}
		}
	}



	// ULTIMOS 4 DIGITOS
	if ((INPUT.b_tipo_cuenta != 0x10 && INPUT.b_tipo_cuenta != 0x20)
			&& GetUlt4Digitos())
	{
		while (TRUE)
		{
			ret = Ult4Digitos();
			if (ret == -1)
			{
				ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
				APM_ResetComm(); //MFBC/05/03/13
				return -1;
			}
			else if (ret == TRUE)
				break;
		}
	}


	// Cuotas - T Credito
	if ((INPUT.b_tipo_cuenta == 0x30 || INPUT.b_tipo_cuenta == 0x40)
			&& RequiereCuotas()) // Credito		//kt-domingo
	{
		if (!GetCuotas())
		{
			ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
			APM_ResetComm(); //MFBC/05/03/13
			return -1;
		}
	}



	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			//OfflineSent(0);
			IncTraceNo();
			MoveInput2Tx();
			PackProcCode(TX_DATA.b_trans, 0xFF);
			PackHostMsg();
			UpdateHostStatus(REV_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();

				if (RSP_DATA.w_rspcode == '5' * 256 + '5')  //MFBC/10/12/13
				{
					RSP_DATA.b_response = NuevoPIN();
				}

				if (RSP_DATA.b_response == TRANS_REJ)
					UpdateHostStatus(NO_PENDING);

			}
			else
				ErrorTransCajas(CA_COMM);			// **SR** 09/09/13

			//SaveTraceNo();
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				if (RSP_DATA.w_rspcode == '0' * 256 + '0')
					RSP_DATA.w_rspcode = 'A' * 256 + 'P';
				INPUT.b_transAcep = TRUE; //MFBC/25/04/13

				SaveRecord();
				IncRocNo(); // Jorge Numa 05-02-2013
				SendCajas(1);	// **SR** 09/09/13
				PackInputP();

			}
			else if (RSP_DATA.b_response == TRANS_REJ)
			{
				IncRocNo(); // Jorge Numa 04-06-13
				SendCajas(1);	// **SR** 09/09/13
				TransEnd(FALSE); //MFBC/07/05/13
				return -1;
			}
		}
		else
			ErrorTransCajas(CA_COMM);			// **SR** 09/09/13
	}
	else
		ErrorTransCajas(CA_COMM);			// **SR** 09/09/13

	TransEnd((BOOLEAN) (RSP_DATA.b_response == TRANS_ACP));
	return TRUE;


}

//******************************************************************************
//  Function        : transDCC
//  Description     : envia la transaccion DCC
//  Input           : N/A
//  Return          : TRUE/FALSE
//  Note            : N/A
//  Globals Changed : N/A
// By Manuel barbaran
//******************************************************************************
BOOLEAN transDCC (void)
{

	INPUT.b_trans = DCC_TRANS;
	ClearResponse();
	PackCommTest(INPUT.w_host_idx, FALSE);
	APM_PreConnect();
	return TRUE;
}




